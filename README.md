This is aimed at Planning and Management of Queue Items and there allocation into Task to run

Main Work flows.

* Allocation of queue items into tasks
* Checkin of items arriving at the lab into tasks
* Finalization of tasks with plate layout and run file generation


Controllers

* Experiment Creation
* Experiment Checkin
* Experiment Finalize
* Queue Item
* Outline Queue Manager

Key Models

* ResultTask  may sheet for data entry
* TaskBarcodes list of plate/list/order/sample identifier linked to task

Key Jobs

* TaskConsolidation
* TaskCheckIn
* TaskFileGenerator

