#
# Need to be pre loaded so available in test factories
#

puts "Register plugin #{File.dirname(__FILE__)}"

::Plugin.register :biorails_planning do
  #====================================================================================================================
  # Basic plugin details
  #
  name         'Biorails Experiment Planning'
  copyright    'Edge Software consultancy ltd.'
  description  'Coasts Orders ,CDB Method, Sample and Results'
  version  '5.0'
  rpm  'biorails_planner'

  url  'https://github.com/biorails/biorails_planning.git'

 # rpm  'bi_nce'
 #====================================================================================================================
  #
  # menu items for admin,wizards and queries menus
  #
  menu  :admin ,'orders',         :url=>'/orders',        :subject=>:inventory, :action=>:use, :icon=>'biorails-order'
  menu  :admin ,'order_profiles', :url=>'/order_profiles',:subject=>:inventory, :action=>:build, :icon=>'biorails-profile'



  #====================================================================================================================
  # New Models
  #
  # Add external models these may be registered as :=
  #
  # :dictionary as a model element
  # :query  as a template for model queries
  # :pivot  as a template for pivot queries
  # :folder with values for the linked business object (:name,:description)
  #
  #
  # Catalogue
  #
  model  ::OrderProfile, :name => "Order Profiles", :dictionary=>:inventory
  model  ::Order, :name => "Orders", :dictionary=>:inventory

  #
  # Reporting
  #
  #
  #====================================================================================================================
  # Patches to apply to core models
  #
  patch_core ::OutlineQueue,  ::Biorails::OutlineQueuePatch
  patch_core ::ProcessVersion, ::Biorails::ProcessVersionPatch

end
