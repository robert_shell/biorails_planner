require File.dirname(__FILE__)+'/../spec_helper'


describe OrdersController do
  render_views


  before(:each) do
    session[:current_user_id] =User.current.id
    session[:current_element_id] = User.current.project_element_id
    OutlineQueue.first.data_element.update_attribute :content, 'RlSample'
    login_feature_spec

    # my fixtures are lacking a project element, and adding one would
    # risk breaking the core fixtures' folder tree
    OutlineQueue.any_instance.stubs(:project_element => ProjectElement.first)
    OrderItem.any_instance.stubs(:sample_id => 3, :sample => CdbSample.find(3))
  end

  context :index do

    it 'should display a query of orders' do
      get :index
      response.should be_success
      assert_template 'shared/report'
      response.body.should have_css("ul.breadcrumb")
      response.body.should_not have_css("span.translation_missing")
      response.body.should_not have_css("option", :text => 'translation missing')
    end

  end

  context :show do

    it 'should show a solid order' do

      @order = Order.find(9901)
      get :show, :id => @order.id.to_s
      response.should be_success
      assert_template 'show'
      response.body.should_not have_css("span.translation_missing")
      response.body.should have_css("ul.breadcrumb")
    end

    it 'should show a liquid order'

    it 'should show a subways orders with tubes'

  end

  context 'new from task' do

    it "displays order form"

    it "contains queue_items from task"

    it "shows list of order profiles linked to process"

    it "shows reference compounds"

    it "shows previously ordered queue items as deselected"

    it "shows out-of-stock ordered queue items as deselected"

    it "doesn't allow duplicate sample ids in an order"
  end

  context 'new from cart' do

    it 'should not warn of duplicate items' do
      @order_profile = OrderProfile.find(9901)


      outline_queue = @order_profile.outline_queue

      query = QueryEngine.query('queue_items.index', :outline_queue => outline_queue)
      cart = Cart.new(:model_name => 'QueueItem')
      cart.reference = outline_queue
      cart.query = query
      cart.save
      # These items both point to the same RlSample
      cart.set_items([9903,9904])

      get :new,
          :cart_id => cart.id,
          :order => {
            :order_profile_id => @order_profile.id.to_s,
            :outline_queue_id => outline_queue.id.to_s,
            :reference_compound_ids_to_create => {
              @order_profile.reference_compounds[0].id.to_s => 'true'
            }
          }
      response.should be_success
      assert_template :new

      flash[:warning].should be_nil
    end

    it 'should warn of duplicate items for liquid' do
      @order_profile = OrderProfile.find(9902)


      outline_queue = @order_profile.outline_queue

      query = QueryEngine.query('queue_items.index', :outline_queue => outline_queue)
      cart = Cart.new(:model_name => 'QueueItem')
      cart.reference = outline_queue
      cart.query = query
      cart.save
      # These items both point to the same RlSample
      cart.set_items([9903,9904])

      get :new,
          :cart_id => cart.id,
          :order => {
            :order_profile_id => @order_profile.id.to_s,
            :outline_queue_id => outline_queue.id.to_s,
            :reference_compound_ids_to_create => {
              @order_profile.reference_compounds[0].id.to_s => 'true'
            }
          }
      response.should be_success
      assert_template :new

      flash[:warning].should_not be_nil
      flash[:warning].should include('3')
    end

    it 'should get a new solid order form' do
      @order_profile = OrderProfile.find(9901)


      outline_queue = @order_profile.outline_queue

      query = QueryEngine.query('queue_items.index', :outline_queue => outline_queue)
      cart = Cart.new(:model_name => 'QueueItem')
      cart.reference = outline_queue
      cart.query = query
      cart.save
      cart.set_items([outline_queue.items.first.id])
      get :new,
          :cart_id => cart.id,
          :order => {
            :order_profile_id => @order_profile.id.to_s,
            :outline_queue_id => outline_queue.id.to_s,
            :reference_compound_ids_to_create => {
              @order_profile.reference_compounds[0].id.to_s => 'true'
            }
          }
      response.should be_success
      assert_template :new
      assert_template :form_items_dry
    end

    it 'should get a new standard liquid order form' do
      @order_profile = OrderProfile.find(9902)

      outline_queue = @order_profile.outline_queue

      query = QueryEngine.query('queue_items.index', :outline_queue => outline_queue)
      cart = Cart.new(:model_name => 'QueueItem')
      cart.reference = outline_queue
      cart.query = query
      cart.save
      cart.set_items([outline_queue.items.first.id])
      get :new,
          :cart_id => cart.id,
          :order => {
            :order_profile_id => @order_profile.id.to_s,
            :outline_queue_id => outline_queue.id.to_s,
            :reference_compound_ids_to_create => {
              @order_profile.reference_compounds[0].id.to_s => 'true'
            }
          }
      response.should be_success
      assert_template :new
      assert_template :form_items_standard_liquid
    end


    it 'should get a new custom order form' do
      @order_profile = OrderProfile.find(9903)

      outline_queue = @order_profile.outline_queue
      query = QueryEngine.query('queue_items.index', :outline_queue => outline_queue)
      cart = Cart.new(:model_name => 'QueueItem')
      cart.reference = outline_queue
      cart.query = query
      cart.save
      cart.set_items([outline_queue.items.first.id])
      get :new,
          :cart_id => cart.id,
          :order => {
            :order_profile_id => @order_profile.id.to_s,
            :outline_queue_id => outline_queue.id.to_s,
            :reference_compound_ids_to_create => {
              @order_profile.reference_compounds[0].id.to_s => 'true'
            }
          }
      response.should be_success
      assert_template :new
      assert_template :form_items_custom_liquid
    end

    it 'should get a new dissolution order form' do
      @order_profile = OrderProfile.find(9904)

      outline_queue = @order_profile.outline_queue
      outline_queue = @order_profile.outline_queue
      query = QueryEngine.query('queue_items.index', :outline_queue => outline_queue)
      cart = Cart.new(:model_name => 'QueueItem')
      cart.reference = outline_queue
      cart.query = query
      cart.save
      cart.set_items([outline_queue.items.first.id])
      get :new,
          :cart_id => cart.id,
          :order => {
            :order_profile_id => @order_profile.id.to_s,
            :outline_queue_id => outline_queue.id.to_s,
            :reference_compound_ids_to_create => {
              @order_profile.reference_compounds[0].id.to_s => 'true'
            }
          }
      response.should be_success
      assert_template :new
      assert_template :form_items_dissolution
    end

    it 'should get a new order form for subways (with tube selection)' do
      @order_profile = OrderProfile.find(9905)

      outline_queue = @order_profile.outline_queue
      query = QueryEngine.query('queue_items.index', :outline_queue => outline_queue)
      cart = Cart.new(:model_name => 'QueueItem')
      cart.reference = outline_queue
      cart.query = query
      cart.save
      cart.set_items([outline_queue.items.first.id])
      get :new,
          :cart_id => cart.id,
          :order => {
            :order_profile_id => @order_profile.id.to_s,
            :outline_queue_id => outline_queue.id.to_s,
            :reference_compound_ids_to_create => {
              @order_profile.reference_compounds[0].id.to_s => 'true'
            }
          }
      response.should be_success
      assert_template :new
      assert_template :form_items_subways

    end

  end

  context :create_from_cart do


    it 'should successfully create a solid order' do
      @order_profile = OrderProfile.find(9901)

      outline_queue = @order_profile.outline_queue

      query = QueryEngine.query('queue_items.index', :outline_queue => outline_queue)
      cart = Cart.new(:model_name => 'QueueItem')
      cart.reference = outline_queue
      cart.query = query
      cart.save
      cart.set_items([outline_queue.items.first.id])

      post :create,
           :cart_id => cart.id,
           :order => {
             :order_profile_id => 9901,
             :outline_queue_id => outline_queue.id,
             :use_internal_recipient => 'true',
             :recipient_user_id => '2',
             :order_items_attributes => {
               '0' => {
                 :is_reference_compound => 'true',
                 :rl_sample_id => '1',
                 :in_order => '1',
                 :queue_item_id => ''
               },
               '1' => {
                 :is_reference_compound => 'false',
                 :rl_sample_id => '',
                 :in_order => '1',
                 :queue_item_id => '9901'
               },
               '3' => {
                 :is_reference_compound => 'false',
                 :rl_sample_id => '',
                 :in_order => '0',
                 :queue_item_id => '9902'
               }
             },
           }
      assert_response :redirect
      assert_redirected_to order_path(assigns[:order])
      flash[:success].should_not be_blank

      assert_equal 2, assigns[:order].order_items.count
    end

    it 'should successfully create a standard liquid order' do
      @order_profile = OrderProfile.find(9902)

      outline_queue = @order_profile.outline_queue

      query = QueryEngine.query('queue_items.index', :outline_queue => outline_queue)
      cart = Cart.new(:model_name => 'QueueItem')
      cart.reference = outline_queue
      cart.query = query
      cart.save
      cart.set_items([outline_queue.items.first.id])

      post :create,
           :cart_id => cart.id,
           :order => {
             :order_profile_id => 9902,
             :outline_queue_id => outline_queue.id,
             :use_internal_recipient => 'true',
             :recipient_user_id => '2',
             :order_items_attributes => {
               '0' => {
                 :is_reference_compound => 'true',
                 :rl_sample_id => '1',
                 :in_order => '1',
                 :queue_item_id => ''
               },
               '1' => {
                 :is_reference_compound => 'false',
                 :rl_sample_id => '',
                 :in_order => '1',
                 :queue_item_id => '9901'
               },
               '3' => {
                 :is_reference_compound => 'false',
                 :rl_sample_id => '',
                 :in_order => '0',
                 :queue_item_id => '9902'
               }
             },
           }
      assert_response :redirect
      assert_redirected_to order_path(assigns[:order])
      flash[:success].should_not be_blank

      assert_equal 2, assigns[:order].order_items.count
    end

    it 'should successfully create a custom liquid order' do
      @order_profile = OrderProfile.find(9903)

      outline_queue = @order_profile.outline_queue

      query = QueryEngine.query('queue_items.index', :outline_queue => outline_queue)
      cart = Cart.new(:model_name => 'QueueItem')
      cart.reference = outline_queue
      cart.query = query
      cart.save
      cart.set_items([outline_queue.items.first.id])

      post :create,
           :cart_id => cart.id,
           :order => {
             :order_profile_id => 9903,
             :outline_queue_id => outline_queue.id,
             :use_internal_recipient => 'true',
             :recipient_user_id => '2',
             :order_items_attributes => {
               '0' => {
                 :is_reference_compound => 'true',
                 :rl_sample_id => '1',
                 :in_order => '1',
                 :queue_item_id => ''
               },
               '1' => {
                 :is_reference_compound => 'false',
                 :rl_sample_id => '',
                 :in_order => '1',
                 :queue_item_id => '9901'
               },
               '3' => {
                 :is_reference_compound => 'false',
                 :rl_sample_id => '',
                 :in_order => '0',
                 :queue_item_id => '9902'
               }
             },
           }
      assert_response :redirect
      assert_redirected_to order_path(assigns[:order])
      flash[:success].should_not be_blank

      assert_equal 2, assigns[:order].order_items.count
    end

    it 'should successfully create a dissolution order' do
      @order_profile = OrderProfile.find(9904)

      outline_queue = @order_profile.outline_queue

      query = QueryEngine.query('queue_items.index', :outline_queue => outline_queue)
      cart = Cart.new(:model_name => 'QueueItem')
      cart.reference = outline_queue
      cart.query = query
      cart.save
      cart.set_items([outline_queue.items.first.id])

      post :create,
           :cart_id => cart.id,
           :order => {
             :order_profile_id => 9904,
             :outline_queue_id => outline_queue.id,
             :use_internal_recipient => 'true',
             :recipient_user_id => '2',
             :order_items_attributes => {
               '0' => {
                 :is_reference_compound => 'true',
                 :rl_sample_id => '1',
                 :in_order => '1',
                 :queue_item_id => ''
               },
               '1' => {
                 :is_reference_compound => 'false',
                 :rl_sample_id => '',
                 :in_order => '1',
                 :queue_item_id => '9901'
               },
               '3' => {
                 :is_reference_compound => 'false',
                 :rl_sample_id => '',
                 :in_order => '0',
                 :queue_item_id => '9902'
               }
             },
           }
      assert_response :redirect
      assert_redirected_to order_path(assigns[:order])
      flash[:success].should_not be_blank

      assert_equal 2, assigns[:order].order_items.count
    end


    it 'should successfully create a subways order' do
      @order_profile = OrderProfile.find(9905)

      outline_queue = @order_profile.outline_queue

      query = QueryEngine.query('queue_items.index', :outline_queue => outline_queue)
      cart = Cart.new(:model_name => 'QueueItem')
      cart.reference = outline_queue
      cart.query = query
      cart.save
      cart.set_items([outline_queue.items.first.id])

      post :create,
           :cart_id => cart.id,
           :order => {
             :order_profile_id => 9905,
             :outline_queue_id => outline_queue.id,
             :use_internal_recipient => 'true',
             :recipient_user_id => '2',
             :order_items_attributes => {
               '0' => {
                 :is_reference_compound => 'true',
                 :rl_sample_id => '1',
                 :in_order => '1',
                 :queue_item_id => '',
                 :tube_id => '101'
               },
               '1' => {
                 :is_reference_compound => 'false',
                 :rl_sample_id => '',
                 :in_order => '1',
                 :queue_item_id => '9901',
                 :tube_id => '201'
               },
               '3' => {
                 :is_reference_compound => 'false',
                 :rl_sample_id => '',
                 :in_order => '0',
                 :queue_item_id => '9903',
                 :tube_id => '301'
               }
             },
           }
      assert_response :redirect
      assert_redirected_to order_path(assigns[:order])
      flash[:success].should_not be_blank

      assert_equal 2, assigns[:order].order_items.count
    end


  end

  context "edit failed order" do

    it "displays order form"

    it "contains queue_items from task"

    it "shows list of order profiles linked to process"

    it "shows reference compounds"

    it "shows previously ordered queue items as deselected"

    it "shows out-of-stock ordered queue items as deselected"

    it "doesn't allow duplicate sample ids in an order"

  end

  context "update" do
    it "saves a valid order"

    it "attempts to send to COASTS"

    it "returns to edit when COASTS submission fails"

    it "exits if order succeeds"
  end

end