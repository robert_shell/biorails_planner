require File.dirname(__FILE__)+'/../spec_helper'

describe ExperimentConsolidationWizardsController do

  render_views

  let(:task) { Task.find(9901) }
  let(:outline_queue) { OutlineQueue.find(9901) }

  before(:each) do
    session[:current_user_id] = User.find_by_login('admin')
    User.current = User.find_by_login(:admin)
  end

  context 'step1' do

    # TODO Someone who knows what step 1 is doing, test it
    # TODO should be the same as the experiment order wizards controller

  end

  context 'step3' do

    context 'step3_create' do
      it 'should run a background job if the task has a dilution map' do
        assert task.dilution_map
        post :step2_create, :id => task.id
        job = Job.find_by_queue_name(assigns[:job_id])
        job.data[:success_url].should eq(step3_edit_experiment_consolidation_wizard_path(task))
        job.data[:fail_url].should eq(step2_experiment_consolidation_wizard_path(task))
        assert_redirected_to "/jobs/waiting/#{job.queue_name}?redirect=true"
      end


      it 'should skip to step 4 if it has no dilution map but has output file templates' do
        ResultTask.any_instance.stubs(:dilution_map => nil)

        post :step2_create, :id => task.id

        assert_redirected_to step4_new_experiment_consolidation_wizard_path(@task)
      end

      it 'should skip to step 5 if it has no diution map or output file templates' do
        ResultTask.any_instance.stubs(:dilution_map => nil)
        ProcessVersion.any_instance.stubs(:process_version_file_templates => [])
        post :step2_create, :id => task.id

        assert_redirected_to step5_experiment_consolidation_wizard_path(@task)
      end

    end
    context 'step3_edit' do

      it 'should show the edit form' do
        get :step3_edit, :id => task.id

        assigns[:plate_data_store].should be_a(Bi::PlateDataStore)
        puts assigns[:dilution_slots].class
        assigns[:dilution_slots].should be_a(ActiveRecord::Relation)
        assigns[:dilution_slots][0].should be_a(DilutionSlot)

        response.should be_success
        response.should render_template :step2_edit
        response.should render_template(:partial => '_shared_buttons')
        response.should render_template(:partial => '_wizard_menu')
        response.should render_template(:partial => 'shared/_messages')
        response.should render_template(:partial => '_init_redips_drag')
        response.body.should_not have_css("span.translation_missing")
      end

    end


    context 'step3_reset' do
      it 'should render the reset partial' do
        get :step3_reset, :id => task.id, :format => :js

        assigns[:plate_data_store].should be_a(Bi::PlateDataStore)

        response.should be_success
        response.should render_template :step2_reset
        response.should render_template(:partial => '_consolidation_result_table')
        response.should render_template(:partial => 'shared/_messages')
        response.body.should_not have_css("span.translation_missing")
      end
    end

    context 'step3_update' do
      it 'should run a background job and go to step three if there are output file templates' do
        JSON.stubs(:parse => {})
        post :step3_update, :id => task.id, :dilution_slot_positions => ''
        job = Job.find_by_queue_name(assigns[:job_id])
        job.data[:success_url].should eq(step4_new_experiment_consolidation_wizard_path(task))
        job.data[:fail_url].should eq(step3_edit_experiment_consolidation_wizard_path(task))
        assert_redirected_to "/jobs/waiting/#{job.queue_name}?redirect=true"
      end

      it 'should run a background job and go to step four if there are output file templates' do
        JSON.stubs(:parse => {})
        ProcessVersion.any_instance.stubs(:process_version_file_templates => [])
        post :step2_update, :id => task.id
        job = Job.find_by_queue_name(assigns[:job_id])
        job.data[:success_url].should eq(step5_experiment_consolidation_wizard_path(task))
        job.data[:fail_url].should eq(step3_edit_experiment_consolidation_wizard_path(task))
        assert_redirected_to "/jobs/waiting/#{job.queue_name}?redirect=true"
      end
    end

  end


  context 'step4' do

    context 'step4_new' do
      it 'should render the output files form' do
        get :step4_new, :id => task.id
        response.should be_success
        response.should render_template :step4_new
        response.should render_template(:partial => '_shared_buttons')
        response.should render_template(:partial => '_wizard_menu')
        response.body.should_not have_css("span.translation_missing")
      end
    end

    context 'step4_preview' do
      it 'should render a preview on request' do
        get :step4_preview, :id => task.id,
            :process_version_file_template_id => task.process.process_version_file_templates[0].id,
            :format => 'js'
        response.should be_success
        response.should render_template :step4_preview
        response.body.should_not have_css("span.translation_missing")
        assigns[:preview_text].should_not be_blank
      end
    end

    context 'step4_create' do
      it 'should run a background job then go to step 4' do
        post :step3_create, :id => task.id,
             :task => {
               :process_version_file_template_ids => {
                 '9901' => '1'
               }
             }
        job = Job.find_by_queue_name(assigns[:job_id])
        job.data[:success_url].should eq(step5_experiment_consolidation_wizard_path(task))
        job.data[:fail_url].should eq(step4_new_experiment_consolidation_wizard_path(task))
        assert_redirected_to "/jobs/waiting/#{job.queue_name}?redirect=true"
      end
    end

  end

  context 'step5' do

    it 'should show a summary' do
      ResultTask.any_instance.stubs(:project_element => ProjectElement.first)
      get :step5, :id => task.id
      response.should be_success
      response.should render_template :step4
      response.should render_template(:partial => '_shared_buttons')
      response.should render_template(:partial => '_wizard_menu')
      response.should render_template(:partial => '_task_summary')
      response.should render_template(:partial => '_consolidation_results')
      response.should render_template(:partial => '_contents_table')

      response.body.should_not have_css("span.translation_missing")
    end

  end

  context 'finalize' do

    it 'should redirect to the task and show a flash' do
      ResultTask.any_instance.stubs(:project_element => ProjectElement.first)
      get :finalize, :id => task.id

      response.should be_redirect
      response.should redirect_to(task_path(task))
      flash[:success].should_not be_blank
      flash[:error].should be_blank
    end

    it 'should redirect back to step 5 if there is an error' do
      get :finalize, :id => task.id

      response.should be_redirect
      flash[:success].should be_blank
      flash[:error].should_not be_blank
      response.should redirect_to(step5_experiment_consolidation_wizard_path(task))
    end

  end


end