require File.dirname(__FILE__)+'/../spec_helper'

describe TaskBarcodesController do

  render_views

  context 'index' do

    before(:each) do
      User.background_session
      session[:current_user_id] =User.current.id
      session[:current_element_id] = User.current.project_element_id
    end

    it 'should display a query of queries when no task is passed in' do
      # /task_barcodes
      get :index
      response.should be_success
      assert_template 'shared/report'
      response.body.should_not have_css("span.translation_missing")
      response.body.should_not have_css("option", :text => 'translation missing')
    end

    it 'should show barcodes for a task' do
      ResultTask.any_instance.stubs(:project_element => ProjectElement.first)
      ResultTask.any_instance.stubs(:project_element_id => ProjectElement.first.id)

      TaskBarcode.create!(
        :reference_type => 'Order',
        :reference_code => '9902',
        :task_id => 9901,
        :position => 0
      )

      TaskBarcode.create!(
        :reference_type => 'CoastsPlate',
        :reference_code => 'Ashape',
        :task_id => 9901,
        :position => 1
      )

      TaskBarcode.create!(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'B0001',
        :task_id => 9901,
        :position => 2
      )

      @task = Task.find(9901)
      @task.task_barcodes.each { |x| x.finalise }

      # /task/9901/task_barcodes
      get :index, :task_id => 9901

      response.body.should have_content @task.element_type_name
      response.body.should have_content @task.name
      response.body.should have_content TaskBarcode.model_name.human(:count => 2)

      # There will be at least one plate diagram (in  this case, there will be 3)
      response.body.should have_css('div.plate_thumbnail_holder')
      response.body.should have_css('table.plate_thumb_table')
      response.body.should have_css('tr.plate_thumb_row')
      response.body.should have_css('td.plate_thumb_well')
      response.body.should have_css('td.plate_thumb_well_empty')
    end

  end

  context 'new' do
    before(:each) do
      User.background_session
      session[:current_user_id] =User.current.id
      session[:current_element_id] = User.current.project_element_id
      Task.stubs(:active_inventory_result => Task.where(:id => 9901))
    end

    it 'should show the form for checking in barcodes (no task)' do
      get :new
      response.should be_success
      assert_template :new

      response.body.should have_css('select#reference_type')
      response.body.should have_css('input#default_task_id_display')
      response.body.should have_css('textarea#reference_codes')
    end

    it 'should show the form for checking  in barcodes for a task' do
      ResultTask.any_instance.stubs(:project_element => ProjectElement.first)
      ResultTask.any_instance.stubs(:project_element_id => ProjectElement.first.id)

      @task = Task.find(9901)
      @task.task_barcodes.each { |x| x.finalise }

      get :new, :default_task_id => 9901


      response.should be_success
      assert_template :new
      response.body.should have_css('ul.breadcrumb')
      response.body.should have_css('select#reference_type')
      response.body.should have_css('input#default_task_id_display', :visible => false)
      response.body.should have_css('textarea#reference_codes')
    end

  end

  context 'create' do

    before(:each) do
      Task.stubs(:active_inventory_result => Task.where(:id => 9901))
      ResultTask.any_instance.stubs(:project_element => ProjectElement.first)
      ResultTask.any_instance.stubs(:project_element_id => ProjectElement.first.id)
      User.background_session
      session[:current_user_id] =User.current.id
      session[:current_element_id] = User.current.project_element_id
    end

    context 'add_barcodes' do

      it 'should reject unfound codes' do
        post :create, :commit => ::I18n.t('add'),
             :reference_type => 'CoastsPlate',
             :reference_codes => 'NOPE,NoWay',
             :default_task_id => 9901
        response.should be_success
        assert_template :new
        assert_not_nil flash[:warning]
        assert_includes flash[:warning], 'NOPE'
        assert_includes flash[:warning], 'NoWay'
      end

      it 'should warn of duplicate codes' do
        post :create, :commit => ::I18n.t('add'),
             :reference_type => 'CoastsPlate',
             :reference_codes => 'ABC123,ABC123',
             :default_task_id => 9901
        response.should be_success
        assert_template :new
        assert_not_nil flash[:error]

        assert_includes(
          flash[:error],
          'You cannot scan CoastsPlate:ABC123 for exp test because it has already been checked in for this task'
        )
        response.body.should have_css('div#barcode_table')
        # it's got at least one row
        response.body.should have_css('tr')
        response.body.should have_css('tr.error')
      end

      it 'should check in a new barcode' do
        post :create, :commit => ::I18n.t('add'),
             :reference_type => 'CoastsPlate',
             :reference_codes => 'ABC123',
             :default_task_id => 9901
        response.should be_success
        assert_template :new

        response.body.should have_css('div#barcode_table')
        # it's got at least one row
        response.body.should have_css('table')
        response.body.should have_css('tr')
      end

    end

    context 'finalise_barcodes' do

      before(:each) do
        Task.stubs(:active_inventory_result => Task.where(:id => 9901))
        ResultTask.any_instance.stubs(:project_element => ProjectElement.first)
        ResultTask.any_instance.stubs(:project_element_id => ProjectElement.first.id)
        User.background_session
        session[:current_user_id] =User.current.id
        session[:current_element_id] = User.current.project_element_id
      end

      it 'should not allow duplicate barcodes' do
        ResultTask.any_instance.stubs(:parent_folder => ProjectElement.first)
        task_barcode = TaskBarcode.create!(
          :reference_type => 'CoastsPlate',
          :reference_code => 'ABC123',
          :task_id => 9901,
          :position => 0
        )
        task_barcode.finalise

        task_barcode = TaskBarcode.create!(
          :reference_type => 'CoastsPlate',
          :reference_code => 'ABC123',
          :task_id => 9901,
          :position => 0
        )
        post :create,
             :task_barcodes => {'0' => task_barcode.attributes}
        response.should be_success
        assert_template :new

        assert_not_nil flash[:error]
      end

      it 'should show collection of created barcodes' do
        ResultTask.any_instance.stubs(:parent_folder => ProjectElement.first)
        TaskBarcode.destroy_all
        task_barcode = TaskBarcode.create!(
          :reference_type => 'CoastsPlate',
          :reference_code => 'ABC123',
          :task_id => 9901,
          :position => 0
        )

        post :create,
             :task_id => 9901,
             # barcodes hash appears when 'add' has been used more than once
             :task_barcodes => {'0' => task_barcode.attributes}
        response.should be_redirect

        assert_redirected_to "/jobs/waiting/#{assigns[:job_id]}?redirect=true"
      end

    end

  end

  context 'show_collection' do

    before(:each) do
      Task.stubs(:active_inventory_result => Task.where(:id => 9901))
      ResultTask.any_instance.stubs(:project_element => ProjectElement.first)
      ResultTask.any_instance.stubs(:project_element_id => ProjectElement.first.id)
      User.background_session
      session[:current_user_id] =User.current.id
      session[:current_element_id] = User.current.project_element_id

      @ids = []
      @ids << TaskBarcode.create(
        :reference_type => 'CoastsPlate',
        :reference_code => 'ABC123',
        :task_id => 9901,
        :position => 0
      ).id
      @ids << TaskBarcode.create(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'A0001',
        :task_id => 9901,
        :position => 1
      ).id
      @ids << TaskBarcode.create(
        :reference_type => 'Order',
        :reference_code => '9909',
        :task_id => 9901,
        :position => 2
      ).id

    end

    it 'should show an arbitrary collection of barcodes' do

      get :show_collection, :ids => @ids

      response.should be_success
      assert_template :show_collection

      response.body.should have_content('ABC123')
      response.body.should have_content('A0001')
      response.body.should have_content('9909')

      response.body.should_not have_css('ul.breadcrumb li')

      response.body.should have_css('table.table')
      # at least one plate diagram
      response.body.should have_css('div.plate_thumbnail_holder')
      response.body.should have_css('table.plate_thumb_table')
      response.body.should have_css('tr.plate_thumb_row')
      response.body.should have_css('td.plate_thumb_well')
      response.body.should have_css('td.plate_thumb_well_empty')
    end


  end

  context 'show' do

    before(:each) do

      ResultTask.any_instance.stubs(:project_element => ProjectElement.first)
      ResultTask.any_instance.stubs(:project_element_id => ProjectElement.first.id)
      User.background_session
      session[:current_user_id] =User.current.id
      session[:current_element_id] = User.current.project_element_id
    end

    it 'should show a summary of a barcode' do
      task_barcode = TaskBarcode.create(
        :reference_type => 'Order',
        :reference_code => '9909',
        :task_id => 9901,
        :position => 2
      )
      get :show, :id => task_barcode.id

      response.should be_success
      assert_template :show

      response.body.should have_text('Order')
      response.body.should have_text('9909')
    end
  end

end
