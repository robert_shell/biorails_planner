require File.dirname(__FILE__)+'/../spec_helper'

describe OrderProfilesController do
  render_views


  before(:each) do
    session[:current_user_id] =User.current.id
    session[:current_element_id] = User.current.project_element_id
    login_feature_spec
  end

  context :index do
    it 'should display a query of order profiles' do
      get :index
      response.should be_success
      assert_template 'shared/report'
      response.body.should have_css("ul.breadcrumb")
      response.body.should_not have_css("span.translation_missing")
      response.body.should_not have_css("option", :text => 'translation missing')
    end

    it "should return error message and home page if trying to edit, update or destroy an order profile that does not exist" do
      find_nonexistent
    end
  end

  context :show do
    it 'should show a solid order profile' do
      @order = Order.find(9901)
      get :show, :id => @order.id.to_s
      response.should be_success
      assert_template 'show'
      response.body.should_not have_css("span.translation_missing")
      response.body.should have_css("ul.breadcrumb")
    end
  end

  context :new do
    it 'should show a new order profile form' do
      get :new
      response.should be_success
      assert_template 'new'
      response.body.should_not have_css("span.translation_missing")
      response.body.should have_css("ul.breadcrumb")
    end

    it 'should show a new order profile form with an outline queue id' do
      @outline_queue = OutlineQueue.find(9901)
      get :new, :outline_queue_id => @outline_queue.id.to_s
      assigns['order_profile'].outline_queue_id.should be(9901)
      response.should be_success
      assert_template 'new'
      response.body.should_not have_css("span.translation_missing")
      response.body.should have_css("ul.breadcrumb")
    end

    context :renew_new_form do

      it "should render the new form with renew_form" do

        get :renew_form,
            :order_profile => {
              :name => 'asdddd',
              :coasts_order_type_id => 1,
              :outline_queue_id => 1,
              :coasts_vial_type_id => 1,
              :orderer_user_id => 1,
              :orderer_coasts_opu_id => 3407,
              :use_internal_recipient => true,
              :recipient_user_id => 1,
              :purpose_id => 1
            }
        response.should be_success
        assert_template :form
      end

      it "should renew form for a solid order type" do
        get :renew_form,
            :order_profile => {
              :name => 'asdddd',
              :coasts_order_type_id => 1
            }
        response.should be_success
        assert_template :form
        response.body.should_not have_css("span.translation_missing")
      end

      it "should renew form for a standard liquid order type" do
        get :renew_form,
            :order_profile => {
              :name => 'asdddd',
              :coasts_order_type_id => 2
            }
        response.should be_success
        assert_template :form
        response.body.should_not have_css("span.translation_missing")
      end

      it "should renew form for a custom liquid order type" do
        get :renew_form,
            :order_profile => {
              :name => 'asdddd',
              :coasts_order_type_id => 3
            }
        response.should be_success
        assert_template :form
        response.body.should_not have_css("span.translation_missing")
      end

      it "should renew form for a dissolution order type" do
        get :renew_form,
            :order_profile => {
              :name => 'asdddd',
              :coasts_order_type_id => 4
            }
        response.should be_success
        assert_template :form
        response.body.should_not have_css("span.translation_missing")
      end

      it "should renew form for a subways order type" do
        get :renew_form,
            :order_profile => {
              :name => 'asdddd',
              :coasts_order_type_id => 5
            }
        response.should be_success
        assert_template :form
        response.body.should_not have_css("span.translation_missing")
      end

      it 'should be possible to add and remove reference compounds' do
        get :renew_form,
            :order_profile => {
              :name => 'asdddd',
              :coasts_order_type_id => 1,
              :outline_queue_id => 1,
              :coasts_vial_type_id => 1,
              :orderer_user_id => 1,
              :orderer_coasts_opu_id => 3407,
              :use_internal_recipient => true,
              :recipient_user_id => 1,
              :purpose_id => 1,
              :sample_id_to_add => 1
            },
            :add_compound => 'true'
        assert_equal 1, assigns[:order_profile].reference_compounds.length
        order_profile_attributes = assigns[:order_profile].attributes
        order_profile_attributes.merge!(
          {:reference_compounds_attributes => assigns[:order_profile].reference_compounds.collect { |x| x.attributes }}
        )
        get :renew_form,
            :order_profile => order_profile_attributes,
            :remove_compound_id => 1
        assert_equal 0, assigns[:order_profile].reference_compounds.length
      end

      it 'should be possible to add order profile weights' do
        get :renew_form,
            :order_profile => {
              :name => 'asdddd',
              :coasts_order_type_id => 1,
              :outline_queue_id => 1,
              :coasts_vial_type_id => 1,
              :orderer_user_id => 1,
              :order_profile_weights_attributes => {
                '0' => {:weight => 1, :no_of_vials => 2}
              },
              :orderer_coasts_opu_id => 3407,
              :use_internal_recipient => true,
              :recipient_user_id => 1,
              :purpose_id => 1
            },
            :new_weight_row => 'true'
        assert_equal 2, assigns[:order_profile].order_profile_weights.length
      end

      it 'should be possible to remove order profile weights' do
        get :renew_form,
            :order_profile => {
              :name => 'asdddd',
              :coasts_order_type_id => 1,
              :outline_queue_id => 1,
              :coasts_vial_type_id => 1,
              :orderer_user_id => 1,
              :order_profile_weights_attributes => {
                '0' => {:weight => 1, :no_of_vials => 2},
                '1' => {:weight => 2, :no_of_vials => 3}
              },
              :orderer_coasts_opu_id => 3407,
              :use_internal_recipient => true,
              :recipient_user_id => 1,
              :purpose_id => 1
            },
            :remove_weight_row => 1
        assert_equal 1, assigns[:order_profile].order_profile_weights.length
      end

    end

  end

  context :create do
    it "should redirect to show after creating an order profile" do
      post :create,
           :order_profile => {
             :name => 'rspec test solid profile',
             :coasts_order_type_id => 1,
             :outline_queue_id => 1,
             :coasts_vial_type_id => 1,
             :orderer_user_id => 1,
             :orderer_coasts_opu_id => 3407,
             :use_internal_recipient => 'true',
             :recipient_user_id => 1,
             :purpose_id => 1,
             :dispensary_opu_id => 3607,
             :order_profile_weights_attributes => {
               '0' => {:weight => 1, :no_of_vials => 2},
               '1' => {:weight => 3, :no_of_vials => 1},
             }
           }
      assert_redirected_to order_profile_path(assigns[:order_profile])
      flash[:success].should_not be_blank
      flash[:error].should be_blank
    end

    it "should redirect to new and show errors after failing to create an order profile" do
      post :create,
           :order_profile => {
             :name => '',
             :coasts_order_type_id => 1,
             :order_profile_weights_attributes => {
               '0' => {:weight => 1, :no_of_vials => 2},
               '1' => {:weight => 3, :no_of_vials => 1},
             },
             :outline_queue_id => 1,
             :coasts_vial_type_id => 1,
             :orderer_user_id => 1,
             :orderer_coasts_opu_id => 3407,
             :use_internal_recipient => 'true',
             :recipient_user_id => 1,
             :purpose_id => 1
           }
      assert_response :success
      assert_template :new
      response.body.should have_content('Error: Failed to create Order Profile.')
      response.body.should have_selector('.help-inline')
      flash[:success].should be_blank
    end
  end

  context :edit do
    it 'should show an edit order profile form' do
      @order_profile= OrderProfile.find(9901)
      get :edit, :id => @order_profile.id.to_s
      response.should be_success
      assert_template :edit
      response.body.should_not have_css("span.translation_missing")
      response.body.should have_css("ul.breadcrumb")
    end

    it 'should redirect from edit when the id is wrong' do
      get :edit, :id => 999999999999999
      response.should be_redirect
      assert_redirected_to :home
      response.body.should_not have_css("span.translation_missing")
    end

    it "should render the edit form with renew_form (called by ajax)" do
      @order_profile= OrderProfile.find(9901)

      get :renew_form,
          :id => @order_profile.id.to_s,
          :order_profile => {
            :name => 'asdddd',
            :coasts_order_type_id => 1,
            :order_profile_weights_attributes => {
              '0' => {:weight => 1, :no_of_vials => 2},
              '1' => {:weight => 3, :no_of_vials => 1},
            },
            :outline_queue_id => 9901,
            :coasts_vial_type_id => 1,
            :orderer_user_id => 1,
            :orderer_coasts_opu_id => 3407,
            :use_internal_recipient => true,
            :recipient_user_id => 1,
            :purpose_id => 1
          }
      response.should be_success
      assert_template :form
    end
  end

  context :update do
    it "should successfully update an order profile" do
      @order_profile= OrderProfile.find(9901)

      post :update,
           :id => @order_profile.id.to_s,
           :order_profile => {
             :name => 'rspec test solid profile',
             :coasts_order_type_id => 1,
             :order_profile_weights_attributes => {
               '0' => {:weight => 1, :no_of_vials => 2},
               '1' => {:weight => 3, :no_of_vials => 1},
             },
             :outline_queue_id => 1,
             :coasts_vial_type_id => 1,
             :orderer_user_id => 1,
             :orderer_coasts_opu_id => 3407,
             :use_internal_recipient => 'true',
             :recipient_user_id => 1,
             :purpose_id => 1,
             :dispensary_opu_id => 3607
           }
      assert_redirected_to order_profile_path(assigns[:order_profile])
      flash[:success].should_not be_blank
      flash[:error].should be_blank
    end

    it "should redirect to edit and show errors on failing to update an order profile" do
      @order_profile= OrderProfile.find(9901)

      post :update,
           :id => @order_profile.id.to_s,
           :order_profile => {
             :name => '',
             :coasts_order_type_id => 1,
             :order_profile_weights_attributes => {
               '0' => {:weight => 1, :no_of_vials => 2},
               '1' => {:weight => 3, :no_of_vials => 1},
             },
             :outline_queue_id => 9901,
             :coasts_vial_type_id => 1,
             :orderer_user_id => 1,
             :orderer_coasts_opu_id => 3407,
             :use_internal_recipient => 'true',
             :recipient_user_id => 1,
             :purpose_id => 1
           }
      response.body.should have_content('Error: Failed to update Order Profile.')
      response.body.should have_selector('.help-inline')
      assert_response :success
      assert_template :edit
      flash[:success].should be_blank
    end
  end

  context :destroy do
    it "should delete an order profile" do
      @order_profile= OrderProfile.find(9901)
      outline_queue = @order_profile.outline_queue

      post :destroy,
           :id => @order_profile.id.to_s

      response.should be_redirect
      assert_redirected_to outline_queue_path(outline_queue)
      flash[:success].should_not be_blank
      flash[:error].should be_blank
    end
  end


end