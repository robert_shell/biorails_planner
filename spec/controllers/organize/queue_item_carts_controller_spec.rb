require File.dirname(__FILE__) + '/../../spec_helper'

describe Organize::QueueItemCartsController do

  let(:items) { {1 => Outline.first.id, 2 => Outline.last.id} }
  let(:outline_queue) { outline_queues(:service_a)}
  let(:cart) { build_cart }
  let(:request) { FactoryGirl.build :request }
  let(:result_task) { tasks(:task_full) }
  let(:list) { List.find_by_name('list') }
  let(:task) { FactoryGirl.build :result_task }

  def build_cart
    query = QueryModel.create_or_update!(:id => 353535,
                                         :name => 'test333',
                                         :description => 'test',
                                         :model => QueueItem)
    query.save

    query.default_columns
    cart = Cart.new(:model_name => 'QueueItem')
    cart.query= query
    pending_state = State.pending.first
    QueueItem.update_all("state_id= #{pending_state.id} ",
                         ["outline_queue_id=? and #{QueueItem::STATE_CASCADE_WHERE_FILTER}",
                          outline_queue.id, pending_state.id, [0, 1, 2]])
    cart.reference = outline_queue
    cart.save!
    cart
  end

  before(:each) do
    session[:current_element_id] = Team.first.project_element_id
    session[:current_user_id] = User.find_by_login(:admin)
    User.background_session
  end

  it "should create with query and items" do
    put :create,
        :query_id => queries(:outline_index).id,
        :outline_queue_id => outline_queue.id,
        :items => items
    response.should be_redirect
    assert_ok assigns(:cart)
  end

  it "should show cart" do
    get :show, :id => cart.id, :outline_queue_id => cart.reference.id
    response.should be_success
    response.should render_template 'show'
  end

  it "should view query for existing cart" do
    get :index, :id => cart.id, :outline_queue_id => cart.reference.id
    response.should be_success
    assert assigns(:query)
    assert assigns(:selected_list)
  end

  it "should update an existing cart" do
    post :update, :id => cart.id,
         :cart => {:state_id => 2},
         :outline_queue_id => cart.reference.id,
         :submit_update => I18n.t('update')
    response.should be_redirect
  end

  it "should destroy an existing cart" do
    delete :destroy, :id => cart.id
    response.should be_redirect
  end

  it "should show select rows dialog" do
    get :select_rows, :format => 'js'
    response.should render_template 'select_rows'
  end


end
