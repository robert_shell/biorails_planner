require File.dirname(__FILE__)+'/../../spec_helper'

describe Organize::QueueItemsController do

  User.background_session

  render_views

  let(:queue_item) { QueueItem.first }
  # let(:queue) { queue_item.queue }
  let(:queue) { FactoryGirl.create :outline_queue }


  before(:each) do
    @request.session[:current_element_id] = Team.first.project_element_id
    @request.session[:current_user_id] = User.current_id #users(:robert_shell)
  end

  it "should get index" do
    get :index
    response.should be_redirect
  end

  it "should get cart list with outline queue id" do
    get :index, :outline_queue_id => queue.id
    assert_response :success
    assert_template 'organize/queue_items/cart_list'
  end

  it "should get show" do
    get :show, :id => queue_item.id
    assert_response :success
    assert_template 'show'
    assert_not_nil assigns(:queue_item)
  end

  it "should show_invalid_id" do
    get :show, :id => 436534463
    assert_response :redirect
  end


  it "should destroy" do
    get :destroy, :id => queue_item.id
    assert_response :redirect
  end

end
