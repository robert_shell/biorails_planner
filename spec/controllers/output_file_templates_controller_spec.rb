require File.dirname(__FILE__)+'/../spec_helper'


describe OutputFileTemplatesController do

  before :each do
    session[:current_user_id] =User.current.id
    session[:current_element_id] = User.current.project_element_id
    User.background_session
  end

  context :index do

    it 'should render a query' do
      get :index
      assert_template 'shared/report'
      response.body.should_not have_css("span.translation_missing")
    end

  end

  context :show do

    before(:each) do
      get :show, :id => 1
      response.should be_success
    end

    it 'should have find an output file template' do
      assert assigns[:output_file_template]
      assert_equal 1, assigns[:output_file_template].id
    end

    it 'should render the show page' do
      assert_template 'output_file_templates/show'
      response.body.should_not have_css("span.translation_missing")
    end

  end

  context :new do

    before(:each) do
      get :new
    end

    it 'should get a new template' do
      assert assigns[:output_file_template]
      assert_nil assigns[:output_file_template].id
    end

    it 'should render the new page' do
      assert_template 'output_file_templates/new'
      response.body.should_not have_css("span.translation_missing")
    end

  end

  context :create do

    it 'should get a template' do
      OutputFileTemplate.any_instance.stubs(:save => true, :id => 1)
      post :create, :output_file_template => {}
      assert assigns[:output_file_template]
      assert_equal 1, assigns[:output_file_template].id
    end

    it 'should render the show page if save succeeds' do
      OutputFileTemplate.any_instance.stubs(:save => true, :id => 1)
      post :create, :output_file_template => {}
      response.should be_redirect
      assert_not_nil flash[:success]
      assert_nil flash[:error]
      assert_redirected_to output_file_template_path(1)
      response.body.should_not have_css("span.translation_missing")
    end

    it 'should render new form if save fails' do
      OutputFileTemplate.any_instance.stubs(:save => false, :id => 1)
      post :create, :output_file_template => {}
      assert_template 'output_file_templates/new'
      assert_nil flash[:success]
      assert_not_nil flash[:error]
      response.body.should_not have_css("span.translation_missing")
    end

  end

  context :edit do

    before(:each) do
      get :edit, :id => 1
    end

    it 'should get a template' do
      assert assigns[:output_file_template]
      assert_equal 1, assigns[:output_file_template].id
    end

    it 'should render the edit page' do
      assert_template 'output_file_templates/edit'
      response.body.should_not have_css("span.translation_missing")
    end

  end

  context :update do

    it 'should get a template' do
      OutputFileTemplate.any_instance.stubs(:update_attributes => true)
      post :update, :id => 1, :output_file_template => {}
      assert assigns[:output_file_template]
      assert_equal 1, assigns[:output_file_template].id
    end

    it 'should render the show page if save succeeds' do
      OutputFileTemplate.any_instance.stubs(:update_attributes => true)
      post :update, :id => 1, :output_file_template => {}
      response.should be_redirect
      assert_not_nil flash[:success]
      assert_nil flash[:error]
      assert_redirected_to output_file_template_path(1)
      response.body.should_not have_css("span.translation_missing")
    end

    it 'should render new form if save fails' do
      OutputFileTemplate.any_instance.stubs(:update_attributes => false)
      post :update, :id => 1, :output_file_template => {}
      assert_template 'output_file_templates/new'
      assert_nil flash[:success]
      assert_not_nil flash[:error]
      response.body.should_not have_css("span.translation_missing")
    end

  end

  context 'destroy' do
    it 'should redirect to the index' do
      delete(:destroy, :id => 2)
      assert_redirected_to '/output_file_templates'
      assert_not_nil flash[:success]
      response.body.should_not have_css("span.translation_missing")
    end
  end

end
