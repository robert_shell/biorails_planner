require File.dirname(__FILE__)+'/../spec_helper'

describe ExperimentOrderWizardsController do

  before(:each) do
    session[:current_user_id] = User.find_by_login('admin')
    User.current = User.find_by_login(:admin)
  end

  let(:result_task) { ResultTask.find_by_name('work_service_a') }
  let(:queue) { result_task.queue }

  describe 'step1' do
    it 'sets a new result task' do
      get :step1_new, outline_queue_id: queue.id, result_task: {description: 'my result'}
      expect(assigns(:result_task)).to be_a_new(ResultTask)
      expect(assigns(:result_task).outline_id).to eq(queue.outline_id);
      expect(assigns(:result_task).parent_folder_id).to eq(queue.project_element_id);
      expect(assigns(:result_task).process_version_id).to eq(queue.processes.output.first.try(:id));
      expect(assigns(:result_task).description).to eq('my result')
    end
    it 'sets the outline_queue' do
      get :step1_new, outline_queue_id: queue.id
      expect(assigns(:outline_queue).id).to eq(queue.id)
    end
    it 'renders step 1' do
      get :step1_new, outline_queue_id: queue.id
      expect(response).to render_template(:step1_new)
    end
  end

  describe 'step1_create' do
    context 'the result task is valid' do
      before :each do
        ResultTask.any_instance.stubs(:valid?).returns true;
        post :step1_create, outline_queue_id: queue.id, result_task: {description: 'my result'}
      end
      it 'sets a new result task' do
        expect(assigns(:result_task).outline_id).to eq(queue.outline_id);
        expect(assigns(:result_task).parent_folder_id).to eq(queue.project_element_id);
        expect(assigns(:result_task).process_version_id).to eq(queue.processes.output.first.try(:id));
        expect(assigns(:result_task).description).to eq('my result')
      end
      it 'sets the outline_queue' do
        expect(assigns(:outline_queue).id).to eq(queue.id)
      end
      it 'renders step 2' do
        expect(response).to redirect_to step2_experiment_order_wizard_url(assigns(:result_task))
      end
    end
    context 'the result task is invalid' do
      it 'renders step 1' do
        ResultTask.any_instance.stubs(:valid?).returns false
        post :step1_create, outline_queue_id: queue.id, result_task: {description: 'my result'}
        expect(response).to render_template(:step1_new)
      end
    end
  end

  describe 'step1_edit' do
    before :each do
      get :step1_edit, id: result_task.id
    end
    it 'gets the result task' do
      expect(assigns(:result_task).id).to eq(result_task.id)
    end
    it 'sets the outline_queue' do
      expect(assigns(:outline_queue).id).to eq(queue.id)
    end
    it 'renders step 1 edit' do
      expect(response).to render_template(:step1_new)
    end
  end

  describe 'step1_update' do
    context 'the result task is valid' do
      before :each do
        ResultTask.any_instance.stubs(:valid?).returns true;
        put :step1_update, id: result_task.id, result_task: {description: 'my result'}
        result_task.reload
      end
      it 'gets and modifies result task' do
        expect(assigns(:result_task).id).to eq(result_task.id)
        expect(result_task.description).to eq('my result')
      end
      it 'redirects to step 2' do
        expect(response).to redirect_to step2_experiment_order_wizard_url(assigns(:result_task))
      end
    end
    context 'the result task is invalid' do
      it 'renders step 1 edit' do
        ResultTask.any_instance.stubs(:valid?).returns false;
        put :step1_update, id: result_task.id, result_task: {description: 'my result'}
        expect(response).to render_template(:step1_new)
      end
    end
  end

  describe 'step2' do
    before :each do
      QueryEngine.stubs(:query).returns mock
      get :step2, id: result_task.id
    end
    it 'gets the result task' do
      expect(assigns(:result_task).id).to eq(result_task.id)
    end
    it 'gets the query' do
      expect(assigns(:query)).to_not be_nil
    end
    it 'renders step2' do
      expect(response).to render_template(:step2)
    end
  end

  describe 'step3_new' do
    before :each do
      @order = mock(order_profile: mock)
      Order.stubs(:from_task).returns @order
      get :step3_new, id: result_task.id
    end
    it 'gets the result task' do
      expect(assigns(:result_task).id).to eq(result_task.id)
    end
    it 'gets the order' do
      expect(assigns(:order)).to eq(@order)
    end
    it 'renders step3_new' do
      expect(response).to render_template(:step3_new)
    end
  end

  describe 'step3_create' do
    before :each do
      Order.any_instance.stubs(:fill_order_from_order_profile).returns true
    end
    context 'a valid order' do
      before :each do
        Order.any_instance.stubs(:save).returns true
        post :step3_create, id: result_task.id
      end
      it 'gets the result task' do
        expect(assigns(:result_task).id).to eq(result_task.id)
      end
      it 'creates a new order' do
        expect(assigns(:order)).to_not be_nil
        expect(assigns(:order).outline_queue_id).to eq(queue.id)
        expect(assigns(:order).task_id).to eq(result_task.id)
      end
      it 'redirects to step4_new' do
        expect(response).to redirect_to step4_new_experiment_order_wizard_url(result_task)
      end
    end
    context 'an invalid order' do
      before :each do
        Order.any_instance.stubs(:save).returns false
        Order.any_instance.expects(:stock_check)
        Order.any_instance.stubs(:order_profile)
        post :step3_create, id: result_task.id
      end
      it 'sets a flash message' do
        expect(flash[:error]).to_not be_nil
      end
      it 'renders step 3_new' do
        expect(response).to render_template(:step3_new)
      end

    end
  end

end

