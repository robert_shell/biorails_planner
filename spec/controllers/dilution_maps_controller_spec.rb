require 'spec_helper'

describe DilutionMapsController do

  render_views

  before(:each) do
    User.background_session
    session[:current_user_id] =User.current.id
    session[:current_element_id] = User.current.project_element_id

    @process_version = ProcessVersion.find(9901)
  end

  context 'show' do

    it 'should render a show page' do
      get :show, :id => 9901
      response.should be_success
      assert_template 'show'
      assert_template 'diagram'
      response.body.should_not have_css("span.translation_missing")
    end

  end

  context 'new' do

    it 'should show a new form when a process version is provided' do
      @process_version.project_element.set_state(State.find_by_level_no(1))

      DilutionMap.find(9901).destroy

      get :new, :process_version_id => 9901
      response.should be_success
      assert_template 'new'
      assert_template 'form'
      assert_template 'diagram'
      response.body.should_not have_css("span.translation_missing")
    end

    it 'should show a new form as javascript when a process version is provided' do
      @process_version.project_element.set_state(State.find_by_level_no(1))

      DilutionMap.find(9901).destroy

      get :new, :process_version_id => 9901, :format => 'js'
      response.should be_success
      assert_template 'new'
      assert_template 'form'
      assert_template 'diagram'
      response.body.should_not have_css("span.translation_missing")
    end

    it 'should return to home if no process version is provided' do
      get :new, :process_version_id => 99999999999999999
      response.should be_redirect
      assert_redirected_to '/'
      flash[:error].should_not be_nil
    end

    it 'should return to home if process version is not changeable' do
      ProcessVersion.any_instance.stubs(:changeable? => false)

      get :new, :process_version_id => 9901
      response.should be_redirect
      flash[:warning].should_not be_nil
    end


  end

  context 'create' do

    it 'should take you to the process version if create is successful' do
      post :create,
           :dilution_map=>{
             :process_version_id=>"9901",
             :coasts_plate_format_id=>"1",
             :source_by_row=>"false",
             :source_length=>"8",
             :length=>"12",
             :block_slot_type=>"samp",
             :block_sample_id=>"8",
             :block_start=>"11",
             :block_end=>"11",
             :dilution_map_slots_attributes=>{
               "0"=>{:slot_type=>"samp", :position=>"0", :sample_id=>""},
               "1"=>{:slot_type=>"samp", :position=>"1", :sample_id=>""},
               "2"=>{:slot_type=>"samp", :position=>"2", :sample_id=>""},
               "3"=>{:slot_type=>"samp", :position=>"3", :sample_id=>""},
               "4"=>{:slot_type=>"samp", :position=>"4", :sample_id=>""},
               "5"=>{:slot_type=>"samp", :position=>"5", :sample_id=>""},
               "6"=>{:slot_type=>"samp", :position=>"6", :sample_id=>""},
               "7"=>{:slot_type=>"samp", :position=>"7", :sample_id=>""},
               "8"=>{:slot_type=>"samp", :position=>"8", :sample_id=>""},
               "9"=>{:slot_type=>"samp", :position=>"9", :sample_id=>""},
               "10"=>{:slot_type=>"high", :position=>"10", :sample_id=>"8"},
               "11"=>{:slot_type=>"low", :position=>"11", :sample_id=>"8"}}
           }
      response.should be_redirect
      assert_redirected_to process_version_path(@process_version)
      flash[:error].should be_blank
      flash[:warning].should be_blank
      flash[:success].should_not be_blank
    end

    it 'should run create js script if create is successful' do
      post :create,
           :dilution_map=>{
             :process_version_id=>"9901",
             :coasts_plate_format_id=>"1",
             :source_by_row=>"false",
             :source_length=>"8",
             :length=>"12",
             :block_slot_type=>"samp",
             :block_sample_id=>"8",
             :block_start=>"11",
             :block_end=>"11",
             :consolidation_algorithm => 'always_split',
             :dilution_map_slots_attributes=>{
               "0"=>{:slot_type=>"samp", :position=>"0", :sample_id=>""},
               "1"=>{:slot_type=>"samp", :position=>"1", :sample_id=>""},
               "2"=>{:slot_type=>"samp", :position=>"2", :sample_id=>""},
               "3"=>{:slot_type=>"samp", :position=>"3", :sample_id=>""},
               "4"=>{:slot_type=>"samp", :position=>"4", :sample_id=>""},
               "5"=>{:slot_type=>"samp", :position=>"5", :sample_id=>""},
               "6"=>{:slot_type=>"samp", :position=>"6", :sample_id=>""},
               "7"=>{:slot_type=>"samp", :position=>"7", :sample_id=>""},
               "8"=>{:slot_type=>"samp", :position=>"8", :sample_id=>""},
               "9"=>{:slot_type=>"samp", :position=>"9", :sample_id=>""},
               "10"=>{:slot_type=>"high", :position=>"10", :sample_id=>"8"},
               "11"=>{:slot_type=>"low", :position=>"11", :sample_id=>"8"}}
           },
           :format => 'js'
      response.should be_success
      assert_template 'create'
    end

    it 'should return you to the new form if create fails' do
      ProcessVersion.any_instance.stubs(:changeable? => true)
      post :create,
           :dilution_map=>{
             :process_version_id=>"9901",
             :coasts_plate_format_id=>"1",
             :source_by_row=>"false",
             :source_length=>"8",
             :length=>"12",
             :block_slot_type=>"samp",
             :block_sample_id=>"8",
             :block_start=>"11",
             :block_end=>"11",
             :consolidation_algorithm => 'always_split',
             :dilution_map_slots_attributes=>{
               "0"=>{:slot_type=>"samp", :position=>"0", :sample_id=>""},
               "1"=>{:slot_type=>"samp", :position=>"1", :sample_id=>""},
               "2"=>{:slot_type=>"samp", :position=>"2", :sample_id=>""},
               "3"=>{:slot_type=>"samp", :position=>"3", :sample_id=>""},
               "4"=>{:slot_type=>"samp", :position=>"4", :sample_id=>""},
               "5"=>{:slot_type=>"samp", :position=>"5", :sample_id=>""},
               "6"=>{:slot_type=>"samp", :position=>"6", :sample_id=>""},
               "7"=>{:slot_type=>"samp", :position=>"7", :sample_id=>""},
               "8"=>{:slot_type=>"samp", :position=>"8", :sample_id=>""},
               "9"=>{:slot_type=>"samp", :position=>"9", :sample_id=>""},
               "10"=>{:slot_type=>"high", :position=>"10", :sample_id=>"8"},
               "11"=>{:slot_type=>"low", :position=>"11", :sample_id=>"8"},
               # Note, this is out of range
               "12"=>{:slot_type=>"low", :position=>"99", :sample_id=>"8"}

             }
           }
      response.should be_success
      assert_template 'new'
      flash[:error].should_not be_blank
      flash[:warning].should be_blank
      flash[:success].should be_blank
    end

    it 'should run the create js script if create fails' do
      ProcessVersion.any_instance.stubs(:changeable? => true)
      post :create,
           :dilution_map=>{
             :process_version_id=>"9901",
             :coasts_plate_format_id=>"1",
             :source_by_row=>"false",
             :source_length=>"8",
             :length=>"12",
             :block_slot_type=>"samp",
             :block_sample_id=>"8",
             :block_start=>"11",
             :block_end=>"11",
             :dilution_map_slots_attributes=>{
               "0"=>{:slot_type=>"samp", :position=>"0", :sample_id=>""},
               "1"=>{:slot_type=>"samp", :position=>"1", :sample_id=>""},
               "2"=>{:slot_type=>"samp", :position=>"2", :sample_id=>""},
               "3"=>{:slot_type=>"samp", :position=>"3", :sample_id=>""},
               "4"=>{:slot_type=>"samp", :position=>"4", :sample_id=>""},
               "5"=>{:slot_type=>"samp", :position=>"5", :sample_id=>""},
               "6"=>{:slot_type=>"samp", :position=>"6", :sample_id=>""},
               "7"=>{:slot_type=>"samp", :position=>"7", :sample_id=>""},
               "8"=>{:slot_type=>"samp", :position=>"8", :sample_id=>""},
               "9"=>{:slot_type=>"samp", :position=>"9", :sample_id=>""},
               "10"=>{:slot_type=>"high", :position=>"10", :sample_id=>"8"},
               "11"=>{:slot_type=>"low", :position=>"11", :sample_id=>"8"},
               # Note, this is out of range
               "12"=>{:slot_type=>"low", :position=>"99", :sample_id=>"8"}

             }
           },
           :format => 'js'
      response.should be_success
      assert_template 'create'
      assert_template 'form'
    end

  end

  context 'edit' do

    it 'should show a form' do
      @process_version.project_element.set_state(State.find_by_level_no(1))


      get :edit, :id => 9901
      response.should be_success
      assert_template 'edit'
      assert_template 'form'
      assert_template 'diagram'
      response.body.should_not have_css("span.translation_missing")
    end

    it 'should show a form as javascript' do
      @process_version.project_element.set_state(State.find_by_level_no(1))
      get :edit, :id => 9901, :format => 'js'
      response.should be_success
      assert_template 'edit'
      assert_template 'form'
      assert_template 'diagram'
      response.body.should_not have_css("span.translation_missing")
    end

    it 'should return to home if wrong dilution map is provided' do
      get :edit, :id => 999999999
      response.should be_redirect
    end

    it 'should return to home if process version is not changeable' do
      ProcessVersion.any_instance.stubs(:changeable? => false)

      get :edit, :id => 9901
      response.should be_redirect
      flash[:warning].should_not be_nil
    end

  end

  context 'update' do
    it 'should take you to the process version if update is successful' do
      ProcessVersion.any_instance.stubs(:changeable? => true)
      @process_version.dilution_map.dilution_map_slots.destroy_all
      post :update,
           :id => 9901,
           :dilution_map=>{
             :process_version_id=>"9901",
             :coasts_plate_format_id=>"1",
             :source_by_row=>"true",
             :source_length=>"12",
             :length=>"12",
             :block_slot_type=>"samp",
             :block_sample_id=>"8",
             :block_start=>"11",
             :block_end=>"11",
             :dilution_map_slots_attributes=>{
               "0"=>{:slot_type=>"samp", :position=>"0", :sample_id=>""},
               "1"=>{:slot_type=>"samp", :position=>"1", :sample_id=>""},
               "2"=>{:slot_type=>"samp", :position=>"2", :sample_id=>""},
               "3"=>{:slot_type=>"samp", :position=>"3", :sample_id=>""},
               "4"=>{:slot_type=>"samp", :position=>"4", :sample_id=>""},
               "5"=>{:slot_type=>"samp", :position=>"5", :sample_id=>""},
               "6"=>{:slot_type=>"samp", :position=>"6", :sample_id=>""},
               "7"=>{:slot_type=>"samp", :position=>"7", :sample_id=>""}
             }
           }
      response.should be_redirect
      assert_redirected_to process_version_path(@process_version)
      flash[:error].should be_blank
      flash[:warning].should be_blank
      flash[:success].should_not be_blank
    end

    it 'should run the update js script if update is successful' do
      ProcessVersion.any_instance.stubs(:changeable? => true)
      @process_version.dilution_map.dilution_map_slots.destroy_all
      post :update,
           :id => 9901,
           :dilution_map=>{
             :process_version_id=>"9901",
             :coasts_plate_format_id=>"1",
             :source_by_row=>"true",
             :source_length=>"12",
             :length=>"12",
             :block_slot_type=>"samp",
             :block_sample_id=>"8",
             :block_start=>"11",
             :block_end=>"11",
             :consolidation_algorithm => 'always_split',
             :dilution_map_slots_attributes=>{
               "0"=>{:slot_type=>"samp", :position=>"0", :sample_id=>""},
               "1"=>{:slot_type=>"samp", :position=>"1", :sample_id=>""},
               "2"=>{:slot_type=>"samp", :position=>"2", :sample_id=>""},
               "3"=>{:slot_type=>"samp", :position=>"3", :sample_id=>""},
               "4"=>{:slot_type=>"samp", :position=>"4", :sample_id=>""},
               "5"=>{:slot_type=>"samp", :position=>"5", :sample_id=>""},
               "6"=>{:slot_type=>"samp", :position=>"6", :sample_id=>""},
               "7"=>{:slot_type=>"samp", :position=>"7", :sample_id=>""}
             }
           },
           :format => 'js'
      assert_template 'update'
    end

    it 'should return you to the new form if update fails' do
      ProcessVersion.any_instance.stubs(:changeable? => true)
      post :update,
           :id => 9901,
           :dilution_map=>{
             :process_version_id=>"9901",
             :coasts_plate_format_id=>"1",
             :source_by_row=>"true",
             :source_length=>"12",
             :length=>"12",
             :block_slot_type=>"samp",
             :block_sample_id=>"8",
             :block_start=>"11",
             :block_end=>"11",
             :dilution_map_slots_attributes=>{
               "0"=>{:slot_type=>"samp", :position=>"0", :sample_id=>""},
               "1"=>{:slot_type=>"samp", :position=>"1", :sample_id=>""},
               "2"=>{:slot_type=>"samp", :position=>"2", :sample_id=>""},
               "3"=>{:slot_type=>"samp", :position=>"3", :sample_id=>""},
               "4"=>{:slot_type=>"samp", :position=>"4", :sample_id=>""},
               "5"=>{:slot_type=>"samp", :position=>"5", :sample_id=>""},
               "6"=>{:slot_type=>"samp", :position=>"6", :sample_id=>""},
               "7"=>{:slot_type=>"samp", :position=>"7", :sample_id=>""},
               "8"=>{:slot_type=>"samp", :position=>"8", :sample_id=>""},
               "9"=>{:slot_type=>"samp", :position=>"9", :sample_id=>""},
               "10"=>{:slot_type=>"high", :position=>"10", :sample_id=>"8"},
               "11"=>{:slot_type=>"low", :position=>"11", :sample_id=>"8"},
               # Note, this is out of range
               "12"=>{:slot_type=>"low", :position=>"99", :sample_id=>"8"}

             }
           }
      response.should be_success
      assert_template 'edit'
      flash[:error].should_not be_blank
      flash[:warning].should be_blank
      flash[:success].should be_blank
    end

    it 'should run the update js script if update fails' do
      ProcessVersion.any_instance.stubs(:changeable? => true)
      post :update,
           :id => 9901,
           :dilution_map=>{
             :process_version_id=>"9901",
             :coasts_plate_format_id=>"1",
             :source_by_row=>"true",
             :source_length=>"12",
             :length=>"12",
             :block_slot_type=>"samp",
             :block_sample_id=>"8",
             :block_start=>"11",
             :block_end=>"11",
             :dilution_map_slots_attributes=>{
               "0"=>{:slot_type=>"samp", :position=>"0", :sample_id=>""},
               "1"=>{:slot_type=>"samp", :position=>"1", :sample_id=>""},
               "2"=>{:slot_type=>"samp", :position=>"2", :sample_id=>""},
               "3"=>{:slot_type=>"samp", :position=>"3", :sample_id=>""},
               "4"=>{:slot_type=>"samp", :position=>"4", :sample_id=>""},
               "5"=>{:slot_type=>"samp", :position=>"5", :sample_id=>""},
               "6"=>{:slot_type=>"samp", :position=>"6", :sample_id=>""},
               "7"=>{:slot_type=>"samp", :position=>"7", :sample_id=>""},
               "8"=>{:slot_type=>"samp", :position=>"8", :sample_id=>""},
               "9"=>{:slot_type=>"samp", :position=>"9", :sample_id=>""},
               "10"=>{:slot_type=>"high", :position=>"10", :sample_id=>"8"},
               "11"=>{:slot_type=>"low", :position=>"11", :sample_id=>"8"},
               # Note, this is out of range
               "12"=>{:slot_type=>"low", :position=>"99", :sample_id=>"8"}

             }
           },
           :format => 'js'
      response.should be_success
      assert_template 'update'
      assert_template 'form'
    end
  end

  context 'renew_form' do
    it 'should update the form with relevant information' do
      post :renew_form,
           :dilution_map => {
             :process_version_id => "9901",
             :coasts_plate_format_id => "2",
             :source_by_row => "true",
             :source_length => "8",
             :length => "8",
             :block_slot_type => "samp",
             :block_sample_id => "1",
             :block_start => "",
             :block_end => ""
           },
           :format => 'js'
      response.should be_success
      assert_template :renew_form
      assert_template 'form'
      assert_template 'diagram'

      flash[:warning].should be_nil
      response.body.should_not have_css("span.translation_missing")
    end

    it 'should warn users if there are not enough sample slots' do
      post :renew_form,
           :id => '9901',
           :dilution_map => {
             :process_version_id => "9901",
             :coasts_plate_format_id => "2", # 384 plate
             :source_by_row => "true", # use the long side
             :source_length => "8",
             :length => "8",
             :block_slot_type => "samp",
             :block_sample_id => "1",
             :block_start => "",
             :block_end => ""
           },
           :format => 'js'
      response.should be_success
      # no sample slots yet, warning that there isn't enough
      flash[:warning].should_not be_nil
      response.body.should_not have_css("span.translation_missing")
    end

  end

  context 'build_wells' do
    it 'should build a block of wells' do
      post :build_wells,
           :dilution_map => {
             :process_version_id => "9901",
             :coasts_plate_format_id => "1",
             :source_by_row => "false",
             :source_length => "8",
             :length => "8",
             :block_slot_type => "samp",
             :block_sample_id => "1",
             :block_start => "0",
             :block_end => "7"
           },
           :format => 'js'
      response.should be_success
      assert_template :renew_form
      assert_template 'form'
      assert_template 'diagram'

      assert_equal 8, assigns(:dilution_map).dilution_map_slots.length

      flash[:warning].should be_nil
      response.body.should_not have_css("span.translation_missing")
    end

    it 'should warn users if there are not enough sample slots' do
      post :build_wells,
           :dilution_map => {
             :process_version_id => "9901",
             :coasts_plate_format_id => "1",
             :source_by_row => "false",
             :source_length => "8",
             :length => "8",
             :block_slot_type => "samp",
             :block_sample_id => "1",
             :block_start => "0",
             :block_end => "3"
           },
           :format => 'js'
      response.should be_success
      assert_template :renew_form
      assert_template 'form'
      assert_template 'diagram'

      assert_equal 4, assigns(:dilution_map).dilution_map_slots.length

      flash[:warning].should_not be_nil
      response.body.should_not have_css("span.translation_missing")
    end
  end

end
