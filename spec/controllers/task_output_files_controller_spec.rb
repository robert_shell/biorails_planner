require File.dirname(__FILE__)+'/../../../../../spec/spec_helper'


describe TaskOutputFilesController do

  render_views

  let(:task) { ResultTask.find_by_name('work_service_d') }
  let(:output_file_template) { OutputFileTemplate.find(1) }
  let(:process_version) { FactoryGirl.create(:process_version) }
  let(:process_version_file_template) { process_version.process_version_file_templates.create(
    :output_file_template_id => OutputFileTemplate.first.id,
    :file_name => 'file',
    :file_type => 'tsv'
  ) }

  before(:each) do
    session[:current_user_id] = User.find_by_login('admin')
    User.current = User.find_by_login(:admin)
    User.background_session


  end

  context :new do

    it 'should render the new form' do
      get :new, :id => task.id

      assert_kind_of Task, assigns[:task]
      assert_template 'task_output_files/new'

      assert_nil flash[:error]
      page.should_not have_missing_translations
    end

    it 'should show an error from a job' do
      Job.stubs(:find_by_queue_name => Job.new)
      Job.any_instance.stubs(:error_messages => 'some error')

      get :new, :id => task.id, :job_name => 'anything'

      assert_not_nil flash[:error]
      page.should_not have_missing_translations

    end

  end

  context :renew_form do

    it 'should provide a form partial with the content of a form' do
      get(:renew_form,
          {
            'id' => task.id,
            'result_task' => {'process_version_file_template_id' => process_version_file_template.id},
            :format => 'js'
          })
      response.should render_template('task_output_files/renew_form')
      response.should render_template(:partial => 'task_output_files/_form')
      page.should_not have_missing_translations
    end

  end

  context :create do
    it 'should delegate file creation to TaskFinalizeJob' do
      post(:create,
           'id' => task.id,
           'result_task' => {'process_version_file_template_id' => process_version_file_template.id},
           'back_url' => "tasks/#{task.id}")
      assert assigns[:job_id]
      job = Job.find_by_queue_name assigns[:job_id]

      assert_equal 2, job.data[:user_id]
      assert_equal task.id, job.data[:task].id
      assert_equal new_task_output_file_path(:id => task.id), job.data[:fail_url]

      assert_redirected_to "/jobs/waiting/#{assigns[:job_id]}?redirect=true"
      page.should_not have_missing_translations

    end

  end

end
