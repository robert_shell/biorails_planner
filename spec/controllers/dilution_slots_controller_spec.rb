require File.dirname(__FILE__)+'/../../../../../spec/spec_helper'

describe DilutionSlotsController do

  before(:each) do
    session[:current_user_id] = User.find_by_login('admin')
    User.current = User.find_by_login(:admin)
    User.background_session
    OutlineQueue.any_instance.stubs(:coasts_orderable? => true)
    Task.any_instance.stubs(:dilution_map => DilutionMap.first)
  end

  let(:experiment_task) { FactoryGirl.create(:result_task, :with_outline_queue) }
  let(:order_profile) { FactoryGirl.create(:order_profile, :solid) }

  context :show do

    before(:each) do
      get :show, :task_id => experiment_task.id
    end

    it 'should render the show partial' do
      assert_template 'dilution_slots/show'
      response.body.should_not have_css("span.translation_missing")
    end

    it 'should get a plate_data_store object' do
      assert_kind_of Bi::PlateDataStore, assigns[:plate_data_store]
    end

  end

  context :new do

    it 'should show warning if dilution slots are already present' do
      Task.any_instance.stubs(:dilution_slots => [1,2,3])
      get :new, :task_id => experiment_task.id
      assert_template 'dilution_slots/new'
      assert_not_nil flash[:warning]
    end

    it 'should render new form' do
      get :new, :task_id => experiment_task.id
      assert_template 'dilution_slots/new'
      assert_nil flash[:warning]
    end

    it 'should show warnings from jobs' do
      Job.stubs(:find_by_queue_name => Job.new)
      get :new, :task_id => experiment_task.id, :job_name => '123456789qwertyuiop'

    end

  end

  context :create do

    before(:each) do
      post(:create,
           :task_id => experiment_task.id,
           :result_task => {
             :consolidation_algorithm => 'always_split'
           }
      )
    end

    it 'should initialise a background job' do
      assert assigns[:job_id]
      job = Job.find_by_queue_name(assigns[:job_id])
      assert job
      assert_equal 2, job.data[:user_id]
      assert_equal "/tasks/#{experiment_task.id}/dilution_slots", job.data[:success_url]
      assert_equal "/tasks/#{experiment_task.id}/dilution_slots/new", job.data[:fail_url]
      assert_equal experiment_task.id, job.data[:task].id
      assert_equal 'Biorails::TaskFinalizeJob', job.job_class
      assert_equal 'run_consolidation', job.job_method
    end

    it 'should redirect to the background job' do
      assert_redirected_to "/jobs/waiting/#{assigns[:job_id]}?redirect=true"
    end

  end

  context :update do

    before(:each) do
      post(:update,
           :task_id => experiment_task.id,
           :dilution_slot_positions => '[]'
      )
    end

    it 'should initialise a background job' do
      assert assigns[:job_id]
      job = Job.find_by_queue_name(assigns[:job_id])
      assert job
      assert_equal 2, job.data[:user_id]
      assert_equal "/tasks/#{experiment_task.id}", job.data[:success_url]
      assert_equal "/tasks/#{experiment_task.id}/dilution_slots", job.data[:fail_url]
      assert_equal experiment_task.id, job.data[:task].id
      assert_equal 'Biorails::TaskFinalizeJob', job.job_class
      assert_equal 'rearrange_slots', job.job_method
    end

    it 'should redirect to the background job' do
      assert_redirected_to "/jobs/waiting/#{assigns[:job_id]}?redirect=true"
    end

  end




end
