require File.dirname(__FILE__)+'/../spec_helper'

describe ProcessVersionFileTemplatesController do
  render_views

  context :new do

    before (:each) do
      session[:current_element_id] = 2
      session[:current_user_id] = User.find_by_login("admin")
      User.current = User.find_by_login(:admin)
      @process_version = FactoryGirl.create(:process_version)
    end

    it 'should render the new form' do
      get :new, :process_version_id => @process_version.id
      assert_template 'process_version_file_templates/new'
      assert_template 'process_version_file_templates/_form'
      response.body.should_not have_missing_translations
    end

    it 'should render the new form as javascript' do
      get :new, :process_version_id => @process_version.id, :format => 'js'
      assert_template 'process_version_file_templates/new'
      assert_template 'process_version_file_templates/_form'
      response.body.should_not have_missing_translations
    end

  end

  context :create do
    before (:each) do
      session[:current_element_id] = 2
      session[:current_user_id] = User.find_by_login("admin")
      User.current = User.find_by_login(:admin)
      @process_version = FactoryGirl.create(:process_version)
    end

    it 'should create a process version file template' do
      post :create,
           :process_version_id => @process_version.id,
           :process_version_file_template => {
             :process_version_id => @process_version.id,
             :output_file_template_id => OutputFileTemplate.first.id,
             :file_name => 'file',
             :file_type => 'tsv'
           }
      assert_redirected_to process_version_file_template_path assigns[:process_version_file_template]
      response.body.should_not have_missing_translations
    end

    it 'should render the new form if file template is invalid' do
      post :create,
           :process_version_id => @process_version.id,
           :process_version_file_template => {
             :process_version_id => @process_version.id,
             :output_file_template_id => OutputFileTemplate.first.id,
             :file_name => '',
             :file_type => 'tsv'
           }
      assert_template 'process_version_file_templates/new'
      assert_template 'process_version_file_templates/_form'
      response.body.should_not have_missing_translations
      flash[:error].should_not be_blank
    end

    it 'should create a process version file template as javascript' do
      post :create,
           :process_version_id => @process_version.id,
           :process_version_file_template => {
             :process_version_id => @process_version.id,
             :output_file_template_id => OutputFileTemplate.first.id,
             :file_name => 'file',
             :file_type => 'tsv'
           },
           :format => 'js'
      assert_template :create
      response.body.should_not have_missing_translations
    end

    it 'should render the new form if file template is invalid as javascript' do
      post :create,
           :process_version_id => @process_version.id,
           :process_version_file_template => {
             :process_version_id => @process_version.id,
             :output_file_template_id => OutputFileTemplate.first.id,
             :file_name => '',
             :file_type => 'tsv'
           },
           :format => 'js'
      assert_template 'process_version_file_templates/create'
      assert_template 'process_version_file_templates/_form'
      response.body.should_not have_missing_translations
      flash[:error].should_not be_blank
    end


  end

  context :edit do

    before (:each) do
      session[:current_element_id] = 2
      session[:current_user_id] = User.find_by_login("admin")
      User.current = User.find_by_login(:admin)
      @process_version = FactoryGirl.create(:process_version)
      @process_version_file_template = @process_version.process_version_file_templates.create(
        :output_file_template_id => OutputFileTemplate.first.id,
        :file_name => 'file',
        :file_type => 'tsv'
      )
    end

    it 'should render the edit form' do
      get :edit, :id => @process_version_file_template.id
      assert_template 'process_version_file_templates/edit'
      assert_template 'process_version_file_templates/_form'
      response.body.should_not have_missing_translations
    end

    it 'should render the edit form as javascript' do
      get :edit, :id => @process_version_file_template.id, :format => 'js'
      assert_template 'process_version_file_templates/edit'
      assert_template 'process_version_file_templates/_form'
      response.body.should_not have_missing_translations
    end


  end

  context :update do

    before (:each) do
      session[:current_element_id] = 2
      session[:current_user_id] = User.find_by_login("admin")
      User.current = User.find_by_login(:admin)
      @process_version = FactoryGirl.create(:process_version)
      @process_version_file_template = @process_version.process_version_file_templates.create(
        :output_file_template_id => OutputFileTemplate.first.id,
        :file_name => 'file',
        :file_type => 'tsv'
      )
    end

    it 'should update a process version file template' do
      post :update,
           :id => @process_version_file_template.id,
           :process_version_file_template => {
             :process_version_id => @process_version.id,
             :output_file_template_id => OutputFileTemplate.first.id,
             :file_name => 'asdfa',
             :file_type => 'tsv'
           }
      assert_redirected_to process_version_file_template_path assigns[:process_version_file_template]
      flash[:success].should_not be_blank
      response.body.should_not have_missing_translations
    end

    it 'should render the edit form if the process version file template is invalid' do
      post :update,
           :id => @process_version_file_template.id,
           :process_version_file_template => {
             :process_version_id => @process_version.id,
             :output_file_template_id => OutputFileTemplate.first.id,
             :file_name => '',
             :file_type => 'tsv'
           }
      assert_template 'process_version_file_templates/edit'
      assert_template 'process_version_file_templates/_form'
      response.body.should_not have_missing_translations
      flash[:error].should_not be_blank
      response.body.should_not have_missing_translations
    end

    it 'should update a process version file template as javascript' do
      post :update,
           :id => @process_version_file_template.id,
           :process_version_file_template => {
             :process_version_id => @process_version.id,
             :output_file_template_id => OutputFileTemplate.first.id,
             :file_name => 'asdfa',
             :file_type => 'tsv'
           },
           :format => 'js'
      assert_template 'update'
      response.body.should_not have_missing_translations
    end

    it 'should render the edit form if the process version file template is invalid as javascript' do
      post :update,
           :id => @process_version_file_template.id,
           :process_version_file_template => {
             :process_version_id => @process_version.id,
             :output_file_template_id => OutputFileTemplate.first.id,
             :file_name => '',
             :file_type => 'tsv'
           },
           :format => 'js'
      assert_template 'process_version_file_templates/update'
      assert_template 'process_version_file_templates/_form'
      response.body.should_not have_missing_translations
      flash[:error].should_not be_blank
      response.body.should_not have_missing_translations
    end

  end

  context :show do
    before :each do
      session[:current_element_id] = 2
      session[:current_user_id] = User.find_by_login("admin")
      User.current = User.find_by_login(:admin)
      @process_version = FactoryGirl.create(:process_version)
      @process_version_file_template = @process_version.process_version_file_templates.create(
        :output_file_template_id => OutputFileTemplate.first.id,
        :file_name => 'file',
        :file_type => 'tsv'
      )
    end

    it 'should render the show page' do
      get :show, :id => @process_version_file_template.id
      assert_template 'process_version_file_templates/show'
      response.body.should_not have_missing_translations
    end
  end

  context :index do

    before :each do
      session[:current_element_id] = 2
      session[:current_user_id] = User.find_by_login("admin")
      User.current = User.find_by_login(:admin)
      @process_version = FactoryGirl.create(:process_version)
      @process_version_file_template = @process_version.process_version_file_templates.create(
        :output_file_template_id => OutputFileTemplate.first.id,
        :file_name => 'file',
        :file_type => 'tsv'
      )
    end

    it 'should show a list of process version file templates' do
      get :index, :process_version_id => @process_version.id
      assert_template 'process_version_file_templates/index'
      response.body.should_not have_missing_translations
    end

  end

end
