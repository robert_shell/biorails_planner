require File.join(Rails.root,'spec','spec_helper')

describe "OrdersController" do

  before(:each) do
    @user = User.background_session
    visit "/home?session_key=#{User.current_id}"
    OrderProfile.any_instance.stubs(:outline_queue).returns(OutlineQueue.first)
    OrderItem.any_instance.stubs(:sample_id).returns(1)
    OrderItem.any_instance.stubs(:sample).returns(CdbSample.first)
    QueueItem.any_instance.stubs(:object).returns(CdbSample.first)
    Cart.any_instance.stubs(:reference).returns(OutlineQueue.first)
  end

  it 'should refresh recipient lookup when the recipient style is changed', :js => true do

    @order_profile = OrderProfile.find(9903)

    outline_queue = @order_profile.outline_queue
    outline_queue.items.each do |x|
      x.update_attributes(
        :data_type => 'RlSample',
        :data_id => ((x.data_id.to_i % 7) + 1).to_s
      )
    end
    cart = Cart.find(9901)
    cart.set_items([outline_queue.items.first.id])

    params = {
      :cart_id => cart.id,
      :order => {
        :order_profile_id => @order_profile.id.to_s,
        :outline_queue_id => outline_queue.id.to_s,
        :reference_compound_ids_to_create => {
          '9903' => 'true'
        }
      }
    }
    visit "/orders/new?#{params.to_query}"

    select 'External', :from => 'order_use_internal_recipient'
    find("input#order_coasts_external_recipient_id_display")
    select 'Internal', :from => 'order_use_internal_recipient'
    find("input#order_recipient_user_id_display")
    select 'External', :from => 'order_use_internal_recipient'
    find("input#order_coasts_external_recipient_id_display")
  end

end