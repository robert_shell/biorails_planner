require File.dirname(__FILE__)+'/../spec_helper'

describe ExperimentItemsController do

  let(:experiment) { ResultTask.find_by_name('exp test') }

  before(:each) do
    @user = User.background_session
    visit "/home?session_key=#{User.current_id}"
  end

  describe 'step 2 of the experiment order wizard' do

    before :each do
      visit step2_experiment_order_wizard_path(experiment, session_key: 2)
      expect(find('#gridcontents')).to have_content('pending') #waits for the biorails query ajax calls to complete
    end

    it 'should add two queue items when the 2 is filled in the input and the allocate-n button is clicked', driver: :selenium , :js => true do
      expect {
        fill_in 'number_of_items', with: 2
        click_link('select_items')
        wait_for_ajax
      }.to change { page.all('.popup').count }.by(2)
    end

    it 'should remove a queue item when the remove link is clicked', :js => true, driver: :selenium do
      expect {
        click_link('Remove', match: :first)
        wait_for_ajax
      }.to change { page.all('.popup').count }.by(-1)
    end

    it 'should remove all queue items when the remove all is clicked', :js => true, driver: :selenium do
      expect(page).to have_selector('.popup', minimum: 2)
      click_link('All')
      wait_for_ajax
      expect(page).to have_selector('.popup', count: 0)
    end

    describe 'selecting checkboxes and adding queue items through the add-to-experiment button' do
      context 'select a checkbox' do
        before :each do
          page.all('.x-grid-row-checker')[0].click
        end
        it 'hides the allocate first-n button and shows the add-to-experiment button', :js => true, driver: :selenium do
          expect(page).to have_selector('#add', visible: true)
          expect(page).to have_selector('#first_n', visible: false)
        end
        context 'click the add-to-experiment button' do
          it 'adds a queue item to the experiment', :js => true, driver: :selenium do
            expect {
              click_link('move_to_experiment')
              wait_for_ajax
            }.to change { page.all('.popup').count }.by(1)
          end
        end
        context 'unselect the checkbox' do
          before :each do
            page.all('.x-grid-row-checker')[0].click
          end
          it 'shows the allocate first-n button and hides the add-to-experiment button', :js => true, driver: :selenium do
            expect(page).to have_selector('#add', visible: false)
            expect(page).to have_selector('#first_n', visible: true)
          end
        end
      end
    end
  end
end
