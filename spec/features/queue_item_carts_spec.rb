require File.dirname(__FILE__)+'/../spec_helper'

describe Organize::QueueItemCartsController do

  Capybara.default_wait_time = 20
  before(:each) do

    @outline_queue = OutlineQueue.find(outline_queues(:coasts))

  end

  # Moved to post  queue_items_cart/id with hash of items {1:id 2:id}
  #
  it "todo: should show entry form for reasons if state change requires a reason", :driver => :selenium, :js => true do
    visit "/outline_queue_cart/#{@outline_queue.id}?session_key=2"
    #
    # post
    # select cancelled
    # set extra field
    #
    select('cancelled', :from => "cart_queue_item_state_id")
    page.should have_content('div#reason')
    page.should_not have_content('div#reason.visuallyhidden')

    select('processing', :from => "cart_queue_item_state_id")
    page.should have_content('div#reason.visuallyhidden')
  end

end