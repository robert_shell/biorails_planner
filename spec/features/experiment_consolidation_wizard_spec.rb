require File.dirname(__FILE__)+'/../../../../../spec/spec_helper'
require File.dirname(__FILE__)+'/../../../../../spec/view_helpers'


describe ExperimentConsolidationWizardsController do

  context :step2 do

    before(:each) do
      @task = ResultTask.find(9901)
      @user = User.background_session
      visit "/home?session_key=#{User.current_id}"

      visit "/experiment_consolidation_wizards/#{@task.id}/step2_edit"
      get_slots
      assert_initial_state
    end


    it 'should allow you to move a well to an empty slot', :js => true, :driver => :selenium do
      @samp1_slot.drag_to(@cell4)

      page.should_not have_css("#consolidation_table tbody tr td:nth-child(1) div")
      page.should have_css("#consolidation_table tbody tr td:nth-child(4) div")
      find("#consolidation_table tbody tr td:nth-child(4) div")[:id].should eq('1')
    end

    it 'should not allow you to move a well to a standard slot', :js => true, :driver => :selenium do
      @samp1_slot.drag_to(@cell9)

      # dragged cell should remain unchanged
      page.should have_css("#consolidation_table tbody tr td:nth-child(1) div")
      find("#consolidation_table tbody tr td:nth-child(1) div")[:id].should eq('1')

      #dropped cell should remain unchanged
      page.should have_css("#consolidation_table tbody tr td:nth-child(9) div")
      find("#consolidation_table tbody tr td:nth-child(9) div")[:id].should eq('3')
    end

    it 'should not allow you to move a well to a high control slot', :js => true, :driver => :selenium do
      @samp1_slot.drag_to(@cell11)

      # dragged cell should remain unchanged
      page.should have_css("#consolidation_table tbody tr td:nth-child(1) div")
      find("#consolidation_table tbody tr td:nth-child(1) div")[:id].should eq('1')

      #dropped cell should remain unchanged
      page.should have_css("#consolidation_table tbody tr td:nth-child(11) div")
      find("#consolidation_table tbody tr td:nth-child(11) div")[:id].should eq('4')
    end

    it 'should not allow you to move a well to a low control slot', :js => true, :driver => :selenium do
      @samp1_slot.drag_to(@cell12)

      # dragged cell should remain unchanged
      page.should have_css("#consolidation_table tbody tr td:nth-child(1) div")
      find("#consolidation_table tbody tr td:nth-child(1) div")[:id].should eq('1')

      #dropped cell should remain unchanged
      page.should have_css("#consolidation_table tbody tr td:nth-child(12) div")
      find("#consolidation_table tbody tr td:nth-child(12) div")[:id].should eq('5')
    end

    it 'should swap the position of two wells', :js => true, :driver => :selenium do
      @samp1_slot.drag_to(@cell2)

      # sample one should now be in slot 2
      page.should have_css("#consolidation_table tbody tr td:nth-child(2) div")
      find("#consolidation_table tbody tr td:nth-child(2) div")[:id].should eq('1')

      # sample two should now be in slot 1
      page.should have_css("#consolidation_table tbody tr td:nth-child(1) div")
      find("#consolidation_table tbody tr td:nth-child(1) div")[:id].should eq('2')
    end

    it 'should return to its page-loaded state when Reset is clicked', :js => true, :driver => :selenium do
      @samp1_slot.drag_to(@cell6)
      @samp2_slot.drag_to(@cell5)

      page.should_not have_css("#consolidation_table tbody tr td:nth-child(1) div")
      page.should_not have_css("#consolidation_table tbody tr td:nth-child(2) div")

      find('.btn-primary').click
      wait_for_ajax

      assert_initial_state
    end

    it 'should not save the position of wells unless Save is clicked', :js => true, :driver => :selenium do
      @samp1_slot.drag_to(@cell6)
      @samp2_slot.drag_to(@cell5)

      page.should_not have_css("#consolidation_table tbody tr td:nth-child(1) div")
      page.should_not have_css("#consolidation_table tbody tr td:nth-child(2) div")
      page.should have_css("#consolidation_table tbody tr td:nth-child(6) div")
      find("#consolidation_table tbody tr td:nth-child(6) div")[:id].should eq('1')
      page.should have_css("#consolidation_table tbody tr td:nth-child(5) div")
      find("#consolidation_table tbody tr td:nth-child(5) div")[:id].should eq('2')

      visit "/experiment_consolidation_wizards/#{@task.id}/step2_edit"

      assert_initial_state
    end

  end

  context :step3 do

    before(:each) do
      @task = ResultTask.find(9901)
      @user = User.background_session
      visit "/home?session_key=#{User.current_id}"

      visit "/experiment_consolidation_wizards/#{@task.id}/step3_new"
    end

    it 'should display a preview when Preview is clicked', :js => true, :driver => :selenium do
      page.should_not have_content('#preview_9901')
      page.click_link('Preview')
      wait_for_ajax
      page.should have_css('#preview_9901')
    end

  end


  # These helpers are used for step2, assuring the dilution slots look correct

  # Helper method to fetch all the cells and stuff
  def get_slots
    # dilution slots
    @samp1_slot = find("#consolidation_table tbody tr td:nth-child(1) div")
    @samp2_slot = find("#consolidation_table tbody tr td:nth-child(2) div")
    @std_slot = find("#consolidation_table tbody tr td:nth-child(9) div")
    @high_slot = find("#consolidation_table tbody tr td:nth-child(11) div")
    @low_slot = find("#consolidation_table tbody tr td:nth-child(12) div")
    # cells in the table
    12.times do |i|
      instance_variable_set "@cell#{i+1}", find("#consolidation_table tbody tr td:nth-child(#{i+1})")
    end
  end

  # A whole battery of test to say the slots are where I expect at the start
  def assert_initial_state
    page.should have_css("#consolidation_table tbody tr td:nth-child(1) div")
    find("#consolidation_table tbody tr td:nth-child(1) div")[:id].should eq('1')
    page.should have_css("#consolidation_table tbody tr td:nth-child(2) div")
    find("#consolidation_table tbody tr td:nth-child(2) div")[:id].should eq('2')
    page.should_not have_css("#consolidation_table tbody tr td:nth-child(3) div")
    page.should_not have_css("#consolidation_table tbody tr td:nth-child(4) div")
    page.should_not have_css("#consolidation_table tbody tr td:nth-child(5) div")
    page.should_not have_css("#consolidation_table tbody tr td:nth-child(6) div")
    page.should_not have_css("#consolidation_table tbody tr td:nth-child(7) div")
    page.should_not have_css("#consolidation_table tbody tr td:nth-child(8) div")
    page.should have_css("#consolidation_table tbody tr td:nth-child(9) div")
    find("#consolidation_table tbody tr td:nth-child(9) div")[:id].should eq('3')
    page.should_not have_css("#consolidation_table tbody tr td:nth-child(10) div")
    page.should have_css("#consolidation_table tbody tr td:nth-child(11) div")
    find("#consolidation_table tbody tr td:nth-child(11) div")[:id].should eq('4')
    page.should have_css("#consolidation_table tbody tr td:nth-child(12) div")
    find("#consolidation_table tbody tr td:nth-child(12) div")[:id].should eq('5')
  end

end