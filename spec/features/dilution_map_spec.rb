require File.dirname(__FILE__)+'/../../../../../spec/spec_helper'
require File.dirname(__FILE__)+'/../../../../../spec/view_helpers'



describe "DilutionMapsController" do

  context 'new' do

    context 'dilution-map_form' do

      before(:each) do
        @process_version = ProcessVersion.find(1)
        ProcessVersion.any_instance.stubs(
          :project_element_id => ProjectFolder.first.id,
          :project_element_id => ProjectFolder.first,
          :changeable? => true
        )
        ProjectFolder.first.set_active
        @user = User.background_session
        visit "/home?session_key=#{User.current_id}"
      end

      it 'should change the source row length when plate format is changed', :js => true, :driver => :selenium do
        Capybara.ignore_hidden_elements = false
        visit "/dilution_maps/new?process_version_id=#{@process_version.id}"

        select('Row', :from => 'Read Plates By')
        select('384', :from => 'Source Plate Format')
        wait_for_ajax
        find('#dilution_map_source_length').value.should eq('24')

        select('Column', :from => 'Read Plates By')
        select('384', :from => 'Source Plate Format')
        wait_for_ajax
        find('#dilution_map_source_length').value.should eq('16')

        select('Row', :from => 'Read Plates By')
        select('96', :from => 'Source Plate Format')
        wait_for_ajax
        find('#dilution_map_source_length').value.should eq('12')

        select('Column', :from => 'Read Plates By')
        select('96', :from => 'Source Plate Format')
        wait_for_ajax
        find('#dilution_map_source_length').value.should eq('8')

        Capybara.ignore_hidden_elements = true
      end

      it 'should change the number of slots in the diagram when length is changed', :js => true, :driver => :selenium do
        visit "/dilution_maps/new?process_version_id=#{@process_version.id}"

        select 'Row of 96-Well Plate (12)', :from => 'Row Length'
        wait_for_ajax
        page.all('.dilution_map tr td').count.should == 12

        select 'Column of 96-Well Plate (8)', :from => 'Row Length'
        wait_for_ajax
        page.all('.dilution_map tr td').count.should == 8

        select 'Column of 384-Well Plate (16)', :from => 'Row Length'
        wait_for_ajax
        page.all('.dilution_map tr td').count.should == 16

        select 'Row of 384-Well Plate (24)', :from => 'Row Length'
        wait_for_ajax
        page.all('.dilution_map tr td').count.should == 24
      end

    end

    context 'dilution map diagram (DilutionMapSlots)' do

      before(:each) do
        @process_version = ProcessVersion.find(1)
        ProcessVersion.any_instance.stubs(
          :project_element_id => ProjectFolder.first.id,
          :project_element_id => ProjectFolder.first,
          :changeable? => true
        )
        ProjectFolder.first.set_active
        login_feature_spec
      end

      it 'should show the well form if you click and drag across the diagram', :js => true, :driver => :selenium do
        visit "/dilution_maps/new?process_version_id=#{@process_version.id}"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        page.should have_css('#dilution_map_block_slot_type')
      end

      it 'should display the sample field when a standard slot type is selected', :js => true, :driver => :selenium do
        visit "/dilution_maps/new?process_version_id=#{@process_version.id}"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Standard', :from => 'Slot Type'
        page.should have_css('#dilution_map_block_sample_id_display')

        select 'Sample', :from => 'Slot Type'
        page.should_not have_css('#dilution_map_block_sample_id_display')

        select 'High Control', :from => 'Slot Type'
        page.should have_css('#dilution_map_block_sample_id_display')

        select 'Blank', :from => 'Slot Type'
        page.should_not have_css('#dilution_map_block_sample_id_display')

        select 'Low Control', :from => 'Slot Type'
        page.should have_css('#dilution_map_block_sample_id_display')
      end


      it 'should create a block of sample slots', :js => true, :driver => :selenium do
        visit "/dilution_maps/new?process_version_id=#{@process_version.id}"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Sample', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax

        page.all('td.samp').count.should eq 4
      end

      it 'should create a block of standard slots', :js => true, :driver => :selenium do
        visit "/dilution_maps/new?process_version_id=#{@process_version.id}"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Standard', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax

        page.all('td.std').count.should eq 4
      end

      it 'should create a block of standard slots with a known sample_id', :js => true, :driver => :selenium do
        visit "/dilution_maps/new?process_version_id=#{@process_version.id}"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Standard', :from => 'Slot Type'
        fill_in_combo 'dilution_map_block_sample_id', :with => '1'
        click_button 'Add'
        wait_for_ajax

        page.all('td.std').count.should eq 4
        page.first('td.std').text.should eq '1'
      end

      it 'should create a block of high control slots', :js => true, :driver => :selenium do
        visit "/dilution_maps/new?process_version_id=#{@process_version.id}"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'High Control', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax

        page.all('td.high').count.should eq 4
      end

      it 'should create a block of high_control slots with a known sample_id', :js => true, :driver => :selenium do
        visit "/dilution_maps/new?process_version_id=#{@process_version.id}"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'High Control', :from => 'Slot Type'
        fill_in_combo 'dilution_map_block_sample_id', :with => '1'
        click_button 'Add'
        wait_for_ajax

        page.all('td.high').count.should eq 4
        page.all('td.high')[0].text.should eq '1'
      end

      it 'should create a block of low control slots', :js => true, :driver => :selenium do
        visit "/dilution_maps/new?process_version_id=#{@process_version.id}"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Low Control', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax

        page.all('td.low').count.should eq 4
      end

      it 'should create a block of low control slots with a known sample_id', :js => true, :driver => :selenium do
        visit "/dilution_maps/new?process_version_id=#{@process_version.id}"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Low Control', :from => 'Slot Type'
        fill_in_combo 'dilution_map_block_sample_id', :with => '1'
        click_button 'Add'
        wait_for_ajax

        page.all('td.low').count.should eq 4
        page.all('td.low')[0].text.should eq '1'
      end

      it 'should override slots with new blocks', :js => true, :driver => :selenium do
        visit "/dilution_maps/new?process_version_id=#{@process_version.id}"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[7])

        select 'Sample', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax
        page.all('td.samp').count.should eq 8

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Standard', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax
        page.all('td.samp').count.should eq 4
        page.all('td.std').count.should eq 4
      end

      it 'should override slots with blank slots', :js => true, :driver => :selenium do
        visit "/dilution_maps/new?process_version_id=#{@process_version.id}"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[7])

        select 'Sample', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax
        all('td.samp').count.should eq 8

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Blank', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax
        page.all('td.samp').count.should eq 4
      end

    end

  end

  context 'edit' do

    after(:all) do
      # At the end rebuild fixtures which this feature test changed
      [DilutionMap, DilutionMapSlot].each {|x|x.destroy_all}
    end

    context 'dilution map form' do

      before(:each) do
        @process_version = ProcessVersion.find(1)
        ProcessVersion.any_instance.stubs(
          :project_element_id => ProjectFolder.first.id,
          :project_element_id => ProjectFolder.first,
          :changeable? => true
        )
        ProjectFolder.first.set_active
        @dilution_map = DilutionMap.find(9901)
        DilutionMap.any_instance.stubs(
          :process_version_id => 1,
          :process_version => @process_version
        )
        login_feature_spec
      end


      it 'should change the source row length when plate format is changed', :js => true, :driver => :selenium do
        Capybara.ignore_hidden_elements = false
        visit "/dilution_maps/#{@dilution_map.id}/edit"

        select('Row', :from => 'Read Plates By')
        wait_for_ajax
        select('384', :from => 'Source Plate Format')
        wait_for_ajax
        find('#dilution_map_source_length').value.should eq('24')

        select('Column', :from => 'Read Plates By')
        wait_for_ajax
        select('384', :from => 'Source Plate Format')
        wait_for_ajax
        find('#dilution_map_source_length').value.should eq('16')

        select('Row', :from => 'Read Plates By')
        wait_for_ajax
        select('96', :from => 'Source Plate Format')
        wait_for_ajax
        find('#dilution_map_source_length').value.should eq('12')

        select('Column', :from => 'Read Plates By')
        wait_for_ajax
        select('96', :from => 'Source Plate Format')
        wait_for_ajax
        find('#dilution_map_source_length').value.should eq('8')

        Capybara.ignore_hidden_elements = true
      end

      it 'should change the number of slots in the diagram when length is changed', :js => true, :driver => :selenium do
        visit "/dilution_maps/#{@dilution_map.id}/edit"

        select 'Row of 96-Well Plate (12)', :from => 'Row Length'
        wait_for_ajax
        page.all('.dilution_map tr td').count.should == 12

        select 'Column of 96-Well Plate (8)', :from => 'Row Length'
        wait_for_ajax
        page.all('.dilution_map tr td').count.should == 8

        select 'Column of 384-Well Plate (16)', :from => 'Row Length'
        wait_for_ajax
        page.all('.dilution_map tr td').count.should == 16

        select 'Row of 384-Well Plate (24)', :from => 'Row Length'
        wait_for_ajax
        page.all('.dilution_map tr td').count.should == 24
      end

    end

    context 'dilution map diagram (DilutionMapSlots)' do

      before(:each) do
        @process_version = ProcessVersion.find(1)
        ProcessVersion.any_instance.stubs(
          :project_element_id => ProjectFolder.first.id,
          :project_element_id => ProjectFolder.first,
          :changeable? => true
        )
        ProjectFolder.first.set_active
        @dilution_map = DilutionMap.find(9901)
        DilutionMap.any_instance.stubs(
          :process_version_id => 1,
          :process_version => @process_version
        )
        login_feature_spec
      end

      it 'should show the well form if you click and drag across the diagram', :js => true, :driver => :selenium do
        visit "/dilution_maps/#{@dilution_map.id}/edit"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        page.should have_css('#dilution_map_block_slot_type')
      end

      it 'should display the sample field when a standard slot type is selected', :js => true, :driver => :selenium do
        visit "/dilution_maps/#{@dilution_map.id}/edit"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Standard', :from => 'Slot Type'
        page.should have_css('#dilution_map_block_sample_id_display')

        select 'Sample', :from => 'Slot Type'
        page.should_not have_css('#dilution_map_block_sample_id_display')

        select 'High Control', :from => 'Slot Type'
        page.should have_css('#dilution_map_block_sample_id_display')

        select 'Blank', :from => 'Slot Type'
        page.should_not have_css('#dilution_map_block_sample_id_display')

        select 'Low Control', :from => 'Slot Type'
        page.should have_css('#dilution_map_block_sample_id_display')
      end


      it 'should create a block of sample slots', :js => true, :driver => :selenium do
        visit "/dilution_maps/#{@dilution_map.id}/edit"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Sample', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax

        page.all('td.samp').count.should eq 8
      end

      it 'should create a block of standard slots', :js => true, :driver => :selenium do
        visit "/dilution_maps/#{@dilution_map.id}/edit"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Standard', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax

        page.all('td.std').count.should eq 6
      end

      it 'should create a block of standard slots with a known sample_id', :js => true, :driver => :selenium do
        visit "/dilution_maps/#{@dilution_map.id}/edit"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Standard', :from => 'Slot Type'
        fill_in_combo 'dilution_map_block_sample_id', :with => '1'
        click_button 'Add'
        wait_for_ajax

        page.all('td.std').count.should eq 6
        page.first('td.std').text.should eq '1'
      end

      it 'should create a block of high control slots', :js => true, :driver => :selenium do
        visit "/dilution_maps/#{@dilution_map.id}/edit"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'High Control', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax

        page.all('td.high').count.should eq 5
      end

      it 'should create a block of high_control slots with a known sample_id', :js => true, :driver => :selenium do
        visit "/dilution_maps/#{@dilution_map.id}/edit"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'High Control', :from => 'Slot Type'
        fill_in_combo 'dilution_map_block_sample_id', :with => '1'
        click_button 'Add'
        wait_for_ajax

        page.all('td.high').count.should eq 5
        page.all('td.high')[0].text.should eq '1'
      end

      it 'should create a block of low control slots', :js => true, :driver => :selenium do
        visit "/dilution_maps/#{@dilution_map.id}/edit"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Low Control', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax

        page.all('td.low').count.should eq 5
      end

      it 'should create a block of low control slots with a known sample_id', :js => true, :driver => :selenium do
        visit "/dilution_maps/#{@dilution_map.id}/edit"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Low Control', :from => 'Slot Type'
        fill_in_combo 'dilution_map_block_sample_id', :with => '1'
        click_button 'Add'
        wait_for_ajax

        page.all('td.low').count.should eq 5
        page.all('td.low')[0].text.should eq '1'
      end

      it 'should override slots with new blocks', :js => true, :driver => :selenium do
        visit "/dilution_maps/#{@dilution_map.id}/edit"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[7])

        select 'Sample', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax
        page.all('td.samp').count.should eq 8

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Standard', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax
        page.all('td.samp').count.should eq 4
        page.all('td.std').count.should eq 6
      end

      it 'should override slots with blank slots', :js => true, :driver => :selenium do
        visit "/dilution_maps/#{@dilution_map.id}/edit"
        wait_for_ajax # Initial reload_form

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[7])

        select 'Sample', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax
        all('td.samp').count.should eq 8

        page.all('.dilution_map tr td')[0].drag_to(page.all('.dilution_map tr td')[3])

        select 'Blank', :from => 'Slot Type'
        click_button 'Add'
        wait_for_ajax
        page.all('td.samp').count.should eq 4
      end

    end

  end


end