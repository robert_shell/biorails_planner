require File.dirname(__FILE__)+'/../../../../../spec/spec_helper'
require File.dirname(__FILE__)+'/../../../../../spec/view_helpers'

describe "OrderProfilesController" do


  before(:each) do
    @user = User.background_session
    visit "/home?session_key=#{User.current_id}"
  end


  it 'should show fields for a solid order profile', :js => true do
    visit "/order_profiles/new"

    fill_in_combo 'order_profile_coasts_order_type_id', :with => 'SOLID'

    find("input#order_profile_order_profile_weights_attributes_0_no_of_vials")
    find("input#order_profile_order_profile_weights_attributes_0_weight")
    find("input#order_profile_orderer_user_id_display")
  end

  it 'should show fields for a standard liquid order profile', :js => true do
    Capybara.ignore_hidden_elements = false
    visit "/order_profiles/new"

    fill_in_combo 'order_profile_coasts_order_type_id', :with => 'STANDARD LIQUID'

    find("select#order_profile_coasts_plate_type_id")
    find("input#order_profile_coasts_plate_layout_id")
    find("select#order_profile_rows_first")
    find("select#order_profile_all_quadrants")

    Capybara.ignore_hidden_elements = true
  end

  it 'should show fields for a custom liquid order profile', :js => true do
    visit "/order_profiles/new"

    fill_in_combo 'order_profile_coasts_order_type_id', :with => 'CUSTOM LIQUID'


    find("input#order_profile_volume")
    find("select#order_profile_coasts_plate_type_id")
  end

  it 'should change the recipient lookup when you change recipient style', :js => true do
    Capybara.ignore_hidden_elements = false

    visit "/order_profiles/new"

    find("input#order_profile_recipient_user_id")
    select 'External', :from => 'recipient_style_select'
    find("input#order_profile_coasts_external_recipient_id")
    select 'Internal', :from => 'recipient_style_select'
    find("input#order_profile_recipient_user_id")
    select 'External', :from => 'recipient_style_select'
    find("input#order_profile_coasts_external_recipient_id")

    Capybara.ignore_hidden_elements = true
  end

  it 'should change the purpose type lookup when you change purpose type', :js => true do
    visit "/order_profiles/new"

    select 'User Purpose', :from => 'purpose_type_select'
    sleep 0.2
    fill_in_combo 'order_profile_purpose_id', :with => 'FUN'

    select 'Project', :from => 'purpose_type_select'
    sleep 0.2
    fill_in_combo 'order_profile_purpose_id', :with => 'BILGE'

    select 'Method', :from => 'purpose_type_select'
    sleep 0.2
    fill_in_combo 'order_profile_purpose_id', :with => 'BIG/H'
  end

  it 'should add a compound to the reference compounds table', :js => true do
    visit "/order_profiles/new"

    fill_in_combo 'order_profile_sample_id_to_add', :with => '7'

    click_link('Add Reference Compound')
    page.should have_selector('table#reference_compounds tr', :count => 2)

    fill_in_combo 'order_profile_sample_id_to_add', :with => '6'

    click_link('Add Reference Compound')
    page.should have_selector('table#reference_compounds tr', :count => 3)
  end

  it 'should not add the same reference compound twice', :js => true do
    visit "/order_profiles/new"

    fill_in_combo 'order_profile_sample_id_to_add', :with => '7'

    click_link('Add Reference Compound')
    page.should have_selector('table#reference_compounds tr', :count => 2)
    fill_in_combo 'order_profile_sample_id_to_add', :with => '7'

    click_link('Add Reference Compound')
    page.should have_selector('table#reference_compounds tr', :count => 2)
  end

  it 'should remove a compound from the table', :js => true do
    OrderProfile.any_instance.stubs(:outline_queue).returns(OutlineQueue.first)
    visit "/order_profiles/9902/edit"
    # add a compound
    fill_in_combo 'order_profile_sample_id_to_add', :with => '7'
    click_link('Add Reference Compound')
    # remove it again
    page.should have_selector('table#reference_compounds tr', :count => 2)
    first(:link, 'Remove').click
    page.should have_selector('table#reference_compounds tr', :count => 1)
  end

  # Because the order profile form renews based on it's content, we need to ensure the errors are still displayed
  #
  # based on this variable:
  # @validate_on_renew = true
  #
  it "should redirect back to the new form when a new order profile is invalid", :js => true do
    visit "/order_profiles/new"
    click_button('order_profile_submit')
    find('.alert-error').should have_content('Error: Failed to create Order Profile.')
    page.should have_selector('.help-inline')

    select 'External', :from => 'recipient_style_select'

    find('.alert-error').should have_content('Error: Failed to create Order Profile.')
    page.should have_selector('.help-inline')
  end

end