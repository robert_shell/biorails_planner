require 'spec_helper'



describe TaskConsolidation do

  context 'run' do

    # This is mostly tested as part of the task_patch_spec

    before(:each) do
      puts "start run_consolidation setup #{t1 = Time.now}"
      CoastsOrderPlate.create(
        :id_order_header_ref => Order.first.coasts_order_id,
        :plate_code => 'ABC123'
      )

      User.background_session
      ProcessVersion.any_instance.stubs(:released? => false,:dilution_map => DilutionMap.first)
      ResultTask.any_instance.stubs(:dilution_map => DilutionMap.first)

      @outline = Outline.find_by_name('services')

      @outline_queue = OutlineQueue.find_by_name('service_a')
      @process_version = Biorails::AssayManager.new(@outline_queue).default_output_process

      @task = ResultTask.create!(:name => 'a', :outline_id => @outline.id,
                                 :process_version_id => @process_version.id)
      puts "end run_consolidation setup #{t2 = Time.now} (#{t2 - t1})"
    end


    it 'should create some dilution slots' do
      ProjectFolder.any_instance.stubs(:changeable? => true)
      assert @task.dilution_slots.none?
      @task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        "ABC123", # small plate
        @task.id
      )
      @task_barcodes[0].task = @task
      @task_barcodes[0].finalise
      job_id = TaskConsolidation.async_run(
        {
          :task => @task,
          :success_url => '/success/url',
          :fail_url => '/fail/url'
        }
      )
      assert @task.dilution_slots.any?, 'no slots'

      job = Job.find_by_queue_name(job_id)
      job.error_messages.should be_empty
    end

    it 'should fill in the test_plate and test_column properties' do
      ProjectFolder.any_instance.stubs(:changeable? => true)
      assert @task.dilution_slots.none?
      @task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        "ABC123", # small plate
        @task.id
      )
      @task_barcodes[0].task = @task
      @task_barcodes[0].finalise
      job_id = TaskConsolidation.async_run(
        {
          :task => @task,
          :success_url => '/success/url',
          :fail_url => '/fail/url'
        }
      )
      assert @task.dilution_slots.any?, 'no slots'

      sheet = @task.sheet
      sheet.grid.each do |label, row|
        row['test_plate'].data_content.should_not be_nil, "test_plate is missing in #{label}"
        row['test_column'].data_content.should_not be_nil, "test_column is missing in #{label}"
        row['slot_type'].data_content.should_not be_nil, "slot_type is missing in #{label}"
      end

    end

  end

  context 'rearrange_slots' do
    before(:each) do
      puts "start rearrange_slots setup #{t1 = Time.now}"

      CoastsOrderPlate.create(
        :id_order_header_ref => Order.first.coasts_order_id,
        :plate_code => 'ABC123'
      )

      User.background_session
      ProcessVersion.any_instance.stubs(:released? => false)
      Task.any_instance.stubs(:dilution_map => DilutionMap.first)
      @outline = Outline.find_by_name('services')

      @outline_queue = OutlineQueue.find_by_name('service_a')
      @process_version = Biorails::AssayManager.new(@outline_queue).default_output_process

      @task = ResultTask.create!(:name => 'a', :outline_id => @outline.id,
                                 :process_version_id => @process_version.id)

      ProjectFolder.any_instance.stubs(:changeable? => true)
      @task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        "ABC123",
        @task.id
      )
      @task_barcodes[0].task = @task
      @task_barcodes[0].finalise
      ProjectElement.any_instance.stubs(:changeable? => nil)

      job = TaskConsolidation.new
      job.task = @task
      @job.consolidation

      @task.reload

      assert @task.dilution_slots.any?

      puts "end rearrange_slots setup #{t2 = Time.now} (#{t2 - t1})"

    end


  end

end