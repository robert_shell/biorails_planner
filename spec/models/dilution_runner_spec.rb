require 'spec_helper'


describe Dilution::Runner do

  context 'initialize' do


    it 'should initialise with the dilution_row_length attribute' do
      runner = Dilution::Runner.new(
        :plate_codes => [],
        :well_codes => [],
        :sample_ids => [],
        :length => 12,
        :source_by_row => true
      )
      assert_equal 12, runner.dilution_row_length
    end

    it 'should get source rows from a plate' do
      runner = Dilution::Runner.new(
        :plate_codes => ['ABC123'],
        :well_codes => [],
        :sample_ids => [],
        :length => 12,
        :source_by_row => true
      )
      assert_equal 2, runner.source_rows.length
      assert_equal 2, runner.source_rows[0].length
      assert_instance_of CoastsPlateWell, runner.source_rows[0][0]

      assert_includes runner.display_source_rows, runner.source_rows[0][0].to_s
    end

    it 'should get a source row from a well' do
      runner = Dilution::Runner.new(
        :plate_codes => [],
        :well_codes => ['A0001'],
        :sample_ids => [],
        :length => 12,
        :source_by_row => true
      )
      assert_equal 1, runner.source_rows.length
      assert_equal 1, runner.source_rows[0].length
      assert_instance_of CoastsPlateWell, runner.source_rows[0][0]
      assert_includes runner.display_source_rows, runner.source_rows[0][0].to_s
    end

    it 'should get a source row from a sample' do
      runner = Dilution::Runner.new(
        :plate_codes => [],
        :well_codes => [],
        :sample_ids => [1],
        :length => 12,
        :source_by_row => true
      )
      assert_equal 1, runner.source_rows.length
      assert_equal 1, runner.source_rows[0].length
      assert_instance_of CdbSample, runner.source_rows[0][0]
      assert_includes runner.display_source_rows, runner.source_rows[0][0].to_s
    end

    it 'should get source rows from mixed plates and wells' do
      runner = Dilution::Runner.new(
        :plate_codes => ['ABC123', '123ABC'],
        :well_codes => ['A0001'],
        :sample_ids => [],
        :length => 12,
        :source_by_row => true
      )
      assert_equal 5, runner.source_rows.length
      assert_equal 2, runner.source_rows[0].length
      assert_equal 1, runner.source_rows[4].length

      assert_includes runner.display_source_rows, runner.source_rows[0][0].to_s
    end

  end

  context 'run_always_split' do

    # sources:
    #
    #   A1 A2 . . . . . . . . . .
    #   A3 A4 . . . . . . . . . .
    #   ...
    #
    #   B1
    #
    #   Result (length: 3, by row):
    #
    #  A1 A2 A3
    #  A4 B1


    it 'should put every well in a line and split it into rows' do
      runner = Dilution::Runner.new(
        :plate_codes => ['ABC123'],
        :well_codes => ['B0001'],
        :sample_ids => [],
        :length => 3,
        :source_by_row => true
      )

      runner.run_always_split
      assert runner.dilution_rows.any?
      assert_equal 2, runner.dilution_rows.length
      assert_equal 3, runner.dilution_rows[0].length
      assert_equal 2, runner.dilution_rows[1].length

      assert_equal(
        ['ABC123:A1', 'ABC123:A2', 'ABC123:B1'],
        runner.dilution_rows[0].collect { |x| x.to_s }
      )
      assert_equal(
        ['ABC123:B2', '123ABC:A1'],
        runner.dilution_rows[1].collect { |x| x.to_s }
      )
    end

    #   Result (length: 3, by column):
    #
    #  A1 A3 A2
    #  A4 B1

    it 'should put every well in a line and split it into columns' do
      runner = Dilution::Runner.new(
        :plate_codes => ['ABC123'],
        :well_codes => ['B0001'],
        :sample_ids => [],
        :length => 3,
        :source_by_row => false
      )

      runner.run_always_split
      assert runner.dilution_rows.any?
      assert_equal 2, runner.dilution_rows.length
      assert_equal 3, runner.dilution_rows[0].length
      assert_equal 2, runner.dilution_rows[1].length

      assert_equal(
        ['ABC123:A1', 'ABC123:B1', 'ABC123:A2'],
        runner.dilution_rows[0].collect { |x| x.to_s }
      )
      assert_equal(
        ['ABC123:B2', '123ABC:A1'],
        runner.dilution_rows[1].collect { |x| x.to_s }
      )
    end

    # sources:
    #
    #   A1 A2 . . . . . . . . . .
    #   A3 A4 . . . . . . . . . .
    #   ...
    #
    #   B1
    #
    #   S1
    #
    #   Result (length: 3, by row):
    #
    #  A1 A2 A3
    #  A4 B1 S1

    it 'should treat a sample like another well' do
      runner = Dilution::Runner.new(
        :plate_codes => ['ABC123'],
        :well_codes => ['B0001'],
        :sample_ids => [6],
        :length => 3,
        :source_by_row => false
      )

      runner.run_always_split
      assert runner.dilution_rows.any?
      assert_equal 2, runner.dilution_rows.length
      assert_equal 3, runner.dilution_rows[0].length
      assert_equal 3, runner.dilution_rows[1].length

      assert_equal(
        ['ABC123:A1', 'ABC123:B1', 'ABC123:A2'],
        runner.dilution_rows[0].collect { |x| x.to_s }
      )
      assert_equal(
        ["ABC123:B2", "123ABC:A1", "6 X006:S:1"],
        runner.dilution_rows[1].collect { |x| x.to_s }
      )
    end

  end

  context 'run_never_split' do

=begin

Sources:

SomeFullRows

  A1  A2  A3  A4  A5  A6  A7  A8  A9  A10 A11 A12
  A13 A14 A15 A16 A17 A18 A19 A20 A21 A22 A23 A24
  A25 A26 A27 A28 A29 A30 A31 A32 A33 A34 A35 A36
  A37 A38 .   .   .   .   .   .   .   .   .   .
  ..

SomeFullColumns

  B1  B2  B3  .   .   .   .   .   .   .   .   .
  B4  B5  B6  .   .   .   .   .   .   .   .   .
  B7  B8  B9  .   .   .   .   .   .   .   .   .
  B10 B11 B12 .   .   .   .   .   .   .   .   .
  B13 B14 .   .   .   .   .   .   .   .   .   .
  B15 B16 .   .   .   .   .   .   .   .   .   .
  B17 B18 .   .   .   .   .   .   .   .   .   .
  B19 B20 .   .   .   .   .   .   .   .   .   .

Individual Wells:

  C1 (ABC123:A1)
  D1 (123ABC:A1)

  result (length 12: by row)

  A1  A2  A3  A4  A5  A6  A7  A8  A9  A10 A11 A12    *
  A13 A14 A15 A16 A17 A18 A19 A20 A21 A22 A23 A24
  A25 A26 A27 A28 A29 A30 A31 A32 A33 A34 A35 A36
  A37 A38 C1  D1  .   .   .   .   .   .   .   .      *
  B1  B2  B3  .   .   .   .   .   .   .   .   .      *
  B4  B5  B6  .   .   .   .   .   .   .   .   .
  B7  B8  B9  .   .   .   .   .   .   .   .   .
  B10 B11 B12 .   .   .   .   .   .   .   .   .      *
  B13 B14 .   .   .   .   .   .   .   .   .   .      *
  B15 B16 .   .   .   .   .   .   .   .   .   .
  B17 B18 .   .   .   .   .   .   .   .   .   .
  B19 B12 .   .   .   .   .   .   .   .   .   .

=end

    it 'should sort, never splitting, reading by row' do
      runner = Dilution::Runner.new(
        :plate_codes => ['SomeFullRows', 'SomeFullColumns'],
        :well_codes => ['A0001', 'B0001'],
        :sample_ids => [],
        :length => 12,
        :source_by_row => true
      )
      runner.run_never_split

      assert_equal 12, runner.dilution_rows.length

      assert_equal 12, runner.dilution_rows[0].length

      assert_equal 4, runner.dilution_rows[3].length
      assert_equal 3, runner.dilution_rows[4].length

      assert_equal 3, runner.dilution_rows[7].length
      assert_equal 2, runner.dilution_rows[8].length


      assert_equal(
        %w{SomeFullRows:A1 SomeFullRows:A2 SomeFullRows:A3 SomeFullRows:A4
        SomeFullRows:A5 SomeFullRows:A6 SomeFullRows:A7 SomeFullRows:A8
        SomeFullRows:A9 SomeFullRows:A10 SomeFullRows:A11 SomeFullRows:A12},
        runner.dilution_rows[0].collect { |x| x.to_s }
      )


      assert_equal %w{SomeFullRows:D1 SomeFullRows:D2 ABC123:A1 123ABC:A1},
                   runner.dilution_rows[3].collect { |x| x.to_s }


      assert_equal %w{SomeFullColumns:A1 SomeFullColumns:A2 SomeFullColumns:A3},
                   runner.dilution_rows[4].collect { |x| x.to_s }

    end

    it 'should sort, never splitting, reading by column' do
      runner = Dilution::Runner.new(
        :plate_codes => ['SomeFullRows', 'SomeFullColumns'],
        :well_codes => ['A0001', 'B0001'],
        :sample_ids => [],
        :length => 8,
        :source_by_row => false
      )
      runner.run_never_split

      assert_equal 15, runner.dilution_rows.length

      assert_equal 6, runner.dilution_rows[0].length
      assert_equal 4, runner.dilution_rows[1].length
      assert_equal 3, runner.dilution_rows[2].length

      assert_equal 8, runner.dilution_rows[12].length
      assert_equal 4, runner.dilution_rows[14].length

      assert_equal(
        %w{SomeFullRows:A1 SomeFullRows:B1 SomeFullRows:C1 SomeFullRows:D1 ABC123:A1 123ABC:A1},
        runner.dilution_rows[0].collect { |x| x.to_s }
      )
      assert_equal(%w{SomeFullRows:A2 SomeFullRows:B2 SomeFullRows:C2 SomeFullRows:D2},
                   runner.dilution_rows[1].collect { |x| x.to_s })

      assert_equal(%w{SomeFullRows:A3 SomeFullRows:B3 SomeFullRows:C3},
                   runner.dilution_rows[2].collect { |x| x.to_s })

      assert_equal(
        %w{SomeFullColumns:A1 SomeFullColumns:B1 SomeFullColumns:C1 SomeFullColumns:D1
           SomeFullColumns:E1 SomeFullColumns:F1 SomeFullColumns:G1 SomeFullColumns:H1},
        runner.dilution_rows[12].collect { |x| x.to_s }
      )

      assert_equal(
        %w{SomeFullColumns:A3 SomeFullColumns:B3 SomeFullColumns:C3 SomeFullColumns:D3},
        runner.dilution_rows[14].collect { |x| x.to_s }
      )

    end


    it 'should treat samples like another well' do

=begin

SomeFullColumns

  B1  B2  B3  .   .   .   .   .   .   .   .   .
  B4  B5  B6  .   .   .   .   .   .   .   .   .
  B7  B8  B9  .   .   .   .   .   .   .   .   .
  B10 B11 B12 .   .   .   .   .   .   .   .   .
  B13 B14 .   .   .   .   .   .   .   .   .   .
  B15 B16 .   .   .   .   .   .   .   .   .   .
  B17 B18 .   .   .   .   .   .   .   .   .   .
  B19 B20 .   .   .   .   .   .   .   .   .   .

Individual Wells:

  C1 (ABC123:A1)
  D1 (123ABC:A1)

Individual Samples

  S1 (1 BI00000001:BS:1)
  S6 (6 DB00000006:BS:1)

Result

  B1  B2  B3  C1  D1  S1  S6  .   .   .   .   .
  B4  B5  B6  .   .   .   .   .   .   .   .   .
  B7  B8  B9  .   .   .   .   .   .   .   .   .
  B10 B11 B12 .   .   .   .   .   .   .   .   .
  B13 B14 .   .   .   .   .   .   .   .   .   .
  B15 B16 .   .   .   .   .   .   .   .   .   .
  B17 B18 .   .   .   .   .   .   .   .   .   .
  B19 B20 .   .   .   .   .   .   .   .   .   .

=end

      runner = Dilution::Runner.new(
        :plate_codes => ['SomeFullColumns'],
        :well_codes => ['A0001', 'B0001'],
        :sample_ids => [1, 6],
        :length => 12,
        :source_by_row => true
      )
      runner.run_never_split

      assert_equal 8, runner.dilution_rows.length

      assert_equal 7, runner.dilution_rows[0].length
      assert_equal 3, runner.dilution_rows[1].length
      assert_equal 3, runner.dilution_rows[2].length
      assert_equal 3, runner.dilution_rows[3].length
      assert_equal 2, runner.dilution_rows[4].length
      assert_equal 2, runner.dilution_rows[5].length
      assert_equal 2, runner.dilution_rows[6].length
      assert_equal 2, runner.dilution_rows[7].length

      assert_equal(
        ["SomeFullColumns:A1",
         "SomeFullColumns:A2",
         "SomeFullColumns:A3",
         "ABC123:A1",
         "123ABC:A1",
         "1 X001:S:1",
         "6 X006:S:1"],
        runner.dilution_rows[0].collect { |x| x.to_s }
      )

    end

  end


  context 'run_reorder_within_group' do

=begin

Sources:

SomeFullRows

  A1  A2  A3  A4  A5  A6  A7  A8  A9  A10 A11 A12
  A13 A14 A15 A16 A17 A18 A19 A20 A21 A22 A23 A24
  A25 A26 A27 A28 A29 A30 A31 A32 A33 A34 A35 A36
  A37 A38 .   .   .   .   .   .   .   .   .   .
  ...

SomeFullColumns

  B1  B2  B3  .   .   .   .   .   .   .   .   .
  B4  B5  B6  .   .   .   .   .   .   .   .   .
  B7  B8  B9  .   .   .   .   .   .   .   .   .
  B10 B11 B12 .   .   .   .   .   .   .   .   .
  B13 B14 .   .   .   .   .   .   .   .   .   .
  B15 B16 .   .   .   .   .   .   .   .   .   .
  B17 B18 .   .   .   .   .   .   .   .   .   .
  B19 B20 .   .   .   .   .   .   .   .   .   .



Individual Wells:

  C1 (ABC123:A1)
  D1 (123ABC:A1)

Result (length: 12, by row)

  A1  A2  A3  A4  A5  A6  A7  A8  A9  A10 A11 A12
  A13 A14 A15 A16 A17 A18 A19 A20 A21 A22 A23 A24
  A25 A26 A27 A28 A29 A30 A31 A32 A33 A34 A35 A36
  A37 A38 B1  B2  B3  B4  B5  B6  B7  B8  B9  C1  *
  B10 B11 B12 B13 B14 B15 B16 B17 B18 B19 B20 D1  *

=end

    it 'it should sort, reordering, by row, without breaking rows' do
      runner = Dilution::Runner.new(
        :plate_codes => ['SomeFullRows', 'SomeFullColumns'],
        :well_codes => ['A0001', 'B0001'],
        :sample_ids => [],
        :length => 12,
        :source_by_row => true
      )
      runner.run_reorder_within_group

      assert_equal 5, runner.dilution_rows.length
      assert_equal 12, runner.dilution_rows[0].length
      assert_equal 12, runner.dilution_rows[1].length
      assert_equal 12, runner.dilution_rows[2].length
      assert_equal 12, runner.dilution_rows[3].length
      assert_equal 12, runner.dilution_rows[4].length

      assert_equal(
        %w{SomeFullRows:D1 SomeFullRows:D2
           SomeFullColumns:A1 SomeFullColumns:A2 SomeFullColumns:A3
           SomeFullColumns:B1 SomeFullColumns:B2 SomeFullColumns:B3
           SomeFullColumns:C1 SomeFullColumns:C2 SomeFullColumns:C3
           ABC123:A1
          },
        runner.dilution_rows[3].collect { |x| x.to_s }
      )

      assert_equal(
        %w{SomeFullColumns:D1 SomeFullColumns:D2 SomeFullColumns:D3
           SomeFullColumns:E1 SomeFullColumns:E2
           SomeFullColumns:F1 SomeFullColumns:F2
           SomeFullColumns:G1 SomeFullColumns:G2
           SomeFullColumns:H1 SomeFullColumns:H2
           123ABC:A1
          },
        runner.dilution_rows[4].collect { |x| x.to_s }
      )
    end

=begin
    Result (Length: 8, by column)

    A1  A13 A25 A37 A2  A14 A26 A38     *
    A3  A15 A28 A4  A16 A28 C1  D1      *
    A5  A17 A29 A6  A18 A30 .   .
    A7  A19 A31 A8  A20 A32 .   .
    A9  A21 A33 A10 A22 A34 .   .
    A11 A23 A35 A12 A24 A36 .   .
    B1  B4  B7  B10 B13 B15 B17 B19
    B2  B5  B8  B11 B14 B16 B18 B20
    B3  B6  B9  B12 .   .   .   .
=end

    it 'it should sort, reordering, by column, without breaking columns' do
      runner = Dilution::Runner.new(
        :plate_codes => ['SomeFullRows', 'SomeFullColumns'],
        :well_codes => ['A0001', 'B0001'],
        :sample_ids => [],
        :length => 8,
        :source_by_row => false
      )
      runner.run_reorder_within_group

      assert_equal 9, runner.dilution_rows.length
      assert_equal 8, runner.dilution_rows[0].length
      assert_equal 8, runner.dilution_rows[1].length
      assert_equal 6, runner.dilution_rows[2].length
      assert_equal 6, runner.dilution_rows[3].length
      assert_equal 6, runner.dilution_rows[4].length
      assert_equal 6, runner.dilution_rows[5].length
      assert_equal 8, runner.dilution_rows[6].length
      assert_equal 8, runner.dilution_rows[7].length
      assert_equal 4, runner.dilution_rows[8].length

      assert_equal(
        %w{SomeFullRows:A1 SomeFullRows:B1 SomeFullRows:C1 SomeFullRows:D1
           SomeFullRows:A2 SomeFullRows:B2 SomeFullRows:C2 SomeFullRows:D2
          },
        runner.dilution_rows[0].collect { |x| x.to_s }
      )

      assert_equal(
        %w{SomeFullRows:A3 SomeFullRows:B3 SomeFullRows:C3
           SomeFullRows:A4 SomeFullRows:B4 SomeFullRows:C4
           ABC123:A1       123ABC:A1
          },
        runner.dilution_rows[1].collect { |x| x.to_s }
      )


    end

=begin
Result (length 8: by column, full columns first)

  B1  B4  B7  B10 B13 B15 B17 B19    *
  B2  B5  B8  B11 B14 B16 B18 B20
  B3  B6  B9  B12 A1  A13 A25 A37    *
  A2  A14 A26 A38 A3  A15 A27 C1     *
  A4  A16 A28 A5  A17 A29 D1  .      *
  A6  A18 A30 A7  A19 A31 .   .
  A8  A20 A32 A9  A21 A33 .   .
  A10 A22 A34 A11 A23 A35 .   .
  A12 A24 A36 .   .   .   .   .
=end

    it 'it should sort, reordering, by column, without breaking columns (plates reversed)' do
      runner = Dilution::Runner.new(
        :plate_codes => ['SomeFullColumns', 'SomeFullRows'],
        :well_codes => ['A0001', 'B0001'],
        :sample_ids => [],
        :length => 8,
        :source_by_row => false
      )
      runner.run_reorder_within_group

      assert_equal 9, runner.dilution_rows.length
      assert_equal 8, runner.dilution_rows[0].length
      assert_equal 8, runner.dilution_rows[1].length
      assert_equal 8, runner.dilution_rows[2].length
      assert_equal 8, runner.dilution_rows[3].length
      assert_equal 7, runner.dilution_rows[4].length
      assert_equal 6, runner.dilution_rows[5].length
      assert_equal 6, runner.dilution_rows[6].length
      assert_equal 6, runner.dilution_rows[7].length
      assert_equal 3, runner.dilution_rows[8].length

      assert_equal(
        %w{SomeFullColumns:A1 SomeFullColumns:B1 SomeFullColumns:C1 SomeFullColumns:D1
           SomeFullColumns:E1 SomeFullColumns:F1 SomeFullColumns:G1 SomeFullColumns:H1
          },
        runner.dilution_rows[0].collect { |x| x.to_s }
      )

      assert_equal(
        %w{SomeFullColumns:A3 SomeFullColumns:B3 SomeFullColumns:C3 SomeFullColumns:D3
           SomeFullRows:A1    SomeFullRows:B1    SomeFullRows:C1    SomeFullRows:D1
          },
        runner.dilution_rows[2].collect { |x| x.to_s }
      )

      assert_equal(
        %w{SomeFullRows:A2    SomeFullRows:B2    SomeFullRows:C2    SomeFullRows:D2
           SomeFullRows:A3    SomeFullRows:B3    SomeFullRows:C3
           ABC123:A1
          },
        runner.dilution_rows[3].collect { |x| x.to_s }
      )

      assert_equal(
        %w{SomeFullRows:A4    SomeFullRows:B4    SomeFullRows:C4
           SomeFullRows:A5    SomeFullRows:B5    SomeFullRows:C5
           123ABC:A1
          },
        runner.dilution_rows[4].collect { |x| x.to_s }
      )


    end

    it 'should treat a sample as another well' do
=begin

  ABC123

    A1 A2 .  .  .  .  .  .  .  .  .  .
    A3 A4 .  .  .  .  .  .  .  .  .  .
    .  .  .  .  .  .  .  .  .  .  .  .
    .  .  .  .  .  .  .  .  .  .  .  .
    .  .  .  .  .  .  .  .  .  .  .  .
    .  .  .  .  .  .  .  .  .  .  .  .
    .  .  .  .  .  .  .  .  .  .  .  .
    .  .  .  .  .  .  .  .  .  .  .  .

  wells

Individual Wells:

  C1 (123ABC:A1)
  D1 (123ABC:A2)

Individual Samples

  S1 (1 BI00000001:BS:1)
  S6 (6 DB00000006:BS:1)

result

  A1 A2 A3 A4 C1 D1 S1 S6 .  .  .  .
  .  .  .  .  .  .  .  .  .  .  .  .
  ...

=end
      runner = Dilution::Runner.new(
        :plate_codes => ['ABC123'],
        :well_codes => ['B0001', 'B0002'],
        :sample_ids => [1, 6],
        :length => 8,
        :source_by_row => true
      )
      runner.run_reorder_within_group

      assert_equal 1, runner.dilution_rows.length
      assert_equal 8, runner.dilution_rows[0].length

      assert_equal(
        ["ABC123:A1",
         "ABC123:A2",
         "ABC123:B1",
         "ABC123:B2",
         "123ABC:A1",
         "123ABC:A2",
         "1 X001:S:1",
         "6 X006:S:1"],
        runner.dilution_rows[0].collect { |x| x.to_s }
      )


    end

  end


  context 'in a task' do

    it 'should initialise a runner for a task' do
      runner = Dilution::Runner.new(
        :plate_codes => ['SomeFullColumns', 'SomeFullRows'],
        :well_codes => ['A0001', 'B0001'],
        :sample_ids => [1, 4],
        :task_id => 9901,
        :length => 8,
        :source_by_row => false
      )

      assert_equal 9901, runner.task_id
    end

    it 'should create dilution slots for the task' do

      ResultTask.any_instance.stubs(
        :process => ProcessVersion.output.first
      )

      result_task = ResultTask.first


      runner = Dilution::Runner.new(
        :plate_codes => ['SomeFullColumns', 'SomeFullRows'],
        :well_codes => ['A0001', 'B0001'],
        :sample_ids => [1, 4],
        :task_id => result_task.id,
        :length => 8,
        :source_by_row => false
      )

      runner.run_reorder_within_group

      runner.create_dilution_slots

      result_task.dilution_slots.reload

      assert result_task.dilution_slots.any?
    end

  end

end