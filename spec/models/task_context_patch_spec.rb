require 'spec_helper'

describe Biorails::TaskContextPatch do

  before(:each) do
    User.background_session
    #Bi::DefaultParameterTypes.new.generate
    @outline = Outline.find_by_name('services')
    #@outline.fill_with_default_template
    assay_manager = Biorails::AssayManager.new(OutlineQueue.find_by_name('service_a'))
    process_version = assay_manager.create_output_process(assay_manager)
    @task = ResultTask.create!(
      name: 'aaaaaaa',
      description: 'aaa',
      process_version_id: process_version.id,
      outline_id: @outline.id
    )
    @task_context = @task.add_context
  end

  it 'should put the method on the task sheet' do
    parameter = @task_context.parameters.where(:name => 'method').first
    outline_method = OutlineMethod.first

    # it has the parameter, it's empty
    assert_nil @task_context.item(parameter).data_content
    # set the outline method, parameter is still empty
    @task_context.outline_method_id = outline_method.id
    assert_nil @task_context.item(parameter).data_content
    # run the method to set the method content
    @task_context.set_default_method_id
    # it's not empty
    assert_not_nil @task_context.item(parameter).data_content

    item = @task_context.item(parameter)
    assert_equal outline_method, item.data
  end

end