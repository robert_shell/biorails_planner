require 'spec_helper'

describe DilutionMapSlot do

  context 'slot_type' do
    it 'should provide options for slot types' do
      types = DilutionMapSlot.slot_types
      assert_equal 5, types.length
      assert_equal 2, types[0].length

      [DilutionMapSlot.human_attribute_name(:sample_slot_type),
       DilutionMapSlot.human_attribute_name(:standard_slot_type),
       DilutionMapSlot.human_attribute_name(:high_control_slot_type),
       DilutionMapSlot.human_attribute_name(:low_control_slot_type),
       DilutionMapSlot.human_attribute_name(:blank_slot_type)].each do |label|
        assert_includes types.collect{|x|x[0]}, label
      end
      ['samp','std','high','low','blank'].each do |label|
        assert_includes types.collect{|x|x[1]}, label
      end
    end


  end

  context 'displaying stuff' do
    it 'should display a translated version of the slot type' do
      dilution_map_slot = DilutionMapSlot.new
      dilution_map_slot.slot_type = 'samp'
      assert_equal DilutionMapSlot.human_attribute_name(:sample_slot_type), dilution_map_slot.display_slot_type

      dilution_map_slot.slot_type = 'std'
      assert_equal DilutionMapSlot.human_attribute_name(:standard_slot_type), dilution_map_slot.display_slot_type

      dilution_map_slot.slot_type = 'high'
      assert_equal DilutionMapSlot.human_attribute_name(:high_control_slot_type), dilution_map_slot.display_slot_type

      dilution_map_slot.slot_type = 'low'
      assert_equal DilutionMapSlot.human_attribute_name(:low_control_slot_type), dilution_map_slot.display_slot_type

      dilution_map_slot.slot_type = 'blank'
      assert_equal DilutionMapSlot.human_attribute_name(:blank_slot_type), dilution_map_slot.display_slot_type
    end

    it 'should display the position 1 indexed instead of 0 indexed' do
      dilution_map_slot = DilutionMapSlot.new
      dilution_map_slot.position = 5
      assert_equal 6, dilution_map_slot.display_position
    end

    it 'should provide a summary for hover text' do
      dilution_map_slot = DilutionMapSlot.new(
        :position => 1,
        :slot_type => 'std',
        :sample_id => CdbSample.find_by_sample_id(1).id
      )
      assert_includes dilution_map_slot.summary, DilutionMapSlot.human_attribute_name(:standard_slot_type)
      assert_includes dilution_map_slot.summary, "2"
      assert_includes dilution_map_slot.summary, CdbSample.find_by_sample_id(1).name.strip
    end

  end


end
