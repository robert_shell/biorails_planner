require 'spec_helper'

describe Dilution::Row do

  before(:each) do
    @row = Dilution::Row.create(['a', 'b', 'c'], 4)
  end

  it 'should initialise with an array (named wells) and a capacity attribute' do
    assert_equal 4, @row.capacity
    assert_equal @row, @row.wells
  end

  it 'should throw an error if it is initialised and its array is longer than capacity' do
    assert_raises(RuntimeError) do
      @row = Dilution::Row.create(['a', 'b', 'c'], 2)
    end
  end

  it 'should tell me if the array is full to capacity' do
    # 3 out of 4
    assert !@row.full?

    @row << 'd'
    # 4 out of 4
    assert @row.full?

    @row << 'killer'
    assert_raises(RuntimeError) do
      @row.full?
    end
  end

  it 'it should tell me how much space is left in the @row' do
    # 3 out of 4
    assert_equal 1, @row.empty_wells

    @row << 'd'
    # 4 out of 4
    assert_equal 0, @row.empty_wells
  end

  it 'should tell me if X @rows are available' do
    # 3 out of 4
    assert @row.has_empty_wells?(1)
    assert !@row.has_empty_wells?(2)

    @row << 'd'
    # 4 out of 4
    assert !@row.has_empty_wells?(1)

    @row << 'killer'
    # 5 out of 4
    assert_raises(RuntimeError) do
      @row.has_empty_wells?(1)
    end
  end

  it 'should summarise its contents with the to_s method' do
    user = User.first
    @row = Dilution::Row.create([user, 777], 4)

    assert_includes @row.summary, '777'
    assert_includes @row.summary, user.to_s
  end

end
