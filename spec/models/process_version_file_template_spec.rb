require 'spec_helper'

describe ProcessVersionFileTemplate do

  before(:each) do
    @process_version = FactoryGirl.create(:process_version)
    @output_file_template = OutputFileTemplate.find(1)
  end

  it 'should have a tsv file type by default' do
    process_version_file_template = ProcessVersionFileTemplate.new
    assert_equal 'tsv', process_version_file_template.file_type
  end

  it 'should fill name from file_name and file_type' do
    process_version_file_template = ProcessVersionFileTemplate.new(
      :file_name => 'this-file',
      :file_type => 'tsv'
    )
    assert_nil process_version_file_template.name
    process_version_file_template.fill_name
    assert_equal 'this-file.tsv', process_version_file_template.name
  end

  it 'should provide three options for file_type' do
    options = ProcessVersionFileTemplate.file_type_options
    assert_equal I18n::t('output_file_types.csv'),  options[0][0]
    assert_equal I18n::t('output_file_types.xlsx'), options[1][0]
    assert_equal I18n::t('output_file_types.tsv'),  options[2][0]

    assert_equal 'csv',  options[0][1]
    assert_equal 'xlsx', options[1][1]
    assert_equal 'tsv',  options[2][1]
  end

  it 'should create a preview text' do
    process_version_file_template = ProcessVersionFileTemplate.new(
      :file_name => 'this-file',
      :file_type => 'tsv',
      :process_version_id => @process_version.id,
      :output_file_template_id => @output_file_template.id
    )
    assert process_version_file_template.preview_text
    # rudimentary guess at tab-separated content
    assert_includes process_version_file_template.preview_text, "\t"
  end

end
