require 'spec_helper'



describe TaskFinalizeJob do

  context 'finalise_barcodes' do

    before(:each) do
      puts "start finalise_barcodes setup #{t1 = Time.now}"
      CoastsOrderPlate.create(
        :id_order_header_ref => Order.first.coasts_order_id,
        :plate_code => 'ABC123'
      )

      User.background_session
      ProcessVersion.any_instance.stubs(:released? => false,:dilution_map => DilutionMap.first)

      @outline = Outline.find_by_name('services')

      @outline_queue = OutlineQueue.find_by_name('service_a')
      @process_version = Biorails::AssayManager.new(@outline_queue).default_output_process


      @task = ResultTask.create!(:name => 'a', :outline_id => @outline.id,
                                 :process_version_id => @process_version.id)

      puts "end finalise_barcodes setup #{t2 = Time.now} (#{t2 - t1})"
    end

    # This method has been put here to try and improve performance
    #
    # The spec is not fully written yet, currently it's just for ease
    # of re-running this block of code.
    it 'should quickly fill in the task sheet' do
      CoastsOrderPlate.create(
        :id_order_header_ref => Order.first.coasts_order_id,
        :plate_code => 'ABC123'
      )


      @task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        "Full384\nFull", #a 96 and a 384 plate
        @task.id
      )

      benchmark = Benchmark.measure do
        @job = Biorails::TaskFinalizeJob.async_finalise_barcodes(
          {
            :task_barcodes => @task_barcodes,
            :user_id => 2,
            :success_url => 'aaaaaa'
          }
        )
        Job.find_by_queue_name(@job).error_messages.should be_empty
      end

      # 482 wells translates to 482 rows,
      # tests show this will usually be around 40 seconds
      time = benchmark.to_a[1]
      puts "482 rows populated in #{time} seconds"
      time.should be < 60

      sheet = @task.sheet
      sheet.grid.each do |label, row|
        row['sample'].data_content.should_not be_nil, "sample is missing in #{label}"
        row['source_plate'].data_content.should_not be_nil, "source_plate is missing in #{label}"
        row['source_row'].data_content.should_not be_nil, "source_row is missing in #{label}"
        row['source_column'].data_content.should_not be_nil, "source_column is missing in #{label}"
      end

    end


    it 'should mark barcodes as complete' do
      @task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        "ABC123", # small plate
        @task.id
      )

      job = Biorails::TaskFinalizeJob.async_finalise_barcodes(
        {
          :task_barcodes => @task_barcodes,
          :user_id => 2,
          :success_url => 'aaaaaa'
        }
      )
      Job.find_by_queue_name(job).error_messages.should be_empty
      @task_barcodes.each do |task_barcode|
        task_barcode.reload
        task_barcode.complete.should be_true
      end
    end


    it 'should set any queue items to completed' do
      @task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        "ABC123", # small plate
        @task.id
      )

      StateFlow.any_instance.stubs(:allow? => true)

      @outline_queue.items.each do |queue_item|
        queue_item.set_state('ordered')
        @task.queue_items << queue_item
      end
      @task.save!

      Biorails::TaskFinalizeJob.async_finalise_barcodes(
        {
          :task_barcodes => @task_barcodes,
          :user_id => 2,
          :success_url => 'aaaaaa'
        }
      )
      item = @task_barcodes[0].queue_items[0]
      item.state.reload
      item.state_name.should eq('delivered')

    end

    it 'should set the order to completed' do
      @task_barcodes, unfound = TaskBarcode.scan_list(
        'Order',
        "#{Order.first.coasts_order_id}",
        @task.id
      )

      coasts_order = CoastsOrder.last
      coasts_order.status_id.should_not eq(CoastsStatus::DEFAULT_COMPLETED)
      coasts_order.status.should_not eq("Arrived")
      order = Order.last
      order.coasts_completed.should_not eq(true)

      Biorails::TaskFinalizeJob.async_finalise_barcodes(
        {
          :task_barcodes => @task_barcodes,
          :user_id => 2,
          :success_url => 'aaaaaa'
        }
      )

      coasts_order = CoastsOrder.first
      coasts_order.status_id.should eq(CoastsStatus::DEFAULT_COMPLETED)
      coasts_order.status.should eq("Arrived")
      order = Order.first
      order.coasts_completed.should eq(true)

    end

  end

  context 'run_consolidation' do

    # This is mostly tested as part of the task_patch_spec

    before(:each) do
      puts "start run_consolidation setup #{t1 = Time.now}"
      CoastsOrderPlate.create(
        :id_order_header_ref => Order.first.coasts_order_id,
        :plate_code => 'ABC123'
      )

      User.background_session
      ProcessVersion.any_instance.stubs(:released? => false, :dilution_map => DilutionMap.first)
      Task.any_instance.stubs(:dilution_map => DilutionMap.first)
      @outline = Outline.find_by_name('services')

      @outline_queue = OutlineQueue.find_by_name('service_a')
      @process_version = Biorails::AssayManager.new(@outline_queue).default_output_process

      @task = ResultTask.create!(:name => 'a', :outline_id => @outline.id,
                                 :process_version_id => @process_version.id)
      puts "end run_consolidation setup #{t2 = Time.now} (#{t2 - t1})"
    end


    it 'should create some dilution slots' do
      ProjectFolder.any_instance.stubs(:changeable? => true)
      assert @task.dilution_slots.none?
      @task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        "ABC123", # small plate
        @task.id
      )
      @task_barcodes[0].task = @task
      @task_barcodes[0].finalise
      job_id = TaskConsolidation.async_run(
        {
          :task => @task,
          :success_url => '/success/url',
          :fail_url => '/fail/url'
        }
      )
      assert @task.dilution_slots.any?, 'no slots'

      job = Job.find_by_queue_name(job_id)
      job.error_messages.should be_empty
    end

    it 'should fill in the test_plate and test_column properties' do
      ProjectFolder.any_instance.stubs(:changeable? => true)
      assert @task.dilution_slots.none?
      @task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        "ABC123", # small plate
        @task.id
      )
      @task_barcodes[0].task = @task
      @task_barcodes[0].finalise
      job_id = TaskConsolidation.async_run(
        {
          :task => @task,
          :success_url => '/success/url',
          :fail_url => '/fail/url'
        }
      )
      assert @task.dilution_slots.any?, 'no slots'

      sheet = @task.sheet
      sheet.grid.each do |label, row|
        row['test_plate'].data_content.should_not be_nil, "test_plate is missing in #{label}"
        row['test_column'].data_content.should_not be_nil, "test_column is missing in #{label}"
        row['slot_type'].data_content.should_not be_nil, "slot_type is missing in #{label}"
      end

    end

  end

  context 'rearrange_slots' do
    before(:each) do
      puts "start rearrange_slots setup #{t1 = Time.now}"

      CoastsOrderPlate.create(
        :id_order_header_ref => Order.first.coasts_order_id,
        :plate_code => 'ABC123'
      )

      User.background_session
      ProcessVersion.any_instance.stubs(:released? => false,:dilution_map => DilutionMap.first)
      Task.any_instance.stubs(:dilution_map => DilutionMap.first)
      @outline = Outline.find_by_name('services')

      @outline_queue = OutlineQueue.find_by_name('service_a')
      @process_version = Biorails::AssayManager.new(@outline_queue).default_output_process

      @task = ResultTask.create!(:name => 'a', :outline_id => @outline.id,
                                 :process_version_id => @process_version.id)

      ProjectFolder.any_instance.stubs(:changeable? => true)
      @task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        "ABC123",
        @task.id
      )
      @task_barcodes[0].task = @task
      @task_barcodes[0].finalise
      ProjectElement.any_instance.stubs(:changeable? => nil)
      @task.run_consolidation
      assert @task.dilution_slots.any?

      puts "end rearrange_slots setup #{t2 = Time.now} (#{t2 - t1})"

    end

    it 'should move slots around' do
      i = 0
      dilution_slot_positions = @task.dilution_slots.collect do |slot|
        i += 1
        [
          slot.id,
          0,
          i
        ]
      end
      @job_id = Biorails::TaskFinalizeJob.async_rearrange_slots(
        {
          :task => @task,
          :success_url => '/success/url',
          :slot_positions => dilution_slot_positions,
          :fail_url => '/fail/url'
        }
      )
      assert @task.dilution_slots.any?
    end

  end

  context 'fill_output_file' do

    # mostly tested in the task patch spec
    before(:each) do
      puts "start fill_output_file setup #{t1 = Time.now}"
      User.background_session
      ProcessVersion.any_instance.stubs(:released? => false)

      @outline = Outline.find_by_name('services')
      @outline_queue = OutlineQueue.find_by_name('service_a')
      @process_version = Biorails::AssayManager.new(@outline_queue).default_output_process
      @task = ResultTask.create!(:name => 'a', :outline_id => @outline.id,
                                 :process_version_id => @process_version.id)

      ProjectFolder.any_instance.stubs(:changeable? => true)
      @task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        "ABC123",
        @task.id
      )
      @task_barcodes[0].task = @task
      @task_barcodes[0].finalise
      ProjectElement.any_instance.stubs(:changeable? => nil)
      ProcessVersion.any_instance.stubs(:dilution_map => DilutionMap.first)
      @task.run_consolidation
      assert @task.dilution_slots.any?

      puts "end fill_output_file setup #{t2 = Time.now} (#{t2 - t1})"
    end

    it 'should generate a file from the task sheet' do
      @process_version_file_template = ProcessVersionFileTemplate.create!(
        :process_version_id => @task.process_version_id,
        :output_file_template_id => OutputFileTemplate.first.id,
        :file_name => 'testing_file',
        :file_type => 'tsv'
      )
      @task.process_version_file_template_id = @process_version_file_template.id
      @job_id = Biorails::TaskFinalizeJob.async_fill_output_file(
        {
          :task => @task,
          :user_id => User.current.id,
          :success_url => '/success/url',
          :fail_url => '/fail/url'
        }
      )
      assert @task.project_element.children.where(:type => 'ProjectAsset').any?, 'no file was created'
    end

  end

end