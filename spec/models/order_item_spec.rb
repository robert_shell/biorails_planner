require File.dirname(__FILE__)+'/../spec_helper'


describe OrderItem do

  before(:each) do
    @order = Order.where("coasts_order_id is not null").first
  end

  it  'should test sample_id' do
    # Reference compounds have their own sample id
    @order_item = @order.order_items.where(:is_reference_compound => true).first
    assert @order_item.sample_id
    assert @order_item.sample_id.is_a?(Integer)
    # Non-reference compounds get their sample id from a lookup based on
    # its queue item's data_id,
    @order_item = @order.order_items.where(:is_reference_compound => false).first
    assert @order_item.sample_id
    assert @order_item.sample_id.is_a?(Integer)
  end

  it  'should test sample' do
    # Reference compounds have their own sample id
    @order_item = @order.order_items.where(:is_reference_compound => true).first
    assert @order_item.sample.is_a?(CdbSample)
    # Non-reference compounds get their sample id from a lookup based on
    # its queue item's data_id,
    @order_item = @order.order_items.where(:is_reference_compound => false).first
    assert @order_item.sample.is_a?(CdbSample)
  end

end