require File.dirname(__FILE__)+'/../spec_helper'


describe OrderProfile do

  let(:powder_profile) { CoastsOrderType.where(:id_item_type_lov_ref => CoastsOrderType::POWDER_TYPE).first.order_profiles.first }
  let(:standard_profile) { CoastsOrderType.where(:id_item_type_lov_ref => CoastsOrderType::STANDARD_LIQUID_TYPE).first.order_profiles.first }
  let(:custom_profile) { CoastsOrderType.where(:id_item_type_lov_ref => CoastsOrderType::CUSTOM_LIQUID_TYPE).first.order_profiles.first }
  let(:dissolution_profile) { CoastsOrderType.where(:id_item_type_lov_ref => CoastsOrderType::DISSOLUTION_LIQUID_TYPE).first.order_profiles.first }
  let(:subways_profile) { CoastsOrderType.where(:id_item_type_lov_ref => CoastsOrderType::SUBWAYS_TYPE).first.order_profiles.first }

  it 'should test initialize' do
    @order_profile = OrderProfile.new
    assert @order_profile.purpose_type
    assert @order_profile.coasts_order_type
    assert_equal false, @order_profile.rows_first
    assert_equal true, @order_profile.use_internal_recipient
    @order_profile = OrderProfile.new(:coasts_order_type_id => 5)
    assert @order_profile.coasts_plate_layout_id
    assert @order_profile.dispensary_opu_id
  end

  it 'should test validate_vienna_maximum_volume' do
    @order_profile = subways_profile
    # from coasts order type, maximum is 100 ul
    @order_profile.volume = 101
    @order_profile.valid?
    assert @order_profile.errors[:volume].any?
    @order_profile.volume = 100
    @order_profile.valid?
    assert @order_profile.errors[:volume].none?
    @order_profile.volume = 90
    @order_profile.valid?
    assert @order_profile.errors[:volume].none?
  end

  it 'should test validate_vienna_minimum_volume' do
    @order_profile = subways_profile
    # from coasts order type, minimum is 10 ul
    @order_profile.volume = 9
    @order_profile.valid?
    assert @order_profile.errors[:volume].any?
    @order_profile.volume = 10
    @order_profile.valid?
    assert @order_profile.errors[:volume].none?
    @order_profile.volume = 20
    @order_profile.valid?
    assert @order_profile.errors[:volume].none?
  end

  it 'should test validate_unique_dissolution_profile_per_queue' do
    initial_order_profile = dissolution_profile
    initial_order_profile.valid?
    assert initial_order_profile.errors[:base].none?
    new_order_profile = OrderProfile.new(
      :coasts_order_type_id => 4,
      :outline_queue_id => dissolution_profile.outline_queue_id
    )
    new_order_profile.valid?
    assert new_order_profile.errors[:base].include?(
             I18n.t('activerecord.errors.models.order_profile.dissolution_exists_for_queue')
           )
  end

  it 'should test volume_for_standard' do
    order_profile = standard_profile
    order_profile.volume = nil
    order_profile.set_volume_for_standard
    assert_equal CoastsOrderType.find(2).volume, order_profile.volume
  end


  it 'should test add_reference_compound' do
    @order_profile = powder_profile
    assert_equal 1, @order_profile.reference_compounds.length
    @order_profile.sample_id_to_add = CdbSample.first.id
    @order_profile.add_reference_compound
    assert_equal 2, @order_profile.reference_compounds.length
    assert @order_profile.reference_compounds.collect { |x| x.sample.sample_id }.include?(CdbSample.first.sample_id)
    @order_profile.sample_id_to_add = CdbSample.last.id
    @order_profile.add_reference_compound
    assert_equal 3, @order_profile.reference_compounds.length
    assert @order_profile.reference_compounds.collect { |x| x.sample.sample_id }.include?(CdbSample.last.sample_id)
    # Duplicate reference compound is not added
    @order_profile.sample_id_to_add = CdbSample.last.id
    @order_profile.add_reference_compound
    assert_equal 3, @order_profile.reference_compounds.length
  end

  it 'should test remove_reference_compound' do
    @order_profile = powder_profile
    @order_profile.sample_id_to_add = CdbSample.first.id
    @order_profile.add_reference_compound
    assert_equal 2, @order_profile.reference_compounds.length
    @order_profile.sample_id_to_add = CdbSample.last.id
    @order_profile.add_reference_compound
    assert_equal 3, @order_profile.reference_compounds.length
    assert @order_profile.reference_compounds.collect { |x| x.sample.sample_id }.include?(CdbSample.first.sample_id)
    # remove
    @order_profile.remove_reference_compound(CdbSample.first.sample_id)
    assert_equal 2, @order_profile.reference_compounds.length
    assert !@order_profile.reference_compounds.collect { |x| x.sample.sample_id }.include?(CdbSample.first.sample_id)
  end

  it 'should test set_preferred' do
    @outline_queue = OutlineQueue.first
    @op_1 = powder_profile
    @op_2 = standard_profile

    @op_1.update_attribute(:preferred, true)
    @outline_queue.reload and @op_1.reload and @op_2.reload
    assert @op_1.preferred
    assert !@op_2.preferred
    assert_equal @op_1.id, @outline_queue.preferred_order_profile_id

    # remove attr accessors (spilling over won't happen from controller)
    @op_1.preferred = nil
    @op_2.preferred = nil
    @op_2.update_attribute(:preferred, true)
    @outline_queue.reload and @op_1.reload and @op_2.reload
    assert !@op_1.preferred
    assert @op_2.preferred
    assert_equal @op_2.id, @outline_queue.preferred_order_profile_id

    # remove attr accessors (spilling over won't happen from controller)
    @op_1.preferred = nil
    @op_2.preferred = nil
    @op_1.update_attribute(:preferred, true)
    @outline_queue.reload and @op_1.reload and @op_2.reload
    assert @op_1.preferred
    assert !@op_2.preferred
    assert_equal @op_1.id, @outline_queue.preferred_order_profile_id
  end

  it 'should test count' do
    @order_profile = powder_profile
    @order_profile.coasts_plate_format_id = 1
    @order_profile.coasts_plate_layout_id = 4
    assert_equal 88, @order_profile.count
    @order_profile.coasts_plate_format_id = 2
    assert_equal 352, @order_profile.count
  end

  it 'should test set_default_volume' do
    order_profile = subways_profile
    order_profile.volume = nil
    order_profile.set_default_volume
    assert_equal CoastsOrderType.find(5).default_order_volume, order_profile.volume
  end

  it 'should test purpose_type_id' do
    order_profile = powder_profile
    order_profile.purpose_type = OrderProfile::METHOD_LOOKUP
    assert_equal OrderProfile::METHOD_PURPOSE_TYPE, order_profile.purpose_type_id
    order_profile.purpose_type = OrderProfile::PROJECT_LOOKUP
    assert_equal OrderProfile::PROJECT_PURPOSE_TYPE, order_profile.purpose_type_id
    order_profile.purpose_type = OrderProfile::USER_PURPOSE_LOOKUP
    assert_equal OrderProfile::USER_PURPOSE_TYPE, order_profile.purpose_type_id
  end

  it 'should test dispensary_options' do
    opts = OrderProfile.dispensary_options
    assert opts.is_a?(Array)
    names = opts.collect { |x| x[0] }
    assert names.include?('BIUS')
    assert names.include?('BIDE')
    ids = opts.collect { |x| x[1] }
    assert ids.include?(9637)
    assert ids.include?(3407)
  end

  it 'should test display_dispensary' do
    order_profile = powder_profile
    order_profile.dispensary_opu_id = 9637
    assert_equal 'BIUS', order_profile.display_dispensary
    order_profile.dispensary_opu_id = 3407
    assert_equal 'BIDE', order_profile.display_dispensary
  end


  it 'should test amount_display' do
    @order_profile = powder_profile
    assert_equal "14.0 mg in 3 vials", @order_profile.amount_display
    @order_profile = standard_profile
    assert_equal "2 ul, 5.0 mg/ml", @order_profile.amount_display
    @order_profile = custom_profile
    assert_equal "1 ul, 5.0 mmol/l", @order_profile.amount_display
  end

  it 'should test liquid_question_mark' do
    assert !powder_profile.liquid?
    assert standard_profile.liquid?
    assert custom_profile.liquid?
    assert dissolution_profile.liquid?
    assert subways_profile.liquid?
  end

  it 'should test no_of_vials' do
    order_profile = powder_profile
    assert_equal 3, order_profile.no_of_vials
  end

  it 'should test summing_order_profile_weights' do
    order_profile = powder_profile
    assert_equal 14, order_profile.order_profile_weights.mg_required
  end

  # ##    ##  ##      ##  ##
  #  ##  ##   ###    ###  ##
  #   ####    ####  ####  ##
  #    ##     ## #### ##  ##
  #   ####    ##  ##  ##  ##
  #  ##  ##   ##      ##  #####
  # ##    ##  ##      ##  #####
  #
  # ###########################
  #

  it 'should test solid_order_xml' do
    @order_profile = powder_profile

    @order = @order_profile.orders.first

    xml = @order_profile.order_xml(@order)
    assert xml.include?("<ROWSET>")
    assert xml.include?("<ORDER>")
    assert xml.include?("<ID>")
    assert xml.include?("<ORDERER>")
    assert xml.include?("<OPU>")
    assert xml.include?("<RECIPIENT>")
    assert xml.include?("<PURPOSE_TYPE")
    assert xml.include?("<PURPOSE_ID>")
    assert xml.include?("<COMMENTS>")

    assert xml.include?("<ORDER_SOLID>")
    assert xml.include?("<ITEM_ID>")
    assert xml.include?("<SAMPLE_ID>")
    assert xml.include?("<AMOUNT>")
    assert xml.include?("<AMOUNT_DIM>")
    assert xml.include?("<VIAL_TYPE>")
  end


  it 'should test standard_order_xml' do
    @order_profile = standard_profile

    @order = @order_profile.orders.first

    xml = @order_profile.order_xml(@order)

    assert xml.include?("<ROWSET>")
    assert xml.include?("<ORDER>")
    assert xml.include?("<ID>")
    assert xml.include?("<ORDERER>")
    assert xml.include?("<OPU>")
    assert xml.include?("<RECIPIENT>")
    assert xml.include?("<PURPOSE_TYPE")
    assert xml.include?("<PURPOSE_ID>")
    assert xml.include?("<COMMENTS>")

    assert xml.include?("<ORDER_STD_SOL>")
    assert xml.include?("<ORDER_STD_SOL_ITEM>")
    assert xml.include?("<ITEM_ID>")
    assert xml.include?("<SAMPLE_ID>")
    assert xml.include?("<AMOUNT_STD_SOL>")
    assert xml.include?("<VOLUME>")
    assert xml.include?("<VOLUME_DIM>")
    assert xml.include?("<FORMAL_CONCENTRATION>")
    assert xml.include?("<MT_PLATE_FORMAT>")
    assert xml.include?("<MT_PLATE_TYPE>")
    assert xml.include?("<MT_PLATE_LAYOUT>")
    assert xml.include?("<COUNT>")
    assert xml.include?("<QUADRANT>")
    assert xml.include?("<Q1>")
    assert xml.include?("<Q2>")
    assert xml.include?("<Q3>")
    assert xml.include?("<Q4>")
    assert xml.include?("<LAYOUT>")
    assert xml.include?("<ROWS_FIRST>")
    assert xml.include?("<ORDER_TYPE>")

  end

  it 'should test custom_order_xml' do
    @order_profile = custom_profile

    @order = @order_profile.orders.first

    xml = @order_profile.order_xml(@order)

    assert xml.include?("<ROWSET>")
    assert xml.include?("<ORDER>")
    assert xml.include?("<ID>")
    assert xml.include?("<ORDERER>")
    assert xml.include?("<OPU>")
    assert xml.include?("<RECIPIENT>")
    assert xml.include?("<PURPOSE_TYPE")
    assert xml.include?("<PURPOSE_ID>")
    assert xml.include?("<COMMENTS>")

    assert xml.include?("<ORDER_CUST_SOL>")
    assert xml.include?("<ITEM_ID>")
    assert xml.include?("<SAMPLE_ID>")
    assert xml.include?("<AMOUNT_CUST_SOL>")
    assert xml.include?("<VOLUME>")
    assert xml.include?("<VOLUME_DIM>")
    assert xml.include?("<CONCENTRATION>")
    assert xml.include?("<CONCENTRATION_DIM>")
    assert xml.include?("<MT_FORMAT>")
    assert xml.include?("<MT_TYPE>")
    assert xml.include?("<ORDER_TYPE>")
  end

  def build_order_for(order_profile)
    @outline_queue = OutlineQueue.first

    @cart = Cart.create
    @query = QueryEngine.query('queue_items.index', :outline_queue => @outline_queue)
    @cart.query = @query
    @cart.model = @query.model
    @cart.save
    @cart.set_items(
      {
        '0' => @outline_queue.items[0].id,
        '1' => @outline_queue.items[1].id
      }
    )
    @order = Order.from_queue_items(@cart, {
      :order_profile_id => order_profile.id,
      :outline_queue_id => @outline_queue.id,
      :reference_compound_ids_to_create => {
        @order_profile.reference_compounds.first.id.to_s => 'true'
      }
    })
    @order.save
    @order.send_to_coasts
    @order
  end

end
