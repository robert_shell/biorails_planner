require 'spec_helper'

describe DilutionMap do

  context 'dilution_map_slots' do

    before(:each) do
      # All dilution maps are identical
      @dilution_map = DilutionMap.first
    end

    it 'should find all sample slots' do
      dilution_map_slots = @dilution_map.dilution_map_slots.sample

      # This is important, if this is an ActiveRecord::Relation then
      # pre-saving validations will be incorrect.
      assert_kind_of Array, dilution_map_slots
      dilution_map_slots.each do |dilution_map_slot|
        assert_equal 'samp', dilution_map_slot.slot_type
      end
    end

    it 'should find all non-sample slots' do
      dilution_map_slots = @dilution_map.dilution_map_slots.non_sample

      # This is important, if this is an ActiveRecord::Relation then
      # pre-saving validations will be incorrect.
      assert_kind_of Array, dilution_map_slots
      dilution_map_slots.each do |dilution_map_slot|
        assert_not_equal 'samp', dilution_map_slot.slot_type
      end
    end

  end

  context 'get_defuaults' do

    it 'should set defaults on a new dilution map' do
      dilution_map = DilutionMap.new

      assert_equal 1, dilution_map.coasts_plate_format_id
      assert_equal false, dilution_map.source_by_row
      assert_equal 8, dilution_map.source_length
      assert_equal 8, dilution_map.length
    end

  end

  context 'display methods' do

    before(:each) { @dilution_map = DilutionMap.new }

    it 'should show a translated version of row or column' do
      @dilution_map.source_by_row = true
      assert_equal DilutionMap.human_attribute_name(:row), @dilution_map.display_source_by_row
      @dilution_map.source_by_row = false
      assert_equal DilutionMap.human_attribute_name(:column), @dilution_map.display_source_by_row
    end

    it 'should show a translated version of the length' do
      @dilution_map.length = 8
      assert_equal DilutionMap.human_attribute_name(:ninety_six_short_side), @dilution_map.display_length
      @dilution_map.length = 12
      assert_equal DilutionMap.human_attribute_name(:ninety_six_long_side), @dilution_map.display_length
      @dilution_map.length = 16
      assert_equal DilutionMap.human_attribute_name(:three_eight_four_short_side), @dilution_map.display_length
      @dilution_map.length = 24
      assert_equal DilutionMap.human_attribute_name(:three_eight_four_long_side), @dilution_map.display_length
    end

  end

  context 'options for forms' do

    it 'should provide two options for coasts_plate_format_id' do
      options = DilutionMap.coasts_plate_format_options

      assert_equal 2, options.length
      assert_equal 2, options[0].length
      assert_equal 2, options[1].length

      assert_includes options.collect { |x| x[0] }, '96'
      assert_includes options.collect { |x| x[0] }, '384'
    end

    it 'should provide 2 options for source by row' do
      options = DilutionMap.source_by_row_options

      assert_equal 2, options.length
      assert_equal 2, options[0].length

      assert_includes options.collect { |x| x[0] }, DilutionMap.human_attribute_name(:row)
      assert_includes options.collect { |x| x[0] }, DilutionMap.human_attribute_name(:column)
    end

    it 'should provide four options for dilution map length' do
      options = DilutionMap.length_options

      assert_equal 4, options.length
      assert_equal 2, options[0].length

      [
        DilutionMap.human_attribute_name(:ninety_six_short_side),
        DilutionMap.human_attribute_name(:ninety_six_long_side),
        DilutionMap.human_attribute_name(:three_eight_four_short_side),
        DilutionMap.human_attribute_name(:three_eight_four_long_side)
      ].each do |option|
        assert_includes options.collect { |x| x[0] }, option
      end
    end

  end

  context 'get_source_length' do

    before(:each) { @dilution_map = DilutionMap.new }

    it 'should know a row of 96 is 12' do
      @dilution_map.attributes = {:coasts_plate_format_id => 1, :source_by_row => true}
      assert_equal 12, @dilution_map.get_source_length
    end

    it 'should know a column of 96 is 8' do
      @dilution_map.attributes = {:coasts_plate_format_id => 1, :source_by_row => false}
      assert_equal 8, @dilution_map.get_source_length
    end

    it 'should know a row of 384 is 24' do
      @dilution_map.attributes = {:coasts_plate_format_id => 2, :source_by_row => true}
      assert_equal 24, @dilution_map.get_source_length
    end

    it 'should know a column of 384 is 16' do
      @dilution_map.attributes = {:coasts_plate_format_id => 2, :source_by_row => false}
      assert_equal 16, @dilution_map.get_source_length
    end

  end

  context 'custom validations' do

    it 'should validate that there are not too many slots for the row' do
      dilution_map = DilutionMap.first
      assert dilution_map.valid?
      dilution_map.dilution_map_slots.create!(
        :slot_type => 'samp',
        :position => '12'
      )
      assert !dilution_map.valid?
      assert_equal I18n.t('activerecord.errors.dilution_map.too_many_slots'), dilution_map.errors.on(:base)
    end

    it 'should validate that there is at least one sample slot' do
      dilution_map = DilutionMap.first
      assert dilution_map.valid?
      dilution_map.dilution_map_slots.sample.each { |x| x.destroy }
      dilution_map.reload
      assert !dilution_map.valid?
      assert_equal I18n.t('activerecord.errors.dilution_map.has_no_samples'), dilution_map.errors.on(:base)
    end

  end

  context 'always_only_split' do

    it 'should be true if the source row is longer than the number of samples' do
      dilution_map = DilutionMap.first
      dilution_map.dilution_map_slots.sample.first.destroy
      dilution_map.dilution_map_slots.reload
      assert dilution_map.dilution_map_slots.sample.count < dilution_map.source_length
      assert dilution_map.only_always_split?
    end

    it 'should be false if the source row is not longer than the number of samples' do
      dilution_map = DilutionMap.first
      assert dilution_map.dilution_map_slots.sample.count >= dilution_map.source_length
      assert !dilution_map.only_always_split?
    end

  end

  context 'warnings' do

    it "should warn users if 'always split' is the only algorithm that'll work" do
      dilution_map = DilutionMap.first
      2.times{ dilution_map.dilution_map_slots.sample.first.destroy}
      dilution_map.dilution_map_slots.reload
      assert_includes(
        dilution_map.warnings,
        "There are fewer samples slots in your dilution map than the length of the source column (8). Currently, you will only be able to use the 'Always Split' consolidation strategy."
      )
    end

  end

  context 'build_new_wells' do


    it 'should create a block of sample wells' do
      dilution_map = DilutionMap.new
      dilution_map.block_start = 0
      dilution_map.block_end = 3
      dilution_map.block_slot_type = 'samp'
      dilution_map.build_new_wells

      assert_equal 4, dilution_map.dilution_map_slots.length
      assert_equal 4, dilution_map.dilution_map_slots.sample.length
      assert_equal 0, dilution_map.dilution_map_slots.non_sample.length
    end

    it 'should create blocks of samples and controls' do
      dilution_map = DilutionMap.new
      dilution_map.block_start = 4
      dilution_map.block_end = 4
      dilution_map.block_slot_type = 'high'
      dilution_map.block_sample_id = 1
      dilution_map.build_new_wells

      dilution_map.block_start = 5
      dilution_map.block_end = 5
      dilution_map.block_slot_type = 'low'
      dilution_map.block_sample_id = 2
      dilution_map.build_new_wells

      dilution_map.block_start = 6
      dilution_map.block_end = 7
      dilution_map.block_slot_type = 'std'
      dilution_map.block_sample_id = 3
      dilution_map.build_new_wells

      slots = dilution_map.dilution_map_slots
      slot_types = slots.collect{|x|x.slot_type}
      slots.each{|slot| assert slot.sample_id}
      %w{std high low}.each do |type|
        assert_includes slot_types, type
      end

      assert_equal 0, dilution_map.dilution_map_slots.sample.length
      assert_equal 4, dilution_map.dilution_map_slots.non_sample.length
    end

    it 'should override existing slots' do
      dilution_map = DilutionMap.new
      dilution_map.block_start = 0
      dilution_map.block_end = 3
      dilution_map.block_slot_type = 'samp'
      dilution_map.build_new_wells

      assert_equal 4, dilution_map.dilution_map_slots.length

      dilution_map.block_start = 2
      dilution_map.block_end = 5
      dilution_map.block_slot_type = 'samp'
      # This should override 2 and add 2 more
      dilution_map.build_new_wells

      assert_equal 6, dilution_map.dilution_map_slots.length
    end

    it 'should error if requisite attributes are not set' do
      dilution_map = DilutionMap.new
      assert_raises(RuntimeError) {dilution_map.build_new_wells}
    end

  end

end
