require 'spec_helper'


describe Biorails::TaskPatch do

  context 'dilution_slots' do

    before(:each) do
      @task = Task.first
      barcode = @task.task_barcodes.create!(
        :reference_type => 'CoastsPlate',
        :reference_code => 'ABC123',
        :position => 0
      )
      barcode.finalise
      ProcessVersion.any_instance.stubs(:dilution_map => DilutionMap.first)
      @task.run_consolidation
    end

    it 'should present slots ordered by row, then column' do
      slots = @task.dilution_slots.by_row
      assert slots.is_a?(ActiveRecord::Relation)
      assert slots[0].is_a?(DilutionSlot)
    end

    it 'should present slots as an array of rows' do
      rows = @task.dilution_slots.rows
      assert_kind_of Array, rows
      row = rows[0]
      assert_kind_of Array, row
      assert_kind_of DilutionSlot, row[0]
      assert_equal [0], row.collect { |x| x.row_no }.uniq
      assert_equal row.collect { |x| x.column_no }.sort, row.collect { |x| x.column_no }
    end

    it 'should re-arrange slots based on array of [id, row_no, column_no]' do
      slots = @task.dilution_slots.by_row
      #                                             \/ This offset matches the input from the UI component
      positions = slots.collect { |x| [x.id, (x.row_no + 1), x.column_no] }

      # switching the second and third position
      #
      positions[1][2] = 2
      positions[2][2] = 1

      assert_equal 1, slots[1].column_no
      assert_equal 2, slots[2].column_no

      job = TaskConsolidation.new
      job.task = @task
      job.dilution_slots_rearrange(positions)

      assert_equal 2, DilutionSlot.find(slots[1].id).column_no
      assert_equal 1, DilutionSlot.find(slots[2].id).column_no
    end

  end

  context 'class methods (scopes)' do

    it 'should find active tasks' do

      Task.first.set_active

      assert Task.active.any?
      Task.active.each do |task|
        assert task.state.level_no >= 1
      end

    end

    it 'should find an active task with a link to  RlSample or CdbSample' do
      Task.first.set_active

      assert_equal 2, Task.active_inventory_result.length

      task = Task.active_inventory_result[0]
      assert_instance_of ResultTask, task
      assert task.active?
      assert task.inventory_linked?
    end

  end

  context "colour_index" do

    before(:each) do
      @task = Task.first
      @task.task_barcodes.create!(
        :reference_type => 'CoastsPlate',
        :reference_code => 'SomeFullColumns',
        :position => 0
      )
      @task.task_barcodes.create!(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'A0001',
        :position => 1
      )
      @task.task_barcodes.each { |x| x.finalise }
      @task.reload
    end

    it 'should generate a list of barcodes identifying plates and wells' do

      index = @task.colour_index

      assert_kind_of Array, index
      assert_equal index.uniq, index
      assert_includes index, 'SomeFullColumns'
      assert_includes index, 'A0001'
    end

    it 'should provide a colour index for each plate and well' do
      assert_equal 0, @task.colour_index_for('SomeFullColumns')
      assert_equal 1, @task.colour_index_for('A0001')
    end

  end

  context 'inventory_linked?' do

    it 'should identify an inventory linked task' do
      @task = OutlineQueue.find_by_name('service_a').tasks.where(:type => 'ResultTask').first
      OutlineQueue.any_instance.stubs(:coasts_orderable? => true)

      assert @task.inventory_linked?
    end

    it 'should return false for a non-inventory linked task' do
      @task = OutlineQueue.find_by_name('service_a').tasks.where(:type => 'ResultTask').first
      OutlineQueue.any_instance.stubs(:coasts_orderable? => false)

      refute @task.inventory_linked?
    end

  end

  context 'coasts_plates' do


    it 'should find plates via barcodes with "CoastsPlate" as the reference_type' do
      @task = Task.first
      @task.task_barcodes.create!(
        :reference_type => 'CoastsPlate',
        :reference_code => 'SomeFullColumns',
        :position => 0
      )
      @task.task_barcodes.create!(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'A0001',
        :position => 1
      )
      @task.task_barcodes.each { |x| x.finalise }

      assert_equal 1, @task.task_barcodes.where(:reference_type => 'CoastsPlate').count
      assert_equal 1, @task.coasts_plates.count

      assert_equal @task.task_barcodes.where(:reference_type => 'CoastsPlate').collect { |x| x.reference },
                   @task.coasts_plates
    end

    it 'should find plates via barcodes with "Order" as the refrence_type' do
      order = Order.first

      CoastsOrderPlate.create!(
        :id_order_header_ref => "#{order.coasts_order_id}",
        :plate_code => 'ABC123'
      )
      CoastsOrderPlate.create!(
        :id_order_header_ref => "#{order.coasts_order_id}",
        :plate_code => '123ABC'
      )

      @task = Task.first
      @task.task_barcodes.destroy_all
      @task.task_barcodes.create!(
        :reference_type => 'Order',
        :reference_code => "#{order.coasts_order_id}",
        :position => 0,
        :complete => true
      )
      order.reload
      assert_equal 2, order.coasts_plates.count
      assert_equal 2, @task.coasts_plates.count

      assert_equal order.coasts_plates, @task.coasts_plates

    end

    it 'should provide me with a list of codes' do
      order = Order.first
      CoastsOrderPlate.create!(
        :id_order_header_ref => "#{order.coasts_order_id}",
        :plate_code => 'ABC123'
      )
      CoastsOrderPlate.create!(
        :id_order_header_ref => "#{order.coasts_order_id}",
        :plate_code => '123ABC'
      )

      @task = Task.first
      @task.task_barcodes.create!(
        :reference_type => 'Order',
        :reference_code => "#{order.coasts_order_id}",
        :position => 0,
        :complete => true
      )
      assert_kind_of Array, @task.coasts_plate_codes
      assert_equal 2, @task.coasts_plate_codes.length
      %w{ABC123 123ABC}.each do |code|
        assert_includes @task.coasts_plate_codes, code
      end
    end

  end

  context 'coasts_plates (references)' do

    it 'should find wells via barcodes with "CoastsPlateWell" as the reference_type' do
      @task = Task.first
      @task.task_barcodes.create(:reference_type => 'CoastsPlateWell', :reference_code => 'A0001', :position => 0)
      @task.task_barcodes.create(:reference_type => 'CoastsPlateWell', :reference_code => 'A0002', :position => 1)
      @task.task_barcodes.each { |x| x.finalise }

      assert_equal 2, @task.task_barcodes.where(:reference_type => 'CoastsPlateWell').count
      assert_equal 2, @task.coasts_plate_wells.count

      assert_equal @task.task_barcodes.where(:reference_type => 'CoastsPlateWell').collect { |x| x.reference },
                   @task.coasts_plate_wells
    end

    it 'should provide me with a list of codes' do
      @task = Task.first
      @task.task_barcodes.create(:reference_type => 'CoastsPlateWell', :reference_code => 'A0001', :position => 0)
      @task.task_barcodes.create(:reference_type => 'CoastsPlateWell', :reference_code => 'B0001', :position => 1)
      @task.task_barcodes.each { |x| x.finalise }
      assert_equal 2, @task.task_barcodes.where(:reference_type => 'CoastsPlateWell').count
      assert_equal 2, @task.coasts_plate_wells.count

      assert_equal %w{A0001 B0001},
                   @task.coasts_plate_well_codes
    end

  end

  context 'source_rows' do

=begin
    SomeFullRows

    # # # # # # # # # # # #
    # # # # # # # # # # # #
    # # # # # # # # # # # #
    # # . . . . . . . . . .
    . . . . . . . . . . . .
    . . . . . . . . . . . .
    . . . . . . . . . . . .
    . . . . . . . . . . . .

      SomeFullColumns

    # # # . . . . . . . . .
    # # # . . . . . . . . .
    # # # . . . . . . . . .
    # # # . . . . . . . . .
    # # . . . . . . . . . .
    # # . . . . . . . . . .
    # # . . . . . . . . . .
    # # . . . . . . . . . .
=end

    before(:each) do
      ProcessVersion.any_instance.stubs(:dilution_map => DilutionMap.first)
      @task = ResultTask.first
      @task.task_barcodes.create(:reference_type => 'CoastsPlate', :reference_code => 'SomeFullRows', :position => 0)
      @task.task_barcodes.create(:reference_type => 'CoastsPlate', :reference_code => 'SomeFullColumns', :position => 1)
      @task.task_barcodes.each{|x|x.finalise}
    end

    it 'should get the plate wells by row' do
      @task.dilution_map.update_attribute(:source_by_row, true)
      rows = @task.source_rows

      # SomeFullRows
      assert_equal 12, rows[0].length
      assert_equal 12, rows[1].length
      assert_equal 12, rows[2].length
      assert_equal 2, rows[3].length
      # SomeFullColumns
      assert_equal 3, rows[4].length
      assert_equal 3, rows[5].length
      assert_equal 3, rows[6].length
      assert_equal 3, rows[7].length
      assert_equal 2, rows[8].length
      assert_equal 2, rows[9].length
      assert_equal 2, rows[10].length
      assert_equal 2, rows[11].length
    end


    it 'should get the plate wells by column' do
     @task.dilution_map.update_attribute(:source_by_row, false)
      rows = @task.source_rows

      # SomeFullRows
      assert_equal 4, rows[0].length
      assert_equal 4, rows[1].length
      assert_equal 3, rows[2].length
      assert_equal 3, rows[3].length
      assert_equal 3, rows[4].length
      assert_equal 3, rows[5].length
      assert_equal 3, rows[6].length
      assert_equal 3, rows[7].length
      assert_equal 3, rows[8].length
      assert_equal 3, rows[9].length
      assert_equal 3, rows[10].length
      assert_equal 3, rows[11].length
      # SomeFullColumns
      assert_equal 8, rows[12].length
      assert_equal 8, rows[13].length
      assert_equal 4, rows[14].length
    end

  end

  context 'consolidation_algorithm_options' do

    before(:each) do
      ProcessVersion.any_instance.stubs(:dilution_map => DilutionMap.first)
      @task = Task.first
      @task.task_barcodes.create(:reference_type => 'CoastsPlate', :reference_code => 'SomeFullRows', :position => 0)
      @task.task_barcodes.create(:reference_type => 'CoastsPlate', :reference_code => 'SomeFullColumns', :position => 1)
      @task.task_barcodes.each{|x|x.finalise}
    end

    it 'should know when the source rows are too long' do
      refute @task.only_always_split?
      @task.dilution_map.update_attribute(:source_length, 2)
      assert @task.only_always_split?
    end

    it 'should provide all options when the source rows are not too long' do
      options = @task.consolidation_algorithm_options
      assert_equal 3, options.length
      options.each do |option|
        assert_equal 2, option.length
      end
      assert_include options.collect { |x| x[0] }, DilutionMap.human_attribute_name(:reorder_within_group)
      assert_include options.collect { |x| x[0] }, DilutionMap.human_attribute_name(:always_split)
      assert_include options.collect { |x| x[0] }, DilutionMap.human_attribute_name(:never_split)

      assert_include options.collect { |x| x[1] }, 'reorder_within_group'
      assert_include options.collect { |x| x[1] }, 'always_split'
      assert_include options.collect { |x| x[1] }, 'never_split'
    end

    it 'should provide "always split" if source rows are too long' do
      @task.dilution_map.update_attribute(:source_length, 2)
      options = @task.consolidation_algorithm_options
      assert_equal 1, options.length
      options.each do |option|
        assert_equal 2, option.length
      end
      assert_include options.collect { |x| x[0] }, DilutionMap.human_attribute_name(:always_split)
      assert_include options.collect { |x| x[1] }, 'always_split'
    end

    it 'should provide a list of the internal names of the algorithms' do
      algorithms = @task.usable_algorithms
      assert_equal 3, algorithms.length
      assert_equal %w{reorder_within_group always_split never_split},
                   algorithms

      @task.dilution_map.update_attribute(:source_length, 2)
      algorithms = @task.usable_algorithms

      assert_equal 1, algorithms.length
      assert_equal %w{always_split}, algorithms
    end

  end

  context 'run_dilution' do

    before(:each) do
      ProcessVersion.any_instance.stubs(:dilution_map => DilutionMap.first)
      @task = Task.first
      @task.task_barcodes.create!(
        :reference_type => 'CoastsPlate',
        :reference_code => "ABC123",
        :position => 0
      )
      @task.task_barcodes.each { |x| x.finalise }
    end

    # This is more thoroughly tested in the dilution_runner_spec
    it 'should run dilution for the objects in a task' do
      @task.dilution_slots.destroy_all
      assert @task.dilution_slots.none?
      assert @task.run_consolidation
      @task.dilution_slots.reload
      assert @task.dilution_slots.any?
    end

    it 'should error if there is no dilution map' do
      ProcessVersion.any_instance.stubs(:dilution_map => nil)
      @task.reload

      refute @task.pre_dilution_check
      assert_includes @task.errors[:base],
                      "You can't perform plate consolidation unless the Process Version has a Dilution Map."
    end

    it 'should error if there are no plates scanned' do
      @task.task_barcodes.destroy_all
      @task.reload

      refute @task.pre_dilution_check
      assert_includes @task.errors[:base],
                      "You can not perform plate consolidation unless you have scanned at least one plate or tube in for this task."
    end

    it 'should error if the algorithm is not usable' do
      @task.dilution_map.update_attribute(:source_length, 1)
      @task.reload

      refute @task.pre_dilution_check
      assert_includes @task.errors[:consolidation_algorithm],
                      "This algorithm can't be used for this map."
    end

    it 'should error if the sources are too long' do
      @task.task_barcodes.create!(
        :reference_type => 'CoastsPlate',
        :reference_code => "Full384",
        :position => 0
      )
      @task.task_barcodes.each { |x| x.finalise }

      @task.dilution_map.update_attribute(:source_length, 2)

      @task.reload

      refute @task.pre_dilution_check
      assert_includes @task.errors[:base],
                      "Some Columns in the source plates are too long, you may want to modify your dilution map or use the 'Always Split' algorithm."
    end

  end

  context :output_files do

    before(:each) do
      @process_version = ProcessVersion.output.last
      @process_version.process_version_file_templates.create!(
        :file_name => 'test_file',
        :file_type => 'tsv',
        :output_file_template_id => OutputFileTemplate.first.id,
      )
      @task = ResultTask.create!(
        :name => 'testtask',
        :process_version_id => @process_version.id,
        :outline_id => @process_version.outline.id
      )
    end

    it 'should have a default file template id' do
      assert @task.process_version_file_template_id
      assert @task.process_version_file_template
    end

    it 'should parse liquid template from output file template' do
      result = @task.output_file_text(Task::TAB_TEMPLATE)
      assert_includes result, "\t"
      @task.parameters.each do |parameter|
        assert_includes result, parameter.name
      end
    end

    it 'should generate text from a process_version_file_template' do
      result = @task.generate_output_file_content
      assert_includes result, "\t"
      @task.parameters.each do |parameter|
        assert_includes result, parameter.name
      end
    end

    it 'should not generate output file when there is no process_version_file_template' do
      @task.process_version_file_template = nil
      @task.process_version_file_template_id = nil
      result = @task.generate_output_file_content
      assert_nil result
    end


    it 'should have a temp file path in the rails tmp directory' do
      assert_includes @task.temp_file_path, Rails.root.to_s
      assert_includes @task.temp_file_path, 'tmp'
      assert_includes @task.temp_file_path, @task.file_name
    end

    it 'should provide a file name based on process_version_file_template' do
      assert @task.file_name
      assert_includes @task.file_name, @task.process_version_file_template.file_name
      assert_includes @task.file_name, @task.process_version_file_template.file_type
    end

    it 'should provide a warning when a file has already been generated' do
      @task.fill_output_file
      # having already been created, there's a project asset with that name
      assert_equal(
        I18n.t(
          'activerecord.errors.task.duplicate_file',
          :name => @task.file_name
        ),
        @task.duplicate_file_warning
      )
    end

  end

  context :fill_output_file do

    it 'should create a tsv file' do
      @process_version = ProcessVersion.output.last
      @process_version.process_version_file_templates.create!(
        :file_name => 'test_file',
        :file_type => 'tsv',
        :output_file_template_id => OutputFileTemplate.first.id
      )
      @task = ResultTask.create!(
        :name => 'testtask',
        :description => 'testtask',
        :process_version_id => @process_version.id,
        :outline_id => @process_version.outline.id
      )
      @task.fill_output_file

      assert @task.project_element.children.where(
               :type => 'ProjectAsset',
               :name => @task.file_name
             ).any?
      assert_empty Dir[@task.temp_file_path]
    end

    it 'should create a csv file' do
      @process_version = FactoryGirl.create(:process_version)
      @process_version.process_version_file_templates.create!(
        :file_name => 'test_file',
        :file_type => 'csv',
        :output_file_template_id => OutputFileTemplate.first.id
      )
      @task = ResultTask.create!(
        :name => 'testtask',
        :description => 'testtask',
        :process_version_id => @process_version.id,
        :outline_id => @process_version.outline.id
      )
      @task.fill_output_file

      assert @task.project_element.children.where(
               :type => 'ProjectAsset',
               :name => @task.file_name
             ).any?
      assert_empty Dir[@task.temp_file_path]
    end

    it 'should create an excel file' do
      @process_version = FactoryGirl.create(:process_version)
      @process_version.process_version_file_templates.create!(
        :file_name => 'test_file',
        :file_type => 'xlsx',
        :output_file_template_id => OutputFileTemplate.first.id
      )
      @task = ResultTask.create!(
        :name => 'testtask',
        :description => 'testtask',
        :process_version_id => @process_version.id,
        :outline_id => @process_version.outline.id
      )
      @task.fill_output_file

      assert @task.project_element.children.where(
               :type => 'ProjectAsset',
               :name => @task.file_name
             ).any?
      assert_empty Dir[@task.temp_file_path]
    end

  end

  context "sync content to filesystem" do

    before(:each) do
      @process_version = ProcessVersion.output.last
      @process_version.process_version_file_templates.create!(
        :file_name => 'test_file',
        :file_type => 'xlsx',
        :output_file_template_id => OutputFileTemplate.first.id
      )
      @task = ResultTask.create!(
        :name => 'testtask',
        :description => 'testtask',
        :process_version_id => @process_version.id,
        :outline_id => @process_version.outline.id
      )
      @task.fill_output_file
    end

    it 'should copy the project asset to the file system' do
      @task.sync_content_to_filesystem
      path = @task.output_file_directory
      files = Dir.entries(path)
      files.should include(@task.file_name)
    end

  end

end
