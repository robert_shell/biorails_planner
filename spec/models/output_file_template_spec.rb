require 'spec_helper'

describe OutputFileTemplate do

  it 'should be a dictionary' do
    output_file_template = OutputFileTemplate.lookup('basic')
    assert_kind_of OutputFileTemplate, output_file_template
    assert_equal 'basic', output_file_template.name

    output_file_templates = OutputFileTemplate.like('')
    assert_kind_of Array, output_file_templates
    assert_equal 5, output_file_templates.length


    output_file_template = OutputFileTemplate.reference(1)
    assert_kind_of OutputFileTemplate, output_file_template
    assert_equal 'basic', output_file_template.name
  end

  it 'should set length when content is set' do
    output_file_template = OutputFileTemplate.new
    output_file_template.content = '1234567890'
    assert_equal 10, output_file_template.length
  end

end
