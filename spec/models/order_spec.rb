require File.dirname(__FILE__)+'/../spec_helper'


describe Order do


  let(:powder_order) do
    Order.joins(:order_profile => [:coasts_order_type]).where(
      'coasts_order_types.id_item_type_lov_ref' => CoastsOrderType::POWDER_TYPE
    ).first
  end
  let(:standard_order) do
    Order.joins(:order_profile => [:coasts_order_type]).where(
      'coasts_order_types.id_item_type_lov_ref' => CoastsOrderType::STANDARD_LIQUID_TYPE
    ).first
  end
  let(:custom_order) do
    Order.joins(:order_profile => [:coasts_order_type]).where(
      'coasts_order_types.id_item_type_lov_ref' => CoastsOrderType::CUSTOM_LIQUID_TYPE
    ).first
  end
  let(:dissolution_order) do
    Order.joins(:order_profile => [:coasts_order_type]).where(
      'coasts_order_types.id_item_type_lov_ref' => CoastsOrderType::DISSOLUTION_LIQUID_TYPE
    ).first
  end
  let(:subways_order) do
    Order.joins(:order_profile => [:coasts_order_type]).where(
      'coasts_order_types.id_item_type_lov_ref' => CoastsOrderType::SUBWAYS_TYPE
    ).first
  end

  let(:powder_profile) { powder_order.order_profile }
  let(:standard_profile) { standard_order.order_profile }
  let(:custom_profile) { custom_order.order_profile }
  let(:dissolution_profile) { dissolution_profile.order_profile }
  let(:subways_profile) { subways_order.order_profile }


  # Used when creating an order profile from a form
  it  'should test initialize_with_order_profile_id' do
    @order_profile = powder_profile
    @order = Order.new(:order_profile_id => @order_profile.id)
    @order.fill_order_from_order_profile
    assert @order.order_profile
    assert_equal @order_profile.use_internal_recipient, @order.use_internal_recipient
    assert_equal @order_profile.recipient_user_id, @order.recipient_user_id
    assert_equal @order_profile.coasts_external_recipient_id, @order.coasts_external_recipient_id
  end

  # TODO test mechanism for creating reference compounds
  it  'should initialize with items for reference compounds'

  # TODO test the mechanism for handling items deselected in the order form
  it  'should test remove_deselected_items'


  it  'should test order_profile_options' do
    @outline_queue = powder_profile.outline_queue
    @outline_queue.update_attribute(:preferred_order_profile_id, powder_profile.id)
    @order = Order.new(:outline_queue_id => @outline_queue.id)
    assert @order.order_profile_options
    assert @order.order_profile_options.is_a?(Array)
    assert_equal 5, @order.order_profile_options.length
    assert @order.order_profile_options.any? { |x| x[0].include?("(Preferred)") }
    assert @order.order_profile_options.any? { |x| x[2] and x[2][:style] == 'font-weight:bold;' }
    assert !@order.order_profile_options.all? { |x| x[0].include?("(Preferred)") }
    assert !@order.order_profile_options.all? { |x| x[2] and x[2][:style] == 'font-weight:bold;' }
  end

  #
  # Note: This only tests the mock system for creating parallel records expected in COASTS.
  #       It is not a guarantee that it will work with actual COASTS integration.
  #
  it  'should test send_to_coasts' do
    @order = Order.create!(
      :name => 'test',
      :use_internal_recipient => true,
      :recipient_user_id => 2,
      :order_profile_id => powder_profile.id,
      :outline_queue_id => powder_profile.outline_queue_id,
      :order_items_attributes => [
        {
          :rl_sample_id => 7,
          :is_reference_compound => true,
          :in_order => true
        }
      ]
    )
    assert @order.coasts_order
    assert @order.order_items.all?{|x|x.coasts_order_details.any?}
  end

  #
  # Order details is a specific query which brings back the data required in OrdersController#show
  #
  it  'should test order_details' do
    @order = powder_order
    @order.link_cart_order_to_queue_items
    details = @order.item_details
    assert_equal 9, details.length
    assert details.all? { |x| x.keys.include?('order_item_id') }
    assert details.all? { |x| x.keys.include?('sample_id') }
    assert details.all? { |x| x.keys.include?('sample_code') }
    assert details.all? { |x| x.keys.include?('saltcode') }
    assert details.all? { |x| x.keys.include?('sample_batch') }
    assert details.all? { |x| x.keys.include?('is_reference_compound') }
    assert details.all? { |x| x.keys.include?('queue_item_id') }
    assert details.all? { |x| x.keys.include?('amount') }
    assert details.all? { |x| x.keys.include?('concentration') }
    assert details.all? { |x| x.keys.include?('coasts_state') }
    assert details.all? { |x| x.keys.include?('vial_id') }
    assert details.all? { |x| x.keys.include?('coasts_order_detail_id') }
  end

  it  'should test coasts_ordered_question_mark' do
    QueueItem.any_instance.stubs(:validate_object_references_is_same => true)
    QueueItemStateObserver.any_instance.stubs(:after_save => true)
    @order = Order.new(:name => 'test', :order_profile_id => powder_profile.id,
                       :outline_queue_id => powder_profile.outline_queue_id)
    @order.order_items << @order.order_items.new(
      :sequence_no => 1,
      :is_reference_compound => true,
      :rl_sample_id => 7,
      :in_order => true
    )
    @order.order_items << @order.order_items.new(
      :sequence_no => 2,
      :is_reference_compound => false,
      :queue_item_id => 9901,
      :in_order => true
    )

    assert !@order.ordered?
    @order.save
    @order.send_to_coasts
    assert @order.ordered?
  end

  it  'should test coasts_order_url' do
    @order = powder_order
    assert @order.coasts_order_url.is_a?(String)
    assert @order.coasts_order_url.include?(@order.coasts_order_id.to_s)
  end

  it  'should test reference_compounds' do
    @order = powder_order
    assert_equal 3, @order.order_items.count
    assert_equal 1, @order.reference_compounds.count
    assert @order.reference_compounds[0].is_a?(OrderItem)
  end

  it  'should test powder_order_question_mark' do
    @order = powder_order
    assert_equal 1, @order.coasts_order_type_id
    assert @order.solid?
    @order = standard_order
    assert_equal 2, @order.coasts_order_type_id
    assert !@order.solid?
    @order = custom_order
    assert_equal 3, @order.coasts_order_type_id
    assert !@order.solid?
  end

  it  'should test standard_order_question_mark' do
    @order = powder_order
    assert_equal 1, @order.coasts_order_type_id
    assert !@order.standard_liquid?
    @order = standard_order
    assert_equal 2, @order.coasts_order_type_id
    assert @order.standard_liquid?
    @order = custom_order
    assert_equal 3, @order.coasts_order_type_id
    assert !@order.standard_liquid?
  end

  it  'should test custom_order_question_mark' do
    @order = powder_order
    assert_equal 1, @order.coasts_order_type_id
    assert !@order.custom_liquid?
    @order = standard_order
    assert_equal 2, @order.coasts_order_type_id
    assert !@order.custom_liquid?
    @order = custom_order
    assert_equal 3, @order.coasts_order_type_id
    assert @order.custom_liquid?
  end

  it  'should test xml' do
    @order = powder_order
    xml = @order.xml
    assert xml
    assert xml.is_a?(String)
    assert xml.include?("<ROWSET>")
    assert xml.include?("<ORDER>")
    @order.order_items.each do |item|
      assert xml.include?(item.sample_id.to_s)
    end
  end


  it  'should test stock_check_powder' do
    @order = powder_order
    @order.stock_check
    item = @order.order_items.last
    # from orderable_samples_ot_1
    assert_operator 0,'<=', item.us_weight
    assert_operator 0,'<=', item.de_weight
  end

  it  'should test stock_check_standard' do
    @order = standard_order
    @order.order_items << @order.order_items.create!(
      :sequence_no => 9,
      :is_reference_compound => true,
      :sample_id => 2,
      :in_order => true
    )
    @order.stock_check
    item = @order.order_items.last
    # from orderable_samples_ot_1
    assert_operator 0,'<=', item.us_weight
    assert_operator 0,'<=', item.de_weight
    # from orderable_samples_ot_2
    assert_operator 0,'<=', item.us_vials
    assert_operator 0,'<=', item.de_vials
  end

  it  'should test stock_check_custom' do
    @order = custom_order
    @order.order_items << @order.order_items.create!(
      :sequence_no => 9,
      :is_reference_compound => true,
      :sample_id => 2,
      :in_order => true
    )
    @order.stock_check
    item = @order.order_items.last
    # from orderable_samples_ot_1
    assert_operator 0,'<=',item.us_weight
    assert_operator 0,'<=', item.de_weight
    # from orderable_samples_ot_5
    assert_operator 0,'<=', item.de_ul
    assert_operator 0,'<=', item.us_ul
    assert item.in_order
  end

  it  'should test stock_check_dissolution' do
    @order = dissolution_order
    @order.order_items << @order.order_items.create!(
      :sequence_no => 9,
      :is_reference_compound => true,
      :sample_id => 2,
      :in_order => true
    )
    @order.stock_check
    item = @order.order_items.last
    # from orderable_samples_ot_1
    assert_operator 0,'<=', item.us_weight
    assert_operator 0,'<=', item.de_weight
    assert item.in_order
  end

  it  'should test stock_check_subways' do
    @order = subways_order
    @order.stock_check
    item = @order.order_items.first
    # from orderble_samples_ot_201
    assert item.subways_stock.is_a?(Array)
    assert item.subways_stock[0].is_a?(Hash)
    assert item.tube_id
  end





end
