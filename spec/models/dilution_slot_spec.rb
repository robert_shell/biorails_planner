require 'spec_helper'




describe DilutionSlot do

  before(:each) do
    ProcessVersion.any_instance.stubs(
      :dilution_map => DilutionMap.first
    )
    @task = ResultTask.first
    @task_barcode = @task.task_barcodes.create(
      :reference_type => 'CoastsPlate',
      :reference_code => 'SomeFullRows',
      :position => 0
    )
    @task_barcode.finalise

    @task.run_consolidation
    @dilution_slot = @task.dilution_slots.first
  end

  context 'to_s' do

    it 'should provide a brief summary of a slot' do
      summary = @dilution_slot.to_s
      assert_includes summary, @task.name
      assert_includes summary, @dilution_slot.row_no.to_s
      assert_includes summary, @dilution_slot.column_no.to_s
      assert_includes summary, @dilution_slot.slot_type
      assert_includes summary, @dilution_slot.coasts_plate_well.to_s
      assert_includes summary, @dilution_slot.cdb_sample.to_s.strip
    end


  end

  context 'plate_well_identifier' do
    it 'should provide a simple identifier of the row' do
      assert_equal 'SomeFullRows,A,1', @dilution_slot.coasts_plate_well_identifier
    end
  end

  context 'source string' do
    it 'should provide a shortened version of the plate code and well co ordinate' do
      assert_equal '...Rows:A1', @dilution_slot.source_string
    end
  end

  context 'summary' do
    it 'should display slot information for a reloacted CoastsPlateWell' do
      summary = @dilution_slot.summary
      [:row_no, :column_no, :display_slot_type, :plate_code, :plate_row, :plate_col].each do |attr|
        assert_includes summary, @dilution_slot.send(attr).to_s
      end
      [:sample_id, :sample_code, :saltcode, :sample_batch, :volume, :volume_unit,
       :concentration, :concentration_unit, :solvent].each do |attr|
        assert_includes summary, @dilution_slot.coasts_plate_well.send(attr).to_s
      end
    end

    it 'should display slot information for a standard space with a sample' do
      @dilution_slot = @task.dilution_slots.where(:slot_type => 'std').first
      @dilution_slot.sample_id = 1
      summary = @dilution_slot.summary
      [:row_no, :column_no, :display_slot_type].each do |attr|
        assert_includes summary, @dilution_slot.send(attr).to_s
      end
      [:sample_id, :sample_code, :saltcode, :sample_batch].each do |attr|
        assert_includes summary, @dilution_slot.cdb_sample.send(attr).to_s
      end
    end

    it 'should display slot information for a standard space without a sample' do
      @dilution_slot = @task.dilution_slots.where(:slot_type => 'std').first
      summary = @dilution_slot.summary
      [:row_no, :column_no, :display_slot_type].each do |attr|
        assert_includes summary, @dilution_slot.send(attr).to_s
      end
    end

  end

end
