require File.join(Rails.root,'spec','spec_helper')
require File.join(Rails.root,'spec','view_helpers')

describe 'BI::SampleProvider' do

  it "get_data should return a string with xml in it" do
    provider =  BI::SampleProvider.mock_mode
    provider.mode ='I'
    xml = provider.get_data()
    assert xml
    assert_match /ROWSET/,xml
    assert_match /ROW/,xml
    assert_match /SAMPLE\_ID/,xml
    assert_match /ACTION/,xml
  end

  it "should convert to hash" do
    provider =  BI::SampleProvider.mock_mode
    provider.mode ='I'
    hash = provider.read_rowset()
    assert hash['ROW']
    assert hash['ROW']['ACTION']
    assert hash['ROW']['SAMPLE_ID']
  end

  it "Should connect/disconnect without exceptions" do
    provider =  BI::SampleProvider.mock_mode
    provider.add_subscriber()
    provider.remove_subscriber()
  end

  it "Should sync with a insert" do
    provider =  BI::SampleProvider.mock_mode
    provider.mode = 'I'
    provider.sync()
  end

  it "Should sync with a update" do
    provider =  BI::SampleProvider.mock_mode
    provider.mode = 'U'
    provider.sync()
  end

  it "Should sync with a CUD" do
    provider = BI::SampleProvider.mock_mode
    provider.mode ='A'
    provider.num_samples = 3
    provider.sync()
  end

  it "Should run pl/sql functions" do
    provider =  BI::SampleProvider.live_mode
    x = provider.biorails_session_version
    assert_not_nil x
    assert x.is_a?(String)
    assert x.size > 4
  end

  it "Should run pl/sql procedures" do
    provider =  BI::SampleProvider.live_mode
    provider.setup_session("test","it")
  end

end
