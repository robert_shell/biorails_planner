require 'spec_helper'


describe TaskBarcode do


  context 'reference types' do


    it "should provide a static, translated lookup of reference types" do
      reference_types = TaskBarcode.reference_types
      assert_instance_of Array, reference_types
      assert_instance_of Array, reference_types[0]

      translated_names = reference_types.collect { |x| x[0] }
      assert_equal 4, translated_names.length
      assert_include translated_names, CoastsPlate.model_name.human
      assert_include translated_names, CoastsPlateWell.model_name.human
      assert_include translated_names, Order.model_name.human
      assert_include translated_names, CdbSample.model_name.human


      class_names = reference_types.collect { |x| x[1] }
      assert_equal 4, class_names.length
      assert_include class_names, 'CoastsPlate'
      assert_include class_names, 'CoastsPlateWell'
      assert_include class_names, 'Order'
      assert_include class_names, 'CdbSample'
    end
  end

  context 'TaskBarcode.scan_list' do

    it "Should scan some CoastsPlate barcodes " do
      task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        'ABC123,123ABC'
      )

      assert_instance_of Array, unfound
      assert_empty unfound
      assert_equal 2, task_barcodes.length
      task_barcode = task_barcodes[0]

      assert_instance_of TaskBarcode, task_barcode
      assert_instance_of CoastsPlate, task_barcode.reference
      assert !task_barcode.complete
    end

    it "should scan some sample ids" do
      task_barcodes, unfound = TaskBarcode.scan_list(
        'CdbSample',
        '8,5'
      )
      assert_instance_of Array, unfound
      assert_empty unfound

      assert_equal 2, task_barcodes.length

      task_barcode = task_barcodes[0]
      assert_instance_of TaskBarcode, task_barcode
      assert_instance_of CdbSample, task_barcode.reference
      assert !task_barcode.complete
    end

    it 'should scan some sample codes (and get the first instance)' do
      task_barcodes, unfound = TaskBarcode.scan_list(
        'CdbSample',
        'ED92:S:1,ED152:S:1'
      )
      assert_instance_of Array, unfound
      assert_empty unfound

      assert_equal 2, task_barcodes.length

      task_barcode = task_barcodes[0]
      assert_instance_of TaskBarcode, task_barcode
      assert_instance_of CdbSample, task_barcode.reference
      assert !task_barcode.complete
    end

    it "Should return an unknown CdbSample barcode in the second returned array" do
      task_barcodes, unfound = TaskBarcode.scan_list(
        'CdbSample',
        'NOBARCODE'
      )

      assert_instance_of Array, unfound
      assert_equal 1, unfound.length
      assert_include unfound, 'NOBARCODE'
    end


    it "Should scan a barcode with a task" do
      task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        'ABC123',
        ResultTask.first.id
      )

      assert_instance_of Array, unfound
      assert_empty unfound
      assert_equal 1, task_barcodes.length
      task_barcode = task_barcodes[0]

      assert_instance_of TaskBarcode, task_barcode
      assert_instance_of CoastsPlate, task_barcode.reference
      assert !task_barcode.complete
      assert_instance_of ResultTask, task_barcode.task
    end

    it "Should return an unknown CoastsPlate barcode in the second returned array" do
      task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        'NOBARCODE'
      )
      assert_equal 0, task_barcodes.count
      assert_instance_of Array, unfound
      assert_equal 1, unfound.length
      assert_include unfound, 'NOBARCODE'
    end

    it "should scan some CoastsPlateWell barcodes" do
      task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlateWell',
        'A0001,A0002'
      )

      assert_instance_of Array, unfound
      assert_empty unfound
      assert_equal 2, task_barcodes.length
      task_barcode = task_barcodes[0]

      assert_instance_of TaskBarcode, task_barcode
      assert_instance_of CoastsPlateWell, task_barcode.reference
      assert !task_barcode.complete
    end

    it "Should return an unknown CoastsPlateWell barcode in the second returned array" do
      task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlateWell',
        'NOBARCODE'
      )

      assert_instance_of Array, unfound
      assert_equal 1, unfound.length
      assert_include unfound, 'NOBARCODE'
    end

    it "should scan some Order barcodes" do
      task_barcodes, unfound = TaskBarcode.scan_list(
        'Order',
        "#{Order.first.coasts_order_id},#{Order.last.coasts_order_id}"
      )

      assert_instance_of Array, unfound
      assert_empty unfound
      assert_equal 2, task_barcodes.length
      task_barcode = task_barcodes[0]

      assert_instance_of TaskBarcode, task_barcode
      assert_instance_of Order, task_barcode.reference
      assert !task_barcode.complete
    end

    it "Should return an unknown Order barcode in the second returned array" do
      task_barcodes, unfound = TaskBarcode.scan_list(
        'Order',
        '999999999999'
      )

      assert_instance_of Array, unfound
      assert_equal 1, unfound.length
      assert_include unfound, '999999999999'
    end

  end

  context 'task_barcode.scan_list (one row per well)' do

    # TODO shorten this right down once the fixtures contain a process with all the required fields
    #
    # sample, method, source_plate, source_column, source_row, test_plate, test_column
    #
    before(:each) do
      User.background_session
      @outline = Outline.find_by_name('services')
      @outline_queue = @outline.queues.last
      @process_version = Biorails::AssayManager.new(@outline_queue).default_output_process
      @task = ResultTask.create!(
        :name => 'a',
        :description => 'a',
        :outline_id => @outline.id,
        :process_version_id => @process_version.id,
        :element_type_id => @outline_queue.element_type_id,
        :parent_folder_id => @outline.project_element_id
      )
    end

    it 'should add rows for each well in a plate' do
      task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlate',
        'ABC123',
        @task
      )
      task_barcodes.each { |tb| tb.task_id = @task.id; tb.finalise }
      @task.reload
      sheet = @task.sheet

      CoastsPlate.find_by_plate_code('ABC123').coasts_plate_wells.each do |well|
        rows = sheet.select_rows(
          :source_plate => well.plate_code,
          :source_row => well.plate_row,
          :source_column => well.plate_col
        )
        assert rows.any?
      end
    end

    it 'should add rows for a well' do
      task_barcodes, unfound = TaskBarcode.scan_list(
        'CoastsPlateWell',
        'A0001',
        @task
      )
      task_barcodes.each { |tb| tb.task_id = @task.id; tb.finalise }
      sheet = @task.sheet

      well = CoastsPlateWell.find_by_plat_tube_barcode('A0001')
      rows = sheet.select_rows(
        :source_plate => well.plate_code,
        :source_row => well.plate_row,
        :source_column => well.plate_col
      )
      assert rows.any?
    end

    it 'should add rows for a sample' do
      task_barcodes, unfound = TaskBarcode.scan_list(
        'CdbSample',
        '3',
        @task
      )
      task_barcodes.each { |tb| tb.task_id = @task.id; tb.finalise }
      sheet = @task.sheet

      sample = CdbSample.find_by_sample_id(3)
      rows = sheet.select_rows(:sample => sample)
      assert rows.any?
    end

    it 'should add rows for each well in an order' do
      Order.any_instance.stubs(
        :coasts_plates => [CoastsPlate.first]
      )
      order = Order.first
      task_barcodes, unfound = TaskBarcode.scan_list(
        'Order',
        "#{order.coasts_order_id}",
        @task
      )
      task_barcodes.each { |tb| tb.task_id = @task.id; tb.finalise }
      sheet = @task.sheet

      assert_not_empty order.coasts_plates

      order.coasts_plates.each do |coasts_plate|
        coasts_plate.coasts_plate_wells.each do |well|
          rows = sheet.select_rows(
            :source_plate => well.plate_code,
            :source_row => well.plate_row,
            :source_column => well.plate_col
          )
          assert rows.any?
        end
      end
    end

  end

  context 'reference' do

    it 'should find a referenced Sample' do
      task_barcode = TaskBarcode.new(
        :reference_type => 'CdbSample',
        :reference_code => '5'
      )
      assert_instance_of CdbSample, task_barcode.reference
      assert_equal 5, task_barcode.reference.sample_id
    end

    it 'should find a referenced Order' do
      task_barcode = TaskBarcode.new(
        :reference_type => 'Order',
        :reference_code => "#{Order.first.id}"
      )
      assert_instance_of Order, task_barcode.reference
      assert_equal Order.first.id, task_barcode.reference.coasts_order.order_id
    end

    it 'should find a referenced CoastsPlate' do
      task_barcode = TaskBarcode.new(
        :reference_type => 'CoastsPlate',
        :reference_code => 'ABC123'
      )
      assert_instance_of CoastsPlate, task_barcode.reference
      assert_equal 'ABC123', task_barcode.reference.plate_code
    end

    it 'should find a referenced CoastsPlateWell' do
      task_barcode = TaskBarcode.new(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'A0001'
      )
      assert_instance_of CoastsPlateWell, task_barcode.reference
      assert_equal 'A0001', task_barcode.reference.plat_tube_barcode
    end

  end

  context 'TaskBarcode.find_reference' do

    it 'should find a referenced Order' do
      reference = TaskBarcode.find_reference('CdbSample', '4')
      assert_instance_of CdbSample, reference
      assert_equal 4, reference.sample_id
    end


    it 'should find a referenced Order' do
      reference = TaskBarcode.find_reference('Order', "#{Order.first.id}")
      assert_instance_of Order, reference
      assert_equal Order.first.id, reference.coasts_order.order_id
    end

    it 'should find a referenced CoastsPlate' do
      reference = TaskBarcode.find_reference('CoastsPlate', 'ABC123')
      assert_instance_of CoastsPlate, reference
      assert_equal 'ABC123', reference.plate_code
    end

    it 'should find a referenced CoastsPlateWell' do
      reference = TaskBarcode.find_reference('CoastsPlateWell', 'A0001')
      assert_instance_of CoastsPlateWell, reference
      assert_equal 'A0001', reference.plat_tube_barcode
    end

    it 'should throw an error if reference_type is unknown' do
      assert_raises RuntimeError do
        TaskBarcode.find_reference('Apple', 'A0001')
      end
    end

  end

  context 'TaskBarcode.find_task_for' do

    before(:all) do
      task = ResultTask.last
      @order = task.orders.first
      CoastsOrderPlate.create!(
        :id_order_header_ref => @order.coasts_order.id,
        :plate_code => 'ABC123'
      )
      @coasts_plate = CoastsPlate.find_by_plate_code('ABC123')
      @coasts_plate.current_order.update_attribute :task_id, task.id
      @coasts_plate_well = CoastsPlateWell.find_by_plat_tube_barcode('A0001')
    end

    it "should find a task for a CoastsPlate" do
      task = TaskBarcode.find_task_for(@coasts_plate)
      assert_instance_of ResultTask, task
    end

    it "should find a task for a CoastsPlateWell" do
      task = TaskBarcode.find_task_for(@coasts_plate_well)
      assert_instance_of ResultTask, task
    end

    it "should find a task for a Order" do
      task = TaskBarcode.find_task_for(@order)
      assert_instance_of ResultTask, task
    end

    it 'should find nothing for a sample' do
      task = TaskBarcode.find_task_for(CdbSample.first)
      assert_nil task
    end

  end

  context 'order' do

    it 'should find nothing for a sample' do
      task_barcode = TaskBarcode.new(
        :reference_type => 'CdbSample',
        :reference_code => '7'
      )
      assert_nil task_barcode.order
    end

    it 'should find an order for a CoastsPlate' do
      task_barcode = TaskBarcode.new(
        :reference_type => 'CoastsPlate',
        :reference_code => 'ABC123'
      )
      assert_instance_of Order, task_barcode.order
    end

    it 'should find an order for a CoastsPlateWell' do
      task_barcode = TaskBarcode.new(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'A0001'
      )
      assert_instance_of Order, task_barcode.order
    end

    it 'should return an order if the barcode is for an order' do
      task_barcode = TaskBarcode.new(
        :reference_type => 'Order',
        :reference_code => "#{Order.first.id}"
      )
      assert_instance_of Order, task_barcode.order
      assert_equal Order.first.id, task_barcode.order.coasts_order_id
    end

  end

  context 'queue_items' do

    before(:each) do
      ResultTask.any_instance.stubs(:process => ProcessVersion.output.first)

      @outline_queue = OutlineQueue.find_by_name('service_a')
      @task = @outline_queue.tasks.where(:type => 'ResultTask').last

    end

    it 'should find queue items for a CoastsPlate' do
      task_barcode = TaskBarcode.new(
        :reference_type => 'CoastsPlate',
        :reference_code => 'ABC123',
        :task_id => @task.id
      )
      assert_instance_of ActiveRecord::Relation, task_barcode.queue_items
      assert_instance_of QueueItem, task_barcode.queue_items[0]
      assert_equal 16, task_barcode.queue_items.length

    end

    it 'should find queue items for a CdbSample' do
      task_barcode = TaskBarcode.new(
        :reference_type => 'CdbSample',
        :reference_code => '2',
        :task_id => @task.id
      )
      assert_instance_of ActiveRecord::Relation, task_barcode.queue_items
      assert_instance_of QueueItem, task_barcode.queue_items[0]
      assert_equal 4, task_barcode.queue_items.length
    end

    it 'should find queue items for a CoastsPlateWell' do
      task_barcode = TaskBarcode.new(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'A0001',
        :task_id => @task.id
      )
      assert_instance_of ActiveRecord::Relation, task_barcode.queue_items
      assert_instance_of QueueItem, task_barcode.queue_items[0]
      assert_equal 4, task_barcode.queue_items.length
    end

    it 'should find find multiple queue items for a CoastsPlateWell (when they have the same sample_id)' do
      task_barcode = TaskBarcode.new(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'A0003',
        :task_id => @task.id
      )
      assert_instance_of ActiveRecord::Relation, task_barcode.queue_items
      assert_instance_of QueueItem, task_barcode.queue_items[0]
      assert_equal 4, task_barcode.queue_items.length
    end

    it 'should find queue items for an Order' do
      CoastsOrderPlate.create(
        :id_order_header_ref => "#{Order.last.coasts_order_id}",
        :plate_code => 'ABC123'
      )

      task_barcode = TaskBarcode.new(
        :reference_type => 'Order',
        :reference_code => "#{Order.last.coasts_order_id}",
        :task_id => @task.id
      )
      assert_instance_of ActiveRecord::Relation, task_barcode.queue_items
      assert_instance_of QueueItem, task_barcode.queue_items[0]
      assert_equal 16, task_barcode.queue_items.length
    end

    it 'should set queue items to ready (order completed) state' do

      CoastsOrderPlate.create(
        :id_order_header_ref => "#{Order.last.coasts_order_id}",
        :plate_code => 'ABC123'
      )

      task_barcode = TaskBarcode.create(
        :reference_type => 'Order',
        :reference_code => "#{Order.last.coasts_order_id}",
        :task_id => @task.id,
        :position => 0
      )
      task_barcode.finalise
      task_barcode.queue_items.each { |x| x.set_state(State.find_by_level_no(1)) }

      assert_instance_of ActiveRecord::Relation, task_barcode.queue_items
      assert_instance_of QueueItem, task_barcode.queue_items[0]
      assert_equal 16, task_barcode.queue_items.length
      task_barcode.queue_items.each do |queue_item|
        assert_equal 'delivered', queue_item.state_name
      end

    end

  end

  context 'finalise' do

    before(:each) do
      @task = ResultTask.last
      @task_barcode = TaskBarcode.create!(
        :reference_type => 'Order',
        :reference_code => "#{Order.last.coasts_order_id}",
        :task_id => @task.id,
        :position => 0
      )
    end

    it 'should update the order to completed' do
      @task_barcode.finalise

      @task_barcode.reference.reload
      assert_equal 'Arrived', @task_barcode.reference.status
    end

    it 'should be marked as complete' do
      @task_barcode.finalise
      assert @task_barcode.complete
    end

  end

  it 'should show the important parts of a barcode in to_s' do
    @task = ResultTask.first
    Order.first.coasts_order_id
    @task_barcode = TaskBarcode.new(
      :reference_type => 'Order',
      :reference_code => "#{Order.first.coasts_order_id}",
      :task_id => @task.id,
      :position => 0
    )
    assert_includes @task_barcode.to_s, @task.name
    assert_includes @task_barcode.to_s, 'Order'
    assert_includes @task_barcode.to_s, "#{Order.first.coasts_order_id}"
  end

  context 'duplicated?, duplicates and duplicate summary' do

    # The four fixtures conflict heavily with these tests
    before(:each) do
      TaskBarcode.destroy_all
      CoastsOrderPlate.create(
        :id_order_header_ref => Order.first.coasts_order_id,
        :plate_code => 'ABC123'
      )
    end

    it 'should detect direct duplicate for a CdbSample' do
      @task = ResultTask.first
      TaskBarcode.create!(
        :reference_type => 'CdbSample',
        :reference_code => '1',
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode = TaskBarcode.create!(
        :reference_type => 'CdbSample',
        :reference_code => '1',
        :task_id => @task.id,
        :position => 1
      )
      assert @task_barcode.duplicated?
      assert_equal 1, @task_barcode.duplicates.length
      assert_includes @task_barcode.duplicate_summary, 'it has already been checked in for this task'
    end

    it 'should detect a CoastsPlateWell containing a CdbSample' do
      @task = ResultTask.first
      TaskBarcode.create!(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'A0001',
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode = TaskBarcode.create!(
        :reference_type => 'CdbSample',
        :reference_code => '1',
        :task_id => @task.id,
        :position => 1
      )
      assert @task_barcode.duplicated?
      assert_equal 1, @task_barcode.duplicates.length
      assert_includes @task_barcode.duplicate_summary,
                      'tubes for this sample have already been checked in for this task (A0001)'
    end

    it 'should detect a CoastsPlate containing a CdbSample' do
      @task = ResultTask.first
      TaskBarcode.create!(
        :reference_type => 'CoastsPlate',
        :reference_code => 'ABC123',
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode = TaskBarcode.create!(
        :reference_type => 'CdbSample',
        :reference_code => '1',
        :task_id => @task.id,
        :position => 1
      )
      assert @task_barcode.duplicated?
      assert_equal 1, @task_barcode.duplicates.length
      assert_includes @task_barcode.duplicate_summary,
                      'plates containing this sample have already been checked in for this task (ABC123)'
    end

    it 'should detect an Order containing a CdbSample' do
      @task = ResultTask.first
      TaskBarcode.create!(
        :reference_type => 'Order',
        :reference_code => "#{Order.first.coasts_order_id}",
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode = TaskBarcode.create!(
        :reference_type => 'CdbSample',
        :reference_code => '1',
        :task_id => @task.id,
        :position => 1
      )
      assert @task_barcode.duplicated?
      assert_equal 1, @task_barcode.duplicates.length
      assert_includes @task_barcode.duplicate_summary,
                      "You cannot scan CdbSample:1 for task_flat because order containing this sample has already been checked in for this task (10000)"
    end


    #TODO specs for CdbSample task barcodes which duplicate the other types

    it 'should detect a direct duplicate for a CoastsPlate' do
      @task = ResultTask.first
      TaskBarcode.create(
        :reference_type => 'CoastsPlate',
        :reference_code => 'ABC123',
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode = TaskBarcode.create(
        :reference_type => 'CoastsPlate',
        :reference_code => 'ABC123',
        :task_id => @task.id,
        :position => 0
      )
      assert @task_barcode.duplicated?
      assert_equal 1, @task_barcode.duplicates.length
      assert_includes @task_barcode.duplicate_summary, 'it has already been checked in for this task'
    end

    it 'should detect a direct duplicate for a CoastsPlateWell' do
      @task = ResultTask.first
      TaskBarcode.create(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'A0001',
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode = TaskBarcode.create(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'A0001',
        :task_id => @task.id,
        :position => 0
      )
      assert @task_barcode.duplicated?
      assert_equal 1, @task_barcode.duplicates.length
      assert_includes @task_barcode.duplicate_summary, 'it has already been checked in for this task'
    end

    it 'should detect a direct duplicate for an Order' do
      @task = ResultTask.first
      TaskBarcode.create(
        :reference_type => 'Order',
        :reference_code => "#{Order.first.coasts_order_id}",
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode = TaskBarcode.create(
        :reference_type => 'Order',
        :reference_code => "#{Order.first.coasts_order_id}",
        :task_id => @task.id,
        :position => 0
      )
      assert @task_barcode.duplicated?
      assert_equal 1, @task_barcode.duplicates.length
      assert_includes @task_barcode.duplicate_summary, 'it has already been checked in for this task'
    end

    it 'should detect a CoastsPlate containing a CoastsPlateWell' do
      @task = ResultTask.first
      @task_barcode1 = TaskBarcode.create(
        :reference_type => 'CoastsPlate',
        :reference_code => 'ABC123',
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode2 = TaskBarcode.create(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'A0001',
        :task_id => @task.id,
        :position => 0
      )
      assert @task_barcode1.duplicated?
      assert_equal 1, @task_barcode1.duplicates.length
      assert_includes @task_barcode1.duplicate_summary, 'tubes in this plate have already been checked in for this task'

      assert @task_barcode2.duplicated?
      assert_equal 1, @task_barcode2.duplicates.length
      assert_includes @task_barcode2.duplicate_summary, 'the plate it is in has already been checked in for this task'
    end

    it 'should detect an Order containg a CoastsPlateWell' do
      @task = ResultTask.first
      @task_barcode1 = TaskBarcode.create(
        :reference_type => 'Order',
        :reference_code => "#{Order.first.coasts_order_id}",
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode2 = TaskBarcode.create(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'A0001',
        :task_id => @task.id,
        :position => 0
      )
      assert @task_barcode1.duplicated?
      assert_equal 1, @task_barcode1.duplicates.length
      assert_includes @task_barcode1.duplicate_summary, 'tubes in this order have already been checked in for this task'

      assert @task_barcode2.duplicated?
      assert_equal 1, @task_barcode2.duplicates.length
      assert_includes @task_barcode2.duplicate_summary, 'it is part of an order which has already been checked in for this task'
    end

    it 'should detect a CdbSample which is in a CoastsPlateWell' do
      @task = ResultTask.first
      TaskBarcode.create(
        :reference_type => 'CdbSample',
        :reference_code => '1',
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode = TaskBarcode.create(
        :reference_type => 'CoastsPlateWell',
        :reference_code => 'A0001',
        :task_id => @task.id,
        :position => 0
      )

      assert @task_barcode.duplicated?
      assert_equal 1, @task_barcode.duplicates.length
      assert_includes @task_barcode.duplicate_summary, 'the sample in this well has already been checked in for this task (1)'
    end

    it 'should detect a CdbSample which is in a CoastsPlate' do
      @task = ResultTask.first
      TaskBarcode.create(
        :reference_type => 'CdbSample',
        :reference_code => '1',
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode = TaskBarcode.create(
        :reference_type => 'CoastsPlate',
        :reference_code => 'ABC123',
        :task_id => @task.id,
        :position => 0
      )

      assert @task_barcode.duplicated?
      assert_equal 1, @task_barcode.duplicates.length
      assert_includes @task_barcode.duplicate_summary, 'samples in this plate have already been checked in for this task (1)'
    end

    it 'should detect a CdbSample which is in an Order' do
      @task = ResultTask.first
      TaskBarcode.create(
        :reference_type => 'CdbSample',
        :reference_code => '1',
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode = TaskBarcode.create(
        :reference_type => 'Order',
        :reference_code => "#{Order.first.coasts_order_id}",
        :task_id => @task.id,
        :position => 0
      )

      assert @task_barcode.duplicated?
      assert_equal 1, @task_barcode.duplicates.length
      assert_includes @task_barcode.duplicate_summary, 'samples in this order have already been checked in for this task (1)'
    end

    it 'should detect an order containing a plate' do
      @task = ResultTask.first
      @task_barcode1 = TaskBarcode.create(
        :reference_type => 'Order',
        :reference_code => "#{Order.first.coasts_order_id}",
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode2 = TaskBarcode.create(
        :reference_type => 'CoastsPlate',
        :reference_code => 'ABC123',
        :task_id => @task.id,
        :position => 0
      )

      assert @task_barcode1.duplicated?
      assert_equal 1, @task_barcode1.duplicates.length
      assert_includes @task_barcode1.duplicate_summary, 'plates in this order have already been checked in for this task'

      assert @task_barcode2.duplicated?
      assert_equal 1, @task_barcode2.duplicates.length
      assert_includes @task_barcode2.duplicate_summary, 'it is part of an order which has already been checked in for this task'
    end

  end

  context 'purge unfinalised' do


    it 'should delete barcodes scanned two hours ago which are not finalised' do
      @task = ResultTask.first
      @task_barcode = TaskBarcode.create!(
        :reference_type => 'Order',
        :reference_code => "#{Order.first.coasts_order_id}",
        :task_id => @task.id,
        :position => 0
      )
      @task_barcode.update_attribute :created_at, (Time.now - 2.hours)
      TaskBarcode.purge_unfinalised
      assert_raises ActiveRecord::RecordNotFound do
        @task_barcode.reload
      end
    end

    it 'should delete unfinalised barcodes by this user' do
      User.background_session
      @task = ResultTask.first
      @task_barcode = TaskBarcode.create!(
        :reference_type => 'Order',
        :reference_code => "#{Order.first.coasts_order_id}",
        :task_id => @task.id,
        :position => 0
      )
      TaskBarcode.purge_unfinalised
      assert_raises ActiveRecord::RecordNotFound do
        @task_barcode.reload
      end
    end
  end


end
