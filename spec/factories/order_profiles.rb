FactoryGirl.define do

  factory :order_profile do

    association :outline_queue, factory: :outline_queue
    association :orderer_user, factory: :user
    association :recipient_user, factory: :user
    association :coasts_order_type, factory: :coasts_order_type
    association :coasts_vial_type, factory: :coasts_vial_type

    trait :solid do
      name
      orderer_coasts_opu_id 3407
      purpose_type 'CoastsApiMethod'
      purpose_id 1
      coasts_vial_type_id 3
      rows_first false
      use_internal_recipient true
      dispensary_opu_id 9507
    end

    trait :with_reference_compounds do
      after(:build) do |order_profile|
        a =  FactoryGirl.build_list(:rl_sample, 5)
        order_profile.rl_samples << a
      end
    end

  end

end