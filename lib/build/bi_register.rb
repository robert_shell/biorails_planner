module Build


  class BiRegister

    CRN_MODELS = [CrnAnimalSex, CrnAntibiotics, CrnAntibodyFormat, CrnAntibodyIsotype,
                  CrnCellLine, CrnCofactor,
                  CrnExpressionSystem, CrnExtSupplier,
                  CrnFraction,
                  CrnInhibitor,
                  CrnIsoenzyme,
                  CrnMedium,
                  CrnOrgStruct,
                  CrnPerson, CrnProject, CrnProteinType,
                  CrnRouteOfAdmin,
                  CrnSolvent, CrnSpecies, CrnStrain, CrnSubstrate,
                  CrnTag, CrnTissue]

    MODELS = [Cart,
              CartItem,
              CdbMethod,
              CdbMethod,
              CdbMethodSetting,
              CdbResult,
              CdbSample,
              CoastsApiMethod,
              CoastsConcentrationDim,
              CoastsExternalRecipient,
              CoastsOpu, CoastsOrder,
              CoastsOrderDetail,
              CoastsOrderPlate,
              CoastsOrderType,
              CoastsOrderTypesPlateType,
              CoastsPlate,
              CoastsPlateFormat,
              CoastsPlateLayout,
              CoastsPlateType,
              CoastsPlateWell,
              CoastsStatus,
              CoastsUserPurpose,
              CoastsVialType,
              CoastsVolumeDim,
              DilutionMap,
              DilutionMapSlot,
              DilutionSlot,
              Order,
              OrderItem,
              OrderItemsCoastsDetail,
              OrderProfile,
              OrderProfileSample,
              OrderProfileWeight,
              OutputFileTemplate,
              RlDepartmentLov,
              RlLabLov,
              RlMethod,
              RlMethodBiology,
              RlMethodDescription,
              RlProject,
              RlResult,
              RlResultLink,
              RlSample,
              RlSamplePhysChemProp,
              TaskBarcode]
    #
    # Setup method to register Cdb Methods is biorails catalogue
    #
    def self.add_cdb_methods_lookups_and_parameters
      concept = DataConcept.register('Internal/Method_source')
      e= ModelElement.register(CdbMethod, "CDB Method", concept)
      puts e
      e=ModelElement.register(OutlineMethod, "Biorails Method", concept)
      puts e

      unless ParameterRole.find_by_name('setting')
        ParameterRole.create(
            :name => 'setting',
            :description => 'A constant of an assay, such as a threshold or operating conditions.  Often used for parameters that need to be tracked but should play no part in the analysis.',
            :mode => 0
        )
      end

      lookup_root = DataConcept.register('cdb/lookups')
      parameter_root = DataConcept.register('cdb/method_settings')

      method_settings = (CdbMethod::SETTING_KEYS + RlMethod::SETTING_KEYS + RlMethodBiology::SETTING_KEYS).uniq
      method_settings.each do |setting|
        e = DataElement.where("lower(name) = ? and data_concept_id = ?", setting, lookup_root.id).first
        puts e
        root = (e ? lookup_root : parameter_root)
        t = ParameterType.register(
            setting,
            e ? DataType::DICTIONARY : DataType::TEXT,
            root
        )
        puts t
        a = ParameterTypeAlias.find_by_name(setting)
        a ||= ParameterTypeAlias.create!(
            :name => setting,
            :description => "Generated parameter type for RlMethod property: #{setting}",
            :parameter_type_id => t.id,
            :parameter_role => (ParameterRole.find_by_name('setting') || ParameterRole.first),
            :data_element => e,
            :data_concept_id => root.id,
            :data_format => DataFormat.where(data_type_id: t.data_type_id).first
        )
        puts a
      end

    end

    def self.cleanup_cleanup_cdb_method_values
      User.background_session
      lookup_root = DataConcept.register('cdb/lookups')
      parameter_root = DataConcept.register('cdb/method_settings')
      method_settings = (CdbMethod::SETTING_KEYS + RlMethod::SETTING_KEYS + RlMethodBiology::SETTING_KEYS).uniq

      puts "Catalog Reset \n ------------------------------------------------------"
      method_settings.each do |setting|

        a = ParameterTypeAlias.where(name: setting, data_concept_id: parameter_root.id).first
        e = DataElement.where("lower(name) = ? and data_concept_id = ?", setting, lookup_root.id).first

        if e && a
          pt = a.parameter_type
          pt.data_type_id = DataType::DICTIONARY
          pt.save

          a.data_element_id = e.id
          a.data_format_id = nil
          a.save

          OutlineParameter.where(parameter_type_alias_id: a.id).each do |op|
            op.data_type_id = DataType::DICTIONARY
            op.data_element_id = e.id
            op.data_format_id = nil
            op.parameter_role = ParameterRole.find_by_name('setting')
            op.outline_parameter_values.delete_all
            op.save
            puts "Parameter #{op}"
          end
        end

      end
      puts "Updating Outlines \n ------------------------------------------------------"
      todo = OutlineMethod.where(external_method_type: 'CdbMethod')
      todo.each do |method|
        OutlineMethodParameter.destroy_all(outline_method_id: method.id)
        method.populate_settings(method.external_method.settings)
        puts "Method #{method} num settings #{method.settings.size}"
      end

    end

    def self.add_state_flows
      ::Bi::StateBuilder.setup
    end

    def self.add_default_parameter_types
      Bi::DefaultParameterTypes.setup
    end

    def self.add_crn_lookups
      CrnMySubscription.register_lookup
    end

    def self.add_coasts_lookups
      list = [
          [CoastsOpu, 'OPU'],
          [CoastsOrderType, 'OrderType'],
          [OrderProfile, 'OrderProfile'],
          [CoastsApiMethod, 'ApiMethod'],
          [CoastsConcentrationDim, 'ConcentrationDim'],
          [CoastsPlateFormat, 'PlateFormat'],
          [CoastsPlateType, 'PlateType'],
          [CoastsPlateLayout, 'PlateLayout'],
          [CoastsUserPurpose, 'UserPurpose'],
          [CoastsVialType, 'VialType'],
          [CoastsVolumeDim, 'VolumeDim'],
          [CoastsPlate, 'Plate']
      ]

      puts "Registering COASTS data elements..."
      root = DataConcept.register("coasts/lookups")
      list.each do |element|
        User.background_session
        begin
          puts ModelElement.register(element[0], element[1], root).inspect
        rescue => ex
          puts "Failed #{element[0]} #{ex.message}"
        end
      end
    end

    def self.add_result_class_parameter_types
      root = DataConcept.register("cdb/result_classes")
      RlResultClassLov.all.each do |rec|
        ParameterType.register(rec.name, DataType::TEXT, root)
      end
    end

    def self.add_cdb_lookups
      list = [
          [CdbMethod, 'method'],
          [CdbSample, 'sample'],
          [RlAnimalSexLov, "Animal_Sex"],
          [RlAnimalSpeciesLov, "Animal_Species"],
          [RlAnimalStrainLov, "Animal_Strain"],
          [RlAssayTechLov, "Assay_Tech"],
          [RlAssayTypeLov, "Assay_Type"],
          [RlAtmosphereLov, "Atmosphere"],
          [RlBusinessLinkTypeLov, "Business_Link_Type"],
          [RlCdbInstanceLov, "Cdb_Instance"],
          [RlCellLineLov, "Cell_Line"],
          [RlCofactorLov, "Cofactor"],
          [RlCompartmentLov, "Compartment"],
          [RlConditionLov, "Condition"],
          [RlDepartmentLov, "Department"],
          [RlDeterminationTypeLov, "Determination_Type"],
          [RlEmployeeLov, "Employee"],
          [RlFractionLov, "Fraction"],
          [RlHumidityLov, "Humidity"],
          [RlInhibitorLov, "Inhibitor"],
          [RlIsoenzymeLov, "Isoenzyme"],
          [RlKeywordLov, "Keyword"],
          [RlLabLov, "Lab"],
          [RlLabSampleSourceLov, "Lab_Sample_Source"],
          [RlLightExposureLov, "Light_Exposure"],
          [RlLipidLov, "Lipid"],
          [RlLipophAqPhaseLov, "Lipoph_Aq_Phase"],
          [RlLipophDeviceLov, "Lipoph_Device"],
          [RlLipophOrgPhaseLov, "Lipoph_Org_Phase"],
          [RlMediumLov, "Medium"],
          [RlMethodClassLov, "Method_Class"],
          [RlMtPlateFormatLov, "Mt_Plate_Format"],
          [RlMtPlateTypeLov, "Mt_Plate_Type"],
          [RlNbeTypeLov, "Nbe_Type"],
          [RlOpticalRotSolventLov, "Optical_Rot_Solvent"],
          [RlOpuLov, "OPU"],
          [RlResultClassLov, "Result_Class"],
          [RlResultRatingElemLov, "Result_Rating_Elem"],
          [RlResultRatingGrpLov, "Result_Rating_Grp"],
          [RlRouteOfAdminLov, "Route_Of_Admin"],
          [RlSaltCodeLov, "Salt_Code"],
          [RlSampleFormatLov, "Sample_Format"],
          [RlSampleTypeLov, "Sample_Type"],
          [RlSolubilitySolventLov, "Solubility_Solvent"],
          [RlSubstrateLov, "Substrate"],
          [RlTechLinkTypeLov, "Tech_Link_Type"],
          [RlThermAnalysisLov, "Therm_Analysis"],
          [RlTissueLov, "Tissue"],
          [RlUnitLov, "Unit"],
          [RlVehicleLov, "Vehicle"],
          [RlVendorLov, "Vendor"],
          [RlWavelengthLov, "Wavelength"]
      ]


      puts "Registering CDB data elements..."
      root = DataConcept.register("cdb/lookups")
      list.each do |element|
        begin
          model_element = ModelElement.register(element[0], element[1], root)
          if model_element
            puts model_element
          else
            puts "Failed #{element[0]}"
          end
        rescue => ex
          puts "Failed #{element[0]} #{ex.message}"
        end
      end
    end

  end
end