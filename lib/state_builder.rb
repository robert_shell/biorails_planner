module Biorails
  #
  # Builder for standard state flow for BI inventory
  #
  class StateBuilder

    include ::Bi::DefaultStates

    def StateBuilder.setup
      builder = StateBuilder.new
      builder.create_states
      builder.create_state_flows
    end

    def create_state_flows
      reset_requesting_state_flow
      reset_org_state_flow
      reset_exec_state_flow
    end

    def create_states
      ::Bi::DefaultStates::EXPECTED_STATE_LEVELS.map do |level_no,names|
        names.each do |name|
           State.create_or_update_by_name!({:name => name, :level_no => level_no, :description => "Required NCE state #{name}"})
        end
      end
    end


    def requesting_state_flow
      StateFlow.find_by_name('Requesting') || StateFlow.create!({:name => 'Requesting', :description => 'Queue Items and Requests'})
    end

    def reset_requesting_state_flow
      state_flow = requesting_state_flow
      state_flow.disable_all
      state_flow.state_usages.clear
      state_flow.reload
      puts "Requesting State flow #{state_flow.dom_id}"

      [draft, signoff,
       cancelled,
       hold, pending,
       accepted, ordered, delivered, order_failed,
       planning, ready,
       analysis, review, completed,
       published].each do |state|
        puts "#{state_flow} add #{state}"
        state_flow.state_usages.create!(:state_id => state.id)
      end

      state_flow.enable(draft, signoff)
      state_flow.enable(draft, pending)

      state_flow.enable(signoff, pending)
      state_flow.enable(signoff, cancelled)

      state_flow.enable(pending, accepted)
      state_flow.enable(pending, cancelled)

      state_flow.enable(accepted, ordered)
      state_flow.enable(accepted, planning)
      state_flow.enable(accepted, hold)


      state_flow.enable(hold, accepted)
      state_flow.enable(hold, cancelled)

      state_flow.enable(ordered, order_failed)
      state_flow.enable(ordered, delivered)

      state_flow.enable(delivered, planning)

      state_flow.enable(order_failed, cancelled)
      state_flow.enable(order_failed, hold)

      state_flow.enable(planning, ready)
      state_flow.enable(planning, hold)

      state_flow.enable(ready, analysis)
      state_flow.enable(ready, cancelled)

      state_flow.enable(analysis, ready)
      state_flow.enable(analysis, review)
      state_flow.enable(analysis, cancelled)

      state_flow.enable(review, analysis)
      state_flow.enable(review, completed)
      state_flow.enable(review, published)
      state_flow.enable(review, cancelled)

      state_flow.enable(completed, published)

      ElementType.where(class_name: [Request].collect{|i|i.to_s}).each do |element_type|
        element_type.state_flow_id = state_flow.id
        element_type.save!
        puts " state flow set on #{element_type.dom_id} #{element_type.name}"
      end
      state_flow
    end

    def org_state_flow
      StateFlow.find_by_name('Organization') || StateFlow.create!({:name => 'Organization', :description => 'Outline,Queue,Processes etc'})
    end

    def reset_org_state_flow
      org_flow = org_state_flow
      org_flow.disable_all
      org_flow.state_usages.clear

      puts "Organization State flow #{org_flow.dom_id}"

      [draft, pending, released,validated, cancelled].each do |state|
        puts "#{org_flow} add #{state}"
        org_flow.state_usages.create!(:state_id => state.id)
      end

      org_flow.enable(draft, pending)
      org_flow.enable(draft, released)
      org_flow.enable(draft, cancelled)

      org_flow.enable(pending, released)

      org_flow.enable(released, cancelled)
      org_flow.enable(released, validated)
      org_flow.enable(released, pending)

      org_flow.enable(validated, cancelled)

      org_flow.enable(cancelled, pending)

      ElementType.where(class_name: [Outline, OutlineQueue, OutlineParameter, ProcessDefinition,
                                     ProcessVersion, ProcessStep, RequestTemplate].collect{|i|i.to_s}
      ).each do |element_type|
        element_type.state_flow_id = org_flow.id
        element_type.save!
        puts " state flow set on #{element_type.dom_id} #{element_type.name}"
      end
      org_flow
    end

    def exec_state_flow
      StateFlow.find_by_name('Execution') || StateFlow.create!({:name => 'Execution', :description => 'Result Tasks, Experiments etc'})
    end



    def reset_exec_state_flow
      exec_flow = exec_state_flow
      exec_flow.disable_all
      exec_flow.state_usages.clear

      puts "Execution State flow #{exec_flow.dom_id}"
      [draft,
       planning, ready, hold, cancelled,
       analysis, review, completed,
       published].each do |state|
        puts "#{exec_flow} add #{state}"
        exec_flow.state_usages.create!(:state_id => state.id)
      end

      exec_flow.enable(draft, planning)

      exec_flow.enable(planning, ready)
      exec_flow.enable(planning, cancelled)
      exec_flow.enable(planning, hold)

      exec_flow.enable(hold, planning)
      exec_flow.enable(hold, ready)
      exec_flow.enable(hold, cancelled)

      exec_flow.enable(ready, analysis)
      exec_flow.enable(ready, cancelled)

      exec_flow.enable(analysis, ready)
      exec_flow.enable(analysis, review)
      exec_flow.enable(analysis, cancelled)

      exec_flow.enable(review, analysis)
      exec_flow.enable(review, completed)
      exec_flow.enable(review, published)
      exec_flow.enable(review, cancelled)

      exec_flow.enable(completed, published)
      ElementType.where(class_name: [ResultTask, Experiment].collect{|i|i.to_s}
      ).each do |element_type|
        element_type.state_flow_id = exec_flow.id
        element_type.save!
        puts " state flow set on #{element_type.dom_id} #{element_type.name}"
      end
      exec_flow

    end


  end

end