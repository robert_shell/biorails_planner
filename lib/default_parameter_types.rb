class DefaultParameterTypes

  attr_accessor :parameter_types

  NAMES =%w{sample method slot_type source_plate source_column source_row test_plate test_column}

  # lookups:
  # sample        - CdbSample
  # source_plate  -  CoastsPlate
  # method        -  CdbMethod
  # slot_type - ['samp','std','high','low','blank']
  #
  def self.setup
    current = self.new
    current.parameter_types = []
    current.remove_sample_type_without_lookup  # counteract existing 'Sample' parameter type
    current.create_model_lookup_parameter_types  # model lookups
    current.create_slot_type_parameter_type     # Lookup
    current.create_string_parameter_types       # string
    current.create_source_column_parameter_type # numeric
    current.create_parameter_type_aliases
  end


  def create_model_lookup_parameter_types
    [
        ['source_plate', CoastsPlate],
        ['method', OutlineMethod],
        ['sample', CdbSample]
    ].each do |parameter, model|
      parameter_type = ParameterType.register(parameter, DataType::DICTIONARY, 'BI')
      data_element = ModelElement.register(model, model.to_s, 'BI')
      if parameter_type.aliases.none?
        parameter_type.aliases.create!(
            :name => parameter_type.name,
            :description => "Automatic parameter for experiment planning",
            :parameter_role_id =>  (ParameterRole.where(mode:("sample"==parameter ? 5: 3)).first || ParameterRole.first).id,
            :status => 1,
            :is_auto_default => true,
            :data_concept_id => DataConcept.find_by_name('BI').id,
            :data_element_id => data_element.id
        )
      end
    end
  end



  def create_slot_type_parameter_type
    parameter_type = ParameterType.register('slot_type', DataType::DICTIONARY, 'BI')
    data_element = ListElement.register('slot_type', %w{samp std high low blank}, 'BI')
    if parameter_type.aliases.none?
      parameter_type.aliases.create!(
          :name => parameter_type.name,
          :description => "Automatic parameter for experiment planning",
          :parameter_role_id => (ParameterRole.find_by_name('condition') || ParameterRole.first).id,
          :status => 1,
          :is_auto_default => true,
          :data_concept_id => DataConcept.find_by_name('BI').id,
          :data_element_id => data_element.id
      )
    end
  end

  def create_string_parameter_types
    %w{source_row test_plate test_column}.each do |parameter|
      self.parameter_types << ParameterType.register(
          parameter, DataType::TEXT, 'BI'
      )
    end
  end


  def create_source_column_parameter_type
    %w{source_column }.each do |parameter|
      # Create ParameterType
      self.parameter_types << ParameterType.register(
        parameter, DataType::NUMERIC, 'BI'
      )
    end
  end



  def create_parameter_type_aliases
    self.parameter_types.each do |parameter_type|
      if parameter_type.aliases.none?
        parameter_type.aliases.create!(
            :name => parameter_type.name,
            :description => "Automatic parameter for experiment planning",
            :parameter_role_id => (ParameterRole.find_by_name('condition') || ParameterRole.first).id,
            :is_auto_default => true,
            :status => 1,
            :data_concept_id => DataConcept.find_by_name('BI').id
        )
      end
    end
  end

  #
  # This method is to deal with the incompatible demo_db and fixtures which have a 'Sample'
  # parameter type which is not connected to a sample lookup
  #
  def remove_sample_type_without_lookup
    parameter_type_alias = ParameterTypeAlias.find_by_name('Sample')
    if parameter_type_alias and parameter_type_alias.data_element.nil?
      parameter_type_alias.update_attribute :name, 'XSample'
      parameter_type_alias.parameter_type.update_attribute :name, 'XSample'
    end
  end

end