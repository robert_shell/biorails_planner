module DefaultStates

  EXPECTED_STATE_LEVELS = {-1 => ['cancelled'],
                           0 => ['draft', 'signoff'],
                           1 => ['pending', 'hold'],
                           2 => ['accepted', 'released', 'delivered', 'order_failed', 'ordered', 'planning'],
                           3 => ['validated', 'ready', 'analysis', 'review', 'completed'],
                           4 => ['published']
  }

  EXPECTED_STATE_NAMES = EXPECTED_STATE_LEVELS.values.flatten

  def draft
    @draft ||= State.lookup('draft')
  end

  def cancelled
    @cancelled ||= State.lookup('cancelled')
  end

  def signoff
    @signoff ||= State.lookup('signoff')
  end

  def pending
    @pending ||= State.lookup('pending')
  end

  def hold
    @hold ||= State.lookup('hold')
  end

  def accepted
    @accepted ||= State.lookup('accepted')
  end

  def released
    @released ||= State.lookup('released')
  end

  def delivered
    @delivered ||= State.lookup('delivered')
  end

  def order_failed
    @order_failed ||= State.lookup('order_failed')
  end

  def ordered
    @order ||= State.lookup('ordered')
  end

  def planning
    @planning ||= State.lookup('planning')
  end

  def validated
    @validated ||= State.lookup('validated')
  end

  def ready
    @ready ||= State.lookup('ready')
  end

  def analysis
    @analysis ||= State.lookup('analysis')
  end

  def review
    @review ||= State.lookup('review')
  end

  def completed
    @completed ||= State.lookup('completed')
  end

  def published
    @published ||= State.lookup('published')
  end

  def validate_have_expected_bi_states
    ok = true
    EXPECTED_STATE_NAMES.each do |name|
      State.lookup(name).nil?
      errors.add(:state, "State '#{name}' not registered in system. Automated state changes may not work as expecte")
      ok = false
    end
    ok
  end

end