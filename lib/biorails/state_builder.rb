module Biorails
  #
  # Builder for standard state flow.  This was originally just the state flow for BI, but has been moved into core as it is probably closer to the
  # common pattern than what we had.  This could be overridden from plugins for other customers.
  #
  # https://biorails.org/projects/boehringer-ingelheim-helpdesk/wiki/CONF_State_Flows
  #
  class StateBuilder

    include ::Biorails::DefaultStates

    def StateBuilder.setup
      builder = StateBuilder.new
      builder.create_state_flows
    end

    def create_state_flows
      reset_requesting_state_flow
      reset_org_state_flow
      reset_exec_state_flow
    end

    def requesting_state_flow
      StateFlow.find_by_name('Requesting') || StateFlow.create!({:name => 'Requesting', :description => 'Queue Items and Requests'})
    end

    #
    # See https://biorails.org/projects/boehringer-ingelheim-helpdesk/wiki/StateFlow_Requesting
    #
    def reset_requesting_state_flow
      state_flow = requesting_state_flow
      state_flow.disable_all
      state_flow.state_usages.clear
      state_flow.reload
      puts "Requesting State flow #{state_flow.dom_id}"

      REQUESTING_STATE_LEVELS.each do |level_no, names|
        names.each do |name|
          state = State.create_or_update_by_name!({:name => name,
                                                   :level_no => level_no,
                                                   :description => "Required BI state #{name} for #{state_flow.name}"})
          x = state_flow.state_usages.create(:state_id => state.id,
                                             :is_orderable => level_no==1 ||level_no==2)
          puts "#{state_flow} add #{state} #{level_no} #{x}"
        end
      end

      state_flow.enable(draft, signoff) # requires project approval
      state_flow.enable(draft, pending) # confirmed at new of request wizard
      #state_flow.enable(draft, ready)

      state_flow.enable(signoff, pending) # approved by project manager
      state_flow.enable(signoff, cancelled) # declined by project manager

      state_flow.enable(cancelled, signoff) # Reapprove by project manager

      state_flow.enable(pending, accepted) # added to task
      state_flow.enable(pending, hold) # removed from task to holding pen (normally order problems)
      state_flow.enable(pending, cancelled) # rejected
      state_flow.enable(pending, ordered) # ordered from queue

      state_flow.enable(hold, accepted) # added back into experiment
      state_flow.enable(hold, cancelled) # cancelled after problems

      state_flow.enable(accepted, pending) # removed from experiment
      state_flow.enable(accepted, ordered) # ordered in experiment
      state_flow.enable(accepted, checked_in) # in lab
      state_flow.enable(accepted, ready) #  finalized

      state_flow.enable(ordered, order_failed) # Marked as order failed from coasts
      state_flow.enable(ordered, delivered) # Marked as delivered from coasts
      state_flow.enable(ordered, checked_in) # appears in lab
      state_flow.enable(ordered, hold) # removed from experiment and placed on hold

      state_flow.enable(order_failed, cancelled) # cancelled after problems
      state_flow.enable(order_failed, hold) # removed from experiment and placed on hold

      state_flow.enable(delivered, checked_in) # appears in lab
      state_flow.enable(delivered, hold) # removed from experiment and placed on hold
      #state_flow.enable(delivered, ready)

      state_flow.enable(checked_in, ready) # finalize task
      state_flow.enable(checked_in, hold) # move items out of experiment to hold
      #state_flow.enable(checked_in, cancelled)

      state_flow.enable(ready, analysis)
      #state_flow.enable(ready, cancelled) # cancel experiment (only allow delete of task and items back to pending)

      state_flow.enable(analysis, review) # reivew results
      #state_flow.enable(analysis, ready)   # restart experiment
      #state_flow.enable(analysis, cancelled) # cancel experiment

      state_flow.enable(review, completed) # mark as completed no results
      state_flow.enable(review, published) # move to publish results
      #state_flow.enable(review, analysis)  not allowed to analyse results in problems found
      #state_flow.enable(review, cancelled) not allowed to cancel if there are review fails

      #state_flow.enable(completed, published)

      ElementType.where(class_name: [Request, RequestTask].collect { |i| i.to_s }).each do |element_type|
        element_type.state_flow_id = state_flow.id
        element_type.save!
        puts " state flow set on #{element_type.dom_id} #{element_type.name}"
      end
      state_flow
    end

    def org_state_flow
      StateFlow.find_by_name('Organization') || StateFlow.create!({:name => 'Organization', :description => 'Outline,Queue,Processes etc'})
    end

    #
    # See https://biorails.org/projects/boehringer-ingelheim-helpdesk/wiki/StateFlow_Organisation
    #
    def reset_org_state_flow
      org_flow = org_state_flow
      org_flow.disable_all
      org_flow.state_usages.clear

      puts "Organization State flow #{org_flow.dom_id}"

      ORGANIZATION_STATE_LEVELS.each do |level_no, names|
        names.each do |name|
          state = State.create_or_update_by_name!({:name => name,
                                                   :level_no => level_no,
                                                   :description => "Required BI state #{name} for #{org_flow.name}"})
          x = org_flow.state_usages.create(:state_id => state.id,
                                           :is_orderable => (state.level_no == 1),
                                           :requires_reason => (state.name=='cancelled'))
          puts "#{org_flow} add #{state} #{level_no} #{x}"
        end
      end

      org_flow.enable(draft, editable)

      org_flow.enable(editable, released)
      org_flow.enable(editable, withdrawn)

      org_flow.enable(released, withdrawn)
      org_flow.enable(released, validated)
      org_flow.enable(released, editable)

      org_flow.enable(validated, withdrawn)

      org_flow.enable(withdrawn, pending)

      ElementType.where(class_name: [Outline, OutlineQueue, OutlineParameter, ProcessDefinition,
                                     ProcessVersion, ProcessStep, RequestTemplate].collect { |i| i.to_s }
      ).each do |element_type|
        element_type.state_flow_id = org_flow.id
        element_type.save!
        puts " state flow set on #{element_type.dom_id} #{element_type.name}"
      end
      org_flow
    end

    def exec_state_flow
      StateFlow.find_by_name('Execution') || StateFlow.create!({:name => 'Execution', :description => 'Result Tasks, Experiments etc'})
    end


    # See https://biorails.org/projects/boehringer-ingelheim-helpdesk/wiki/StateFlow_Execution

    def reset_exec_state_flow
      exec_flow = exec_state_flow
      exec_flow.disable_all
      exec_flow.state_usages.clear

      puts "Execution State flow #{exec_flow.dom_id}"
      EXECUTION_STATE_LEVELS.each do |level_no, names|
        names.each do |name|
          state = State.create_or_update_by_name!({:name => name,
                                                   :level_no => level_no,
                                                   :description => "Required BI state #{name} for #{exec_flow.name}"})
          x = exec_flow.state_usages.create(:state_id => state.id,
                                            :is_orderable => (state.level_no == 1),
                                            :requires_reason => (state.name=='cancelled'))
          puts "#{exec_flow} add #{state} #{level_no} #{x}"
        end
      end

      exec_flow.enable(draft, planning)

      exec_flow.enable(planning, ready)
      exec_flow.enable(planning, hold)
      exec_flow.enable(planning, cancelled)

      exec_flow.enable(hold, planning)
      exec_flow.enable(hold, cancelled)

      exec_flow.enable(ready, analysis)
      exec_flow.enable(ready, planning)
#      exec_flow.enable(ready, cancelled)

      exec_flow.enable(analysis, review)
#      exec_flow.enable(analysis, cancelled)

      exec_flow.enable(review, analysis)
      exec_flow.enable(review, completed)
      exec_flow.enable(review, published)
#      exec_flow.enable(review, cancelled)

#      exec_flow.enable(completed, published)
      ElementType.where(class_name: [ResultTask, Experiment].collect { |i| i.to_s }
      ).each do |element_type|
        element_type.state_flow_id = exec_flow.id
        element_type.save!
        puts " state flow set on #{element_type.dom_id} #{element_type.name}"
      end
      exec_flow

    end


  end

end