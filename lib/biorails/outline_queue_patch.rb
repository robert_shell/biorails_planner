module  Biorails::OutlineQueuePatch

  def self.included(base)

    base.send(:include, InstanceMethods)
    #
    # All validation, relations etc
    #
    base.class_eval do
      has_many :order_profiles do
      end

      has_many :orders

      belongs_to :preferred_order_profile, :class_name => 'OrderProfile', :foreign_key => :preferred_order_profile_id

      scope :ordering, joins(:parameter => [:data_element]).where("data_elements.type = 'ModelElement' ")

      scope :ordering_with_edit_right,state_level_no([State::PENDING_LEVEL, State::PROCESSING_LEVEL]).has_right(:action => Role::RIGHT_UPDATE).ordering.visible

    end

  end


  module InstanceMethods

    def orderable?
      false
    end

  end

end