module Biorails::DefaultStates

  REQUESTING_STATE_LEVELS = {-1 => ['cancelled'],
                             0 => ['draft', 'signoff'],
                             1 => ['pending', 'hold',],
                             2 => ['accepted', 'checked_in', 'delivered', 'order_failed', 'ordered'],
                             3 => ['ready', 'analysis', 'review'],
                             4 => ['completed', 'published']
  }

  ORGANIZATION_STATE_LEVELS = {-1 => ['withdrawn'],
                               0 => ['draft'],
                               1 => ['editable'],
                               2 => ['released'],
                               3 => ['validated',]
  }
  EXECUTION_STATE_LEVELS = {-1 => ['cancelled'],
                            0 => ['draft'],
                            1 => ['pending'],
                            2 => ['planning'],
                            3 => ['ready', 'analysis', 'review'],
                            4 => ['completed', 'published']
  }


  EXPECTED_STATE_NAMES = [REQUESTING_STATE_LEVELS.values,
                          ORGANIZATION_STATE_LEVELS.values,
                          EXECUTION_STATE_LEVELS.values].flatten.uniq


  def draft
    @draft ||= State.lookup('draft')
  end

  def cancelled
    @cancelled ||= State.lookup('cancelled')
  end

  def withdrawn
    @cancelled ||= State.lookup('withdrawn')
  end

  def signoff
    @signoff ||= State.lookup('signoff')
  end

  def pending
    @pending ||= State.lookup('pending')
  end

  def editable
    @editable ||= State.lookup('editable')
  end

  def hold
    @hold ||= State.lookup('hold')
  end

  def accepted
    @accepted ||= State.lookup('accepted')
  end

  def released
    @released ||= State.lookup('released')
  end

  #
  # order marked as delivered by external system
  #
  def delivered
    @delivered ||= State.lookup('delivered')
  end

  #
  # Sample physically checked in to experiment
  #
  def checked_in
    @checked_in ||= State.lookup('checked_in')
  end

  def order_failed
    @order_failed ||= State.lookup('order_failed')
  end

  def ordered
    @order ||= State.lookup('ordered')
  end

  def planning
    @planning ||= State.lookup('planning')
  end

  def validated
    @validated ||= State.lookup('validated')
  end

  def ready
    @ready ||= State.lookup('ready')
  end

  def analysis
    @analysis ||= State.lookup('analysis')
  end

  def review
    @review ||= State.lookup('review')
  end

  def completed
    @completed ||= State.lookup('completed')
  end

  def published
    @published ||= State.lookup('published')
  end
  #
  # Check state flow against expected rules for a set type to flow
  #
  def self.valid_flow_with_expected(state_flow,expected)
    warnings =[]
    if state_flow && state_flow.valid?
      expected.each do |level_no, names|
        names.each do |name|
          state = State.lookup(name)
          state_usage = state_flow.get(name)
          if state.nil?
            warnings << "State '#{name}' not registered in system, as expected in BI business rules"
            ok = false
          elsif state.level_no != level_no
            warnings << "State '#{name}' not at expected '#{level_no}' as expected in BI business rules"
            ok = false
          elsif state_usage.nil?
            warnings << "State '#{name}' not registered in state flow #{state_flow}, as  expected in BI business rules"
            ok = false
          end
        end
        ok = false
      end
    end
    warnings
  end

  def validate_have_expected_bi_states
    ok = true
    EXPECTED_STATE_NAMES.each do |name|
      State.lookup(name).nil?
      errors.add(:state, "State '#{name}' not registered in system. Automated state changes may not work as expected")
      ok = false
    end
    ok
  end

end