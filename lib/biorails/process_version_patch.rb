module Biorails::ProcessVersionPatch

    def self.included(base)

      base.extend(ClassMethods)
      base.send(:include, InstanceMethods)
      #
      # All validation, relations etc
      #
      base.class_eval do

        has_many :process_version_file_templates

        has_one :dilution_map

      end
    end

    module ClassMethods

      def without_dilution_map
        ProcessVersion.where('id NOT IN (SELECT DISTINCT(process_version_id) FROM dilution_maps)')
      end

    end

    module InstanceMethods

      def inventory_linked?
        false
      end

      def create_exp_planning_columns
        parameter_type_names = %w{sample method source_plate source_row source_column slot_type test_plate test_column  }

        parameter_type_names.each do |parameter_type_name|
          parameter_type = ParameterType.register(
              parameter_type_name, DataType.find_by_name('numeric').id, 'BI'
          )
          parameter_type_alias = parameter_type.aliases.last
          if parameter_type_alias
            outline_parameter = self.outline.outline_parameters.where(
                :parameter_type_alias_id => parameter_type_alias.id
            ).first
            if outline_parameter.nil?
              outline_parameter = self.outline.outline_parameters.create!(
                  :name => parameter_type_alias.name,
                  :description => parameter_type_alias.description,
                  :parameter_type_id => parameter_type.id,
                  :parameter_type_alias_id => parameter_type_alias.id,
                  :data_type_id => parameter_type.data_type_id,
                  :parameter_role_id => parameter_type_alias.parameter_role_id,
                  :data_element_id => parameter_type_alias.data_element_id,
                  :result_sheet_flag => true
              )
            end
            if self.parameters.where(
                :outline_parameter_id => outline_parameter
            ).none?
              parameter = self.root.add_parameter(outline_parameter)
              parameter.update_attributes(
                  # method will be changeable, the rest should be fixed
                  :fixed => (parameter_type_name != 'method')
              )
            end
          end
        end

      end

    end
end
