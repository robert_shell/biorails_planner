#
# @todo rjs aug/2014 should split 1 job per task to do are not link
#

class Biorails::TaskFinalizeJob < ::Biorails::BackgroundJob

#
# TaskBarcodesController#create
# TaskBarcodesController#finalise_barcodes
#
# This is finishing the inventory check-in process
# * marking orders as complete
# * setting queue items to active
# * filling in the source rows in the task sheet
# * marking task barcode as complete
#
  def finalise_barcodes(options={})
    Task.transaction do
      self.job_id = options[:uid]
      #
      # @TODO pass id, Not sure passing full object will work 100% of time if run on difference thread
      #
      task_barcodes = options[:task_barcodes]
      message("Started barcode check-in #{DateTime.now}")

      return Job.ended(options[:uid], {:status => 'Failed', :error_messages => 'No user_id specified'}) if options[:user_id].blank?
      User.background_session(options[:user_id])

      Biorails::Dba.set_session_info("#{self.class}.finalise_barcodes(#{self.job_id})", "initialize")

      task_barcodes.each do |task_barcode|
        unless task_barcode.complete?
          begin
            tmp_file = File.join(Rails.root, 'tmp', "task_#{task_barcode.task_id}_barcodes.lock")
            Biorails::BackgroundJob.run_block_with_lock(tmp_file) do
              message("[#{::I18n.l(Time.now, :format => :short)}] #{"Starting finalise for #{task_barcode}"}")
              sheet = task_barcode.sheet
              sheet.populate_from_queue
              sheet.save!
              task_barcode.finalise
              message("[#{::I18n.l(Time.now, :format => :short)}] #{"Finishing finalise for #{task_barcode}".ljust(85)}")
            end
          rescue SystemExit => er
            message("Job exited because a duplicate job was found, you may have clicked the button twice. #{::I18n.l(Time.now, :format => :short)}")
          end
        end
      end

      message("Finished barcode check-in #{::I18n.l(Time.now, :format => :short)}")

      Job.ended(
          options[:uid],
          {:status => 'Run ok', :goto_url => options[:success_url]}
      )
    end
  rescue Exception => ex
    logger.error ex.message
    logger.info ex.backtrace.join("\n")
    Rails.logger.error ex.message
    Rails.logger.info ex.backtrace.join("\n")
    Job.ended(options[:uid], {:status => 'Exception', :error_messages => ex.message})
  end


#
# TaskOutputFilesController#create
#
# Here the output file is generated then saved to the folder tree
# * validations on task including task sheet properties
# * liquid gathers data from required models
# * this is formatted according to liquid template
# * the file is saved to it's location in the file tree
# * the task is set to completed
#
  def fill_output_file(options={})
    Task.transaction do
      self.job_id = options[:uid]
      return Job.ended(
          options[:uid],
          {:status => 'Failed', :error_messages => 'No task specified', :goto_url => options[:fail_url]}
      ) if options[:task].blank?

      return Job.ended(
          options[:uid],
          {:status => 'Failed', :error_messages => 'No user_id specified', :goto_url => options[:fail_url]}
      ) if options[:user_id].blank?
      User.current = User.find(options[:user_id])
      task = options[:task]

      ProjectElement.current = folder = task.parent_folder

      Biorails::Dba.set_session_info("#{self.class}.fill_output_file(#{self.job_id})", "initialize")

      return Job.ended(options[:uid], {:status => 'Failed', :error_messages => 'No Process found', :goto_url => options[:fail_url]}
      ) unless task.process

      return Job.ended(options[:uid], {:status => 'Failed', :error_messages => "Folder #{folder} not changeable by #{User.current}", :goto_url => options[:fail_url]}
      ) unless folder and folder.changeable?

      message("Started finalising task  #{::I18n.l(Time.now, :format => :short)}")

      message("[#{::I18n.l(Time.now, :format => :short)}] starting task validation ")
      valid = (task.valid? and task.validate_mandatory_flags)
      message("[#{::I18n.l(Time.now, :format => :short)}] ended task validation ")

      if valid
        message("[#{::I18n.l(Time.now, :format => :short)}] generating output file ")
        project_asset = task.fill_output_file
        message("[#{::I18n.l(Time.now, :format => :short)}] finished generating output file ")
        unless project_asset and project_asset.valid?
          return Job.ended(options[:uid],
                           {
                               :status => 'Failed',
                               :error_messages => "Generated file was invalid: #{project_asset.errors.full_messages.to_sentence}",
                               :goto_url => options[:fail_url]
                           }
          )
        end
      else
        return Job.ended(options[:uid],
                         {
                             :status => 'Failed',
                             :error_messages => "Task is not valid: #{task.errors.full_messages.to_sentence}",
                             :goto_url => options[:fail_url]
                         }
        )
      end

      message("Finished finalising task #{::I18n.l(Time.now, :format => :short)}")

      Job.ended(options[:uid], {:status => 'Run ok', :goto_url => "/project_assets/#{project_asset.id}"})
    end
  rescue Exception => ex
    logger.error ex.message
    logger.info ex.backtrace.join("\n")
    Rails.logger.error ex.message
    Rails.logger.info ex.backtrace.join("\n")
    Job.ended(
        options[:uid],
        {
            :status => 'Exception',
            :error_messages => ex.message,
            :goto_url => options[:fail_url]
        }
    )
  end

#
# ExperimentConsolidationWizardsController#step3_create
#
# Here the output file is generated then saved to the folder tree
# For all selected process_version_file_templates
# * validations on task including task sheet properties
# * liquid gathers data from required models
# * this is formatted according to liquid template
# * the file is saved to it's location in the file tree
# * the task is set to completed
#
#TODO - this method is almost identical to the preceeding one and should be refactored.  It also needs automated tests
  def fill_output_files(options={})
    Task.transaction do
      self.job_id = options[:uid]
      return Job.ended(
          options[:uid],
          {:status => 'Failed', :error_messages => 'No task specified', :goto_url => options[:fail_url]}
      ) if options[:task].blank?

      return Job.ended(
          options[:uid],
          {:status => 'Failed', :error_messages => 'No user_id specified', :goto_url => options[:fail_url]}
      ) if options[:user_id].blank?
      User.current = User.find(options[:user_id])
      task = options[:task]

      ProjectElement.current = folder = task.parent_folder

      Biorails::Dba.set_session_info("#{self.class}.fill_output_file(#{self.job_id})", "initialize")

      return Job.ended(options[:uid], {:status => 'Failed', :error_messages => 'No Process found', :goto_url => options[:fail_url]}
      ) unless task.process

      return Job.ended(options[:uid], {:status => 'Failed', :error_messages => "Folder #{folder} not changeable by #{User.current}", :goto_url => options[:fail_url]}
      ) unless folder and folder.changeable?

      message("Started finalising task  #{::I18n.l(Time.now, :format => :short)}")

      message("[#{::I18n.l(Time.now, :format => :short)}] starting task validation ")
      valid = (task.valid? and task.validate_mandatory_flags)
      message("[#{::I18n.l(Time.now, :format => :short)}] ended task validation ")

      if valid
        task.process_version_file_templates.each do |process_version_file_template|
          message("[#{::I18n.l(Time.now, :format => :short)}] generating output file for #{process_version_file_template.name} ")
          task.process_version_file_template = process_version_file_template
          project_asset = task.fill_output_file
          message("[#{::I18n.l(Time.now, :format => :short)}] finished generating output file for #{process_version_file_template.name} ")
          unless project_asset and project_asset.valid?
            return Job.ended(options[:uid],
                             {
                                 :status => 'Failed',
                                 :error_messages => "Generated file was invalid: #{project_asset.errors.full_messages.to_sentence}",
                                 :goto_url => options[:fail_url]
                             }
            )
          end
        end
      else
        return Job.ended(options[:uid],
                         {
                             :status => 'Failed',
                             :error_messages => "Task is not valid: #{task.errors.full_messages.to_sentence}",
                             :goto_url => options[:fail_url]
                         }
        )
      end

      message("Finished finalising task #{::I18n.l(Time.now, :format => :short)}")

      Job.ended(options[:uid], {
                                 :status => 'Run ok',
                                 :goto_url => "#{options[:success_url]}"
                             })
    end
  rescue Exception => ex
    logger.error ex.message
    logger.info ex.backtrace.join("\n")
    Rails.logger.error ex.message
    Rails.logger.info ex.backtrace.join("\n")
    Job.ended(
        options[:uid],
        {
            :status => 'Exception',
            :error_messages => ex.message,
            :goto_url => options[:fail_url]
        }
    )
  end

end