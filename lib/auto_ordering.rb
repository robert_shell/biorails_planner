class AutoOrdering

  def self.test_run
    x = self.new
    x.run(true)
  end

  def self.ordering_run
    x = self.new
    x.run(false)
  end


  def run(disable_ordering=false)
    begin
      @logger = Logger.new("#{Rails.root}/log/automted_ordering.log")

      run_start_message

      # get all automated dissolution order profiles
      order_profiles = get_order_profiles

      order_profiles.each do |order_profile|
        @order_profile = order_profile
        @logger.info "Checking for dissolution candidates for #{@order_profile.to_s}"

        # build list of dissolution candidates
        @sample_ids = @order_profile.automated_order_candidates
        next if sample_list_depleted?(0)

        remove_sufficient_samples
        next if sample_list_depleted?(1)

        remove_samples_with_active_orders
        next if sample_list_depleted?(2)

        # make the order
        notify_order_content
        if disable_ordering
          @logger.info "    Ordering is disabled for debugging purposes, no orders made."
        else
          create_order
        end
      end

      run_end_message
    rescue => er
      @logger = Logger.new("#{Rails.root}/log/automted_ordering.log")
      @logger.info "ERROR: #{er.message}"
      @logger.info er.backtrace.join("\n")
    end
  end

  #
  # Get all the automated order profiles, currently these can only be dissolution profiles.
  #
  def get_order_profiles
    order_profiles = OrderProfile.joins(:coasts_order_type).where(
      :automated => 1,
      :coasts_order_types => {
        :id_item_type_lov_ref => CoastsOrderType::DISSOLUTION_LIQUID_TYPE
      }
    )
    @logger.info "#{order_profiles.count} automated order profiles found\n"
    order_profiles
  end

  # return the list of sample ids which did NOT find an order item which is active for this coasts_order_type
  #
  # item > order > order_profile > coasts_order_type_id = this coasts order profile_id
  # item > order > coasts_order > status is not in completed_ids or failed_ids
  # sample_id in @sample_ids
  #
  def remove_samples_with_active_orders

    inactive_state_ids = CoastsStatus::COMPLETED + CoastsStatus::FAILED
    @sample_ids.in_groups_of(1000) do |sample_ids|
      order_items = OrderItem.joins(
        :order => [:coasts_order, :order_profile]
      ).where(
        'order_profiles.coasts_order_type_id' => @order_profile.coasts_order_type_id,
        'rl_sample_id' => sample_ids.compact
      ).where(
        CoastsOrder.arel_table[:status_id].not_in inactive_state_ids
      )
      @sample_ids -= order_items.collect { |x| x.rl_sample_id }
    end
  end

  #
  # This serves two purposes
  #
  # 1. As a flag to stop stock check, order check, order creation if there are no candidates left
  # 2. To make a note in the automated ordering log of what is happening in the process
  #
  def sample_list_depleted?(message_code)
    if @sample_ids.none?
      log_no_samples
      true
    else
      case message_code
        when 0
          @logger.info "  #{@sample_ids.count} samples found before stock check or order check."
        when 1
          @logger.info "  #{@sample_ids.count} samples found after stock check, before order check."
        when 2
          @logger.info "  #{@sample_ids.count} samples have insufficient stock and are not in an active restock order."
      end
      false
    end
  end

  #
  # This takes a dissolution order profile and
  #
  def remove_sufficient_samples
    # collect order profiles from same queue
    outline_queue = @order_profile.outline_queue
    check_order_profiles = outline_queue.order_profiles.liquid
    check_order_profiles.each do |check_profile|
      # Stop checking if there are no candidates
      next if @sample_ids.none?
      if check_profile.standard_liquid?
        sufficient_sample_ids = sufficient_for_standard_liquid(check_profile)
      elsif check_profile.custom_liquid?
        sufficient_sample_ids = sufficient_for_custom_liquid(check_profile)
      elsif check_profile.subways?
        sufficient_sample_ids = sufficient_for_subways(check_profile)
      end
      @sample_ids -= sufficient_sample_ids.uniq
    end

    @sample_ids
  end

  # read stock check and see which of the candidates have sufficient stock to be ordered
  #
  # stock check is the hash of stock check results
  # sample ids is the current list of candidate samples.
  #
  # returns an array of sample ids which can be safely ordered
  def sufficient_for_standard_liquid(check_profile)
    sufficient_sample_ids = []
    @sample_ids.in_groups_of(1000) do |sample_ids|
      stock_check = CoastsStockCheck.amount_for_order_profile(check_profile, sample_ids.compact)
      sample_ids.each do |sample_id|
        stock_check_result = stock_check[sample_id]
        if stock_check_result
          if (stock_check_result['liquid_de_ul'] and stock_check_result['liquid_de_count'] > 0) or
            (stock_check_result['liquid_us_ul'] and stock_check_result['liquid_us_count'] > 0)
            sufficient_sample_ids << sample_id
          end
        end
      end
    end
    sufficient_sample_ids
  end

  # read stock check and see which of the candidates have sufficient stock to be ordered
  #
  # stock_check is the hash of stock check results
  # @sample_ids is the current list of candidate samples.
  # required volume is the amount of ul to run a stock check against
  #
  # returns an array of sample ids which can be safely ordered
  def sufficient_for_custom_liquid(check_profile)
    sufficient_sample_ids = []
    required_volume = check_profile.volume
    @sample_ids.in_groups_of(1000) do |sample_ids|
      stock_check = CoastsStockCheck.amount_for_order_profile(check_profile, sample_ids.compact)
      sample_ids.each do |sample_id|
        stock_check_result = stock_check[sample_id]
        if stock_check_result
          if (stock_check_result['liquid_de_ul'] and stock_check_result['liquid_de_ul'] > required_volume) or
            (stock_check_result['liquid_us_ul'] and stock_check_result['liquid_us_ul'] > required_volume)
            sufficient_sample_ids << sample_id
          end
        end
      end
    end
    sufficient_sample_ids
  end

  # read stock check and see which of the candidates have sufficient stock to be ordered
  #
  # As subways stock checks return multiple options, this uses the highest volume soluble tube
  #
  # stock_check is the hash of stock check results
  # @sample_ids is the current list of candidate samples.
  # required volume is the amount of ul to run a stock check against
  #
  # returns an array of sample ids which can be safely ordered
  def sufficient_for_subways(check_profile)
    sufficient_sample_ids = []
    required_volume = check_profile.volume
    @sample_ids.in_groups_of(1000) do |sample_ids|
      stock_check = CoastsStockCheck.amount_for_order_profile(check_profile, sample_ids.compact)
      sample_ids.each do |sample_id|
        stock_check_result = stock_check[sample_id]
        if stock_check_result
          soluble_samples = stock_check_result.select { |x| x['solubility'] == "SOLUBLE" }
          if soluble_samples.any?
            max_result = soluble_samples.collect { |x| x['orderable_volume'] }.max
            if max_result > required_volume
              sufficient_sample_ids << sample_id
            end
          end
        end
      end
    end
    sufficient_sample_ids
  end

  #
  # create an order
  #
  # To be run after stock check, previous order test etc.
  #
  # Takes a list of @sample_ids and creates the order
  def create_order
    order = Order.new(
      :order_profile_id => @order_profile.id,
      :use_internal_recipient => @order_profile.use_internal_recipient,
      :recipient_user_id => @order_profile.recipient_user_id,
      :coasts_external_recipient_id => @order_profile.coasts_external_recipient_id,
      :outline_queue_id => @order_profile.outline_queue_id,
      :automatic => true
    )
    @sample_ids.sort.each_with_index do |sample_id, index|
      order.order_items << order.order_items.new(
        :is_reference_compound => true,
        :rl_sample_id => sample_id,
        :sequence_no => (index + 1)
      )
    end
    order.save!
    order.send_to_coasts
    if order.ordered?
      notify_ordered(order)
    else
      notify_order_failed(order)
      order.destroy
    end

  end

  #
  # An order was made, stick it in the log
  #
  def notify_ordered(order)
    @logger.info "    Order successfully placed, id: #{order.id}, \"name: #{order}\""
  end

  #
  # This order failed
  #
  def notify_order_failed
    @logger.info "    Order failed!"
  end

  #
  # Note the start of an automated ordering run
  #
  def run_start_message
    msg = "\n#{'='*100}\n"
    msg << "Starting automated ordering check @ #{Time.now}\n"
    msg << "#{'='*100}\n"
    @logger.info(msg)
  end

  #
  # Note the end of an automated ordering run
  #
  def run_end_message
    msg = "\n#{'='*100}\n"
    msg << "Ending automated ordering check @ #{Time.now}\n"
    msg << "#{'='*100}\n\n"
    @logger.info(msg)
  end

  #
  # Note that a given order profile has no candidates for an automated order
  #
  def log_no_samples
    @logger.info "  No samples on this queue are out of stock, in the selected state ('#{@order_profile.check_state}') and not subject to a dissolution order."
  end

  def notify_order_content
    @logger.info "    #{@order_profile} has #{@sample_ids.count} candidates"
    @sample_ids.sort.in_groups_of(8).each do |sample_group|
      msg = ""
      sample_group.compact.each_with_index do |sample_id, index|
        msg << sample_id.to_s.ljust(12)
      end
      @logger.info "      #{msg}  "
    end
  end

end