=begin

Builder to create a Cascade tier level result pivot query.

This need to pull results and properties in from various services linked to the tier.

Need add columns with the following patterns :-

* request_item
* request_item.request
* cascade_tier
* cascade_iter.cascade
* cdb_sample
* queue_items(process_version) pivoted on process_version_id (may need also pivot via outline_method_id)
* queue_items(process_version).outline_method(outline_method_id) pivoted by outline_method_id
* queue_items(process_version).request_row
* queue_items(process_version).result_row.task_items(parameter)
* queue_items(process_version).request_row
* queue_items(process_version).result_row.task_items(parameter)
* queue_items(process_version).cdb_results(method)
* queue_items(process_version).cdb_sample

The pivot is via process_version, and outline_method

Base columns direct from tier item

* request details, tier_items -(FK)-> request_items -(FK)-> requests
* material, tier_item -> data_id -> 3rd party table

Then pivot column per service :-

* queue item, tier_items -(FK)-> queue_items( for set outline_queue_id or process_version_id)
* task results, tier_items -(FK)-> queue_items(= :outline_queue_id) (result_task_context_id)-> task_items (= :parameter_id)
* request properties, tier_items -(FK)-> queue_items(= :outline_queue_id) (request_task_context_id)-> task_items ( = :parameter_id)

Options

1) pivot using case and group by to separate queue_item etc
2) Add conditional joins to query to can join queue_item once per service

Have been looking to implement 2 and allow custom join information to be added for to FK based joins generated from rails meta data
Would need for each column added to link


* A Query container many relations
* A relation has a alias and parent
* join style (inner,left,right,outer)
* Each relation projects many columns

=end
module QueryEngine
  class CdbCascadeTierQueryBuilder < ::QueryEngine::ModelQueryBuilder

    attr_accessor :cascade_tier
    attr_accessor :outline_queues
    attr_accessor :model

    def self.register
      ::QueryEngine.register_builder(::QueryEngine::CdbCascadeTierQueryBuilder, {:instances => CascadeTier})
    end

    def initialize(options={})
      super(options)
      if options[:source]
        self.cascade_tier = CascadeTier.find(options[:source])
        self.id = self.cascade_tier.dom_id
      else
        raise("Not :source=> CascadeTier instance passed into query builder ")
      end
      self.model = TierItem
    end

    def dom_id=(value)
      self.cascade_tier = ModelExtras.from_dom_id(value)
    end

    def dom_id
      self.cascade_tier.dom_id
    end

    def object
      self.cascade_tier
    end

    def cascade
      self.cascade_tier.cascade if self.cascade_tier
    end

    #
    # Categories of Data allowed, generally request linked data and service results
    #
    def allowed_categories()
      [['Request', 'request']].concat(cascade_tier.cascade_services.collect { |i| [i.path, i.id] })
    end

    COASTS_ORDER_TYPE_IDS = [1, 2, 3, 5, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 21, 22, 23]

    #
    # Allowed sources generally the core request tables and linked proceses
    #
    #  ['North America',
    #   [['United States','US'],'Canada']],
    #    ['Europe',
    #     ['Denmark','Germany','France']]
    #   ]
    #
    def allowed_sources(category)
      case category.to_s
        when 'request'
          [
              ['Tier', [['Request Details', 'request'], ['Item', 'tier_item']]]
          ]
        when /^([0-9]*)$/
          cascade_service = cascade_tier.cascade_services.find_by_id(category)

          if cascade_service
            process_version = cascade_service.process_version
            outline_queue = cascade_service.outline_queue
            results = []
            props = []
            stock_source =[]
            cdb_results = []
            cols = [[QueueItem.model_name.human, process_version.dom_id('todo')],
                    [OutlineQueue.model_name.human, outline_queue.dom_id('queue')]]
            begin
              process_version.template.contexts.each do |i|
                cols << ["#{QueueItem.model_name.human} (#{i.outline_method.path})", i.dom_id('qi')]
              end
              process_version.template.contexts.each do |i|
                cols << ["#{OutlineMethod.model_name.human} (#{i.outline_method.path})", i.dom_id('om')]
              end
              results = cascade_service.outline_queue.processes.output.collect { |i| [i.path, i.dom_id] }
              props << [process_version.path, process_version.dom_id]
              cdb_results = outline_queue.outline_method_queues.where(
                  :source_type => 'CdbMethod'
              ).collect { |i| [" #{i.path}", i.dom_id('rslt')] }.compact
            rescue => ex
              Rails.logger.debug ex.backtrace.join("\n")
              Rails.logger.info ex.message
            end
            outline_queue.order_profiles.each do |order_profile|
              stock_source << ["Type #{order_profile.coasts_order_type_id}: #{order_profile.name} ",
                               "tier_item.queue_items(process_version_id=#{process_version.id}).cdb_sample.stocks_ot#{order_profile.coasts_order_type_id}"]
            end
            [
                ["Service #{process_version.path}", cols],
                ["Stock Levels", stock_source],
                ["Properties", props],
                ["Experiments", results],
                ["Cdb Results", cdb_results]
            ]

          else
            Rails.logger.error "Error on CdbCascadeTierQueryBuilder.allowed_source(#{category}), no matching queue "
            []
          end
        else
          Rails.logger.error "Error on CdbCascadeTierQueryBuilder.allowed_source(#{category}), can't handle '#{category}' "
          []
      end
    end

    def process_version_columns(sub_model, process_version)
      if sub_model == '_todo'
        model_columns(QueueItem,
                      :path => "tier_item.queue_items(process_version_id=#{process_version.id}).",
                      :reference_id => process_version.id,
                      :reference_type => process_version.class.to_s,
                      :group => process_version.path)
      elsif sub_model == '_proc'
        model_columns(ProcessVersion,
                      :path => "tier_item.queue_items(process_version_id=#{process_version.id}).process_version",
                      :reference_id => process_version.id,
                      :reference_type => process_version.class.to_s,
                      :group => process_version.path)
      else
        parameter_columns(process_version)
      end
    end

    def outline_method_columns(sub_model, outline_method)
      cdb_method = outline_method.external_method
      if cdb_method && cdb_method.is_a?(CdbMethod)
        cdb_method.results_column_options(
            "tier_item.queue_items(outline_queue_id=#{outline_method.outline_queue_id}).cdb_results(id_method_ref=#{cdb_method.id}).")
      end
    end

    def outline_method_queue_columns(sub_model, outline_method_queue)
      method = outline_method_queue.outline_method
      cdb_method = outline_method_queue.source
      if method
        base_path = "tier_item.queue_items(outline_queue_id=#{outline_method_queue.outline_queue_id} and outline_method_id=#{outline_method_queue.outline_method_id})."
        if sub_model == '_todo'
          model_columns(QueueItem,
                        :path => base_path,
                        :reference_id => outline_method_queue.id,
                        :reference_type => outline_method_queue.class.to_s,
                        :group => outline_method_queue.path)
        elsif sub_model == '_meth'
          model_columns(OutlineMethod,
                        :path => "#{base_path}.outline_method(outline_method_id=#{outline_method_queue.outline_method_id}).",
                        :reference_id => outline_method_queue.id,
                        :reference_type => outline_method_queue.class.to_s,
                        :group => outline_method_queue.path)

        elsif cdb_method && cdb_method.is_a?(CdbSample)
          cdb_method.results_column_options("#{base_path}.cdb_results(id_method_ref=#{cdb_method.id}).")
        end
      end
    end

    #
    # Queue Items
    #
    def outline_queue_columns(sub_model, outline_queue)
      if sub_model == '_todo'
        model_columns(QueueItem,
                      :path => "tier_item.queue_items(outline_queue_id=#{outline_queue.id}).",
                      :reference_id => outline_queue.id,
                      :reference_type => instance.class.to_s,
                      :group => outline_queue.path)
      else
        model_columns(outline_queue.class,
                      :path => "tier_item.queue_items(outline_queue_id=#{outline_queue.id}).queue.",
                      :reference_id => outline_queue.id,
                      :reference_type => outline_queue.class.to_s,
                      :group => outline_queue.path)
      end
    end

    #
    # columns linked to process/method row from template
    #
    def task_context_columns(sub_model, task_context)
      if sub_model == '_qi'
        model_columns(QueueItem,
                      :path => "tier_item.queue_items(template_task_context_id=#{task_context.id}).",
                      :reference_id => task_context.id,
                      :reference_type => task_context.class.to_s,
                      :group => task_context.process.path)
      elsif sub_model == '_om'
        model_columns(OutlineQueue,
                      :path => "tier_item.queue_items(template_task_context_id=#{task_context.id}).outline_method",
                      :reference_id => task_context.id,
                      :reference_type => task_context.class.to_s,
                      :group => task_context.process.path)
      end

    end

    #
    # Allowed columns for the current source (table or process)
    #
    def allowed_columns(source)
      return [] if source.blank?
      case source
        when 'request'
          model_columns(Request, :path => 'request_item.request.', :group => 'Request Details')
        when 'tier_item'
          model_columns(TierItem, :path => '', :group => 'Tier Item')
        when 'material'
          model_columns(cascade.data_element.model,
                        :path => "#{cascade.data_element.model}.",
                        :reference_id => cascade.id,
                        :reference_type => cascade.class.to_s)
        when ModelExtras::DOM_ID_REGEX
          sub_model = $3
          instance = ModelExtras.from_dom_id(source)
          case instance
            when ProcessVersion
              process_version_columns(sub_model, instance)

            when OutlineMethod
              outline_method_columns(sub_model, instance)

            when OutlineMethodQueue
              outline_method_queue_columns(sub_model, instance)

            when OutlineQueue
              outline_queue_columns(sub_model, instance)

            when TaskContext
              task_context_columns(sub_model, instance)

            else
              allowed_model_columns(source)

          end
        else
          allowed_model_columns(source)
      end
    rescue => ex
      Rails.logger.debug ex.backtrace.join("\n")
      Rails.logger.info ex.message
      []
    end


    def query
      @query ||= ::QueryEngine.query('tier_item.results', :cascade_tier => self.cascade_tier)
    end


    protected

    def update
      super
      cascade_tier.query = self.query
      cascade_tier.save
      setup
    end

    #---------------------------------------------------------------------------------------------------------------------
    # Internals
    #---------------------------------------------------------------------------------------------------------------------
    #
    # Post initialize setup query, columns and basic of
    #
    def setup
      unless self.cascade_tier.nil?
        self.query = self.cascade_tier.query
        self.columns = self.query.columns.all.collect { |i| {:label => i.label,
                                                             :group => i.group_name,
                                                             :name => i.name,
                                                             :path => i.source_text,
                                                             :reference_id => i.reference_id,
                                                             :reference_type => i.reference_type} } unless query.nil?
        self.outline_queues = self.cascade_tier.outline_queues
      end

      if self.query.nil? || self.columns.nil? || self.outline_queues.nil? || self.cascade_tier.nil?
        raise "Can't initialize Builder. Tier is not fully defined"
      end
    end

    #
    # List of all parameters for a process
    #
    def parameter_columns(process)
      items = []
      outline_queue = process.outline_queues.first
      process.parameters.sort { |a, b| a.name <=> b.name }.each do |parameter|
        path = if process.output?
                 "request_item.queue_items(outline_queue_id=#{outline_queue.id}).result_row.task_items(parameter_id=#{parameter.id}).data_content"
               else
                 "request_item.queue_items(outline_queue_id=#{outline_queue.id}).request_row.task_items(parameter_id=#{parameter.id}).data_content"
               end
        items << {:id => parameter.dom_id,
                  :group => process.path,
                  :reference_type => parameter.class.to_s,
                  :reference_id => parameter.id,
                  :name => "#{process.path}_#{parameter.name}".downcase.gsub(/\W/, ''),
                  :label => "#{parameter.name}",
                  :path => path}
      end
      items
    end

  end
end