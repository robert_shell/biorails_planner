module QueryEngine

  class OutlineQueueQueryBuilder < ::QueryEngine::OutlineQueueQueryBuilder

    attr_accessor :rl_method

    def self.register
      QueryEngine.register_builder(::QueryEngine::CdbOutlineQueueQueryBuilder, {:instances => ::OutlineQueue})
    end

    #
    # model: name of model to use as first category
    # name: name to use when creating a new query
    # subject: subject area for created query
    # query: Source query we are editing
    #
    def initialize(options={})
      super(options)
      self.max_depth = 2
      self.outline_queue = options[:source]
      self.model = QueueItem
      self.query = options[:query]
    end


    #---------------------------------------------------------------------------------------------------------------------
    # Internals
    #---------------------------------------------------------------------------------------------------------------------
    #
    # Post initialize setup query, columns and basic of
    #
    def setup
      self.query ||= ::QueryEngine.query('outline_queue.cdb_results', :outline_queue => outline_queue)
      self.columns = self.query.columns.all.collect { |i| {:label => i.label,
                                                           :group => i.group_name,
                                                           :name => i.name,
                                                           :path => i.source_text,
                                                           :reference_id => i.reference_id,
                                                           :reference_type => i.reference_type} }
      if self.query.nil? || self.columns.nil?
        raise "Can't initialize Builder. No model defined"
      end
      self.columns
    end

    COASTS_ORDER_TYPE_IDS = [1, 2, 3, 5, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 21, 22, 23]

    def allowed_queue_item_sources()
      props =[]
      results =[]
      stock_source =[]
      outline_queue.processes.collect do |i|
        if i.output?
          results << [i.path, i.dom_id]
        else
          props << [i.path, i.dom_id]
        end
      end
      methods = []
      begin
        methods =outline_queue.outline_methods.where(
            "outline_queues.external_method_type='CdbMethod' and outline_queues.external_method_id is not null"
        ).all.compact.collect { |i| ["#{i}", i.dom_id] }
      rescue => ex
        methods =outline_queue.outline_methods.all.select { |i| i.external_method }.compact.collect { |i| [" #{i}", i.dom_id] }
      end
      outline_queue.order_profiles.each do |order_profile|
        stock_source << ["Type #{order_profile.coasts_order_type_id}: #{order_profile.name} ",
                         "queue_item.cdb_sample.stocks_ot#{order_profile.coasts_order_type_id}"]
      end
      unused = COASTS_ORDER_TYPE_IDS - outline_queue.order_profiles.collect { |i| i.coasts_order_type_id }.uniq

      unused.each do |i|
        stock_source << ["Type #{i}: Unused stock source", "queue_item.cdb_sample.stocks_ot#{i}"]
      end

      [
          ["Queued Item", [
                            ['Details', 'queue_items()'],
                            ['Template', 'queue_item.template_row'],
                            ['Method', 'queue_item.template_row.outline_method'],
                            ['Service', 'queue_item.template_row.definition.process'],
                            ['State', 'queue_item.state'],
                            ['Order', 'queue_item.order']
                        ]],
          ["Stock Levels", stock_source],
          ["Properties", props],
          ["Experiments", results],
          ["Cdb Results", methods]
      ]
    end

    def allowed_dom_id_columns(dom_id)
      if /(^.*)_([0-9]+)(.*)$/ =~ dom_id
        instance = ModelExtras.from_dom_id(dom_id)
        case instance
          when ProcessVersion
            parameter_columns(instance)
          when OutlineMethod
            method = instance.external_method
            if method
              method.results_column_options("queue_item.cdb_results(id_method_ref=#{method.id})")
            end
          else
            []
        end
      else
        []
      end
    rescue Exception => ex
      Rails.logger.info ex.backtrace.join("\n")
      Rails.logger.warn ex.message
      return []
    end


  end

end