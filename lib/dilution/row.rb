# This is an array for the resulting row for dissolution plates
#
# It is an array with a customised capacity attribute, and a few methods to
# query where it is in relation to it's capacity
#


module Dilution

  class Dilution::Row < Array

    attr_accessor :capacity

    def Row.create(array, capacity)
      x = Dilution::Row.new(array)
      x.capacity = capacity

      if x.length > x.capacity
        x.throw_capacity_error
      end

      x
    end

    def wells
      self
    end

    def full?
      if self.length > self.capacity
        throw_capacity_error
      end
      self.length == self.capacity
    end

    # Can I add 'required' to this row without exceeding capacity?
    def has_empty_wells?(required)
      return false if self.full?
      (self.capacity - self.length) >= required
    end

    # How many empty wells does this have?
    def empty_wells
      self.capacity - self.length
    end

    def summary
      wells.collect { |x| x.to_s }.join(', ')
    end

    def throw_capacity_error
      puts "Dilution row is beyond capacity (#{self.capacity}):\n#{self.summary}"
      raise RuntimeError, "Dilution row is beyond capacity (#{self.capacity}):\n#{self.summary}"
    end

  end

end

