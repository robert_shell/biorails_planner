# This class runs the algorithms, it has 2 parts:
#
# * Create example plates
# * Run algorithms

module Dilution

  class Dilution::Runner

    attr_accessor :source_rows
    attr_accessor :dilution_rows
    attr_accessor :dilution_row_length
    attr_accessor :task_id

    #
    # initialise arguments:
    #
    # :plate_codes    - a list of barcodes from CoastsPlate.plate_code (including those extracted from orders)
    # :well_codes     - a list of barcodes from CoassPlateWell.plat_tube_barcode
    # :length         - the length of the output rows
    # :source_by_row  - read the plates by row or by column (boolean to match DilutionMap)
    # :task_id        - Attaches the results back into this task at the end
    #
    def initialize(args={})
      self.source_rows = []
      self.dilution_rows = []
      self.dilution_row_length = args[:length]
      self.task_id = args[:task_id]
      get_source_rows(args[:plate_codes], args[:well_codes], args[:sample_ids], args[:source_by_row])
    end

    #
    # Initialise the source rows for the consolidation algorithm
    #
    # * plate_codes identify CoastsPlates which are then split in to rows
    # * well_codes identify CoasstsPlateWells, which are treated as individual rows
    # * sample_ids identify CdbSamples, which are treated as individual rows
    #
    def get_source_rows(plate_codes, well_codes, sample_ids, source_by_row)
      self.source_rows = []
      plate_codes.each do |plate_code|
        coasts_plate = CoastsPlate.find(plate_code)
        rows = source_by_row ? coasts_plate.coasts_plate_wells.rows : coasts_plate.coasts_plate_wells.columns
        rows.each { |row| self.source_rows << row }
      end
      well_codes.each do |well_code|
        coasts_plate_well = CoastsPlateWell.find_by_plat_tube_barcode(well_code)
        self.source_rows << [coasts_plate_well] if coasts_plate_well
      end
      sample_ids.each do |sample_id|
        cdb_sample = CdbSample.find_by_sample_id(sample_id)
        self.source_rows << [cdb_sample] if cdb_sample
      end
    end

    def display_source_rows
      result = ""
      self.source_rows.each_with_index do |source_row, idx|
        result << "Result Row #{idx}\n"
        result << "#{source_row.collect { |well| well.to_s }.join(', ')}\n\n"
      end
      puts result
      result
    end

    def display_dilution_rows
      result = ""
      self.dilution_rows.each_with_index do |row, idx|
        result << "Dilution row #{idx}:\n"
        result << "#{row.summary}\n\n"
      end
      puts result
      result
    end

    # Described in slide 5 of the plate consolidation use cases ppt
    # "Always Split" - option
    def run_always_split
      # all of the samples in all the plates in a single array
      self.source_rows.flatten.each_slice(self.dilution_row_length) do |dilution_row|
        self.dilution_rows << Dilution::Row.create(dilution_row, self.dilution_row_length)
      end
    end

    # Described in slide 4 of the plate consolidation use cases ppt
    # "Never Split" - option
    def run_never_split
      # All the rows with a length more than one
      self.source_rows.select { |x| x.length > 1 }.each do |row|
        self.dilution_rows << Dilution::Row.create(row, self.dilution_row_length)
      end
      # "Fill gaps in assay plates with single compounds"
      self.source_rows.select { |x| x.length == 1 }.each do |row|
        find_space_for_row(row)
      end
    end

    # Described in slide 2 of the plate consolidation use cases ppt
    # "Complete Fit / Re-Order Within Group" - Option
    def run_reorder_within_group
      # all the rows in all the plates
      self.source_rows.each do |row|
        find_space_for_row(row)
      end
    end

    # Note Reorder always will not, now, be used.
    # Instead reorder within group will
    # I'm keeping this in case there is any arguments

    # Described in slide 3 of the plate consolidation use cases ppt
    # "Complete Fit / Re-Order Always" - Option
=begin
    def run_reorder_always
      # all of the samples in all the plates in a single array
      self.dilution_rows = []
      rows = self.plates.flatten(1)
      # All the rows with a length more than one
      until rows.select { |x| x.length > 1 }.empty?
        row = rows.select { |x| x.length > 1 }[0]
        # adds this to first available row, returns row
        dilution_row = Dilution::Row.create(row, self.dilution_row_length)
        self.dilution_rows << dilution_row
        rows.delete(row)

        n = dilution_row.empty_wells
        until n == 0 or dilution_row.full?
          row = rows.select { |x| x.length == n }[0]
          if row
            dilution_row.concat(row.flatten)
            n = dilution_row.empty_wells
            rows.delete(row)
          end
          n -= 1 if rows.select { |x| x.length == n }.none?
        end

      end
      # "Fill gaps in assay plates with single compounds"
      rows.select { |x| x.length == 1 }.each do |row|
        find_space_for_row(row)
      end
    end
=end


    def find_space_for_row(row)
      # If there's no suitable rows, make a new one
      row_with_space = self.dilution_rows.select { |x| x.has_empty_wells?(row.length) }[0]
      if row_with_space
        row_with_space.concat(row)
      else
        # There's no rows with space, start a new one for this...
        x = Dilution::Row.create(row, self.dilution_row_length)
        self.dilution_rows << x
        x
      end
    end

    def create_dilution_slots
      if task_id and dilution_rows.any?
        Task.transaction do
          task = Task.find(task_id)
          task.dilution_slots.destroy_all
          dilution_map = task.dilution_map
          sample_slots = dilution_map.dilution_map_slots.sample
          non_sample_slots = dilution_map.dilution_map_slots.non_sample

          dilution_rows.each_with_index do |dilution_row, row_idx|
            dilution_row.each_with_index do |content, well_idx|
              if content.is_a?(CoastsPlateWell)
                column_no = sample_slots[well_idx].position
                task.dilution_slots.create!(
                  :row_no => row_idx,
                  :column_no => column_no,
                  :sample_id => content.cdb_sample.sample_id,
                  :plate_code => content.plate_code,
                  :plate_row => content.plate_row,
                  :plate_col => content.plate_col,
                  :slot_type => 'samp'
                )
              else
                column_no = sample_slots[well_idx].position
                task.dilution_slots.create!(
                  :row_no => row_idx,
                  :column_no => column_no,
                  :sample_id => content.sample_id,
                  :slot_type => 'samp'
                )
              end
            end
            non_sample_slots.each do |dilution_map_slot|
              task.dilution_slots.create!(
                :row_no => row_idx,
                :column_no => dilution_map_slot.position,
                :sample_id => (dilution_map_slot.cdb_sample ? dilution_map_slot.cdb_sample.sample_id : nil),
                :slot_type => dilution_map_slot.slot_type
              )
            end
          end
        end
      end
    end

  end

end