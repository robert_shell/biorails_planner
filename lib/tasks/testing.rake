unless Rails.env == 'production'

  require 'rspec/core/rake_task'

  namespace :test do

    desc 'Run all plugin biorails_planner specs '
    RSpec::Core::RakeTask.new(:biorails_plannerx) do |task|
      task.fail_on_error = true
      task.pattern = ['vendor/plugins/biorails_planner/spec/**/*_spec.rb',
                      'vendor/plugins/biorails_planner/spec/*_spec.rb']
    end

  end
end

