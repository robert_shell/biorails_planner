class QueueItemStateObserver < ActiveRecord::Observer

  include ::Biorails::DefaultStates # Get states

  observe QueueItem

  #
  # Update Summary from state + coasts order and other actions
  # Update state from changes task_id,order_ref
  # Set -1/0/1 status
  def before_save(queue_item)
    queue_item.status_summary = detect_last_action_from(queue_item) unless queue_item.changed.include?("status_summary")
    queue_item.state = detect_state_from(queue_item) unless queue_item.changed.include?("state_id")
    queue_item.status = Status.from_state(queue_item.state)
  end

  def before_destroy(queue_item)
    queue_item.status_summary = "Deleted by #{User.current}"
    queue_item.state_id = cancelled.id unless queue_item.changed.include?("state_id")
    queue_item.status = Status.from_state(hold)
  end

  #
  # Update request.state to when late queue_item is moved of frozen/published or ignored level
  #
  # Added trap to will only run for cascade once 1 item in every tier.
  #
  #
  def after_save(queue_item)
    if (not queue_item.changed.include?("state_id"))
      return(true)
    end
    calling_trigger = Biorails.set_current_observer(self)
    begin
      Rails.logger.debug("QueueItemStateObserver firing on #{queue_item.name} #{queue_item.dom_id}")
      # Logger for state changes
      # there should be an Oracle trigger, see 4.5
      QueueItemHistory.add_change(queue_item) if (Rails.env.test? || Rails.env.development?) && !Biorails::Check.oracle?

      if calling_trigger.nil?
        update_request_summary_state(queue_item) if queue_item.request_id
        update_task_summary_state(queue_item) if queue_item.task_id
      end

    rescue => e
      Rails.logger.debug e.backtrace.join("\n")
      Rails.logger.info "ERROR: Was working on #{queue_item}.after_save, error #{e.message}"

    ensure
      Biorails.set_current_observer(calling_trigger)
      return true # a working queue_item is more important than this automation
    end

  end

  #
  # See if the item is only partially filled in or still active
  #
  def ignore_some_business_object?(business_object, queue_item)
    (business_object.nil? or
        queue_item.nil? or
        business_object.published? or
        business_object.cancelled? or
        business_object.queue_items.size == 0
    queue_item.state.active? or
        queue_item.state == business_object.state or
        (queue_item.state.level_no >= 0 and
            queue_item.state.level_no < business_object.state.level_no))
  end

  #
  # Update request to match queue_items
  #
  # 1) All cancelled -> cancel task
  # 2) Update to lowest live level
  #
  def update_request_summary_state(queue_item)
    request = queue_item.request
    return(true) if request.nil? || ignore_some_business_object?(request, queue_item) || request.has_empty_cascade_services?

    folder = request.project_element
    max_level_request_queue_item = request.queue_items.max_state_level.first
    min_live_request_queue_item = request.queue_items.min_state_level.first

    if min_live_request_queue_item and
        min_live_request_queue_item.state.level_no >= State::FROZEN_LEVEL and
        !folder.must_sign_change?(min_live_request_queue_item.state)

      folder.set_state(min_live_request_queue_item.state)

    elsif max_level_request_queue_item.state.level_no <= State::CANCELLED_LEVEL

      folder.set_state(max_level_request_queue_item.state)
    end

  end

  #
  # Update task to match queue_items
  #
  # 1) All cancelled -> cancel task
  # 2) Update to lowest live level
  #
  def update_task_summary_state(queue_item)
    result_task = queue_item.task
    return(true) if result_task.nil? || ignore_some_business_object?(result_task, queue_item)

    folder = result_task.project_element
    max_level_request_queue_item = result_task.queue_items.max_state_level.first
    min_live_request_queue_item = result_task.queue_items.min_state_level.first

    if min_live_request_queue_item.state.level_no >= State::FROZEN_LEVEL and !folder.must_sign_change?(min_live_request_queue_item.state)

      folder.set_state(min_live_request_queue_item.state)

    elsif max_level_request_queue_item.state.level_no <= State::CANCELLED_LEVEL

      folder.set_state(max_level_request_queue_item.state)
    end

  end


  #
  # New Ideal to have a status text based on state + order_ref +
  #
  def detect_state_from(queue_item)
    new_state = queue_item.state

    if queue_item.changed.include?("order_ref")
      new_state = queue_item.order_ref ? ordered : order_failed
    end

    if queue_item.changed.include?("task_id")
      new_state = queue_item.task_id ? (queue_item.order_ref ? ordered : accepted) : hold
    end

    new_state
  end

  #deprecated, as probably no longer of value.  Originally (before had its own state flow) intended to provide a better understanding
  #of what was happening as things changed state - better state flow probably superceeds this
  def detect_last_action_from(queue_item)
    new_status = queue_item.status_summary

    if queue_item.changed.include?("order_ref")
      new_status = queue_item.order_ref ? "order '#{queue_item.order_ref}' in progress" : 'order removed'
    end

    if queue_item.changed.include?("task_id")
      new_status = queue_item.task_id ? 'added to task' : 'removed from task'
    end

    if queue_item.changed.include?("result_task_context_id")
      new_status = queue_item.result_task_context_id ? 'added to worksheet row' : 'removed from worksheet row'
    end
    new_status
  end

end
