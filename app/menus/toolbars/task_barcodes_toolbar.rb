Biorails::MenuSystem.register :toolbar, TaskBarcode do

  if current_object
    menu_item :task_barcodes, task_barcodes_path
    menu_item :show, task_barcopde_path(current_object) unless params[:action] == 'show'
  elsif params[:action] == 'index'
    menu_item :new, new_task_barcode_path
  else
    menu_item :task_barcodes, task_barcodes_path
    menu_item :new, new_task_barcode_path
  end

  include_menu :current_query

end
