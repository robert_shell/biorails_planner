Biorails::MenuSystem.register :toolbar, ResultTask do

  if current_object
    include_menu :task_menus

    if current_object.queue

      task_disabled = !current_object.active?

      append_menu :planning_menu do
        menu_separator
        if right?(Role::MODULE_OUTLINE, Role::RIGHT_USE)
          ElementType.for_class(ResultTask).each do |element|
            menu_item element.name,
                      step1_new_outline_queue_experiment_order_wizard_path(current_object.queue, element_type_id: element),
                      {text: "New #{element.name}"}
          end
        end
        menu_separator
        menu_item :orders, list_orders_path(task_id: current_object.id), :disabled => task_disabled
        menu_item :checkin, new_task_barcode_path(task_id: current_object.id), :disabled => task_disabled
        menu_item :finalize, step1_experiment_consolidation_wizard_path(current_object), :disabled => task_disabled
        menu_separator
        current_object.queue.active_tasks.each do |task|
          menu_item task.dom_id, task_path(task), text: task.name, disabled: (task.id == current_object.id)
        end
      end
    end

  else
    ElementType.wizards.for_class(ResultTask).each do |element|
      menu_item element.name, new_element_url(element), :iconCls => "#{element.css_icon_name}", :text => "New #{element.name}"
    end
  end


end
