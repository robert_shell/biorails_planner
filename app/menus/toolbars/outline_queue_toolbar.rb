Biorails::MenuSystem.register :toolbar, OutlineQueue do

  if current_object
    right_to_build = right?(Role::MODULE_QUEUE, Role::RIGHT_BUILD)
    is_disabled = !current_object.changeable? || (current_object.folder && current_object.folder.locked? && !current_object.folder.locked_by_me?)

    append_menu :edit_menu do
      if right?(Role::MODULE_INVENTORY, Role::RIGHT_BUILD)
        menu_item :order_profiles, list_order_profiles_path(outline_queue_id: current_object)
      end
    end

    append_menu :add_menu do
      if right?(Role::MODULE_INVENTORY, Role::RIGHT_BUILD)
        menu_item :order_profile, new_order_profile_path(outline_queue_id: current_object)
      end
    end


    append_menu :report_menu do
      menu_item :orders, orders_outline_queue_path(current_object)
      menu_item :cdb_results, pivot_rl_results_path(:outline_queue_id => current_object.id)
    end

    append_menu :planning_menu do
      menu_separator
      if right_to_build && !is_disabled
        ElementType.for_class(ResultTask).each do |element|
          menu_item element.name,
                    step1_new_outline_queue_experiment_order_wizard_path(current_object, element_type_id: element),
                    {text: t('menu.new', :name => element.name)}
        end
      end
      menu_separator
      current_object.active_tasks.each do |task|
        menu_item task.dom_id, task_path(task), text: task.name
      end
    end

  end


end
