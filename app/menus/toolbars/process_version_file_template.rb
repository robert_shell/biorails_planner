Biorails::MenuSystem.register :toolbar, ProcessVersionFileTemplate do

  if current_object
    is_parent_changeable = (current_object && current_object.process_version && current_object.process_version.changeable?)

    unless %w{edit update}.include?(params[:action])
      menu_item :edit,
                edit_process_version_file_template_path(current_object),
                :disabled => (!is_parent_changeable)
    end

    unless params[:action] == 'show'
      menu_item :show,
                process_version_file_template_path(current_object)
    end

  end
end

