Biorails::MenuSystem.register :query, OutlineQueue do

  if current_object
    is_disabled = !current_object.changeable? ||
        (current_object.folder && current_object.folder.locked? && !current_object.folder.locked_by_me?)

    right_to_build = right?(Role::MODULE_QUEUE, Role::RIGHT_BUILD)

      sub_menu :planning_menu do
        menu_item :show, outline_queue_path(current_object)
        menu_item :manage, queue_items_cart_path(current_object), :disabled => true
        menu_separator

        current_object.active_tasks.each do |task|
          menu_item task.dom_id, task_path(task), text: task.name
        end

      end

    include_menu :current_query

    if current_query
      back_url = queue_items_cart_path(current_object)
      update_url= cart_queue_items_cart_path( current_object,  back_url: back_url  )
      assign_url= assign_queue_items_cart_path(current_object,  back_url: back_url )

      sub_menu :cart, id: 'menu_cart' do

        menu_item :update, '#', disabled: !right_to_build,
                  class: 'cart_selection',
                  js: "Ext.getCmp('qpanel').showCart('#{update_url}',true); return false;",
                  tooltip: 'Update state/priority of items from cart'


        menu_item :assign, '#',disabled: !right_to_build,
                  class: 'cart_selection',
                  js: "Ext.getCmp('qpanel').showCart('#{assign_url}',true); return false;",
                  tooltip: 'Change assign of  items to experiments'

        if current_object.try(:preferred_order_profile)
          order_url = cart_orders_path(outline_queue_id:current_object.id,
                                       back_url: back_url )

          menu_item :order,'#', disabled: !right_to_build,
                    class: 'cart_selection',
                    js: "Ext.getCmp('qpanel').showCart('#{order_url}',true); return false;",
                    tooltip: 'Order items from cart'
        end

        ElementType.for_class(ResultTask).each do |element|
          task_url = cart_experiment_order_wizards_path(outline_queue_id: current_object,
                                                        element_type_id: element,
                                                        back_url:back_url)
          menu_item element.dom_id('new'),'#',
                    text: "New #{element.name}",
                    disabled: !right_to_build,
                    class: 'cart_selection',
                    js: "Ext.getCmp('qpanel').showCart('#{task_url}',false); return false;",
                    tooltip: 'New #{element.name} from selected items in cart'

        end

        menu_separator
        menu_item :clear_cart, '#',
                  js: "Ext.getCmp('qpanel').clearCart(); return false;",
                  tooltip: 'Clear cart'
      end
    end
  end
end
