Biorails::MenuSystem.register :toolbar, OutputFileTemplate do

  if current_object
    menu_item :output_file_templates, output_file_templates_path
    menu_item :show, output_file_template_path(current_object) unless params[:action] == 'show'
    unless ['edit', 'update'].include?(params[:action])
      menu_item :edit, edit_output_file_template_path(current_object)
    end
  elsif params[:action] == 'index'
    menu_item :new, new_output_file_template_path
  end

  include_menu :current_query

end