Biorails::MenuSystem.register :toolbar, Outline do

  if current_object
    append_menu :report_menu do
      menu_separator
      menu_item :cdb_results, pivot_rl_results_path(:outline_id => current_object.id)
    end
  end
end
