Biorails::MenuSystem.register :toolbar, ProcessVersion do

  if current_object
    is_parent_changeable = (current_object && current_object.process_definition && current_object.process_definition.changeable?)

    append_menu :edit_menu do
      menu_separator
      if right?(Role::MODULE_PROCESS, Role::RIGHT_BUILD)
        if current_object.inventory_linked?
          menu_item :edit_dilution_map,
                    edit_dilution_map_path(current_object.dilution_map),
                    :disabled => (is_disabled? or !current_object.dilution_map)

        end
      end
    end

    append_menu :add_menu do
      menu_separator
      if current_object.try(:inventory_linked?)
        menu_item :add_dilution_map,
                  new_dilution_map_path(:process_version_id => current_object.id),
                  :disabled => (is_disabled? or current_object.dilution_map)
        menu_item :add_file_template,
                  new_process_version_file_template_path(:process_version_id => current_object.id),
                  :disabled => is_disabled?
      end
    end

    append_menu :report_menu do
      menu_separator
      menu_item :file_templates,
                process_version_file_templates_path(:process_version_id => current_object.id)
    end
  end


end
