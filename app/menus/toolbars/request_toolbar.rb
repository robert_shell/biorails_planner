Biorails::MenuSystem.register :toolbar, Request do

  if current_object
    append_menu :report_menu do
      menu_separator
      menu_item :cdb_results, pivot_rl_results_path(:request_id => current_object.id)
    end
  end


end
