class Organize::QueueItemsCartController < ApplicationController

  use_authorization Role::MODULE_QUEUE,
                    Role::RIGHT_USE => [:show],
                    Role::RIGHT_BUILD => [:cart, :assign, :run, :redo, :list, :create]


  before_filter :verify_folder_is_changeable, :only => [:run]

  #
  # Show Query with new start users cart of for this query
  #
  def show
    setup_query
    @selected_list = @cart.get_items
  end

  #
  # Show cart to update of properties
  #
  def cart
    setup_items
  end

  #
  # assign items to new experiment
  #
  def assign
    setup_items
  end

  #
  # post, modify items in cart
  #
  def update
    setup_items
    if @cart.cart_items.size ==0
      flash[:warning] = 'No items updated'
      redirect_to(params[:back_url] ||home_url)
    elsif params['submit_assign']
      reassign_items(params[:queue_item])
    elsif params['submit_update']
      update_items(params[:queue_item])
    end
  end

  protected


  def update_items(row)
    row[:status_summary] ='bulk update from cart'
    row.map { |name, value| row.delete(name) if value.blank? }
    if State.where('id=? and level_no < 2', row[:state_id]).exists?
      @cart.list.each { |queue_item| queue_item.clear_task_and_update(row) }
      flash[:success] = "Update #{@cart.list.size} Items and removed from experiments"
    else
      @cart.update_action(row)
      flash[:success] ="Updated #{@cart.list.size} Items"
    end
    return redirect_to(params[:back_url] ||home_url)
  end


  def reassign_items(row)
    logger.info "reassign_items to #{row.inspect}"
    if row[:task_id].blank?
      row[:status_summary] ='bulk removed experiment from cart'
      @cart.list.each { |queue_item| queue_item.clear_task_and_update(row) }
      flash[:success] ="Updated #{@cart.list.size} Items"
    else
      task = Task.find(row[:task_id])
      if task.changeable?
        task.sheet.populate_from_queue( @cart.list )
        flash[:success] ="Updated #{@cart.list.size} Items"
      else
        flash[:warning] ='New experiment is not changeable'
      end
    end
    return redirect_to(params[:back_url] ||home_url)
  end


  #
  # Find existing cart and get outline_queue
  #
  def setup_cart
    @cart = Cart.new
    @cart.query = @query
    @cart.model = QueueItem
    @cart.reference = @outline_queue
    @cart
    @cart.set_items(params[:items]) unless params[:items].blank?
  end

  def setup_query
    @outline_queue = OutlineQueue.find(params[:id])
    @project_folder = @outline_queue.project_element
    @query = QueryEngine.query('queue_items.index', :outline_queue => @outline_queue)
    @query_manager = QueryEngine::QueryGrid.new(@query)
    setup_cart
  end

  def setup_items
    setup_query
    @queue_item = QueueItem.new(params[:queue_item])
    @list = ::List.new(params[:list])
    @user_request = Request.new(params[:user_request])
    @result_task = ResultTask.new(params[:result_task])
  end

end
