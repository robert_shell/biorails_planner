# == Queue Item Controller
# This is used to display a queue item and allows the user to delete it
#
#
# == Actions
#
# * index        all queue items global query
# * list         list all items for given outline
# * new/create   create a new item
# * edit/update  edit a exiting item
# * destroy      destroy item and all its dependent objects
#
# == Notice
#
# Copyright © 2006 Robert Shell, Alces Ltd All Rights Reserved
# See license agreement for additional rights
#
class Organize::QueueItemsController < ApplicationController
  unloadable
  use_authorization Role::MODULE_QUEUE,
                    Role::RIGHT_USE => [:index,:show, :status, :results, :items],
                    Role::RIGHT_BUILD => [:new, :edit, :update, :destroy, :update_item, :upload,
                                          :new_task, :remove_cart, :add_cart, :update_cart, :clear_cart]

  before_filter :find_queue, :only => [:index, :results, :work, :show_cart, :remove_cart,
                                       :add_cart, :update_cart, :clear_cart, :upload]
  before_filter :find_item, :only => [:show, :is_reason_required, :destroy]
  before_filter :verify_folder_is_changeable, :only => [:update_cart, :run, :edit, :upload]

  helper Organize::OutlineQueuesHelper
  #
  # Report on all queue items
  #
  # All QueueItemContext records
  #
  def index
    if params[:outline_queue_id]
      @outline_queue = OutlineQueue.find(params[:outline_queue_id])
      @project_folder = @outline_queue.folder
      @query = QueryEngine.query('queue_items.index', :outline_queue => @outline_queue)
      respond_to do |format|
        format.html { render :action => 'cart_list' }
      end
    else
      @query = QueryEngine.query('queue_items.index')
    end
  end

  #
  # Report on all work packages in system
  #
  # All work packages in the system
  #
  def packages
    @query = QueryEngine.query('queue_items.packages')
    respond_to do |format|
      format.html { render :action => 'index' }
    end
  end

  def results
    subject = :organization
    name = @outline_queue.dom_id(:results)
    @query = QueryEngine.query('queue_items.packages', :outline_queue => @outline_queue)
    @title = t('queue_items.results.title')

    respond_to do |format|
      format.html { render :action => 'index' }
    end
  end

  def show
    @outline_queue = @queue_item.queue
    @user_request = @queue_item.request
  end

  def destroy
    @queue_item.destroy
    redirect_to :action => 'index'
  end

  def is_reason_required
    respond_to do |format|
    format.json{render :json=>{:required => @queue_item.requires_reason_for_change_to(params[:state])}}
      end
  end

  protected

  def find_item
    if params[:id] == 'carts'
      flash[:warning] = t('organize.queue_items.non_selected')
    end
    @queue_item =QueueItem.find(params[:id])
  rescue Exception => ex
    logger.warn flash[:warning]= "Exception in #{self.class}: #{ex.message}"
    return show_access_denied
  end

  def find_queue
    if params[:project_element_id]
      @project_folder = ProjectElement.load(params[:project_element_id])
      @outline_queue = @project_folder.reference
    else
      @outline_queue = OutlineQueue.find(params[:outline_queue_id])
      @project_folder = @outline_queue.folder
    end

    unless @outline_queue && @outline_queue.is_a?(OutlineQueue)
      return show_access_denied
    end

    set_project_folder(@project_folder)
  rescue Exception => ex
    logger.warn flash[:warning]= "Exception in #{self.class}: #{ex.message}"
    return show_access_denied
  end

end
