#
# = Experiment Order Wizard Controller
#
# Copyright © The Edge Software Consultancy 2006-2013 All rights reserved
# See license agreement for additional rights
#
class ExperimentOrderWizardsController < ApplicationController
  layout 'wizard'

  include Biorails::WizardExtras

  use_authorization Role::MODULE_OUTLINE,
                    Role::RIGHT_BUILD => [:step1_new, :step1_create, :step1_edit, :step1_next, :step1_update,
                                          :step2, :step3, :confirm, :abandon]

  before_filter :setup_outline_queue, :only => [:cart,:step1_new, :step1_create]
  before_filter :get_task_and_queue, :only => [:step1_edit, :step1_update, :step2, :step3, :step4, :confirm]

  #
  # Model new order based on list of items from cart
  #
  def cart
    if params[:items]
      @queue_items = QueueItem.find(params[:items].values)
    end
    render 'step1_new', locals: {
                          outline: @outline_queue.outline,
                          project_folder: @outline_queue.project_element,
                          element_type: @result_task.element_type,
                          url: step1_create_outline_queue_experiment_order_wizard_path(outline_queue_id: @outline_queue.id)
                      }
  end


  def step1_new
    render 'step1_new', locals: {
                          outline: @outline_queue.outline,
                          project_folder: @outline_queue.project_element,
                          element_type: @result_task.element_type,
                          url: step1_create_outline_queue_experiment_order_wizard_path(outline_queue_id: @outline_queue.id)
                      }
  end

  #
  # @todo may need to run self.task = folder.run_internal(self.process_version, params[:task]) to make sure
  # build task correct with all steps etc.
  #
  def step1_create
    @result_task = @project_folder.run_internal(@result_task.process, params[:result_task])
    if @result_task.save
      if params[:items]
        @result_task.add_queue_items( QueueItem.find(params[:items].values) )
      end
      redirect_to step2_experiment_order_wizard_url(@result_task)
    else
      step1_new
    end
  end

  def step1_edit
    render :step1_new, locals: {outline: @outline_queue.outline,
                                project_folder: @outline_queue.project_element,
                                element_type: @result_task.element_type,
                                url: step1_update_experiment_order_wizard_path(@result_task)}
  end

  def step1_update
    @result_task.update_attributes(params[:result_task])
    if @result_task.save
      redirect_to step2_experiment_order_wizard_url(@result_task)
    else
      step1_edit
    end
  end

  def step2
    @query = QueryEngine.query('queue_items.index', outline_queue: @result_task.queue)
    @query_manager = QueryEngine::QueryGrid.new(@query)
    @filter_set = @query.system_filter_definition('default') do |set|
      set.clear
      set.add_filter("outline_queue_id = #{outline_queue.id}")
      set.add_filter('task_id is null ')
      set.add_filter("state.level_no in 1\n2")
    end
    @query.set_filter('filter_set_id' => @filter_set.id)
    @result_task.check_capacities
    flash.now[:warning] = @result_task.warnings.join("<br/>") if @result_task.queue_items.any? and @result_task.warnings.any?
    render locals: {outline_queue: @result_task.queue}
  end

  def step3
    if @outline_queue.order_profiles.any?
      render 'step3'
    elsif params[:back]
      redirect_to step2_experiment_order_wizard_path(@result_task)
    else
      flash[:notice] = t('wizard.no_order_profiles')
      redirect_to step4_experiment_order_wizard_path(@result_task)
    end
  end

  def step4

  end

  def confirm
    set_confirmed
    redirect_to task_path(@result_task)
  end

  def abandon
    @result_task = ResultTask.find_by_id(params[:id])
    if @result_task
      @result_task.queue_items.each { |queue_item| queue_item.update_attribute(:task_id, nil) }
      @result_task.orders.each { |order| order.update_attribute(:task_id, nil) }
      @result_task.destroy
    end
    element_type = @result_task.element_type.name if @result_task
    element_type ||= ElementType.find_by_class_name('ResultTask').name
    flash[:success] = t('wizard.destroy_success_notice', :element_type => element_type)
    redirect_to outline_queue_path(OutlineQueue.find(params[:outline_queue_id]))
  end

  protected

  #
  #
  #
  def set_confirmed
    @result_task.fill_with_queue_items
    @result_task.reload
    @result_task.set_processing
    @result_task.reload
    @result_task.to_html(false)
  end

  def get_task_and_queue
    @result_task ||= ResultTask.find(params[:id])
    @outline_queue ||= @result_task.queue
    @queue_items = []
    check_state_flows
  end

  #
  # Setup for a specific outline queue with check access
  #
  def setup_outline_queue
    begin
      @outline_queue ||= OutlineQueue.visible.find(params[:outline_queue_id])
      @result_task ||= ResultTask.new_instance_for_queue(@outline_queue, params[:result_task])
      @result_task.element_type_id = params[:element_type_id] unless params[:element_type_id].blank?
      @queue_items = []
      @project_folder = @outline_queue.project_element
      set_project_folder(@project_folder)
      check_state_flows
    rescue Exception => ex
      flash[:error] = t('error.finding', :name => @outline_queue, :message => ex.message) if !@outline_queue
      logger.debug ex.backtrace.join("\n")
      logger.warn "Failed OutlineQueuesController.setup_outline_queue, #{ex.message}"
      return show_access_denied
    end
  end

  #
  # Add big error message in state flows
  #
  def check_state_flows
    problems =[]
    if defined? @outline_queue
      qi = @outline_queue.items.first
      problems.concat Biorails::DefaultStates.valid_flow_with_expected(@outline_queue.state_flow, Biorails::DefaultStates::ORGANIZATION_STATE_LEVELS)
      problems.concat Biorails::DefaultStates.valid_flow_with_expected(qi.state_flow, Biorails::DefaultStates::REQUESTING_STATE_LEVELS) if qi
    end
    if defined? @result_task
      problems.concat Biorails::DefaultStates.valid_flow_with_expected(@result_task.state_flow, Biorails::DefaultStates::EXECUTION_STATE_LEVELS)
    end
    flash.now[:error] = problems.join("<br/>").html_safe if problems.size > 0
  end

end
