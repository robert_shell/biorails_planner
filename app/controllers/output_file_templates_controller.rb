class OutputFileTemplatesController < ApplicationController

  use_authorization Role::MODULE_PROCESS,
                    :admin => [:index, :show, :new, :create, :edit, :update, :destroy]

  before_filter :get_output_file_template, :only => [:show, :edit, :update, :destroy]

  def index
    render_query('output_file_templates.index')
  end

  def show

  end

  def new
    @output_file_template = OutputFileTemplate.new
  end

  def create
    @output_file_template = OutputFileTemplate.new(params[:output_file_template])
    if @output_file_template.save
      flash.now[:success] = t('notice.created', :name => OutputFileTemplate.model_name.human)
      redirect_to output_file_template_path(@output_file_template)
    else
      flash.now[:error] = t('error.creating',
                            :name => OutputFileTemplate.model_name.human,
                            :message => '')
      render :new
    end
  end

  def edit

  end

  def update
    if @output_file_template.update_attributes(params[:output_file_template])
      flash.now[:success] = t('notice.updated', :name => OutputFileTemplate.model_name.human)
      redirect_to output_file_template_path(@output_file_template)
    else
      flash.now[:error] = t('error.updating',
                            :name => OutputFileTemplate.model_name.human,
                            :message => '')
      render :new
    end
  end

  def destroy
    @output_file_template.destroy
    flash.now[:success] = t('notice.deleted', :name => OutputFileTemplate.model_name.human)
    redirect_to output_file_templates_path
  end

  def help

  end

  protected

  def get_output_file_template
    @output_file_template = OutputFileTemplate.find(params[:id])
  end

end
