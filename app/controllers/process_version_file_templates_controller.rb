class ProcessVersionFileTemplatesController < ApplicationController

  helper TSVHelper

  use_authorization Role::MODULE_PROCESS,
                    Role::RIGHT_BUILD => [:new, :create, :show]

  before_filter :get_process_version, :only => [:new, :create]
  before_filter :get_process_version_file_template, :only => [:destroy, :show, :edit, :update]

  # index only works with process version id
  def index
    show_access_denied unless params[:process_version_id]
    @process_version = ProcessVersion.find(params[:process_version_id])
  end

  def new
    @wizard = params[:wizard]
    @process_version_file_template = ProcessVersionFileTemplate.new(
        :process_version_id => @process_version.id
    )
    respond_to do |format|
      format.js
      format.html
    end
  end

  def create
    @wizard = params[:wizard]
    @process_version_file_template = ProcessVersionFileTemplate.new(params[:process_version_file_template])
    if @process_version_file_template.save
      respond_to do |format|
        format.js do
          @success = true
          @process_version = @process_version_file_template.process_version
        end
        format.html do
          flash.now[:success] = I18n.t('notice.created',
                                       :name => ProcessVersionFileTemplate.model_name.human)
          redirect_to process_version_file_template_path(@process_version_file_template)
        end
      end
    else
      flash.now[:error] = I18n.t('error.creating',
                                 :name => ProcessVersionFileTemplate.model_name.human,

                                 :message => @process_version_file_template.errors.on(:base))
      respond_to do |format|
        format.html { render :new }
        format.js do
          @success = false
          @process_version = @process_version_file_template.process_version
        end
      end

    end
  end

  def edit
    @wizard = params[:wizard]
  end

  def update
    @wizard = params[:wizard]
    if @process_version_file_template.update_attributes(params[:process_version_file_template])
      respond_to do |format|
        format.js do
          @success = true
          @process_version = @process_version_file_template.process_version
        end
        format.html do
          flash.now[:success] = I18n.t('notice.updated',
                                       :name => ProcessVersionFileTemplate.model_name.human)
          redirect_to process_version_file_template_path(@process_version_file_template)
        end
      end
    else
      flash.now[:error] = I18n.t('error.creating',
                                 :name => ProcessVersionFileTemplate.model_name.human,

                                 :message => @process_version_file_template.errors.on(:base))
      respond_to do |format|
        format.html { render :edit }
        format.js do
          @success = false
          @process_version = @process_version_file_template.process_version
        end
      end
    end
  end

  def show

  end

  def destroy
    @outline_queue = @process_version.outline_queue
    @process_version_file_template.destroy
    if @process_version_file_template.destroyed?
      flash.now[:success] = t('notice.deleted', :name => ProcessVersionFileTemplate.model_name.human)
    else
      flash.now[:warning] = t('error.deleting',
                              :name => ProcessVersionFileTemplate.model_name.human,
                              :message => @process_version_file_template.errors[:base])

    end
    respond_to { |format| format.js { @wizard = true } }
  end

  protected

  def get_process_version_file_template
    @process_version_file_template = ProcessVersionFileTemplate.find(params[:id])
    @process_version = @process_version_file_template.process_version
  end

  def get_process_version
    if params[:process_version_id]
      @process_version = ProcessVersion.find(params[:process_version_id])
    else
      show_access_denied
    end
  end


end
