class TaskOutputFilesController < ApplicationController

  helper(TSVHelper)

  use_authorization Role::MODULE_TASK,
                    :use => [:new, :create, :show]

  before_filter :get_task

  def new
    if params[:job_name]
      @job = Job.find_by_queue_name(params[:job_name])
      flash.now[:error] = @job.error_messages if @job
    end
    flash.now[:warning] = @task.duplicate_file_warning if @task.duplicate_file_warning
  end

  def renew_form
    if @task.duplicate_file_warning
      flash.now[:warning] = @task.duplicate_file_warning
      @show_messages_in_form = true
    end
    flash.now[:warning] = @task.duplicate_file_warning if @task.duplicate_file_warning
  end

  def create
    @task.result_validation_on_save = true
    @task.attributes = params[:result_task] if params[:result_task]
    @job_id = Biorails::TaskFinalizeJob.async_fill_output_file(
        {
            :task => @task,
            :user_id => User.current.id,
            :fail_url => new_task_output_file_path(:id => @task.id)
        }
    )
    redirect_to(job_path(:action => 'waiting', :id => @job_id, :redirect => true))
  end



  protected

  def get_task
    @task = Task.find(params[:id])
    @task.attributes = params[:result_task] if params[:result_task]
  rescue => ex
    logger.info ex.message
    logger.info ex.backtrace.join("\n")
    flash[:error] = ex.message
    redirect_to home_url()
  end

end
