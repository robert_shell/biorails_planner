#
# = Experiment Order Wizard Controller
#
# Copyright © The Edge Software Consultancy 2006-2013 All rights reserved
# See license agreement for additional rights
#
class ExperimentItemsController < ApplicationController


  use_authorization Role::MODULE_QUEUE,
                    Role::RIGHT_BUILD => [:update, :destroy]

  def update
    @result_task = ResultTask.find(params[:id])
    queue_items = QueueItem.find(params[:queue_item_ids] || [])
    if params[:remove]
      @result_task.delete_queue_items queue_items
    else
      @result_task.add_queue_items queue_items
    end
    @result_task.reload
    @result_task.check_capacities
    flash.now[:warning] = @result_task.warnings.join("<br/>") if @result_task.warnings.any?
  end

  def destroy
    @result_task = ResultTask.find(params[:id])
    @result_task.queue_items.clear
    @result_task.reload
    @result_task.check_capacities
    render :update
    flash.now[:warning] = @result_task.warnings.join("<br/>") if @result_task.warnings.any?
  end


end
