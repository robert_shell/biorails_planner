class TaskBarcodesController < ApplicationController

  include ActionView::Helpers::TextHelper

  use_authorization Role::MODULE_PROCESS,
                    Role::RIGHT_USE => [:show, :index, :show_collection, :new, :create, :destroy]


  before_filter :get_default_task, :only => [:new, :create, :destroy]
  before_filter :get_task, :only => [:new, :create, :show_collection]
  before_filter :get_task_barcode, :only => [:destroy, :show]

  before_filter :check_experiments_present, :only => [:new, :create, :destroy]

  before_filter :get_task_barcodes_from_form, :only => [:create, :destroy]
  before_filter :get_task_barcodes_from_params, :only => [:show_collection]


  before_filter :check_folder_changeable, :only => [:new, :create, :destroy]

  def index
    if params[:task_id]
      @task = Task.find(params[:task_id])
      @task_barcodes = @task.task_barcodes.where(:complete => true)
      render :show_collection
    else
      render_query('task_barcodes.index')
    end
  end

  # Scan in a list of barcoded items
  def new
    TaskBarcode.purge_unfinalised
    if params[:job_name]
      @job = Job.find_by_queue_name(params[:job_name])
      flash.now[:error] = @job.error_messages if @job
    end
  end

  def create
    if params[:commit] == t('add')
      add_barcodes and return
    else
      finalise_barcodes
    end
  end

  # Confirmation after create, shows a summary of the scanned barcodes
  # and their related objects
  def show_collection
    TaskBarcode.purge_unfinalised
  end

  def show

  end

  def destroy
    @task_barcode.destroy
    check_for_duplicates
    if @task_barcode.destroyed?
      @task_barcodes.delete(@task_barcode)
      flash.now[:success] = t('notice.removed',
                              :name => TaskBarcode.model_name.human)
    else
      flash.now[:error] = t('error.deleting',
                            :name => TaskBarcode.model_name.human)
    end
    respond_to do |format|
      format.js
      format.html do
        redirect_to task_task_barcodes_path(@task)
      end
    end
  rescue Exception => er
    logger.info er.message
    logger.info er.backtrace.join("\n")
    flash.now[:error] = t('error.deleting',
                          :name => TaskBarcode.model_name.human,
                          :message => er.message)
  end

  protected

  def check_for_duplicates
    @dups = @task_barcodes.select { |x| x.duplicated? and !x.automatic }
    if @dups.any?
      flash.now[:error] =t('activerecord.errors.task_barcode.list_contains_duplicates')
    end
  end

  def add_barcodes
    if !params[:reference_codes].blank?
      @new_task_barcodes, @unfound_codes = TaskBarcode.scan_list(
          params[:reference_type],
          params[:reference_codes],
          params[:default_task_id]
      )
      @task_barcodes.concat(@new_task_barcodes)
      if @unfound_codes.any?
        flash.now[:warning] = t(
            'activerecord.errors.task_barcode.codes_not_found',
            :codes => truncate(@unfound_codes.uniq.join(', '),length: 1000)
        )
      end
      check_for_duplicates
      display_capacity_warnings
    else
      flash.now[:error] = t('activerecord.errors.task_barcode.reference_codes_blank')
    end
    render :new
  end

  def finalise_barcodes
    begin
      check_for_duplicates
      if @dups.any?
        render :new
      else
        @job_id = Biorails::TaskFinalizeJob.async_finalise_barcodes(
            {
                :task_barcodes => @task_barcodes,
                :user_id => User.current.id,
                :success_url => task_path(@task)
            }
        )
        redirect_to(job_path(:action => 'waiting', :id => @job_id, :redirect => true))
      end
    rescue => er
      logger.info er.message
      logger.info er.backtrace.join("\n")
      flash.now[:error] = t('error.creating',
                            :name => TaskBarcode.model_name.human,
                            :message => er.message)
      render :new
    end
  end

  def get_task_barcode
    @task_barcode = TaskBarcode.find(params[:id])
    @task = @task_barcode.task
  end

  # params[:default_task_id] can come from the form, it's the currently entered default task
  # params[:task_id] comes from the url, it is the task from which we entered this form
  #
  def get_default_task
    @default_result_task = ResultTask.find(params[:default_task_id]) if params[:default_task_id]
    @default_result_task ||= Task.active_inventory_result.first
  end

  def get_task
    @task = if params[:task_id]
              Task.visible.where(id: params[:task_id]).first
            elsif ProjectElement.current.reference.is_a?(Task)
              ProjectElement.current.reference
            else
              nil
            end
    if @task and params[:action] != 'show_collection'
      TaskBarcode.reset_for_task(@task)
      @default_result_task = @task
      @task_barcodes = @task.task_barcodes
    end
  end

  def get_task_barcodes_from_params
    if params[:ids] and !params[:ids].empty?
      @task_barcodes = TaskBarcode.find(params[:ids])
    else
      @task_barcodes = []
    end
  end

  def get_task_barcodes_from_form
    if params[:task_barcodes]
      @task_barcodes = params[:task_barcodes].collect do |index, barcode_params|
        barcode = TaskBarcode.find(barcode_params[:id])
        barcode.task_id = barcode_params[:task_id]
        barcode
      end
    else
      @task_barcodes = []
    end
  end

  def check_experiments_present
    if Task.active_inventory_result.none?
      flash[:error] = t('activerecord.errors.task_barcode.no_suitable_tasks')
      redirect_to '/' and return
    end
  end

  def check_folder_changeable
    return show_access_denied(t('warning.not_changeable', :name => @task.name)) if @task and !@task.changeable?
  end

  def display_capacity_warnings
    if @task_barcodes and @task_barcodes.any?
      warnings = @task_barcodes.collect{|task_barcode| task_barcode.capacity_warning}
      warnings.compact!
      flash.now[:warning] = warnings.join("<br/>") if warnings.any?
    end
  end


end
