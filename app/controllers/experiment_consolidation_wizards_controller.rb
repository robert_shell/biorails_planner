#
# = Experiment Consolidation Wizard Controller
#
# Copyright © The Edge Software Consultancy 2006-2013 All rights reserved
# See license agreement for additional rights
#
class ExperimentConsolidationWizardsController < ApplicationController
  layout 'wizard'

  include Biorails::WizardExtras

  use_authorization Role::MODULE_QUEUE,
                    Role::RIGHT_BUILD => [
                        :step1,
                        :step2, :step2_create, :step2_edit, :step2_update,
                        :step3_new, :step3_preview, :step3_create,
                        :step4
                    ]

  before_filter :get_result_task
  before_filter :display_capacity_warnings

  # Edit Experiment
  #
  # Basic details for run for confirmation
  #
  def step1
    @state_counts = @result_task.queue_items.count(group:'state_id')
    @states = State.where(id:@state_counts.keys).order('level_no','name')
    render locals: {outline_queue: @result_task.queue}
  end


  def step1_save
    unless @result_task.update_attributes(params[:task])
      return render :step1
    end
    if params[:remove_state_id]
      @result_task.reload
      @result_task.task_remove_queue_items(@result_task.queue_items.in_state_id(params[:remove_state_id].keys))
    end
    redirect_to step2_experiment_consolidation_wizard_path(@result_task)
  end

  #
  # Task sheet Display for Editing and confirmation
  #
  def step2

  end

  #
  # On 'Next' in step 1
  #
  # Consolidation is carried out
  # Then on to step2_edit
  #
  # TODO skip straight to step4_new when there is no dilution map
  #
  def step3_create
    if @result_task.dilution_slots.any?
      redirect_to step3_edit_experiment_consolidation_wizard_path(@result_task)

    elsif TaskConsolidation.is_suitable_task?(@result_task)
      @job_id = TaskConsolidation.async_run(
          {
              task: @result_task,
              success_url: step3_edit_experiment_consolidation_wizard_path(@result_task),
              fail_url: step2_experiment_consolidation_wizard_path(@result_task)
          }
      )
      redirect_to(job_path(:action => 'waiting', :id => @job_id, :redirect => true))
    else
      if @result_task.process.process_version_file_templates.any?
        redirect_to step4_new_experiment_consolidation_wizard_path(@result_task)
      else
        redirect_to step5_experiment_consolidation_wizard_path(@result_task)
      end
    end
  end

  #
  # When step2_create is complete
  #
  # This shows the user their consolidation plan
  # and allows them to re-arrange the slots
  #
  def step3_edit
    @plate_data_store = PlateDataStore.new(@result_task.id)
    @dilution_slots = @result_task.dilution_slots.by_row
  end

  def step3_reset
    @plate_data_store = PlateDataStore.new(params[:id])
  end

  #
  # On 'Next' for step 2
  #
  # * enact any re-arranged slots
  #
  def step3_update
    if @result_task.process.process_version_file_templates.any?
      success_url = step4_new_experiment_consolidation_wizard_path(@result_task)
    else
      success_url = step5_experiment_consolidation_wizard_path(@result_task)
    end
    @job_id = TaskConsolidation.async_rearrange(
        {
            task: @result_task,
            success_url: success_url,
            slot_positions: JSON.parse(params[:dilution_slot_positions]),
            fail_url: step3_edit_experiment_consolidation_wizard_path(@result_task)
        }
    )
    redirect_to(job_path(:action => 'waiting', :id => @job_id, :redirect => true))
  end

  def step4_new

  end

  def step4_preview
    @process_version_file_template = ProcessVersionFileTemplate.find(params[:process_version_file_template_id])
    @result_task.process_version_file_template_id = params[:process_version_file_template_id]
    @preview_text = @result_task.generate_output_file_content.split("\n")[0...5].join("\n")
  end

  def step4_create
    @result_task.process_version_file_template_ids = params[:task][:process_version_file_template_ids].collect { |k, v| k }
    @job_id = Biorails::TaskFinalizeJob.async_fill_output_files(
        {
            :task => @result_task,
            :user_id => User.current.id,
            :fail_url => step4_new_experiment_consolidation_wizard_path(@result_task),
            :success_url => step5_experiment_consolidation_wizard_path(@result_task)
        }
    )
    redirect_to(job_path(:action => 'waiting', :id => @job_id, :redirect => true))
  end

  def step5
    @dilution_slots = @result_task.dilution_slots.by_row
    @plate_data_store = PlateDataStore.new(@result_task.id)
  end

  def finalize
    begin
      @result_task.tasks.each do |child_task|
        child_task.cascade_items(@result_task.items) # copy samples and setup to children
        child_task.reload
        child_task.to_html(false)
      end
      @result_task.sync_content_to_filesystem
      @result_task.reload
      @result_task.set_completed
#      @result_task.queue_items.each{|q|q.set_state(State.lookup("ready"))}
      flash[:success] = t('experiment_consolidation_wizards.finalise_success')
      redirect_to task_path(@result_task)
    rescue => er
      logger.warn er.message
      logger.info er.backtrace.join("\n")
      flash[:error] = er.message
      redirect_to step5_experiment_consolidation_wizard_path(@result_task)
    end
  end

  private

  def display_capacity_warnings
    if @result_task
      @result_task.check_capacities
      if @result_task.warnings.any?
        flash.now[:warning] = @result_task.warnings.join("<br/>")
      end
    end
  end

  def get_result_task
    @result_task = ResultTask.find(params[:id])
    @outline_queue = @result_task.queue
    # this makes some partials from the dilution slots controller work.
    @task = @result_task
  end

end
