#
# :outline_queue object queue to work on
# :properties true/false
#
QueryEngine.register('queue_items.index') do

  parameter :outline_queue, :type => :object, :lookup => 'internal/outline_queue', :default => OutlineQueue.first
  #
  # Need to be pivot on Queue Items plus request properties
  #
  model QueueItem

  right Role::MODULE_QUEUE, Role::RIGHT_USE

  outline_queue = value(:outline_queue)
  if outline_queue
    name  outline_queue.dom_id('manage')
    description "Items for #{outline_queue.path}"
    builder_ref 'OutlineQueue.new'
    reference   outline_queue
  end

  column('id', :hide => true)
  column('request_service.process_version.name',:label => 'Service')
  column('template_row.outline_method.name',  :label => 'Method')
  column('data_name',
         :template => " <a href='/queue_items/{id}' target='_blank'> {data_name} </a> ",
         :width => 150)
  column('request.name',
         :template => '<a href="/requests/{request_id}">{request.name}</a>')
  column('queue.outline.name',  :hide => true)
  column('status_summary',label:'Summary',  :hide => true)
  column('state.name',label:'State')
  column('queue.name',
         :label=>'Assay',
         :template => '<a href="/outline_queues/{outline_queue_id}">{queue.name}</a>',
         :hide => true)
  column('order_ref',
         :label=>'OrderRef.',
         :template => '<a href="/orders/{order_ref}">{order_ref}</a>')

  if outline_queue && outline_queue.respond_to?(:order_profiles)
    outline_queue.order_profiles.each do |order_profile|
      column("cdb_sample.stocks_ot#{order_profile.coasts_order_type_id}.counter_bc",:label=>"BC #{order_profile.name}")
      column("cdb_sample.stocks_ot#{order_profile.coasts_order_type_id}.counter_rdg",:label=>"RDG #{order_profile.name}")
    end
  end


  column('task.name',
         :label=>'Expt.',
         :template => '<a href="/tasks/{task_id}">{task.name}</a>')
  column('result_task_context_id', :hide => true)
  column('result_row.label',
         :label=>'Expt. Row',
         :template => '<a href="/task_contexts/{result_task_context_id}">{result_row.label}</a>')
  column('request.requested_by_member.team.name')
  column('request.requested_by_member.user.name')
  column('days_old')
  #column('time_in_queue',:calc_dsl=> query_calcs ,  :hide => true)
  column('task_id', :hide => true)
  column('created_at',
         :template => "{time_in_queue} ago",
         :filterable => false, :hide => false)
  column('request.created_at')
  column('updated_at', :hide => true)
  column('expected_at', :hide => true)
  column('data_priority.name',
         :sortable => true, :filterable => true, :width => 50)
  column('state.level_no', :hide => true)
  column('request.description')
  column('comments')
  column('request.project.team.name',
         :label=>::I18n.t('activerecord.models.project', :count => 1))
  column(':background_class')
  column('result_row.task_id', :hide => true)
  column('template_row.outline_method_id', :hide => true)
  column('request_service.process_version_id', :hide => true)
  column('result_row.task_id', :hide => true)
  column('result_row.task_id', :hide => true)
  column('priority_id', :hide => true)
  column('queue.outline_id', :hide => true)
  column('outline_queue_id', :hide => true)
  column('state_id', :hide => true)
  column('request_service.task_id',
         :template => "<a href='tasks/{request_service.task_id}'>#{::I18n.t('activerecord.models.task', :count => 1)}</a>",
         :hide => true)
  column('result_task_context_id',
         :template => "<a href='task_contexts/{result_task_context_id}'>#{::I18n.t('activerecord.models.request', :count => 1)}</a>",
         :label => ::I18n.t('activerecord.attributes.queue_item.result_task_context_id'),
         :hide => true)
  column('request_task_context_id',
         :template => "<a href='task_contexts/{request_task_context_id}'>#{translated('properties')}</a>",
         :hide => true)
  column('request_id',
         :template => "<a href='requests/{request_id}'>#{::I18n.t('activerecord.models.request', :count => 1)}</a>",
         :hide => true)

  if outline_queue

    button('all') do |set, options|
      set.add_filter("outline_queue_id = #{outline_queue.id}")
    end

    button('todo') do |set, options|
      set.add_filter("outline_queue_id = #{outline_queue.id}")
      set.add_filter("state.level_no in 1\n2")
      set.add_filter('task_id is null ')
    end

    button('running') do |set, options|
      set.add_filter("outline_queue_id = #{outline_queue.id}")
      set.add_filter("state.level_no = 1\n2")
      set.add_filter('task_id is not null ')
    end

    button('done') do |set, options|
      set.add_filter("outline_queue_id = #{outline_queue.id}")
      set.add_filter("state.level_no in 3\n4")
    end

    button('cancelled') do |set, options|
      set.add_filter("outline_queue_id = #{outline_queue.id}")
      set.add_filter('state.level_no =-1')
    end

    button('default') do |set, options|
      set.add_filter("outline_queue_id = #{outline_queue.id}")
      set.add_filter('task_id is null ')
      set.add_filter("state.level_no in 1\n2" )
    end


  end
end
