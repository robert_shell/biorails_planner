QueryEngine.register('queue_items.packages') do


  right Role::MODULE_QUEUE, Role::RIGHT_USE
  model Task

  column('name', :template => '<a href="/tasks/{id}" >{name}</a>', :label => 'Name', :order => 1, :width => 180)
  column('project_element.element_type.name', :label => 'Type', :order => 2, :width => 50)
  column('project_element.state.name', :label => 'State', :order => 3, :width => 50)
  column('outline.name', :label => 'Outline', :order => 4, :width => 100, :template => '<a href="/outlines/{outline_id}" >{outline.name}</a>')
  column('process.name', :label => 'Protocol', :order => 5, :width => 150, :template => '<a href="/process_versions/{process_version_id}">{process.name}</a>')
  column('assigned_member.user.name', :label => 'User', :order => 6, :width => 90)
  column('assigned_member.team.name', :label => 'Team', :order => 7, :width => 90)
  column('created_at',  :order => 8, :width => 70)
  column('updated_at', :hide => true)
  column('id', :hide => true, :label => 'Task ID')
  column('started_at', :hide => true, :label => 'Started At', :order => 8, :width => 70)
  column('expected_at', :hide => true, :label => 'Expected At', :order => 9, :width => 70)
  column('type', :hide => true, :label => 'Object Type', :order => 11, :width => 70)
  column('project_element.reference_type', :hide => true, :label => 'Reference Object Type')

  column('outline_id', :hide => true, :label => 'Outline ID')
  column('process_version_id', :hide => true, :label => 'Protocol ID')
  column('project_element.state_id', :hide => true, :label => 'State ID')
  column('project_element.state.level_no', :hide => true, :label => 'Level No')
  column('project_element.element_type_id', :hide => true, :label => 'Element Type ID')
  column('assigned_member.team_id', :hide => true, :label => 'Team ID')
  column('assigned_member.user_id', :hide => true, :label => 'User ID')
  column('created_by_user_id', :hide => true, :label => 'Created By User ID')
  column('updated_by_user_id', :hide => true, :label => 'Updated By User ID')


  button("All") do |set|
    set.clear
    set.add_sql_filter QueueItem::SQL_LINKED_RESULT_TASK
  end

  button("My Active") do |set|
    set.add_sql_filter QueueItem::SQL_LINKED_RESULT_TASK
    set.add_filter('assigned_member.user_id = :user_id')
    set.add_filter("project_element.state.level_no in 1\n2")
  end

  button("Teams' Active") do |set|
    set.add_sql_filter QueueItem::SQL_LINKED_RESULT_TASK
    set.add_filter("project_element.state.level_no in 1\n2")
    set.add_filter('assigned_member.team_id = :my_team_ids')
  end

  button("All Active") do |set|
    set.add_sql_filter QueueItem::SQL_LINKED_RESULT_TASK
    set.add_filter("project_element.state.level_no in 1\n2")
  end

  button("Today") do |set|
    set.add_sql_filter QueueItem::SQL_LINKED_RESULT_TASK
    set.add_filter('created_at > yesterday')
  end

  button("Last Week") do |set|
    set.add_sql_filter QueueItem::SQL_LINKED_RESULT_TASK
    set.add_filter('created_at > last week')
  end
end
