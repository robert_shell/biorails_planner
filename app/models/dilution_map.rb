class DilutionMap < ActiveRecord::Base

  belongs_to :process_version

  delegate :name, :to => :process_version, :prefix => :process_version, :allow_nil => true

  has_many :dilution_map_slots, :dependent => :destroy do

    def sample
      # not using this as it will always return an empty array during the create from
      #where(:slot_type => 'samp')
      sort { |a, b| a.position <=> b.position }.select { |x| x.slot_type == 'samp' }
    end

    def non_sample
      sort { |a, b| a.position <=> b.position }.select { |x| x.slot_type != 'samp' }
    end

  end

  belongs_to :coasts_plate_format

  accepts_nested_attributes_for :dilution_map_slots

  validates_numericality_of :length,
                            :only_integer => true,
                            :greater_than => 0,
                            :more_than_or_equal_to => Proc.new { |x| x.get_source_length },
                            :allow_nil => false
  validates_presence_of :process_version_id
  validates_presence_of :coasts_plate_format_id
  validates_presence_of :length
  validates_presence_of :source_length

  validate :validate_slots_fit_in_row
  validate :validate_has_some_sample_slots

  after_initialize :set_defaults

  def deep_clone
    #If upgrading to rails 3.1+ use dup instead of clone
    self.clone.tap do |new_map|
      self.dilution_map_slots.each { |slot| new_map.dilution_map_slots << slot.clone }
    end
  end

  def set_defaults
    self.coasts_plate_format_id ||= 1
    self.source_by_row ||= false
    get_source_length if source_length.nil?
    self.length ||= 8
  end

  def display_source_by_row
    if source_by_row
      DilutionMap.human_attribute_name(:row)
    else
      DilutionMap.human_attribute_name(:column)
    end
  end

  def DilutionMap.coasts_plate_format_options
    # I wouldn't normally do this but there will only ever be 2 options in this table
    CoastsPlateFormat.all.collect { |x| [x.text, x.id] }
  end

  def DilutionMap.source_by_row_options
    [
        [DilutionMap.human_attribute_name(:row), true],
        [DilutionMap.human_attribute_name(:column), false]
    ]
  end

  def DilutionMap.length_options
    [
        [DilutionMap.human_attribute_name(:ninety_six_short_side), 8],
        [DilutionMap.human_attribute_name(:ninety_six_long_side), 12],
        [DilutionMap.human_attribute_name(:three_eight_four_short_side), 16],
        [DilutionMap.human_attribute_name(:three_eight_four_long_side), 24]
    ]
  end

  def display_length
    case length
      when 8
        DilutionMap.human_attribute_name(:ninety_six_short_side)
      when 12
        DilutionMap.human_attribute_name(:ninety_six_long_side)
      when 16
        DilutionMap.human_attribute_name(:three_eight_four_short_side)
      when 24
        DilutionMap.human_attribute_name(:three_eight_four_long_side)
      else
        logger.info "Unexpected length on dilution map: #{length}. DilutionMap id: #{self.id}"
        length
    end
  end

  def get_source_length
    if coasts_plate_format_id
      self.source_length = case coasts_plate_format_id
                             when 1
                               if source_by_row
                                 12
                               else
                                 8
                               end
                             when 2
                               if source_by_row
                                 24
                               else
                                 16
                               end
                           end
    else
      self.source_length
    end
  end

  def validate_slots_fit_in_row
    if length
      if dilution_map_slots.length > length
        errors.add :base, I18n.t('activerecord.errors.dilution_map.too_many_slots')
      end
    end
  end

  def validate_has_some_sample_slots
    if dilution_map_slots.sample.none?
      errors.add :base, I18n.t('activerecord.errors.dilution_map.has_no_samples')
    end
  end

  def only_always_split?
    dilution_map_slots.sample.count < source_length
  end

  def consolidation_algorithm_options
    if only_always_split?
      [[DilutionMap.human_attribute_name(:always_split), 'always_split']]
    else
      [
          [DilutionMap.human_attribute_name(:reorder_within_group), 'reorder_within_group'],
          [DilutionMap.human_attribute_name(:always_split), 'always_split'],
          [DilutionMap.human_attribute_name(:never_split), 'never_split']
      ]
    end
  end



  #
  # General warnings about a dilution map
  #
  # - Not enough sample slots for source row, (can only use ALWAYS_SPLIT algorithm)
  #
  def warnings
    warnings = []
    if only_always_split? and dilution_map_slots.any?
      warnings << ::I18n.t('activerecord.warnings.dilution_map.too_few_samples',
                           :row => display_source_by_row.downcase,
                           :required_count => source_length)
    end
    warnings
  end

  attr_accessor :block_start
  attr_accessor :block_end
  attr_accessor :block_slot_type
  attr_accessor :block_sample_id

  def build_new_wells
    if block_start and block_end and block_slot_type
      range = [block_start.to_i, block_end.to_i]

      (range.min..range.max).each do |pos|

        # Remove wells from in-memory collection, NOT from database
        self.dilution_map_slots.select { |x| x.position == pos }.each do |slot|
          self.dilution_map_slots.delete(slot)
        end

        unless self.block_slot_type == 'blank'
          if self.block_slot_type == 'samp'
            sample_id ||= nil
          else
            sample_id = self.block_sample_id
          end

          self.dilution_map_slots << self.dilution_map_slots.new(
              :position => pos,
              :slot_type => self.block_slot_type,
              :sample_id => sample_id
          )
        end

      end
    else
      raise "Required for a new block are not present"
    end
  end


end
