class DilutionMapSlot < ActiveRecord::Base

  belongs_to :dilution_map
  delegate :process_version, :to => :dilution_map, :allow_nil => true

  belongs_to :coasts_plate_well, :primary_key => [:plate_code, :plate_row, :plate_col]

  belongs_to :cdb_sample, :foreign_key => :sample_id

  def DilutionMapSlot.slot_types
    [
        [DilutionMapSlot.human_attribute_name(:sample_slot_type), 'samp'],
        [DilutionMapSlot.human_attribute_name(:standard_slot_type), 'std'],
        [DilutionMapSlot.human_attribute_name(:high_control_slot_type), 'high'],
        [DilutionMapSlot.human_attribute_name(:low_control_slot_type), 'low'],
        [DilutionMapSlot.human_attribute_name(:blank_slot_type), 'blank']
    ]
  end

  def display_slot_type
    case slot_type
      when 'samp'
        DilutionMapSlot.human_attribute_name(:sample_slot_type)
      when 'std'
        DilutionMapSlot.human_attribute_name(:standard_slot_type)
      when 'high'
        DilutionMapSlot.human_attribute_name(:high_control_slot_type)
      when 'low'
        DilutionMapSlot.human_attribute_name(:low_control_slot_type)
      when 'blank'
        DilutionMapSlot.human_attribute_name(:blank_slot_type)
    end
  end

  def display_position
    position + 1
  end

  def summary
    sum = "Position: #{display_position}\nSlot Type: #{display_slot_type}"
    sum << "\nSample: #{cdb_sample.name.strip}" if cdb_sample
    sum
  end

  def to_s
    summary
  end

end
