class ProcessVersionFileTemplate < ActiveRecord::Base

  acts_as_dictionary

  validates_uniqueness_of :name, :scope => :process_version_id

  belongs_to :process_version
  belongs_to :output_file_template

  before_validation :fill_name

  after_initialize :default_values

  validates_presence_of :file_name
  validates_presence_of :file_type
  validates_presence_of :process_version
  validates_presence_of :output_file_template_id

  delegate :content, :to => :output_file_template

  def default_values
    self.file_type ||= 'tsv'
  end

  def fill_name
    self.name = "#{file_name}.#{file_type}"
  end

  def ProcessVersionFileTemplate.file_type_options
    [
      [I18n::t('output_file_types.csv'), 'csv'],
      [I18n::t('output_file_types.xlsx'), 'xlsx'],
      [I18n::t('output_file_types.tsv'), 'tsv']
    ]
  end

  def preview_text
    if self.process_version and self.output_file_template
      template_task = process_version.template
      liquid_template = output_file_template.content
      template_task.output_file_text(liquid_template)
    else
      ''
    end
  end

end
