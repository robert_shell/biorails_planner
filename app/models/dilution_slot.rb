class DilutionSlot < ActiveRecord::Base

  belongs_to :task

  belongs_to :coasts_plate_well, :foreign_key => [:plate_code, :plate_row, :plate_col]

  belongs_to :cdb_sample, :primary_key => :sample_id, :foreign_key => :sample_id

  delegate :name, :to => :cdb_sample, :prefix => :cdb_sample, :allow_nil => true

  delegate :sample_code, :sample_batch, :saltcode, :saltcode_text, :to => :cdb_sample, :allow_nil => true

  validates_presence_of :task_id

  validates_presence_of :row_no
  validates_presence_of :column_no
  validates_presence_of :slot_type

  attr_accessor :marked
  attr_accessor :colour_index
  attr_accessor :colour

  def to_s
    "#{task.name if task}: #{row_no}:#{column_no}(#{slot_type}) - #{coasts_plate_well} #{cdb_sample.to_s.strip if cdb_sample}"
  end

  def coasts_plate_well_identifier
    [plate_code, plate_row, plate_col].join(',')
  end

  def source_string
    if coasts_plate_well
      if plate_code.length > 4
        "...#{plate_code[-4..-1]}:#{coasts_plate_well.coord}"
      else
        "#{plate_code}:#{coasts_plate_well.coord}"
      end
    else
      "#{cdb_sample.id if cdb_sample} "
    end
  end


  def summary
    if coasts_plate_well
      <<TEXT
Row: #{row_no}
Column: #{column_no}
Slot Type: #{display_slot_type}
From Well: #{plate_code}:#{plate_row}#{plate_col}
Sample Id: #{coasts_plate_well.sample_id}
Sample: #{coasts_plate_well.sample_code}:#{coasts_plate_well.saltcode}:#{coasts_plate_well.sample_batch}
Volume: #{coasts_plate_well.volume}#{coasts_plate_well.volume_unit}
Concentration: #{coasts_plate_well.concentration}#{coasts_plate_well.concentration_unit}
Solvent: #{coasts_plate_well.solvent}
TEXT
    elsif sample_id
      <<TEXT
Row: #{row_no}
Column: #{column_no}
Slot Type: #{display_slot_type}
Sample Id: #{sample_id}
Sample: #{sample_code}:#{saltcode}:#{sample_batch}
TEXT
    else
      <<TEXT
Row: #{row_no}
Column: #{column_no}
Slot Type: #{display_slot_type}
TEXT
    end
  end

  SLOT_TYPE_TRANSLATIONS = {
      'samp' => DilutionMapSlot.human_attribute_name(:sample_slot_type),
      'std' => DilutionMapSlot.human_attribute_name(:standard_slot_type),
      'high' => DilutionMapSlot.human_attribute_name(:high_control_slot_type),
      'low' => DilutionMapSlot.human_attribute_name(:low_control_slot_type)
  }

  def display_slot_type
    SLOT_TYPE_TRANSLATIONS[slot_type]
  end

  def sample_name
    cdb_sample ? cdb_sample.name : ''
  end

  def task_contexts
    if task and coasts_plate_well
      task.contexts.joins(
          :result_queue_items
      ).where(
          'queue_items.data_type' => ['RlSample', 'CdbSample'],
          'queue_items.data_id' => coasts_plate_well.sample_id
      )
    end
  end

end
