class TaskBarcode < ActiveRecord::Base

  belongs_to :task

  delegate :sheet, :to => :task, :allow_nil => false
  delegate :name, :to => :task, :prefix => 'task', :allow_nil => true

  belongs_to :updated_by_user, :foreign_key => "updated_by_user_id", :class_name => 'User'
  belongs_to :created_by_user, :foreign_key => "created_by_user_id", :class_name => 'User'

  acts_as_audited

  validates_presence_of :reference_type
  validates_presence_of :reference_code
  validates_presence_of :position

  validates_presence_of :task, :if => :complete

  before_destroy :on_destroy_remove_matching_task_contexts

  # This is not used because CoastsPlateWell is only unique by plate_code, plate_row and plate_col
  # The same pattern is in used, but hand-rolled to accommodate this
  # belongs_to :reference, :polymorphic => true
  #
  # Also be aware that reference_code is a string, but includes values for order_id

  ##
  # @todo should translations be moved to helper so pick up current locale of session user?
  ##
  def TaskBarcode.reference_types
    [
        [CoastsPlate.model_name.human, 'CoastsPlate'],
        [CoastsPlateWell.model_name.human, 'CoastsPlateWell'],
        [Order.model_name.human, 'Order'],
        [CdbSample.model_name.human, 'CdbSample']
    ]
  end

  # This should be usable to scan plates, tubes or orders into a specific experiment,
  # or scan them independently of an experiment and assign them to a group.
  #
  # Parameters:
  # * type - the type of object being scanned, only one type can be scanned at once
  # * codes - string, key fields being scanned, separated by tab, newline or comma
  # * task_id - in this case all will be scanned added to the given experiment
  #
  def TaskBarcode.scan_list(type, codes, task_id=nil)
    task_barcodes = []
    unfound_codes = []

    codes.split(/[\n\t,]/).each do |code|
      code = code.strip
      next if code.blank?
      reference = TaskBarcode.find_reference(type, code)
      if reference

        task = TaskBarcode.find_task_for(reference)
        task ||= Task.find(task_id) if task_id
        if task
          last_code = task.task_barcodes.order('position desc').first
          n = last_code ? last_code.position + 1 : 0
        else
          n = 0
        end

        task_barcodes << TaskBarcode.create!(
            :reference_type => type,
            :reference_code => code,
            :position => n,
            :task_id => task ? task.id : nil,
            :complete => false
        )
      else
        unfound_codes << code
      end
    end
    return task_barcodes, unfound_codes.uniq
  end


  def reference
    TaskBarcode.find_reference(reference_type, reference_code)
  end

  def deletable?
    self.task.changeable?
  end

  def TaskBarcode.find_reference(type, code)
    case type.to_s
      when 'Order'
        coasts_order = CoastsOrder.find_by_order_id(code)
        if coasts_order
          coasts_order.order
        else
          nil
        end
      when 'CdbSample'
        CdbSample.lookup(code)
      else
        raise "Unknown barcode type"
    end
  end

  def TaskBarcode.find_task_for(reference)
    case reference
      when CoastsPlate, CoastsPlateWell
        order = reference.current_order
      when Order
        order = reference
      when CdbSample
        order = nil
    end
    if order
      order.task
    end
  end

  def order
    case reference_type
      when 'CoastsPlate', 'CoastsPlateWell'
        reference.current_order
      when 'Order'
        reference
      when CdbSample
        nil
    end
  end


  def checked_in
    checked_in ||= State.find_by_name('checked_in')
  end

  #
  # Mass update of queue items to m
  #
  def update_queue_items_for_samples_to_checked_in
    QueueItem.where(task_id: task, data_id: samples, data_type: 'CdbSample').each do |queue_item|
      queue_item.status_summary = "Checked into ''#{task.name}' by '#{User.current.login}'"
      queue_item.state = checked_in if checked_in
      queue_item.save
    end
  end

  def finalise
    if !self.complete?
      TaskBarcode.transaction do
        order.complete_order if order
        remove_automatic_duplicates
        fill_in_source_parameters
        update_queue_items_for_samples_to_checked_in
        self.complete = true
        if self.valid?
          self.save
        else
          raise ActiveRecord::Rollback, "Invalid barcode"
        end
      end
    end
  end

  def remove_automatic_duplicates
    automated_duplicates.each { |task_barcode| task_barcode.destroy }
  end

  #
  #  If the task has the experiment planning columns for the source well...
  #
  # * source_plate
  # * source_row
  # * source_column
  #
  # These should be filled in from the content of the task barcode.
  #
  attr_accessor :coasts_plates
  attr_accessor :samples

  def fill_in_source_parameter_for_sample
    row_labels = sheet.select_rows(
        :sample => self.reference,
        :source_plate => nil,
        :source_column => nil,
        :source_row => nil
    )
    if row_labels.none?
      add_row_for_sample(self.reference)
    end
    self.samples = [reference]
  end


  def fill_in_source_parameter_for_plate
    self.samples = CdbSample.where(
        :sample_id => coasts_plate_wells.collect { |w| w.sample_id }.uniq
    ).all

    self.coasts_plates = CoastsPlate.where(
        :plate_code => coasts_plate_wells.collect { |w| w.plate_code }.uniq
    ).all
    coasts_plate_wells.each do |coasts_plate_well|
      # If there is already a row or rows for this well, ignore it.
      row_labels = rows_for_well(coasts_plate_well)
      if row_labels.empty?
        # Look for a row for this sample which don't have a slot type
        # slot type means it's been scanned in before
        row_labels = sheet.select_rows(
            :sample => self.samples.detect { |s| s.sample_id == coasts_plate_well.sample_id },
            :source_plate => nil
        )
        if row_labels.any?
          # update source and slot type for these rows
          row_labels.each do |row_label|
            update_task_sheet_row_for_well(row_label, coasts_plate_well)
          end
        else
          # if there are queue items for this sample make one for each of those
          # if it's completely unknown, new row for this well
          add_row_for_well(coasts_plate_well)
        end
      end
    end
  end

  def rows_for_well(coasts_plate_well)
    sheet.select_rows(
        :source_plate => coasts_plate_well.plate_code,
        :source_column => coasts_plate_well.plate_col,
        :source_row => coasts_plate_well.plate_row
    )
  end

  def fill_in_source_parameters
    if self.reference
      if self.reference_type == 'CdbSample'
        fill_in_source_parameter_for_sample
      else
        fill_in_source_parameter_for_plate
      end
    end
    sheet.save!
  end

  def add_row_for_sample(sample)
    if sample
      queue_items = queue_items_for_sample
      if queue_items.any?
        queue_items.each do |queue_item|
          row_label = sheet.next_available_label
          options = {
              :method => queue_item.outline_method_name, :sample => sample
          }
          sheet.row_update(row_label, options)
          queue_item.update_attribute(:result_task_context_id, sheet.context(row_label).id)
        end
      else
        row_label = sheet.next_available_label
        options = {
            :sample => sample,
        }
        sheet.row_update(row_label, options)
      end
    end
  end


  def add_row_for_well(coasts_plate_well)
    queue_items = queue_items_for_well(coasts_plate_well)
    if queue_items.any?
      queue_items.each do |queue_item|
        add_row_for_well_and_queue_item(coasts_plate_well, queue_item)
      end
    else
      row_label = sheet.next_available_label
      update_task_sheet_row_for_well(row_label, coasts_plate_well)
    end
  end

  def add_row_for_well_and_queue_item(coasts_plate_well, queue_item)
    row_label = sheet.next_available_label
    update_task_sheet_row_for_well(
        row_label,
        coasts_plate_well,
        {:method => queue_item.outline_method_name}
    )
    queue_item.update_attribute(:result_task_context_id, sheet.context(row_label).id)
  end

  def queue_items_for_well(coasts_plate_well)
    task.queue_items.where(
        :data_type => %w{RlSample CdbSample},
        :data_name => self.samples.detect { |s| s.sample_id == coasts_plate_well.sample_id }.to_s
    ).uniq
  end

  def update_task_sheet_row_for_well(row_label, coasts_plate_well, extra_attributes={})
    coasts_plate = self.coasts_plates.detect { |p| p.plate_code == coasts_plate_well.plate_code }
    sample = self.samples.detect { |s| s.sample_id == coasts_plate_well.sample_id }
    sample ||= CdbSample.where(:sample_id => coasts_plate_well.sample_id).first
    options = {
        :source_plate => coasts_plate,
        :source_column => coasts_plate_well.plate_col,
        :source_row => coasts_plate_well.plate_row,
        :sample => sample
    }
    options.merge!(extra_attributes)
    sheet.row_update(row_label, options)
  end

  def coasts_plate_wells
    if task.process.dilution_map
      source_by_row = task.process.dilution_map.source_by_row
    else
      source_by_row = true
    end
    case reference_type
      when 'CoastsPlateWell'
        [reference]
      when 'CoastsPlate'
        if source_by_row
          reference.coasts_plate_wells.by_row
        else
          reference.coasts_plate_wells.by_column
        end
      when 'Order'
        if source_by_row
          reference.coasts_plates.collect { |plate| plate.coasts_plate_wells.by_row }.flatten
        else
          reference.coasts_plates.collect { |plate| plate.coasts_plate_wells.by_column }.flatten
        end
      when 'CdbSample'
        []
    end
  end

  def sample_ids
    coasts_plate_wells.collect { |x| x.sample_id }.uniq
  end


  def queue_items
    case reference_type
      when 'CoastsPlate'
        queue_items_for_coasts_plate
      when 'CoastsPlateWell'
        queue_items_for_coasts_plate_well
      when 'Order'
        queue_items_for_order
      when 'CdbSample'
        queue_items_for_sample
      else
        raise "Unknown barcode type"
    end
  end

  def queue_items_for_sample
    if task and task.result_queue_items.any? and reference_type == 'CdbSample'
      task.result_queue_items.joins(:cdb_sample).where(
          'cdb_samples.sample_id' => reference.sample_id
      )
    else
      []
    end
  end

  def queue_items_for_coasts_plate_well
    if task and task.result_queue_items.any? and reference_type == 'CoastsPlateWell'
      task.result_queue_items.joins(:cdb_sample).where(
          'cdb_samples.sample_id' => reference.sample_id
      )
    else
      []
    end
  end

  def queue_items_for_coasts_plate
    if task and task.result_queue_items.any? and reference_type == 'CoastsPlate'
      well_sample_ids = reference.coasts_plate_wells.collect { |x| x.sample_id }
      task.result_queue_items.joins(:cdb_sample).where(
          'cdb_samples.sample_id' => well_sample_ids
      )
    else
      []
    end
  end

  def queue_items_for_order
    if task and task.result_queue_items.any? and reference_type == 'Order'
      coasts_plates = reference.coasts_plates
      wells = coasts_plates.collect { |x| x.coasts_plate_wells }.flatten
      well_sample_ids = wells.collect { |x| x.sample_id }
      task.result_queue_items.joins(:cdb_sample).where(
          'cdb_samples.sample_id' => well_sample_ids
      )
    else
      []
    end
  end

  def to_s
    "#{reference_type}:#{reference_code} for #{task_name}"
  end

  def duplicate_summary
    # Caching at the start of this method to avoid unnecessary querying
    dups = non_automated_duplicates
    if dups.any?
      @summary_parts = []
      case reference_type

        when 'CoastsPlate'
          if dups.select { |x| x.reference_type == 'CoastsPlate' }.any?
            @summary_parts << "it has already been checked in for this task"
          end
          if dups.select { |x| x.reference_type == 'CoastsPlateWell' }.any?
            dup_wells = dups.select { |x| x.reference_type == 'CoastsPlateWell' }.collect { |x| x.reference.plat_tube_barcode }.uniq
            @summary_parts << "tubes in this plate have already been checked in for this task (#{dup_wells.join(', ')})"
          end
          if dups.select { |x| x.reference_type == 'Order' }.any?
            dup_orders = dups.select { |x| x.reference_type == 'Order' }.collect { |x| x.reference.coasts_order.order_id }.uniq
            @summary_parts << "it is part of an order which has already been checked in for this task (#{dup_orders.join(', ')})"
          end
          if dups.select { |x| x.reference_type == 'CdbSample' }.any?
            dup_plates = dups.select { |x| x.reference_type == 'CdbSample' }.collect { |x| x.reference.sample_id }.uniq
            @summary_parts << "samples in this plate have already been checked in for this task (#{dup_plates.join(', ')})"
          end

        when 'CoastsPlateWell'
          if dups.select { |x| x.reference_type == 'CoastsPlateWell' }.any?
            @summary_parts << "it has already been checked in for this task"
          end
          if dups.select { |x| x.reference_type == 'CoastsPlate' }.any?
            @summary_parts << "the plate it is in has already been checked in for this task (#{reference.plate_code})"
          end
          if dups.select { |x| x.reference_type == 'Order' }.any?
            dup_orders = dups.select { |x| x.reference_type == 'Order' }.collect { |x| x.reference.coasts_order.order_id }.uniq
            @summary_parts << "it is part of an order which has already been checked in for this task (#{dup_orders.join(', ')})"
          end
          if dups.select { |x| x.reference_type == 'CdbSample' }.any?
            dup_plates = dups.select { |x| x.reference_type == 'CdbSample' }.collect { |x| x.reference.sample_id }.uniq
            @summary_parts << "the sample in this well has already been checked in for this task (#{dup_plates.join(', ')})"
          end

        when 'Order'
          if dups.select { |x| x.reference_type == 'Order' }.any?
            @summary_parts << "it has already been checked in for this task"
          end
          if dups.select { |x| x.reference_type == 'CoastsPlateWell' }.any?
            dup_wells = dups.select { |x| x.reference_type == 'CoastsPlateWell' }.collect { |x| x.reference.plat_tube_barcode }.uniq
            @summary_parts << "tubes in this order have already been checked in for this task (#{dup_wells.join(', ')})"
          end
          if dups.select { |x| x.reference_type == 'CoastsPlate' }.any?
            dup_plates = dups.select { |x| x.reference_type == 'CoastsPlate' }.collect { |x| x.reference.plate_code }.uniq
            @summary_parts << "plates in this order have already been checked in for this task (#{dup_plates.join(', ')})"
          end
          if dups.select { |x| x.reference_type == 'CdbSample' }.any?
            dup_plates = dups.select { |x| x.reference_type == 'CdbSample' }.collect { |x| x.reference.sample_id }.uniq
            @summary_parts << "samples in this order have already been checked in for this task (#{dup_plates.join(', ')})"
          end

        when 'CdbSample'
          if dups.select { |x| x.reference_type == 'CdbSample' }.any?
            @summary_parts << "it has already been checked in for this task"
          end
          if dups.select { |x| x.reference_type == 'Order' }.any?
            dup_orders = dups.select { |x| x.reference_type == 'Order' }.collect { |x| x.reference.coasts_order.order_id }.uniq
            @summary_parts << "order containing this sample has already been checked in for this task (#{dup_orders.join(', ')})"
          end
          if dups.select { |x| x.reference_type == 'CoastsPlateWell' }.any?
            dup_wells = dups.select { |x| x.reference_type == 'CoastsPlateWell' }.collect { |x| x.reference.plat_tube_barcode }.uniq
            @summary_parts << "tubes for this sample have already been checked in for this task (#{dup_wells.join(', ')})"
          end
          if dups.select { |x| x.reference_type == 'CoastsPlate' }.any?
            dup_plates = dups.select { |x| x.reference_type == 'CoastsPlate' }.collect { |x| x.reference.plate_code }.uniq
            @summary_parts << "plates containing this sample have already been checked in for this task (#{dup_plates.join(', ')})"
          end

      end
      "You cannot scan #{self.to_s} because #{@summary_parts.join(', ')}"
    else
      nil
    end
  end

  def duplicated?
    non_automated_duplicates.any?
  end

  def non_automated_duplicates
    self.duplicates.select { |x| x.automatic != true }
  end

  def automated_duplicates
    self.duplicates.select { |x| x.automatic == true }
  end

  def duplicates
    case reference_type
      when 'CoastsPlate'
        plate_duplicates
      when 'CoastsPlateWell'
        well_duplicates
      when 'Order'
        order_duplicates
      when 'CdbSample'
        sample_duplicates
    end
  end

  def sample_duplicates
    if reference_type == 'CdbSample'
      duplicates = other_sample_barcodes_for_sample_id_and_task
      duplicates.concat(other_plate_well_barcodes_for_sample_id_and_task)
      duplicates.concat(other_plate_barcodes_for_sample_id_and_task)
      duplicates.concat(other_order_barcodes_for_sample_id_and_task)
      duplicates
    else
      raise "Wrong duplicate method called! Expected reference type to be 'CdbSample' but was '#{reference_type}'"
    end
  end

  def other_sample_barcodes_for_sample_id_and_task
    TaskBarcode.where(
        :reference_type => 'CdbSample',
        :reference_code => self.reference_code.to_s,
        :task_id => task_id
    ).where("id != #{id}")
  end

  def other_plate_well_barcodes_for_sample_id_and_task
    plate_barcodes = TaskBarcode.where(
        :reference_type => 'CoastsPlateWell',
        :task_id => task_id
    ).where("id != #{id}")
    plate_barcodes.select { |bc| bc.reference.sample_id == self.reference.sample_id }
  end

  def other_plate_barcodes_for_sample_id_and_task
    plate_barcodes = TaskBarcode.where(
        :reference_type => 'CoastsPlate',
        :task_id => task_id
    ).where("id != #{id}")
    plate_codes = plate_barcodes.collect { |x| x.reference_code.to_s }
    wells_for_sample = CoastsPlateWell.where(:plate_code => plate_codes,
                                             :sample_id => self.reference_code).all
    plate_codes_for_sample = wells_for_sample.collect { |x| x.plate_code }.uniq
    plate_barcodes.select { |bc| plate_codes_for_sample.include?(bc.reference_code) }
  end

  def other_order_barcodes_for_sample_id_and_task
    plate_barcodes = TaskBarcode.where(
        :reference_type => 'Order',
        :task_id => task_id
    ).where("id != #{id}")
    orders = plate_barcodes.collect { |x| x.order }
    plate_codes = orders.collect { |x| x.coasts_plates.collect { |x| x.plate_code } }.flatten
    wells_for_sample = CoastsPlateWell.where(:plate_code => plate_codes,
                                             :sample_id => self.reference_code).all
    plates = wells_for_sample.collect { |w| w.coasts_plate }.flatten
    plate_barcodes.select { |x| x.order.coasts_plates.any? { |y| plates.include?(y) } }
  end

  def order_duplicates
    if reference_type == 'Order'
      wells_in_order = reference.coasts_plates.collect { |y| y.coasts_plate_wells }.flatten
      duplicates = other_barcodes_for_orders_and_task(reference_code)
      duplicates.concat(other_barcodes_for_plates_and_task(reference.coasts_plates.collect { |x| x.plate_code }))
      duplicates.concat(other_barcodes_for_wells_and_task(wells_in_order.collect { |x| x.plat_tube_barcode }.uniq))
      sample_ids = reference.coasts_plates.collect { |x| x.coasts_plate_wells }.flatten.collect { |x| x.sample_id }.uniq
      duplicates.concat(other_barcodes_for_samples(sample_ids))
      duplicates
    else
      raise "Wrong duplicate method called! Expected reference type to be 'Order' but was '#{reference_type}'"
    end
  end


  def well_duplicates
    if reference_type == 'CoastsPlateWell'
      duplicates = other_barcodes_for_wells_and_task(reference.plat_tube_barcode)
      duplicates.concat(other_barcodes_for_plates_and_task(reference.plate_code))
      duplicates.concat(other_barcodes_for_orders_and_task(reference.coasts_orders.collect { |x| x.order_id.to_s }))
      duplicates.concat(other_barcodes_for_samples(reference.sample_id))
      duplicates
    else
      raise "Wrong duplicate method called! Expected reference type to be 'CoastsPlateWell' but was '#{reference_type}'"
    end
  end


  def plate_duplicates
    if reference_type == 'CoastsPlate'
      duplicates = other_barcodes_for_plates_and_task(reference_code)
      duplicates.concat(other_barcodes_for_orders_and_task(reference.coasts_orders.collect { |x| x.order_id.to_s }))
      duplicates.concat(other_barcodes_for_wells_and_task(reference.coasts_plate_wells.collect { |x| x.plat_tube_barcode }.uniq))
      duplicates.concat(other_barcodes_for_samples(reference.coasts_plate_wells.collect { |x| x.sample_id }.flatten))
      duplicates
    else
      raise "Wrong duplicate method called! Expected reference type to be 'CoastsPlate' but was '#{reference_type}'"
    end
  end

  def other_barcodes_for_samples(sample_ids)
    sample_ids = [sample_ids] unless sample_ids.is_a?(Array)
    sample_barcodes = TaskBarcode.where(
        :reference_type => 'CdbSample',
        :task_id => task_id
    ).where("id != #{id}")
    sample_barcodes.select { |x| sample_ids.include?(x.reference.sample_id) }
  end

  def other_barcodes_for_wells_and_task(plat_tube_barcodes)
    TaskBarcode.where(
        :reference_type => 'CoastsPlateWell',
        :reference_code => plat_tube_barcodes,
        :task_id => task_id
    ).where("id != #{id}")
  end


  def other_barcodes_for_plates_and_task(plate_codes)
    TaskBarcode.where(
        :reference_type => 'CoastsPlate',
        :reference_code => plate_codes,
        :task_id => task_id
    ).where("id != #{id}")
  end

  def other_barcodes_for_orders_and_task(coasts_orders)
    TaskBarcode.where(
        :reference_type => 'Order',
        :reference_code => coasts_orders,
        :task_id => task_id
    ).where("id != #{id}")
  end

  # This is used to avoid false duplicate check-ins
  def TaskBarcode.purge_unfinalised
    # If this user checked it in, but didn't finalise, it can go now.
    if User.current_id
      TaskBarcode.where(
          :created_by_user_id => User.current_id
      ).where(
          "complete is null or complete = ?", false
      ).destroy_all
    end
  end

  # get rid of automated task barcodes and dilution result for a task (on return from finalize wizard)
  def TaskBarcode.reset_for_task(task)
    task.task_barcodes.where(:automatic => true).destroy_all
    # Get rid of dilution slots,
    # any change to task barcodes means consolidation needs to be re-run
    task.dilution_slots.destroy_all
  end

  #
  # When removing a task barcode from a task,
  # this seeks and destroys the relevant rows from the task sheet
  #
  def on_destroy_remove_matching_task_contexts
    TaskBarcode.transaction do
      self.task_contexts.each do |x|
        unless reference_type == 'CdbSample' and x.result_queue_item
          x.destroy
          errors.add(:base, "Can not destroy, task rows did not allow removal: #{x.errors.full_messages}") unless x.destroyed?
        end
      end
    end
  end

  def task_contexts
    if reference_type == 'Order'
      plate_codes = reference.coasts_plates.collect { |x| x.plate_code }
      @row_labels = plate_codes.collect { |plate_code| task.sheet.select_rows('source_plate' => plate_code) }
    else
      @row_labels = task.sheet.select_rows(task_context_conditions)
    end
    task.contexts.where(:label => @row_labels)
  end


  def task_context_conditions
    case reference_type
      when 'CoastsPlate'
        {'source_plate' => reference.plate_code}
      when 'CoastsPlateWell'
        {
            'source_plate' => reference.plate_code,
            'source_row' => reference.plate_row,
            'source_column' => reference.plate_col
        }
      when 'CdbSample'
        {
            'sample' => reference,
            'source_plate' => nil,
            'source_row' => nil,
            'source_column' => nil
        }
    end
  end

  #
  #  ###    ###   ####    ###    ###   #####  #####  #   #
  # #   #  #   #  #   #  #   #  #   #    #      #     # #
  # #      #####  ####   #####  #        #      #      #
  # #   #  #   #  #      #   #  #   #    #      #      #
  #  ###   #   #  #      #   #   ###   #####    #      #
  #

  # This will be used to work out if finalising this task barcode will push its task over capacity...
  def amended_task_count
    if task.capacity_style and task.max_queue_items
      count = task.capacity_count
      if self.reference
        if self.reference_type == 'CdbSample'
          if task.capacity_style == 0
            queue_item_sample_ids = self.task.queue_items.collect { |x| x.cdb_sample.sample_id }
            count += 1 unless queue_item_sample_ids.include?(self.reference.sample_id)
          else
            count += 1
          end
        else
          if task.capacity_style == 0
            well_sample_ids = self.coasts_plate_wells.collect { |well| well.sample_id }.uniq
            queue_item_sample_ids = self.task.queue_items.collect { |x| x.cdb_sample.sample_id }.uniq
            well_sample_ids.reject! { |sample_id| queue_item_sample_ids.include?(sample_id) }
            count += well_sample_ids.count
          else
            new_well_count = self.coasts_plate_wells.select { |well| rows_for_well(well).none? }.count
            count += new_well_count
          end
        end
      end
      count
    else
      nil
    end
  end

  # Will this task barcode put its task over capacity?
  def exceeds_task_capacity?
    task.max_queue_items and amended_task_count > task.max_queue_items
  end

  def capacity_warning
    if self.exceeds_task_capacity?
      I18n.t(
          "activerecord.warnings.task_barcode.capacity_exceeded_#{task.capacity_style}",
          :code => reference_code,
          :capacity => task.max_queue_items,
          :count => amended_task_count
      )
    end
  end


end