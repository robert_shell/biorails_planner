#
# This is a data holder to speed the display of plate diagrams and
# plate consolidation diagrams in experiment planning.
#
# Mostly it needs to load three kinds of data when it's initialised
#
# * CoastsPlate(s)
# * CoastsPlateWell(s)
# * DilutionSlot(s)
#
# These should all be the records which are under a given Task
#
# All loaded data should have the related records required to
# display the consolidation map:
#
# CoastsPlate
# - CoastsPlateWell    *
# CoastsPlateWell
#
# DilutionSlot
# - CoastsPlateWell   *
#
# The CoastsPlateWells a DilutionSlot points to will also belong to a
# CoastsPlate or be a CoastsPlateWell in the task. THEY SHOULD ONLY BE LOADED ONCE!

class PlateDataStore

  attr_accessor :task
  attr_accessor :coasts_plates
  attr_accessor :scanned_coasts_plate_wells
  attr_accessor :coasts_plate_wells
  attr_accessor :dilution_slot_rows
  attr_accessor :cdb_samples

  # Used to decide on the colour of a cell in the display
  # This is an array of first plate_codes an second plat_tube_barcodes from coasts_plate_wells
  attr_accessor :colour_index_lookup

  def PlateDataStore.new(task_id)
    store = super()
    store.coasts_plates = []
    store.cdb_samples = []
    store.scanned_coasts_plate_wells = []
    store.coasts_plate_wells = {}
    store.dilution_slot_rows = []
    store.colour_index_lookup = []

    store.task = Task.find(task_id)
    store.get_plates
    store.get_scanned_wells
    store.get_scanned_samples
    store.get_dilution_slots

    # avoid looking up list of ids twice
    store.task.colour_index = store.colour_index_lookup

    store
  end


  def get_plates
    self.task.coasts_plates.each do |coasts_plate|
      self.coasts_plates << coasts_plate
      self.colour_index_lookup << coasts_plate.plate_code
    end
    CoastsPlateWell.where(:plate_code => self.coasts_plates.collect { |x| x.plate_code }).each do |coasts_plate_well|
      set_well(coasts_plate_well)
    end
  end

  def get_well(id)
    self.coasts_plate_wells[id.to_s]
  end

  def set_well(well)
    self.coasts_plate_wells[well.id.to_s] = well
  end

  def get_scanned_wells
    self.task.coasts_plate_wells.each do |coasts_plate_well|
      set_well(coasts_plate_well)
      self.colour_index_lookup << coasts_plate_well.plat_tube_barcode
      self.scanned_coasts_plate_wells << coasts_plate_well
    end
  end

  def get_scanned_samples
    self.task.cdb_samples.each do |cdb_sample|
      self.cdb_samples << cdb_sample
      self.colour_index_lookup << cdb_sample.sample_id
    end
  end

  def get_dilution_slots
    self.task.dilution_slots.rows.each_with_index do |row,idx|
      dilution_slot_rows[idx] = []
      row.each do |dilution_slot|
        # add the slot to the list of slots
        self.dilution_slot_rows[idx][dilution_slot.column_no] = dilution_slot
        # save a second lookup by setting the dilution_slot.coasts_plate_well
        # from the already scanned wells

        if dilution_slot.plate_code
          well = get_well(dilution_slot.coasts_plate_well_identifier)
          dilution_slot.coasts_plate_well = well if well
          if colour_index_lookup.include?(dilution_slot.plate_code)
            dilution_slot.colour_index = colour_index_lookup.find_index(dilution_slot.plate_code)
            if dilution_slot.slot_type == 'samp'
              dilution_slot.marked = (
              (@task.dilution_map.source_by_row and dilution_slot.plate_col == 1) or
                  (!@task.dilution_map.source_by_row and dilution_slot.plate_row == 'A')
              )
            end
          end
          dilution_slot.colour_index ||= colour_index_lookup.find_index(dilution_slot.coasts_plate_well.plat_tube_barcode)
          dilution_slot.colour = CoastsPlate::COLOURS[dilution_slot.colour_index % CoastsPlate::COLOURS.length]
        elsif dilution_slot.sample_id and dilution_slot.slot_type == 'samp'
          dilution_slot.colour_index ||= colour_index_lookup.find_index(dilution_slot.sample_id)
          dilution_slot.colour = CoastsPlate::COLOURS[dilution_slot.colour_index % CoastsPlate::COLOURS.length]
        end
      end
    end
  end


end
