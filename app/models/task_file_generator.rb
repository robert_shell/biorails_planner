#
# Generator to Write Files to task folder and mapped directory structure.
#
class TaskFileGenerator < ::Biorails::BackgroundJob

  attr_accessor :task

  delegate :sheet, :process, :project_element, :queue_items, :queue, :task_barcodes, :to => :task

  attr_accessor :process_version_file_template_id
  attr_accessor :process_version_file_template
  attr_accessor :process_version_file_template_ids
  attr_accessor :process_version_file_templates
  attr_accessor :output_file_template
  attr_accessor :temp_file

  def initialize(task)
    self.task = task
    self.set_default_file_template
  end


  TAB_TEMPLATE = <<TEXT
{% data_row %}
  method_id
  queue_item_id
  request_id
  sample_id
  {{ task.task_sheet_headers }}
{% enddata_row %}
{% for row in task.rows %}
  {% data_row %}
    {{row.method_id}}
    {{row.queue_item_id}}
    {{row.request_id}}
    {{row.sample.sample_id}}
    {{row.task_sheet_row}}
  {% enddata_row %}
{% endfor %}
TEXT



  def set_default_file_template
    if self.process and self.process.process_version_file_templates.any?
      self.process_version_file_template_id= self.process.process_version_file_templates.first.id
    end
  end

  def process_version_file_template_ids=(value)
    value == [value] unless value.is_a?(Array)
    @process_version_file_template_ids = value
    self.process_version_file_templates= process.process_version_file_templates.where(:id => value).all
  end

  def process_version_file_template_ids
    if super
      super
    else
      @process_version_file_template_ids = []
    end
  end

  def process_version_file_template_id=(value)
    @process_version_file_template_id = value
    self.process_version_file_template= process.process_version_file_templates.find_by_id(value)
  end

  def pre_process_template(template)
    template.strip
  end

  def output_file_text(template=Task::TAB_TEMPLATE)
    #template = pre_process_template(template)
    text = Liquid::Template.parse(template).render('task' => self)
    post_process_template(text)
  end

  def post_process_template(text)
    text.gsub("\n\n", "\n")
  end

  def generate_output_file_content
    if process_version_file_template
      template = process_version_file_template.content
      output_file_text(template)
    else
      nil
    end
  end

  def temp_file_path
    File.join(Rails.root, 'tmp', file_name)
  end

  def file_name
    process_version_file_template.name
  end

  #
  # File Types
  #
  # * tsv
  # * csv
  # * xlsx
  #
  #
  def fill_output_file
    if self.process_version_file_template

      self.process_version_file_template = process_version_file_template
      self.output_file_template = process_version_file_template.output_file_template

      content = generate_output_file_content

      case process_version_file_template.file_type
        when 'tsv'
          # * tsv - as is, file.write(content)
          File.open temp_file_path, 'w' do |file|
            file.write(content)
          end

        when 'csv'
          # * csv - convert to csv
          data = CSV.parse(content, {:col_sep => "\t"})
          CSV.open(temp_file_path, 'w') do |file|
            data.each do |row|
              file << row
            end
          end

        when 'xlsx', 'xls'
          # * xlsx - convert to xlsx with axlsx gem
          data = CSV.parse(content, {:col_sep => "\t"})
          Axlsx::Package.new do |p|
            p.workbook.add_worksheet(:name => self.name) do |sheet|
              data.each { |d| sheet.add_row(d) }
            end
            p.serialize(temp_file_path)
          end
      end
      create_project_asset_for_file(temp_file_path, true)
    else
      nil
    end
  end

  def create_project_asset_for_file(file, destroy_source=false)
    file = File.open(file) unless file.is_a?(File)

    project_asset = project_element.children.find_by_name(process_version_file_template.name)

    if project_asset
      project_asset.update_attributes!(content_file => File.open(file), :content_changed => true)
    else
      project_asset = self.project_element.add_element(
          :name => file_name,
          :element_type_id => ElementType.register(ProjectAsset),
          :description => file_name,
          :content_file => File.open(file)
      )
      if project_asset.valid? and destroy_source
        FileUtils.rm(file)
      end
    end
    project_asset
  end

  #
  # This looks for project assets named for the current process_verison_file_template
  # within this task.
  #
  def duplicate_file_warning
    if process_version_file_template
      if project_element.children.find_by_name(process_version_file_template.name)
        I18n.t('activerecord.errors.task.duplicate_file', :name => file_name)
      end
    end
  end

  # For file sync

  def output_file_directory
    File.join(SystemSetting[:task_output_file_root], self.project_element.self_and_ancestors.collect { |x| x.name })
  end

  def build_output_file_directory
    FileUtils.mkdir_p(output_file_directory)
  rescue => ex
    self.errors.add(:output_file_path, "Cant Create file system directory '#{output_file_directory}', #{ex.message}")
  end

  def sync_content_to_filesystem
    FileUtils.rm_rf(output_file_directory)
    build_output_file_directory
    project_assets = self.project_element.children.where(:type => 'ProjectAsset')
    project_assets.each do |project_asset|
      FileUtils.cp(project_asset.current_version.file, File.join(output_file_directory, project_asset.name))
    end
  end

end