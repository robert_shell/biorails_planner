class OutputFileTemplate < ActiveRecord::Base

  acts_as_dictionary

  has_many :process_version_file_templates

  validates_presence_of :name
  validates_uniqueness_of :name
  validates_presence_of :content

  has_many :process_versions

  def content=(text)
    super(text)
    self.length = text.length
  end

end
