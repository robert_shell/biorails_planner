class TaskConsolidation < ::Biorails::BackgroundJob

  attr_accessor :task
  attr_accessor :dilution_map
  attr_accessor :colour_index


  BI_CDB_STANDARD_NAMES =[:method, :source_plate, :source_column, :source_row, :test_plate, :test_column, :slot_type]
  BI_CDB_REQUIRED_NAMES =[:sample, :method, :source_plate, :source_column, :source_row, :test_plate, :test_column]

  delegate :id, :name, :outline, :sheet, :errors, :queue_items, :queue, :process, :project_element, :task_barcodes, :dilution_slots, :to => :task

  delegate :dilution_map, :to => :process, :allow_nil => true
  delegate :consolidation_algorithm, :to => :dilution_map, :allow_nil => true

  def TaskConsolidation.missing_columns(process)
    Bi::DefaultParameterTypes::NAMES - process.parameters.collect { |i| i.name.downcase }
  end

  def TaskConsolidation.is_suitable_task?(task)
    task.dilution_map && TaskConsolidation.missing_columns(task.process).size == 0
  end


  def preferred_order_profile
    self.queue.try(:preferred_order_profile) || OrderProfile.first
  end

  # Should return an array of plates and wells which corresponds to their colour in the UI diagram
  def colour_index
    return @colour_index unless @colour_index.nil?
    @colour_index = coasts_plate_codes
    @colour_index.concat(coasts_plate_well_codes)
    @colour_index.concat(sample_ids)
    @colour_index
  end

  # object_code can be a CoastsPlate.plate_code or a CoastsPlateWell.plat_tube_barcode
  #
  # This will set them sequentially and give us an index for CoastsPlate::COLOURS
  #

  def colour_index_for(object_code)
    if self.colour_index.include?(object_code)
      self.colour_index.find_index(object_code)
    else
      nil
    end
  end

  def inventory_linked?
    (task.instance_of?(ResultTask) or task.instance_of?(Experiment)) and task.queue and self.queue.coasts_orderable?
  end


  def coasts_plates
    barcodes = self.task_barcodes.where(
        :reference_type => ['CoastsPlate', 'Order'],
        :complete => true
    ).order('position asc')
    coasts_plates = barcodes.collect do |task_barcode|
      if task_barcode.reference_type == 'CoastsPlate'
        task_barcode.reference
      elsif task_barcode.reference_type == 'Order'
        task_barcode.reference.coasts_plates
      end
    end
    coasts_plates.flatten
  end

  def cdb_samples
    barcodes = self.task_barcodes.where(
        :reference_type => 'CdbSample'
    )
    barcodes.collect { |bc| bc.reference }
  end

  def sample_ids
    cdb_samples.collect { |x| x.sample_id }
  end

  def coasts_plate_codes
    coasts_plates.collect { |coasts_plate| coasts_plate.plate_code }
  end

  def coasts_plate_wells
    barcodes = self.task_barcodes.where(:reference_type => ['CoastsPlateWell'], :complete => true).order('position asc')
    coasts_plate_wells = barcodes.collect do |task_barcode|
      task_barcode.reference
    end
    coasts_plate_wells.flatten
  end

  def coasts_plate_well_codes
    coasts_plate_wells.collect { |coasts_plate| coasts_plate.plat_tube_barcode }.uniq
  end

  def source_rows
    if dilution_map
      if dilution_map.source_by_row
        coasts_plates.collect { |x| x.coasts_plate_wells.rows }.flatten(1)
      else
        coasts_plates.collect { |x| x.coasts_plate_wells.columns }.flatten(1)
      end
    end
  end


  def only_always_split?
    self.source_rows.any? { |r| r.length > dilution_map.source_length }
  end

  def consolidation_algorithm_options
    if dilution_map
      if only_always_split?
        [[DilutionMap.human_attribute_name(:always_split), 'always_split']]
      else
        [
            [DilutionMap.human_attribute_name(:reorder_within_group), 'reorder_within_group'],
            [DilutionMap.human_attribute_name(:always_split), 'always_split'],
            [DilutionMap.human_attribute_name(:never_split), 'never_split']
        ]
      end
    else
      []
    end
  end

  def usable_algorithms
    consolidation_algorithm_options.collect { |x| x[1] }
  end

  def consolidate
    Task.transaction do
      if process
        message("Starting checks")
        if pre_dilution_check
          runner = Dilution::Runner.new(
              :plate_codes => coasts_plate_codes,
              :well_codes => coasts_plate_well_codes,
              :sample_ids => sample_ids,
              :task_id => self.id,
              :length => dilution_map.dilution_map_slots.sample.length,
              :source_by_row => dilution_map.source_by_row
          )
          case consolidation_algorithm
            when 'always_split'
              message("Starting always_split")
              runner.run_always_split
            when 'never_split'
              message("Starting never_split")
              runner.run_never_split
            when 'reorder_within_group'
              message("Starting reorder_within_group")
              runner.run_reorder_within_group
          end
          message("Creating dilution_slots")
          runner.create_dilution_slots
          dilution_slots.reload
          message("Marking up task sheet")
          mark_dilution_slots_on_sheet
          return true
        else
          return false
        end
      end
    end
  end

  #
  # List of allowed methods can be deligated to  task.queue.outline_methods
  #
  def mark_dilution_slots_on_sheet
    if self.queue
      method_options = self.queue.outline_methods.collect { |i| i.name }
    else
      method_options = []
    end
    missing = BI_CDB_REQUIRED_NAMES - self.process.parameters.collect { |i| i.name.downcase.to_sym }
    if missing.size == 0
      dilution_slots.each do |dilution_slot|
        task_sheet_rows = rows_for_dilution_slot(dilution_slot)
        fill_rows_for_dilution_slot(task_sheet_rows, dilution_slot)
      end
    else
      @warnings ||=[]
      @warnings << "CDB #{BI_CDB_REQUIRED_NAMES.join(",")} parameters not found!"
      Rails.logger.error "CDB #{missing.join(",")} parameters not found!"
    end
    sheet.save!
  end


  def rows_for_dilution_slot(dilution_slot)
    # rows which have already been marked with this position
    row_labels = rows_matching_dilution_slot_destination(dilution_slot)

    if row_labels.empty? and dilution_slot.slot_type == 'samp'
      # rows which have come from this slots source well
      row_labels = rows_matching_dilution_slot_source(dilution_slot)
    end

    if row_labels.empty?
      row_labels = [sheet.next_available_label]
    end

    row_labels
  end

  def rows_matching_dilution_slot_source(dilution_slot)
    if dilution_slot.plate_code
      sheet.select_rows(
          :source_plate => dilution_slot.plate_code,
          :source_row => dilution_slot.plate_row,
          :source_column => dilution_slot.plate_col
      )
    else
      result = sheet.select_rows(
          :source_plate => nil,
          :source_row => nil,
          :source_column => nil,
          :slot_type => dilution_slot.slot_type,
          :sample => dilution_slot.cdb_sample
      )
      if result.empty?
        # at first there will be rows for single samples, but no slot type
        result = sheet.select_rows(
            :source_plate => nil,
            :source_row => nil,
            :source_column => nil,
            :slot_type => nil, #slot type is ensured empty here to avoid standards
            :sample => dilution_slot.cdb_sample
        )
      end
      result
    end
  end

  def rows_matching_dilution_slot_destination(dilution_slot)
    sheet.select_rows(
        :test_plate => dilution_slot.row_no + 1,
        :test_column => dilution_slot.column_no + 1,
        :slot_type => dilution_slot.slot_type
    )
  end

  def fill_rows_for_dilution_slot(task_sheet_rows, dilution_slot)

    defaults = {
        :sample => dilution_slot.cdb_sample_name,
        :slot_type => dilution_slot.slot_type,
        :test_plate => dilution_slot.row_no + 1,
        :test_column => dilution_slot.column_no + 1,
        :source_plate => dilution_slot.plate_code,
        :source_row => dilution_slot.plate_row,
        :source_column => dilution_slot.plate_col}

    task_sheet_rows.each do |row_label|
      defaults[:test_plate] = dilution_slot.row_no + 1
      defaults[:test_column] = dilution_slot.column_no + 1
      sheet.row_update(row_label, defaults)
    end
  end


  def pre_dilution_check
    errors.clear
    if !dilution_map
      errors.add(:base, ::I18n.t('activerecord.errors.task.no_dilution_map'))
    end
    if task_barcodes.none?
      errors.add(:base, ::I18n.t('activerecord.errors.task.no_plates'))
    end
    unless usable_algorithms.include?(consolidation_algorithm)
      errors.add(:consolidation_algorithm, ::I18n.t('activerecord.errors.task.invalid_algorithm'))
    end
    if dilution_map and
        (consolidation_algorithm != 'always_split' and source_rows.any? { |row| row.length > dilution_map.dilution_map_slots.sample.length })
      errors.add(
          :base,
          ::I18n.t('activerecord.errors.task.sources_too_long',
                   :rows => dilution_map.source_by_row ? ::I18n.t('rows') : ::I18n.t('columns'))
      )
    end
    return self.errors.none?
  end


  #
  # Ticket #26901
  #
  # Before consolidation any queue items which are not covered by checked-in task barcodes are added on
  #
  # These will have an attribute to identify them as automatically added barcodes
  #
  def add_missing_barcodes_for_queue_items
    if self.sheet.parameters['sample']
      # these should be all the samples referenced in the sample column
      samples = sheet.grid.collect { |x, y| y['sample'].data if y['sample'] }.compact.uniq
      self.task_barcodes.each do |task_barcode|
        samples -= CdbSample.where(:sample_id => task_barcode.sample_ids).all
      end
      samples.each do |sample|
        task_barcode = task_barcodes.create!(
            :reference_type => 'CdbSample',
            :reference_code => sample.sample_id,
            :position => task_barcodes.order('position desc').any? ? task_barcodes.order('position desc').first.position + 1 : 0,
            :automatic => true
        )
        task_barcode.finalise
      end
    end
  end

  def remove_automated_barcodes
    task_barcodes.where(:automatic => true).destroy_all
  end

  def dilution_slots_rearrange(positions)
    positions = Hash[positions.collect { |x| [x[0].to_i, x[1..-1]] }]
    task.dilution_slots.where(:slot_type => 'samp').each do |dilution_slot|
      position = positions[dilution_slot.id]
      if position
        if (position[0] - 1) != dilution_slot.row_no or position[1] != dilution_slot.column_no
          dilution_slot.update_attributes(:row_no => position[0] - 1, :column_no => position[1])
        end
      end
    end
    mark_dilution_slots_on_sheet
  end

  def rearrange(options={})
    Task.transaction do

      self.job_id = options[:uid]

      return Job.ended(options[:uid], {:status => 'Failed', :error_messages => 'No task specified'}) if options[:task].blank?
      return Job.ended(options[:uid], {:status => 'Failed', :error_messages => 'No user_id specified'}) if options[:user_id].blank?
      User.current = User.find(options[:user_id])
      self.task = options[:task]

      ProjectElement.current = folder = task.parent_folder

      Biorails::Dba.set_session_info("#{self.class}.rearrange_slots(#{self.job_id})", "initialize")

      return Job.ended(options[:uid], {:status => 'Failed', :error_messages => 'No Process found'}) unless task.process
      return Job.ended(options[:uid], {:status => 'Failed', :error_messages => "Folder #{folder} not changeable by #{User.current}"}) unless folder and folder.changeable?

      message("Started rearrange process  #{DateTime.now}")

      dilution_slots_rearrange(options[:slot_positions])

      message("Finished rearrange process #{DateTime.now}")

      Job.ended(
          options[:uid],
          {:status => 'Run ok', :goto_url => options[:success_url]}
      )
    end
  rescue Exception => ex
    logger.error ex.message
    logger.info ex.backtrace.join("\n")
    Rails.logger.error ex.message
    Rails.logger.info ex.backtrace.join("\n")
    Job.ended(
        options[:uid],
        {
            :status => 'Exception',
            :error_messages => ex.message,
            :goto_url => options[:fail_url]
        }
    )

  end

  #
  # DilutionSlotsController#create
  #
  # Here the plate consolidation algirthm is being run
  # * remove any existing DilutionSlots for the task
  # * create DilutionSlots for a task
  # * fill in the test_plate and test_column properties on the task sheet
  #
  def run(options={})
    Task.transaction do
      self.job_id = options[:uid]

      if options[:user_id].blank?
        return Job.ended(
            options[:uid],
            {:status => 'Failed', :error_messages => 'No user_id specified',
             :goto_url => options[:fail_url]
            }
        )
      end
      if options[:task].blank?
        return Job.ended(
            options[:uid],
            {:status => 'Failed', :error_messages => 'No task specified',
             :goto_url => options[:fail_url]
            }
        )
      end
      if options[:task].consolidation_algorithm.nil?
        return Job.ended(
            options[:uid],
            {:status => 'Failed', :error_messages => 'No algorithm specified',
             :goto_url => options[:fail_url]
            }
        )
      end
      User.current = User.find(options[:user_id])
      self.task = options[:task]

      ProjectElement.current = folder = task.parent_folder

      Biorails::Dba.set_session_info("#{self.class}.submit(#{self.job_id})", "initialize")

      return Job.ended(options[:uid], {:status => 'Failed', :error_messages => 'No Process found'}) unless task.process
      return Job.ended(options[:uid], {:status => 'Failed', :error_messages => "Folder #{folder} not changeable by #{User.current}"}) unless folder and folder.changeable?

      message("[#{DateTime.now}] Started consolidation process  ")

      sheet = task.sheet
      sheet.populate_from_queue
      sheet.save!

      message("[#{DateTime.now}] Populated from queue")

      remove_automated_barcodes
      message("[#{DateTime.now}] Removed duplicate barcodes")

      add_missing_barcodes_for_queue_items
      message("[#{DateTime.now}] Added missing barcodes")

      success = self.consolidate
      message("[#{DateTime.now}] Finished consolidation process")

      if success
        message("[#{DateTime.now}] Redirecting to #{options[:success_url]}")
        Job.ended(
            options[:uid],
            {:status => 'Run ok', :goto_url => options[:success_url]}
        )
      else
        message("[#{DateTime.now}] Redirecting to #{options[:fail_url]}")
        Job.ended(
            options[:uid],
            {
                :status => 'Failed',
                :error_messages => task.errors.full_messages.to_sentence,
                :goto_url => "#{options[:fail_url]}"
            }
        )
      end
    end
  rescue Exception => ex
    logger.error ex.message
    logger.info ex.backtrace.join("\n")
    Rails.logger.error ex.message
    Rails.logger.info ex.backtrace.join("\n")
    Job.ended(options[:uid],
              {:status => 'Exception', :error_messages => ex.message,
               :goto_url => options[:fail_url]})
  end

end