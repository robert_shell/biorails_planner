module Organize::QueueItemsCartHelper

  def selector_for_item(queue_item,idx)
    if queue_item.can_plan?
      check_box_tag "items[#{idx}]",queue_item.id ,true
    elsif queue_item.state.level_no < 1
      check_box_tag("items[#{idx}]",queue_item.id ,false)
    elsif queue_item.state.level_no > 2
       "#{check_box_tag("items[#{idx}]",queue_item.id ,false, :disabled => true)} Finalized".html_safe
    end
  end

end
