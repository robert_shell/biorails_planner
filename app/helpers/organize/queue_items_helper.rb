module Organize::QueueItemsHelper

  def new_task_button_message
    if current_project_folder.changeable? and right?(Role::MODULE_QUEUE, Role::RIGHT_BUILD)
      ""
    elsif current_project_folder.changeable?
      t('queue_items.queue_items_in_cart.no_rights')
    else
      t('queue_items.queue_items_in_cart.not_changeable', :element => @outline_queue.element_type.name)
    end
  end




end
