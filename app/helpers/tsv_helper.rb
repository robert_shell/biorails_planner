module TSVHelper

  # This class is purely to convert flat TSV plain text files into a html table

  def display_tsv(source_text)
    if source_text
      table_content = ""

      header_row = "<tr><th>#{source_text.split("\n")[0].split("\t").join("</th><th>")}</th></tr>"

      source_text.split("\n")[1..-1].each do |row|
        table_content << '<tr>'
        row.split("\t").each do |cell|
          table_content << "<td>#{cell}</td>"
        end
        table_content << '</tr>'
      end
      return <<HTML
    <table class="table table-condensed" style="background-color:white;">
      <thead>
        #{header_row}
      </thead>
      <tbody>
        #{table_content}
      </tbody>
    </table>
HTML
    else
      ''
    end
  end

end