B5::Application.routes.draw do

  scope :module => "organize" do

    resources :queue_items_cart,  :only => [:show,:update],  :controller => 'queue_items_cart' do

      member do
        post :assign
        post :cart
        post :order
        post :run
        post :redo
        post :list
        post :update
      end
    end

    resources :queue_items do
      collection do
        get :packages
        get :results
        get :new_task
        get :work
        get :upload
        post :upload
      end
      member do
        get :is_reason_required
      end


    end
  end

  resources :outline_queues do
    resource :experiment_order_wizard, only: [] do
      collection do
        get :step1_new
        post :step1_create
      end
    end
  end

  resources :experiment_items, only: [:update, :destroy]

  resources :experiment_consolidation_wizards, :only => [] do
    collection do
    end
    member do
      get :step1
      put :step1_save

      get :step2

      get :step3_create
      get :step3_edit
      post :step3_reset
      post :step3_update

      get :step4_new
      get :step4_preview
      put :step4_create
      post :step4_create
      put :finalize
      post :finalize

      get :step5
    end
  end

  resources :experiment_order_wizards, only: [] do
    collection do
      get :abandon
    end
    member do
      put :step1_update
      get :step1_edit
      get :step2
      get :step3
      get :step4
      post :confirm
      get :abandon
    end
  end



  resources :task_output_files, :only => [:new, :show, :create] do
    member do
      put :create
      post :create
      get :renew_form
      get :preview
    end
  end

  resources :output_file_templates do
    collection do
      get :help
    end
  end

  resources :rl_results do
    member do
      get :result
    end
    collection do
      get :pivot
    end
  end

  resources :dilution_maps do
    collection do
      post :renew_form
      post :build_wells
      put :renew_form
      put :build_wells
    end
  end


  resources :task_barcodes, :except => [:update, :edit] do
    collection do
      get :show_collection
    end

  end

  resources :cdb_samples, :controller => 'rl_sample'

  resources :rl_samples do
    member do
      get :sample

      resource :query, :controller => 'rl_sample_query' do
        collection do
          post :add_columns
          get :update_options
        end
      end
    end

  end

  resources :cdb_methods, :controller => 'rl_method'

  resources :rl_methods do
    member do
      get :method
    end
  end

  resources :process_version_file_templates

  scope :module => "organize" do


    resources :queue_items do
      collection do
        get :packages
        get :results
        get :new_task
        get :work
        get :upload
        post :upload

        resources :carts, :as => 'queue_carts', :controller => 'queue_item_carts' do
          member do
            get :new_task
            post :run
            get :renew_form
          end
        end

      end
    end


    resources :outline_queues do
      member do
        get :orders
      end
    end

  end

  resources :tasks do

    resources :task_barcodes, :only => [:index]

    resources :dilution_slots,
              :only => [:new] do
      collection do
        get :show
        put :create
        post :update
      end
    end

    collection do
      get :results
      post :refresh # extra call to allow for form refresh
    end
    member do
      get :sheet
      get :copy
      get :update_preview_and_show
      post :select
      post :assign_member_to
      get :assign
      post :assign
      get :update_row
      get :print
      get :flexible
      get :update_queue
      get :clear
      get :remove
      post :remove
      get :results
      get :notify
      get :close
      get :fill
      get :recalc
    end
  end

  #TODO: this is just a subclass of Task. We should be re-using the Task routes
  resources :experiments, :controller => 'tasks' do
    resources :task_barcodes, :only => [:index]
    collection do
      get :results
      post :refresh # extra call to allow for form refresh
    end
    member do
      get :sheet
      get :copy
      get :update_preview_and_show
      get :select
      post :select
      get :assign
      post :assign
      get :update_row
      get :print
      get :flexible
      get :update_queue
      get :clear
      get :remove
      post :assign_member_to
      post :remove
      get :results
      get :notify
      get :close
      get :fill
      get :recalc
    end
  end

end
