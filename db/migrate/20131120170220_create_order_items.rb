class CreateOrderItems < ActiveRecord::Migration
  def self.up
    create_table :order_items, :force => true do |t|
      t.integer :queue_item_id
      t.integer :order_id
      t.integer :sequence_no
      t.boolean :is_reference_compound
      t.integer :rl_sample_id

      t.timestamps
    end
  end

  def self.down
    drop_table :order_items
  end
end
