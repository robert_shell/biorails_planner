class AddOutputFileTemplateIdToProcessVersion < ActiveRecord::Migration
  def self.up
    add_column :process_versions, :output_file_template_id, :integer
  end

  def self.down
    remove_column :process_versions, :output_file_template_id
  end
end
