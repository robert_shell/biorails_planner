class CreateOrders < ActiveRecord::Migration
  def self.up
    create_table :orders, :force => true do |t|
      t.string :name
      t.integer :outline_queue_id
      t.integer :order_profile_id

      t.timestamps
    end
  end

  def self.down
    drop_table :orders
  end
end
