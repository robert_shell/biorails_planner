class AddConsolidationAlgorithmToDilutionMap < ActiveRecord::Migration
  def self.up
    add_column :dilution_maps, :consolidation_algorithm, :string
  end

  def self.down
    remove_column :dilution_maps, :consolidation_algorithm
  end
end
