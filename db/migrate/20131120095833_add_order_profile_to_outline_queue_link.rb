class AddOrderProfileToOutlineQueueLink < ActiveRecord::Migration
  def self.up
    add_column_if_missing :order_profiles, :outline_queue_id, :integer
    add_column_if_missing :outline_queues, :preferred_order_profile_id, :integer
  end

  def self.down
    remove_column :order_profiles, :outline_queue_id
    remove_column :order_profiles, :preferred_order_profile_id
  end
end
