class CreateOutputFileTemplates < ActiveRecord::Migration
  def self.up
    create_table :output_file_templates do |t|
      t.string :name
      t.integer :length
      t.binary :content, :limit => 10.megabytes

      t.timestamps
    end

  end

  def self.down
    drop_table :output_file_templates
  end
end
