class AddDefaultToOutputFileTemplates < ActiveRecord::Migration
  def self.up
    add_column :output_file_templates, :default, :boolean
  end

  def self.down
    remove_column :output_file_templates, :default
  end
end
