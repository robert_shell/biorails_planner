class AddOrderRequiredColumnsToOrderProfiles < ActiveRecord::Migration
  def self.up
    add_column :order_profiles, :orderer_rl_employee_lov_id, :integer
    add_column :order_profiles, :orderer_coasts_opu_id, :integer
    add_column :order_profiles, :recipient_rl_employee_lov_id, :integer


    add_column :order_profiles, :purpose_type, :string
    add_column :order_profiles, :purpose_id, :integer
  end

  def self.down
    remove_column :order_profiles, :orderer_rl_employee_lov_id
    remove_column :order_profiles, :orderer_coasts_opu_id
    remove_column :order_profiles, :recipient_rl_employee_lov_id

    remove_column :order_profiles, :purpose_type
    remove_column :order_profiles, :purpose_id
  end
end
