class CreateProcessVersionFileTemplates < ActiveRecord::Migration
  def self.up
    create_table :process_version_file_templates do |t|

      t.integer :process_version_id
      t.integer :output_file_template_id
      t.string :file_name
      t.string :file_type
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :process_version_file_templates
  end
end
