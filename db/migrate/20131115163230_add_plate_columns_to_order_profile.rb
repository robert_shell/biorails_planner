class AddPlateColumnsToOrderProfile < ActiveRecord::Migration
  def self.up
    add_column :order_profiles, :coasts_plate_format_id, :integer
    add_column :order_profiles, :coasts_plate_type_id, :integer
    add_column :order_profiles, :coasts_plate_layout_id, :integer
  end

  def self.down
    remove_column :order_profiles, :coasts_plate_format_id
    remove_column :order_profiles, :coasts_plate_type_id
    remove_column :order_profiles, :coasts_plate_layout_id
  end
end
