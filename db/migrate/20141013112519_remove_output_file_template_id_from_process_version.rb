class RemoveOutputFileTemplateIdFromProcessVersion < ActiveRecord::Migration
  def self.up
    remove_column :process_versions, :output_file_template_id
  end

  def self.down
    add_column :process_versions, :output_file_template_id, :integer
  end
end
