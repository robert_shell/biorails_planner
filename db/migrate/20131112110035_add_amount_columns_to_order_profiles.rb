class AddAmountColumnsToOrderProfiles < ActiveRecord::Migration
  def self.up
    add_column :order_profiles, :volume, :integer
    add_column :order_profiles, :weight, :float
  end

  def self.down
    remove_column :order_profiles, :volume
    remove_column :order_profiles, :weight
  end
end
