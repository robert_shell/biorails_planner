class AddTaskIdToOrders < ActiveRecord::Migration
  def self.up
    add_column :orders, :task_id, :integer
  end

  def self.down
    remove_column :orders, :task_id
  end
end
