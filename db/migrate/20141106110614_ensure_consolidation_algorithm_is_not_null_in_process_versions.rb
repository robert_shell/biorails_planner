class EnsureConsolidationAlgorithmIsNotNullInProcessVersions < ActiveRecord::Migration
  def self.up
    execute <<SQL
      update dilution_maps
      set consolidation_algorithm = 'always_split'
      where consolidation_algorithm is null
SQL
    change_column :dilution_maps, :consolidation_algorithm, :string, :null => false

  end

  def self.down
    change_column :dilution_maps, :consolidation_algorithm, :string, :null => true
  end
end
