class CreateDilutionMaps < ActiveRecord::Migration
  def self.up
    create_table :dilution_maps do |t|
      t.integer :process_version_id
      t.integer :length
      t.integer :coasts_plate_format_id
      t.integer :source_length
      t.boolean :source_by_row

      t.timestamps
    end
  end

  def self.down
    drop_table :dilution_maps
  end
end
