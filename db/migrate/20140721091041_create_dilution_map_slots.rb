class CreateDilutionMapSlots < ActiveRecord::Migration
  def self.up
    create_table :dilution_map_slots do |t|
      t.string :slot_type
      t.integer :dilution_map_id
      t.integer :position
      t.integer :sample_id

      t.timestamps
    end
  end

  def self.down
    drop_table :dilution_map_slots
  end
end
