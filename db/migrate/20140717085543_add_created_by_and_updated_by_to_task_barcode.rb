class AddCreatedByAndUpdatedByToTaskBarcode < ActiveRecord::Migration
  def self.up
    add_column :task_barcodes, :created_by_user_id, :integer
    add_column :task_barcodes, :updated_by_user_id, :integer
  end

  def self.down
    remove_column :task_barcodes, :created_by_user_id
    remove_column :task_barcodes, :updated_by_user_id
  end
end
