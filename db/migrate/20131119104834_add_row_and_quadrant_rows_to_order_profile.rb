class AddRowAndQuadrantRowsToOrderProfile < ActiveRecord::Migration
  def self.up
    add_column :order_profiles, :rows_first, :boolean
    add_column :order_profiles, :all_quadrants, :boolean
    add_column :order_profiles, :coasts_vial_type_id, :integer

  end

  def self.down
    remove_column :order_profiles, :rows_first
    remove_column :order_profiles, :all_quadrants
    remove_column :order_profiles, :coasts_vial_type_id
  end
end
