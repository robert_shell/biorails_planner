class OrderProfileSampleDenormalize < ActiveRecord::Migration

  def self.up
    add_column_if_missing :order_profile_samples,:sample_id,:integer
    add_column_if_missing :order_profile_samples,:data_name,:string
    add_column_if_missing :order_profile_samples,:data_id,:string
    add_column_if_missing :order_profile_samples,:data_type,:string
    if table_exists?('rl_sample')
    execute <<SQL
update order_profile_samples
set sample_id = (select sample_id from rl_sample x where x.id = rl_sample_id),
    data_type = 'RlSample',
    data_id = rl_sample_id
SQL
   end
  end

  def self.down
    remove_column :order_profile_samples,:sample_id
    remove_column :order_profile_samples,:data_name
    remove_column :order_profile_samples,:data_id
    remove_column :order_profile_samples,:data_type
  end

end
