class CreateTaskBarcodes < ActiveRecord::Migration
  def self.up
    create_table :task_barcodes do |t|
      t.integer :task_id
      t.integer :position
      t.string :reference_type
      t.string :reference_code
      t.boolean :complete

      t.timestamps
    end
  end

  def self.down
    drop_table :task_barcodes
  end
end
