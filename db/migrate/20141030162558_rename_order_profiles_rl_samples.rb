class RenameOrderProfilesRlSamples < ActiveRecord::Migration
  def self.up
    rename_table_if_exists :order_profiles_rl_samples, :order_profile_samples
    #
    # Handler developer issues
    #
    rename_table_if_exists :order_profiles_comps_ref, :order_profile_samples unless table_exists?('order_profile_samples')
    rename_table_if_exists :order_profiles_comp_refs, :order_profile_samples unless table_exists?('order_profile_samples')
  end

  def self.down
    rename_table  :order_profiles_samples,:order_profiles_rl_samples
  end
end
