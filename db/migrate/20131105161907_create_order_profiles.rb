class CreateOrderProfiles < ActiveRecord::Migration
  def self.up
    create_table :order_profiles, :force => true do |t|
      t.string :name
      t.integer :coasts_order_type_id

      t.timestamps
    end
  end

  def self.down
    drop_table :order_profiles
  end
end
