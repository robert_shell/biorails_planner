class OrderItemAddInOrderFlag < ActiveRecord::Migration
  def self.up
    add_column :order_items,:in_order,:boolean,null:false, default:false
  end

  def self.down
    remove_column :order_items,:in_order
  end
end
