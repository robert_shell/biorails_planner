class CreateDilutionSlots < ActiveRecord::Migration
  def self.up
    create_table :dilution_slots do |t|
      t.integer :task_id
      t.integer :row_no
      t.integer :column_no
      t.integer :sample_id
      t.string :plate_code
      t.string :plate_row
      t.integer :plate_col
      t.string :slot_type
      t.integer :sample_id


      t.timestamps
    end
  end

  def self.down
    drop_table :dilution_slots
  end
end
