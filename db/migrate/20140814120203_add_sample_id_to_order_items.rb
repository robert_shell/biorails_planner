class AddSampleIdToOrderItems < ActiveRecord::Migration
  def self.up
    # when this column is true, the coasts order is finished and does
    # not need to be polled by the ordering rake task
    add_column_if_missing :order_items, :sample_id, :integer
    add_column_if_missing :order_items, :data_name, :string
    run_sql <<SQL
update order_items set data_name = (select data_name from queue_items where queue_items.id = order_items.queue_item_id)
SQL
    if ActiveRecord::Base.connection.table_exists? 'rl_sample'
      run_sql <<SQL
update order_items set sample_id = (select sample_id from rl_sample where rl_sample.id = order_items.rl_sample_id)
SQL
    end
  end

  def self.down
    remove_column :order_items, :sample_id
    remove_column :order_items, :data_name
  end
end
