class AddAutomaticToTaskBarcode < ActiveRecord::Migration
  def self.up
    add_column :task_barcodes, :automatic, :boolean, :default => false
  end

  def self.down
    remove_column :task_barcodes, :automatic
  end
end
