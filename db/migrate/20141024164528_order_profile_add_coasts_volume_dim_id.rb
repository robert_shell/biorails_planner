class OrderProfileAddCoastsVolumeDimId < ActiveRecord::Migration
  def self.up
    add_column_if_missing :order_profiles, :coasts_volume_dim_id ,:integer
  end

  def self.down
    remove_column :order_profiles, :coasts_volume_dim_id
  end
end
