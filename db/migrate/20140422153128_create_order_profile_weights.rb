class CreateOrderProfileWeights < ActiveRecord::Migration
  def self.up
    create_table :order_profile_weights,:force => true do |t|
      t.integer :order_profile_id
      t.integer :no_of_vials
      t.float :weight
    end

    rename_column_if_found :order_profiles, :weight, :old_weight

    # Bob says this will fail in sql due to oracle sequence appearing in SQL.
    sql =if Biorails::Check.oracle? then
      <<SQL
insert into order_profile_weights (id, order_profile_id, weight, no_of_vials)
select  ORDER_PROFILE_WEIGHTS_SEQ.nextval,  id,  old_weight,  1
from order_profiles
where old_weight is not null
SQL
    else
      <<SQL
insert into order_profile_weights (id, order_profile_id, weight, no_of_vials)
select  1,  id,  old_weight,  1
from order_profiles
where old_weight is not null
SQL
    end
    execute(sql)
  end

  def self.down
    drop_table :order_profile_weights
    rename_column :order_profiles, :old_weight, :weight
  end
end
